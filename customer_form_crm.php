<?php
     include('header_customer_form.php');
    include('class/functions.php');
    $con = new functions();

    if (!empty($_REQUEST["key"])) {
        
        $validate_key = "SELECT cd.client_id,cd.bde_user_id,cd.company_name,cd.contact_number,cd.contact_person,cd.email_id,cd.client_key,pp.package,pp.rate,p.product_name FROM client_details AS cd INNER JOIN product_payment AS pp ON cd.client_id=pp.client_id INNER JOIN products AS p ON pp.p_id=p.p_id WHERE cd.client_key = '".$_REQUEST["key"]."' AND cd.payment_status = 'Paid' ";
        //$validate_key = "SELECT cd.client_id,cd.bde_user_id,cd.company_name,cd.contact_number,cd.contact_person,cd.email_id,cd.client_key,pp.package,pp.rate,p.product_name FROM client_details AS cd INNER JOIN product_payment AS pp ON cd.client_id=pp.client_id INNER JOIN products AS p ON pp.p_id=p.p_id WHERE cd.client_key = '7e889ee89cfb76f453d816ca4d7bfd08' AND cd.client_id =126 AND cd.payment_status = 'Paid' ";
        $validate_key_result = $con->data_select($validate_key);
        if($validate_key_result == "no"){
            header('Location: page_not_found.php');
        }
        


    }else{  
        header('Location: 404.html');
    }

?>
<!-- java script start -->

<style type="text/css">
    #note{
        color: red;
    }
</style>

<script type="text/javascript">

$('body').on("contextmenu",function(e){
    //return false;
});

$('body').css({
    '-webkit-touch-callout': 'none',
    '-webkit-user-select': 'none',
    '-khtml-user-select': 'none',
    '-moz-user-select': 'none',
    '-ms-user-select': 'none',
    'user-select': 'none'
});

$(document).keydown(function(event){
    if(event.keyCode==123){
        return false;
   }
    else if(event.ctrlKey && event.shiftKey && event.keyCode==73){        
        return false;  //Prevent from ctrl+shift+i
   }else if(event.keyCode==93){
        return false;
   }else if(event.ctrlKey && event.keyCode==85){        
        return false;  //Prevent from ctrl+u
   }else if(event.ctrlKey && event.keyCode==83){        
        return false;  //Prevent from ctrl+s
   }else if(event.ctrlKey && event.keyCode==80){        
        return false;  //Prevent from ctrl+p
   }
});

$(document).ready(function(){

   
       // var cust_id = $("#cust_hidden_id").val();
       
     
        $("#addMKSCustomer").click(function(){
           // var userName = $("#")

            var temp = validateBlankFields();

            //return false;

            if(temp == false){
                alert('All fields are mendetory..');
                return false;
            }
                      
            $("#ajax-loader").show();
            $("#addMKSCustomer").hide();
            
            $.ajax({

                url:"ajax_service/mks_customer_ajax.php",
                //async: false,
                data:$("#mksaccountform").serialize()+"&action=addMKSCustomer",
                success:function(data){
                    console.log(data);
                   // alert(data);
                    if(data == 1)
                    {
                        alert("Welcome to MakeMySMS!!!");
                        $("#ajax-loader").hide();
                        $("#addMKSCustomer").show();
                        window.close();
                        location.reload();
                       
                    }
                    else
                    {
                        alert("Data not inserted");
                        $("#ajax-loader").hide();
                        $("#addMKSCustomer").show();
                        
                    }
                    
                }
            });
        });


});

function checkuserName(){
    
    var userName = $("#userName").val();
    var product_name = $("#product_name").val();

     $.ajax({
        url:"ajax_service/mks_customer_ajax.php",
        data:"userName="+userName+"&product_name="+product_name+"&action=checkuserName",
        async:false,
        //dataType:"json",
        success:function(data){
            //console.log(data);
            //return false;
            //alert(data);
            if(data == "no")
            {
                //alert(data);
                $("#errMsgUser").css("color","green");
                $("#errMsgUser").html("User Name is available.");
                $("#errFlag").val("1");
            }
            else
            {
                //alert(data);
                $("#errMsgUser").css("color","red");
                $("#errMsgUser").html("User Name is already registered.");
                $("#errFlag").val("0");
                return false;
                
            }
        }
    });
}

 function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) )
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}

 function alphaNumeric(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}

function validateBlankFields(){

    var fullName = $('#fullName').val();
    var compName = $('#compName').val();
    var contactNumber = $('#contactNumber').val();
    var emailId = $('#emailId').val();
    var userName = $('#userName').val();
    var password = $('#password').val();
    var product_name = $('#product_name').val();
    var msg_content = $('#msg_content').val();
    var errFlag = $('#errFlag').val();

    if(fullName == ''){
        $('#fullName').css('border-color','#e74c3c');
        $('#fullName').focus();
        return false;
    }else{
        $('#fullName').css('border-color','#3c763d');
    }

    if(compName == ''){
        $('#compName').css('border-color','#e74c3c');
        $('#compName').focus();
        return false;
    }else{
        $('#compName').css('border-color','#3c763d');
    }

    if(contactNumber == ''){
        $('#contactNumber').css('border-color','#e74c3c');
        $('#contactNumber').focus();
        return false;
    }else{
        $('#contactNumber').css('border-color','#3c763d');
    }

    if(emailId == ''){
        $('#emailId').css('border-color','#e74c3c');
        $('#emailId').focus();
        return false;
    }else{
        $('#emailId').css('border-color','#3c763d');
    }

    if(userName == ''){
        $('#userName').css('border-color','#e74c3c');
        $('#userName').focus();
        return false;
    }else{
        $('#userName').css('border-color','#3c763d');
    }

    /*if(password == ''){
        $('#password').css('border-color','#e74c3c');
        $('#password').focus();
        return false;
    }else{
        if(password.length < 8){
            alert('Password should be 8 digit or more.');
            $('#password').focus();
            return false;
        }else{
            $('#password').css('border-color','#3c763d');
        }        
    }*/

    if(product_name == 'SMS-Transactional'){
        var senderId = $('#senderId').val();
        if(senderId == ''){
            $('#senderId').css('border-color','#e74c3c');
            $('#senderId').focus();
            return false;
        }else{
            if(senderId.length < 6){
                alert('Sender Id should be 6 digit.');
                $('#senderId').focus();
                return false;
            }else{
                $('#senderId').css('border-color','#3c763d');
            }            
        }
    }

    if(msg_content == 'blank'){
        $('#msg_content').css('border-color','#e74c3c');
        $('#msg_content').focus();
        return false;
    }else{
        if(msg_content.length < 30){
            alert('Message content should be 30 - 70 characters.');
            $('#msg_content').focus();
            return false;
        }else{
            $('#msg_content').css('border-color','#3c763d');
        }        
    }

    if(errFlag == '' || errFlag == '0'){
        $('#userName').val('');
        return false;
    }
    return true;
}

</script>
<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
        <div id="customer_form_page-wrapper">

            <div class="page-content">
<!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Customer
                                <small>Form </small>
                            </h1>
                           
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                 <div class="row">

                    <!-- Validation States -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Customer</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationStates" class="panel-collapse collapse in">
                                
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="mksaccountform" role="form">
                                        <input type="hidden" class="form-control" name="client_hidden_id" value="<?php echo $validate_key_result[0]['client_id']?>" placeholder="Placeholder Text" required>
                                        <input type="hidden" class="form-control" name="sales_hidden_id" value="<?php echo $validate_key_result[0]['bde_user_id']?>" placeholder="Placeholder Text" required>
                                        <input type="hidden" class="form-control" name="product_name" id="product_name" value="<?php echo $validate_key_result[0]['product_name']?>" placeholder="Placeholder Text" required>
                                        <input type="hidden" class="form-control" name="package" value="<?php echo $validate_key_result[0]['package']?>" placeholder="Placeholder Text" required>
                                        <input type="hidden" class="form-control" name="rate" value="<?php echo $validate_key_result[0]['rate']?>" placeholder="Placeholder Text" required>
                                         <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Credit Allocated</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" value="<?php echo $validate_key_result[0]['product_name'].': '.$validate_key_result[0]['package'] ?>" placeholder="Placeholder Text" readonly required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Full Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" value="<?php echo $validate_key_result[0]['contact_person'] ?>" id="fullName" name="fullName" onkeypress="return onlyAlphabets(event,this);" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Company Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" value="<?php echo $validate_key_result[0]['company_name'] ?>" id="compName" name="compName" onkeypress="return alphaNumeric(event,this);" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Contact Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" value="<?php echo $validate_key_result[0]['contact_number'] ?>" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Email Id</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" value="<?php echo $validate_key_result[0]['email_id'] ?>" id="emailId" name="emailId" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">User Name(For Login)</label>
                                            <div class="col-sm-10">
                                                <span id="errMsgUser">&nbsp;</span>
                                                <input type="text" class="form-control" maxlength="20" minlength="8" id="userName" name="userName" onkeyup="return checkuserName();" onkeypress="return alphaNumeric(event,this);" placeholder="USERNAME" required>
                                                <span class="help-block"></span>
                                                <input type="hidden" id="errFlag" value="1">
                                            </div>
                                        </div>
                                        <div class="form-group has-success hidden">
                                            <label class="col-sm-2 control-label">Password(For Login)</label><span id="errMsgPwd"></span>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" maxlength="20" minlength="8" id="password" name="password" placeholder="PASSWORD" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <?php 
                                         #   if($validate_key_result[0]['product_name'] == 'SMS-Transactional')
                                          #  {                                                                                            
                                        ?>   
                                            <div class="form-group has-success addSenderIdDiv">
                                                <label class="col-sm-2 control-label">Sender ID</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" maxlength="6" id="senderId"  name="senderId" onkeypress="return onlyAlphabets(event,this);" style="text-transform: uppercase;" placeholder="Enter Only 6 Digit Character Sender Id" required>
                                                    <label id="note">Note: There should be a relationship between sender ID and Company name</label>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                        <?php
                                         #   }
                                        ?>
                                        
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Message Content</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" maxlength="70" minlength="30" id="msg_content"  name="msg_content" placeholder="Message Content" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>                                       
                                        
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <div  align="center" id="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                                                <h1 align="center"><button type="button"  id="addMKSCustomer" class="btn btn-default">Submit</button></h1>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                  

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
<?php
 include('footer_crm.php');
?>