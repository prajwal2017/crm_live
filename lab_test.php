<?php
include("header.php");
include("side_navigation.php");
include("./class/functions.php");
$obj1 = new functions();

$status[1]="Active";
$status[0]="Inactive";

$select_option = "select id ,name from euip_master where status = 1";
$data_select  = $obj1->data_select($select_option);
if($data_select !="no"){
   $options='<option value="">Select One:</option>';
    foreach($data_select as $key => $vallue){
      $options.='<option value="'.$data_select[$key]['id'].'">'.$data_select[$key]['name'].'</option>';

    }

}


if(isset($_POST['lab_action']) && ($_POST['lab_action'] == "insert") ){    
    $sql = "insert into lab_test(name,parameter,unit,specification,repeatability,status,created) values('".$_POST['name']."','".$_POST['parameter']."','".$_POST['unit']."','".$_POST['specification']."','".$_POST['repeatibility']."',".$_POST['status'].",'".date("Y-m-d h:i:s")."')";
    $obj1->data_insert($sql);
    //echo "data insreted";

}


if(isset($_POST['lab_action']) && ($_POST['lab_action'] == "product_update") ){    
    $sql = "update lab_test set name ='".$_POST['name']."',parameter ='".$_POST['parameter']."',unit ='".$_POST['unit']."',specification ='".$_POST['specification']."',repeatability ='".$_POST['repeatibility']."', status =".$_POST['status']." where id=".$_POST['product_id'];

    $obj1->data_update($sql);
    //echo "data insreted";
}


$sql_lab_test ="select id,name,parameter,unit,specification,repeatability,status from lab_test ";
$data = $obj1->data_select($sql_lab_test);
//print_r($data);
$obj1->__destruct();


?>

    <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                LAB TEST MASTER
                                <small>LAB TEST Management</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">LAB TEST</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->



                       <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12" id ="product_info">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title" style ="float:none">
                                    <span>LAB TEST  Info</span>
                                    <span class="pull-right"><button class="btn btn-white" onclick="add_product();">Add Lab TEST</button></span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Parameter</th>
                                                <th>Unit</th>
                                                <th>Specification</th>
                                                <th>Repeatability</th>  
                                                <th>Status</th>                                                                                          
                                                <th>Action</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php

                                               if($data !="no"){

                                                          foreach($data as $key =>$value){
                                                                echo "<tr class='gradeA'>";
                                                                echo "<td>".$data[$key]['id']."</td>";
                                                                echo "<td>".$data[$key]['name']."</td>";
                                                                echo "<td>".$data[$key]['parameter']."</td>";
                                                                 echo "<td>".$data[$key]['unit']."</td>";                                                                 
                                                                  echo "<td>".$data[$key]['specification']."</td>";
                                                                   echo "<td>".$data[$key]['repeatability']."</td>";
                                                                echo "<td>".$status[$data[$key]['status']]."</td>";                                                                

                                                                echo ' <td> 
                                                                        <div class="btn-group">
                                                                              <a class="btn btn-green btn-xs" role="button" onclick="edit_product(\''.$data[$key]['id'].'\');">Edit</a>
                                                                              <a class="btn btn-red btn-xs" role="button"   onclick="delete_product(\''.$data[$key]['id'].'\');">Inactive</a>
                                                                               <a class="btn btn-green btn-xs" role="button" onclick="active_product(\''.$data[$key]['id'].'\');">Active</a>

                                                                              
                                                                         </div>
                                                                  </td>';
                                                                echo "</tr>";

                                                        }
                                               }
                                    
                                        ?>
                                           
                                           
                                          
                                        </tbody>
                                    </table>
                                </div>
                                 <!-- /.table-responsive -->
                                
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                     <!-- ************Add user div  ***************  -->
                                 <div class="col-lg-12 add_user"  id ="add_user">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>ADD LAB TEST</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form"  method= "post">
                                        <div class="form-group">
                                            <label for="textInput" class="col-sm-2 control-label"> Name:</label>
                                            <div class="col-sm-10">
                                                <input class="form-control" type="text" name="name" id="name"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Parameter:</label>
                                            <div class="col-sm-10">
                                            
                                                    <input class="form-control" type="text" name="parameter" id="parameter"/>
                                                
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Unit:</label>
                                            <div class="col-sm-10">
                                            
                                                    <input class="form-control" type="text" name="unit" id="unit"/>
                                             
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Specification:</label>
                                            <div class="col-sm-10">
                                                 <input class="form-control" type="text" name="specification" id="specification"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Repeatibility:</label>
                                            <div class="col-sm-10">
                                                 <input class="form-control" type="text" name="repeatibility" id="repeatibility"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Sop:</label>
                                            <div class="col-sm-10">
                                                 <input class="form-control" type="file" name="sop" id="sop"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <select name="status"  id ="status" class="form-control" required>
                                                    <option value="">Select One:</option>
                                                    <option value="1">Active</option>
                                                    <option  value="0">Inactive</option>
                                                  
                                                </select>
                                            </div>
                                        </div>
                               
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" class="form-control" id="action" name="lab_action" value="insert">
                                                 <input type="hidden" class="form-control" id="product_id" name="product_id" value="">
                                                <button type="submit" class="btn btn-default" value="save">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <!-- **********  Add user div  ******************--> 

                </div>
                <!-- /.row -->

                <!-- end PAGE TITLE ROW -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->

  <script>
      $(function() {
         //alert("kundan");
         $("#add_user").hide();

});

      function add_product(){
         $("#product_info").hide();
          $("#add_user").show();
      }

      function edit_product(id){     

        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=lab_test_edit",
               dataType: "json",
              success:function(data){       
                  $("#add_user").show();
                  $("#product_info").hide();
                  $("#name").val(data[0].name);
                  $("#parameter").val(data[0].parameter)
                  $("#unit").val(data[0].unit)
                  $("#specification").val(data[0].specification);
                  $("#repeatibility").val(data[0].repeatability);
                  $("#action").val("product_update");
                  $("#product_id").val(data[0].id);
                 // alert('#status option[value="'+data[0].status+'"]');
                  //$('#status option[value="'+data[0].status+'"]');
                  $('#status').val(data[0].status);


               
              }
            });


      }

      function delete_product(id){

       //alert(id);
        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=lab_test_Inactive",
              success:function(data){
               // alert(data);
                 location.reload();
              }
            });

      }

      function active_product(id){
        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=lab_test_active",
              success:function(data){
               // alert(data);
                 location.reload();
              }
            });
      }
  </script>      


<?php
include("footer.php");