<?php
if(version_compare(PHP_VERSION, '5.3.0', '<')){
    set_magic_quotes_runtime(false);
}
?>
<?php
/**
 * Description of functions
 *
 * @author sms
 */


$root = $_SERVER['DOCUMENT_ROOT'];
ini_set('memory_limit', '2048M');

$smtp_path = $root."/crm/smtpmail/class.phpmailer.php";

/*echo $smtp_path;
exit;*/

include_once 'db_connect.php';
//include "smtpmail/class.phpmailer.php"; // include the class name
include ($smtp_path); // include the class name

class functions extends db_connect {
	
	public function test_input($data)
	{
		$data = trim(strtolower($data));
		$data = strip_tags($data);
		$data = htmlspecialchars($data);
		$data = mysqli_real_escape_string($this->mysqli,$data);
		return $data;
	}
	/* Test_Input Function closed Here */
	
	public function get_datetime()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date1=date( "Y-m-d h:i:s");
		return $date1;
	}
	/* Function get_datetime closed here*/
	
	public function data_insert($sql)
	{
		$result= $this->mysqli->query($sql) or die($this->mysqli->error);
		return $result;
	}
	
	public function data_insert_return_id($sql)
	{
		$result= $this->mysqli->query($sql) or die($this->mysqli->error);
		return $id=$this->mysqli->insert_id;
	}
	/* Insert Function Closed here  */
	
	public function data_select($sql)
	{ 
		$select=$this->mysqli->query($sql) or die($this->mysqli->error);      
		if($select->num_rows==0){
			return 'no';
		}  else {
			while ($row = $select->fetch_array(MYSQLI_ASSOC)) {
				$data[]=$row;
			}
			return $data;
		}   
	}
	/* Select function closed here */
	
	public function data_update($sql)
	{
		$update=  $this->mysqli->query($sql) or die($this->mysqli->error);
		return $update;
	} 
	
	public function data_delete($sql)
	{
		$update=  $this->mysqli->query($sql) or die($this->mysqli->error);
		return $update;
	} 
    // Update Function Closed Here
	function mobile_validate($mobile)
	{
		if(preg_match('/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/', $mobile,$matches)){
          //  print_r($matches);
			return true;
		}
		return false;
	}
	function dbRowInsert($table_name, $form_data)
	{

		$fields = array_keys($form_data);
		$fields=implode(',',$fields);
		$data=implode("','",$form_data);
		
		$sql = "INSERT INTO ".$table_name."(".$fields.")VALUES('".$data."')";

		$result= $this->mysqli->query($sql) or die($this->mysqli->error);
		return $result;

	}
	function dbRowDelete($table_name, $where_clause)
	{
		$where = " where 1 "; 
		if(!empty($where_clause)){

			foreach($where_clause as $key => $val){

				$where .= " and ".$key." = '".$val."'" ; 

			}
		}

		$sql_delete =" Delete From ".$table_name." ". $where." ";
		echo  $sql_delete;
		

	}
	
	function dbRowUpdate($table_name, $form_data, $where_clause)
	{

		$sql = "UPDATE ".$table_name." SET ";
		$sets = array();
		foreach($form_data as $column => $value){

			$sets[] = "`".$column."` = '".$value."'";

		}

		$sql .= implode(', ', $sets);
		$sql .= $whereSQL;          
		return mysql_query($sql);

	}
	public function send_simple_mail($subject,$body,$to,$reply_to,$from_name){
		  
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("support@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");

     /* $mail->AddCC("aakash@mobisofttech.co.in", "Recepient 1");
      $mail->AddCC($reply_to, "Recepient 2");
      $mail->AddCC("hr@mobisofttech.co.in", "Recepient 3");
      $mail->AddCC("priya@mobisofttech.co.in", "Recepient 3");

      $mail->AddBCC("support@mobisofttech.co.in", "Support");
      $mail->AddReplyTo($reply_to, "Reply-To"); */// indicates ReplyTo headers
      

      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
  }

  public function send_simple_mail_approved($subject,$body,$to,$salesEmail,$reply_to,$from_name){
      
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("support@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);
      $mail->AddCC($salesEmail, "Recepient 2");
      $mail->AddCC($reply_to, "Recepient");
      //  $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
      // $mail->AddCC("pradnya.m@mobisofttech.co.in", "ADMIN 1 test");//admin
     /* $mail->AddCC("aakash@mobisofttech.co.in", "Recepient 1");
      $mail->AddCC($reply_to, "Recepient 2");
      $mail->AddCC("hr@mobisofttech.co.in", "Recepient 3");
      $mail->AddCC("priya@mobisofttech.co.in", "Recepient 3");

      $mail->AddBCC("support@mobisofttech.co.in", "Support");
      $mail->AddReplyTo($reply_to, "Reply-To"); */// indicates ReplyTo headers
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }

  public function send_simple_mail2($subject,$body,$to,$reply_to,$from_name){
      
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("support@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);//accounts
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati test");
      $mail->AddCC("pradnya.m@mobisofttech.co.in", "ADMIN 1");//admin

     /* $mail->AddCC($reply_to, "Recepient 2");
      $mail->AddCC("hr@mobisofttech.co.in", "Recepient 3");
      $mail->AddCC($salesEmail, "Recepient 4");
      
      $mail->AddCC("priya@mobisofttech.co.in", "Recepient 5");

      $mail->AddBCC("support@mobisofttech.co.in", "Support");*/
      $mail->AddReplyTo($reply_to, "Reply-To"); // indicates ReplyTo headers
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }

  public function send_simple_mail3($subject,$body,$to,$reply_to,$salesEmail,$from_name){
      
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("support@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);//accounts
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati test");
      // $mail->AddCC("pradnya.m@mobisofttech.co.in", "Recepient 1");//pradeep
      // $mail->AddCC("hr@mobisofttech.co.in", "Recepient 2");
     
     /* $mail->AddCC("akash@mobisofttech.co.in", "Recepient 3");
      $mail->AddCC($salesEmail, "Recepient 4");
      $mail->AddCC("priya@mobisofttech.co.in", "Recepient 5");
      $mail->AddBCC("support@mobisofttech.co.in", "Support");*/
      
     

      
      $mail->AddReplyTo($reply_to, "Reply-To"); // indicates ReplyTo headers
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }

  public function send_attached_mail($subject,$body,$to,$attachment_name,$reply_to,$from_name){
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("contact@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Recepient 1");
     /* $mail->AddCC("aakash@mobisofttech.co.in", "Recepient 1");
      $mail->AddCC($reply_to, "Recepient 2");*/
      //$mail->AddBCC("mail1@domain.com", "Recepient 1");
      $mail->AddReplyTo($reply_to, "Reply-To"); // indicates ReplyTo headers
      $arrlength=count($attachment_name);
      for($x=0;$x<$arrlength;$x++)
      {
      	$mail->AddAttachment($attachment_name[$x]);
      }
      //$mail->AddAttachment($attachment_name);
      
      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
      
  }

  public function send_quotation_mail($subject,$body,$to,$attachment_name,$reply_to,$from_name){
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      $mail->Username = "quotation@mobisofttech.co.in";
      $mail->Password = "manoj@#$%123";

      $mail->SetFrom("quotation@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);
      //$mail->AddCC("aakash@mobisofttech.co.in", "Recepient 1");
      // $mail->AddCC($reply_to, "Recepient 2");
      //$mail->AddCC("quotation@mobisofttech.co.in", "Recepient");
      //$mail->AddBCC("quotation@mobisofttech.co.in", "Self Recepient");
      // $mail->AddReplyTo($reply_to, "Reply-To"); // indicates ReplyTo headers
      $arrlength=count($attachment_name);
      for($x=0;$x<$arrlength;$x++)
      {
      	$mail->AddAttachment($attachment_name[$x]);
      }
      //$mail->AddAttachment($attachment_name);
      
      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
      
  }

  public function send_simple_credit_mail($body,$to,$reply_to,$from_name){
  	
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom($reply_to,'Mobisoft Technology India Pvt Ltd');
      $mail->AddReplyTo("support@mobisofttech.co.in", "Reply-To"); // indicates ReplyTo headers
      $mail->Subject = 'Credit Allocation';
      $mail->Body = $body;
      $mail->AddAddress($to);
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
      // $mail->AddCC("aakash@mobisofttech.co.in", "Aakash Singh");
      // $mail->AddCC("priya@mobisofttech.co.in", "Priya Sharma");
      // $mail->AddCC($reply_to,$from_name);
      // $mail->AddBCC("hr@mobisofttech.co.in", "HR");
      // $mail->AddBCC("accounts@mobisofttech.co.in", "Accounts");
      $mail->AddBCC("support@mobisofttech.co.in", "Support");
      

      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
  }
  
  public function credit_mail_admin_approval($body,$to,$reply_to,$from_name){
    
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom($reply_to,'Mobisoft Technology India Pvt Ltd');
      $mail->AddReplyTo("support@mobisofttech.co.in", "Reply-To"); // indicates ReplyTo headers
      $mail->Subject = 'Credit Allocation HSBU Approval';
      $mail->Body = $body;
      $mail->AddAddress($to);
     /* $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
      $mail->AddCC("aakash@mobisofttech.co.in", "Aakash Singh");
      $mail->AddCC("priya@mobisofttech.co.in", "Priya Sharma");*/
      // $mail->AddCC("support@mobisofttech.co.in", "Support");
      // $mail->AddCC($reply_to,$from_name);
      // $mail->AddBCC("hr@mobisofttech.co.in", "HR");
      // $mail->AddBCC("accounts@mobisofttech.co.in", "Accounts");
      // $mail->AddBCC("support@mobisofttech.co.in", "Support");
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }
   public function credit_mail_BH_approval($body,$to,$reply_to,$from_name){
    
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom($reply_to,'Mobisoft Technology India Pvt Ltd');
      $mail->AddReplyTo("support@mobisofttech.co.in", "Reply-To"); // indicates ReplyTo headers
      $mail->Subject = 'Credit Allocation Business Head Approval';
      $mail->Body = $body;
      $mail->AddAddress($to);
     /* $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
      $mail->AddCC("aakash@mobisofttech.co.in", "Aakash Singh");
      $mail->AddCC("priya@mobisofttech.co.in", "Priya Sharma");*/
      // $mail->AddCC("support@mobisofttech.co.in", "Support");
      // $mail->AddCC($reply_to,$from_name);
      // $mail->AddBCC("hr@mobisofttech.co.in", "HR");
      // $mail->AddBCC("accounts@mobisofttech.co.in", "Accounts");
      // $mail->AddBCC("support@mobisofttech.co.in", "Support");
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }
  public function send_to_own($body,$to,$reply_to,$from_name){
    
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom($reply_to,'Mobisoft Technology India Pvt Ltd');
      $mail->AddReplyTo("support@mobisofttech.co.in", "Reply-To"); // indicates ReplyTo headers
      $mail->Subject = 'Credit Request Approved by Admin';
      $mail->Body = $body;
      $mail->AddAddress($reply_to);

      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
      $mail->AddCC("aakash@mobisofttech.co.in", "akash");
      $mail->AddCC("priya@mobisofttech.co.in", "Priya Sharma");
      $mail->AddCC($reply_to,$from_name);
      $mail->AddBCC("hr@mobisofttech.co.in", "HR");
      $mail->AddBCC("accounts@mobisofttech.co.in", "Accounts");
      $mail->AddBCC("support@mobisofttech.co.in", "Support");
      

      if(!$mail->Send()){
        return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
        return 1;
      }
  }
  public function send_attachment_credit_mail($body,$to,$reply_to,$from_name,$attachment_name){
  	
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom($reply_to,'Mobisoft Technology India Pvt Ltd');
      $mail->AddReplyTo("support@mobisofttech.co.in", "Reply-To"); // indicates ReplyTo headers
      $mail->Subject = 'Credit Allocation Attach';
      $mail->Body = $body;
      $mail->AddAddress($to);
      // $mail->AddCC("shailang.k@mobisofttech.co.in", "Shailang Kharsati");
     /* $mail->AddCC("aakash@mobisofttech.co.in", "Aakash Singh");
      $mail->AddCC("priya@mobisofttech.co.in", "Priya Sharma");*/
      $mail->AddCC($reply_to,$from_name);
      /*$mail->AddBCC("hr@mobisofttech.co.in", "HR");
      $mail->AddBCC("accounts@mobisofttech.co.in", "Accounts");*/
      $mail->AddBCC("support@mobisofttech.co.in", "Support");
      
      $arrlength=count($attachment_name);
      for($x=0;$x<$arrlength;$x++)
      {
      	$mail->AddAttachment($attachment_name[$x]);
      }

      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
  }


  public function send_image_mail($subject,$body,$user_list){
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);     
      
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";
      
      $mail->SetFrom("business@mobisofttech.co.in","Mobisoft Technology");
      $mail->Subject = $subject;
      $mail->AddReplyTo("business@mobisofttech.co.in", "ReplyTo"); // indicates ReplyTo headers
      //$mail->AddAttachment($attachment_name);
      $mail->IsHTML(true);
      //$mail->AddEmbeddedImage('img/user-profile-1.jpg', 'logoimg', 'img/user-profile-1.jpg'); // attach file logo.jpg, and later link to it using identfier logoimg
      /*$mail->Body = "<h1>Test 1 of PHPMailer html</h1>
      <p>This is a test picture: <img src=\"cid:logoimg\" /></p>";*/
      $mail->Body = $body;
      $mail->CharSet="windows-1251";
      $mail->CharSet="utf-8";

      foreach($user_list as $key => $val){
      	
      	$mail->AddAddress($user_list[$key]['email_id']);

      	if(!$mail->Send()){
      		return json_encode("Mailer Error: " . $mail->ErrorInfo);
      	}

      	$mail->ClearAllRecipients();
          $mail->ClearAddresses();  // each AddAddress add to list
          $mail->ClearCCs();
          $mail->ClearBCCs();

      }

      
      //$mail->AltBody="This is text only alternative body.";


  }

  function convert_number($number) 
  { 
  	if (($number < 0) || ($number > 999999999)) 
  	{ 
  		throw new Exception("Number is out of range");
  	} 

  	$Gn = floor($number / 1000000);  /* Millions (giga) */ 
  	$number -= $Gn * 1000000; 
  	$kn = floor($number / 1000);     /* Thousands (kilo) */ 
  	$number -= $kn * 1000; 
  	$Hn = floor($number / 100);      /* Hundreds (hecto) */ 
  	$number -= $Hn * 100; 
  	$Dn = floor($number / 10);       /* Tens (deca) */ 
  	$n = $number % 10;               /* Ones */ 

  	$res = ""; 

  	if ($Gn) 
  	{ 
  		$res .= convert_number($Gn) . " Million"; 
  	} 

  	if ($kn) 
  	{ 
  		$res .= (empty($res) ? "" : " ") . 
  		convert_number($kn) . " Thousand"; 
  	} 

  	if ($Hn) 
  	{ 
  		$res .= (empty($res) ? "" : " ") . 
  		convert_number($Hn) . " Hundred"; 
  	} 

  	$ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
  		"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
  		"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
  		"Nineteen"); 
  	$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
  		"Seventy", "Eigthy", "Ninety"); 

  	if ($Dn || $n) 
  	{ 
  		if (!empty($res)) 
  		{ 
  			$res .= " and "; 
  		} 

  		if ($Dn < 2) 
  		{ 
  			$res .= $ones[$Dn * 10 + $n]; 
  		} 
  		else 
  		{ 
  			$res .= $tens[$Dn]; 

  			if ($n) 
  			{ 
  				$res .= "-" . $ones[$n]; 
  			} 
  		} 
  	} 

  	if (empty($res)) 
  	{ 
  		$res = "zero"; 
  	} 

  	return $res; 
  } 


  function encrypt_decrypt($string) {
  	
  	$string_length=strlen($string); 
  	$encrypted_string=""; 
    /**
    *For each character of the given string generate the code
    */
    for ($position = 0;$position<$string_length;$position++){         
    	$key = (($string_length+$position)+1);
    	$key = (255+$key) % 255;
    	$get_char_to_be_encrypted = SUBSTR($string, $position, 1); 
    	$ascii_char = ORD($get_char_to_be_encrypted); 
        $xored_char = $ascii_char ^ $key;  //xor operation 
        $encrypted_char = CHR($xored_char); 
        $encrypted_string .= $encrypted_char; 
    } 
    /**
    *Return the encrypted/decrypted string
    */
    return $encrypted_string; 
}


}

?>
