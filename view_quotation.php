<?php
    
    include('header_sidebar_crm.php');   
    //include('class/functions.php');
    $con = new functions();
    $user_id = $_SESSION['user_id'];
    $c_date = date("Y-m-d");
    //$currentdate= date('Y-m-d\T00:00:00');
    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "4" ){
        
        //$user_id = "1";//temp variable are used intesd of session for testing purpose
        $qry = "SELECT ud.fname,ud.lname,ud.role,q.* FROM quotation as q inner join user_details as ud on q.user_id = ud.user_id WHERE q.user_id ='".$user_id."' AND q.c_date like '".$c_date."%' ORDER BY q.quotation_id DESC";
        $result = $con->data_select($qry);
       /* echo "<pre>";
        print_r($result);
        exit;*/
    }else if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "8" || $_SESSION['role'] == "9")
    {
       // $qry = "SELECT * FROM customer_details ORDER BY cust_id ASC";
        $qry = "SELECT ud.fname,ud.lname,ud.role,q.* FROM quotation as q inner join user_details as ud on q.user_id = ud.user_id WHERE q.c_date like '".$c_date."%' ";
        $result = $con->data_select($qry);

        /*echo "<pre>";
        print_r($result);
        exit;*/
    }
   
    $qry1 = "SELECT * FROM products WHERE flag = 1 ";
    $result1 = $con->data_select($qry1);

    $reassign_user = "SELECT user_id,fname,lname,role FROM user_details WHERE role !=1 AND user_id !=$user_id AND flag = 1 ";
    $reassign_user_result = $con->data_select($reassign_user);

?>
<style type="text/css">
.overlay {
  
  opacity: 0.5;
  filter: alpha(opacity=0);
  position: fixed;
  top: 0;
  left: 0;
  z-index: 900;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4) !important;
  display: none;

}

.ajax-loader1{
    position: absolute;
    top: 50%;
    right: 35%;
    z-index: 1;
}
</style>

<script src="js_functions/bde_bdm_js.js"></script>
<script src="js_functions/tc_js.js"></script>

<script type="text/javascript">


$(document).ready(function(){    

});

function viewQuotation(idd){
    //alert(idd);
    
    $.ajax({
        url:"ajax_service/view_leads_ajax.php",
        data:"quotation_id="+idd+"&action=getQuotationData",
       // dataType:"json",
        success:function(data){
            //console.log(data);
            //alert(data);
            $("#example-table1").find("tr:gt(0)").remove();
            $("#show_data").append(data);
           
        }
    });
}
 
function resendQuotation(idd){
    //alert(idd);
    $("body").css("background-color","#00000");
    $('body').css('opacity','0.7');
    $(".ajax-loader1").show();

       $.ajax({
        url:"ajax_service/resend_quotation_ajax.php",
        data:"quotation_id="+idd+"&action=resendQuotation",
        success:function(data){
            //console.log(data);
           // alert(data);
            if(data == 1){               
                $("body").css("background-color","");
                $('body').css('opacity','');
                $(".ajax-loader1").hide();
                alert("Quotation sent successfully!");
            }else{
                $("body").css("background-color","");
                $('body').css('opacity','');
                $(".ajax-loader1").hide();
                alert("Quotation not sent.");
            }
        }
    });
}

function getQuotation(){
  
  var sdateSearch = $("#sdateSearch").val();
  var edateSearch = $("#edateSearch").val();
  
  $.ajax({
      url:"ajax_service/quotation_ajax.php",
      data:"sdateSearch="+sdateSearch+"&edateSearch="+edateSearch+"&action=getQuotation",
      //dataType:"json",
      success:function(data){
        //console.log(data);
        //alert(data);
        $('#quotation_data').empty();
        var oTable = $("#example-table").dataTable();
        oTable.fnDestroy();
        $('#quotation_data').html(data);
        $("#example-table").dataTable();
         
      }
  });
}


</script>

<div id="appendOverlay">
    <div id="page-wrapper">

        <div class="page-content">

            <!-- begin PAGE TITLE ROW -->
            <div  align="center" class="ajax-loader1" style="display:none" ><img src="img/ajax-loader.gif"></div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <h1>View Quotation 
                            <small>Quotation Details</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                            </li>
                            <li class="active">View Quotation</li>
                        </ol>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- end PAGE TITLE ROW -->
            <div class="row">
                <div class="col-md-3">
                    <div>
                        <b> From</b> 
                        <input type="date" class="form-control" id="sdateSearch" name="sdateSearch" placeholder="Placeholder Text" value="<?php echo $c_date;?>" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <b> To</b> 
                        <input type="date" class="form-control" id="edateSearch" name="edateSearch" placeholder="Placeholder Text" value="<?php echo $c_date;?>" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <b> &nbsp;</b><br>
                        <button type="button" class="btn btn-default" onclick="getQuotation()">Search</button> 
                    </div>
                </div>
            </div>
                         
            <br>
            <!-- begin ADVANCED TABLES ROW -->
            <div class="row">

                <div class="col-lg-12">

                    <div class="portlet portlet-default">
                        <div class="portlet-heading">
                            <div class="portlet-title">
                                <h4>View Quotation</h4>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <input type="hidden" id="hidden_q_id" value="">
                                <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                    <thead >
                                    <tr>
                                       <th>Sr No.</th>
                                       <?php
                                            if($_SESSION['role'] == "1"){
                                              echo "                                                  
                                              <th>User</th>
                                              ";
                                            }
                                           
                                        ?>

                                        <th>Company Name</th>
                                        <th>Contact Person</th>
                                        <th>Contact Number</th>
                                        <th>Email Id</th>                                       
                                        <th>Send Date</th>
                                        <th>View / Re-Send Quotation</th>
                                        <?php
                                            if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
                                              echo "                                                  
                                                <th>Re-Assign</th>
                                                <th>Action_For_Quotation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                 ";
                                            }
                                            else if ($_SESSION['role'] == "4") {
                                               echo "                                                  
                                                
                                                <th>Action_For_Quotation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                 ";
                                            }
                                           
                                        ?>
                                        
                                        
                                        
                                    </tr>
                                    </thead>
                                    <tbody id="quotation_data">
                                    <?php
                                        if($result > 0){

                                            foreach ($result as $key => $value) {
                                                $role = "";
                                                if($result[$key]['role'] == 1){
                                                    $role = "CEO";
                                                }else if($result[$key]['role'] == 2){
                                                    $role = "BDM";
                                                }else if($result[$key]['role'] == 3){
                                                    $role = "BDE";
                                                }else if($result[$key]['role'] == 4){
                                                    $role = "Telle Caller";
                                                }else if($result[$key]['role'] == 5){
                                                    $role = "RM";
                                                }
                                                $sr = $key + 1;
                                                echo "<tr id='row".$sr."'>";

                                                echo "<td>".$sr."</td>";
                                                if($_SESSION['role'] == "1"){
                                                    
                                                echo "<td >".$result[$key]['fname']." ".$result[$key]['lname']." - ".$role."</td>";                                                   
                                               
                                               }
                                              
                                               echo "<td id='cnm".$sr."'>".$result[$key]['company_name']."</td>";
                                               echo "<td id='cno_person".$sr."'>".$result[$key]['contact_person']."</td>";
                                               echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
                                               echo "<td id='eid".$sr."'>".$result[$key]['email_id']."</td>";
                                               echo "<td >".$result[$key]['c_date']."</td>";
                                               echo "<td align='center'><a href='#' data-toggle='modal' data-target='#flexModalQuotation' title='View Details' onclick='return viewQuotation(".$result[$key]['quotation_id'].");'><i class='fa fa-file-text btn-orange btn-sm'></i></a>  <a href='javascript:void(0)' title='Re-Send Quotation' onclick='resendQuotation(".$result[$key]['quotation_id'].")'><i class='fa fa-exchange btn-green btn-sm'></i></a></td>"; 
                                               if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){

                                                    $get_client_id = "SELECT * FROM client_details WHERE cust_id =".$result[$key]['cust_id']." ";
                                                    $get_client_id_result = $con->data_select($get_client_id);
                                                    
                                                  echo "<input type='hidden' id='c_add".$sr."' value='".$get_client_id_result[0]['address']."'>";
                                                  echo"<td align='center'><a href='#'  data-toggle='modal' data-target='#reassignLeadModal' title='Re-Assign Lead' onclick='return reassign(".$sr.",".$result[$key]['cust_id'].",".$get_client_id_result[0]['client_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-mail-forward btn-green btn-sm'></i></a></td>";
                                                  echo "<td align='center'><a href='#'  data-toggle='modal' data-target='#closeLeadModal' title='Make Payment' onclick='return accept(".$sr.",".$result[$key]['cust_id'].",".$get_client_id_result[0]['client_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#rejectLeadModal' title='Reject Lead' onclick='return reject(".$sr.",".$result[$key]['cust_id'].",".$get_client_id_result[0]['client_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#delayLeadModal' title='Set Reminder' onclick='return delay(".$sr.",".$result[$key]['cust_id'].",".$get_client_id_result[0]['client_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i></a></td>";
                                                }
                                                else if($_SESSION['role'] == "4")
                                                {
                                                  echo "<td align='center'><div class='btn-group btn-group-sm'>
                                                            <a href='#' data-toggle='modal' data-target='#openLeadTCModal' title='Make Lead' onclick='return acceptTC(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-thumbs-o-up btn-green btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#rejectLeadTCModal' title='Reject Call' onclick='return rejectTC(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#delayLeadTCModal' title='Set Reminder' onclick='return delayTC(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['quotation_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
                                                            </div></td>";                                                            
                                                  
                                                }
                                           
                                               echo "</tr>";
                                            }
                                       }
                                       
                                     
                                    ?>
                                       
                                  </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.portlet-body -->
                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /.col-lg-12 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.page-content -->

    </div>
    <!-- /#page-wrapper -->
    <!-- end MAIN PAGE CONTENT -->
         
</div>

<?php
    if($_SESSION['role'] == "4"){
        include 'tc_modal.php';
    }

    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
        include 'bde_bdm_modal.php';
    }
?>

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="flexModalQuotation" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="smscountclose" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Quotation Details</h4>
            </div>
            <div class="modal-body">
                
                <div class="table-responsive">
                    <table id="example-table1" class="table table-striped table-bordered table-hover table-green">
                        <thead>
                            <tr>
                               <th>Sr.</th>
                               <th>Product Name</th>
                               <th>Quantity</th>
                               <th>Rate</th>
                               <th>Validity</th>

                            </tr>
                        </thead>
                        <tbody id="show_data">
                           
                           
                      </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
               <!--  <button type="button" class="btn btn-green">Save changes</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
    $(function(){
        $('#example-table').DataTable();
    });
</script>
   
<?php
 include('footer_crm.php');
?>