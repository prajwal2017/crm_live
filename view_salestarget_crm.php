<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();
    $curent_date = date("Y-m");

    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){      
        $user_id = $_SESSION['user_id'];
        $qry = "select * from user_details ud inner join sales_target st on ud.user_id = st.user_id where st.user_id='".$user_id."' and st.s_date='".$curent_date."' order by st.st_id desc";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"){
        $qry = "select * from user_details ud inner join sales_target st on ud.user_id = st.user_id where ud.role=2 and st.s_date='".$curent_date."' order by st.st_id desc";
        /*echo $qry;
        exit;*/
        $result = $con->data_select($qry);
        
    }

    $qry1 = "select user_id,fname,lname from user_details where role= '3'";
    $result1 = $con -> data_select($qry1);

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "7"){

        $get_bdm = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 2 AND flag = '1' ";
        $get_bdm_result = $con->data_select($get_bdm);

        $get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND flag = '1' ";
        $get_bde_result = $con->data_select($get_bde);
    }

    if($_SESSION['role'] == "2"){
        
        $get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND reporting_id = '".$user_id."' AND flag = '1' ";
        $get_bde_result = $con->data_select($get_bde);
    }
   

   // $get_tc = "SELECT user_id,fname,lname,role FROM user_details WHERE role =4 AND reporting_id = '".$user_id."' AND flag = '1' ";
    $get_tc = "SELECT user_id,fname,lname,role FROM user_details WHERE role =4 AND flag = '1' ";
    $get_tc_result = $con->data_select($get_tc);
       

?>


<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="js_functions/datepicker-calendar.js"></script>
<style>
.ui-datepicker-calendar {
    display: none;
}
</style>

<script type="text/javascript">

    $(document).ready(function(){
       /* var test ;
        $("#bde_name").val('BDE');
        test = $("#bde_name").val();
        $("#assigned_user_id").val(test);*/

        showSalesTarget();

        $("#update").click(function(){
            //alert("inside submit");
            var temp=validateField();
            if(temp== false)
            {
                return false;
            }
            $.ajax({

                url:"ajax_service.php",
                data:$("#updateSalesTarget").serialize()+"&action=updatesalestarget",
                success:function(data){
                    console.log(data);
                    if(data == "success")
                    {
                        alert("Data updated successfully");
                        
                        location.reload();
                    }
                    else
                    {
                        alert("Data not updated");
                        
                    }
                }
            });
        });

    });
        function viewData(stid,sr)
        {
            //alert("Inside edit "+user_id);
            var sp_name = $("#sales_name"+sr).html();
            /*alert(sp_name);*/
            $("#salesperson_name").val(sp_name);

            $.ajax({
                url:"ajax_service.php",
                data:"stid="+stid+"&action=viewsalestarget",
                dataType:"json",
                success:function(data){
                    //alert("inside ajax");
                    console.log(data);
                    
                    $("#salesperson").val(data[0].user_id); 
                    $("#target").val(data[0].target_amount);                   
                    $("#st_id").val(data[0].st_id);
                    $("#achieved").val(data[0].achieved);
                    $("#balance_target").val(data[0].target_total_amount - data[0].target_amount);
                    //$("#role").val(data[0].role);
                    $("#status").val(data[0].flag);

                    /*if(data[0].flag == "1")
                    {
                        $("#status1").attr("selected",true);
                    }
                    else
                    {
                        $("#status0").attr("selected",true);
                    }*/
                }
            });
            
        }

        
        function deleteData(stid,sr)
        {
            //alert("Inside delete");
            var confirm_delete = confirm("Are you sure you want to delete?");
                if(confirm_delete == true)
                    {
                        $.ajax({

                            url:"ajax_service.php",
                            data:"stid="+stid+"&action=deletesalestarget",
                            success:function(data){
                                console.log(data);
                                if(data == "success")
                                {
                                    alert("Data deleted successfully");
                                    $("#row"+sr).fadeOut();
                                    location.reload();
                                }
                                else
                                {
                                    alert("Data not deleted");
                                    
                                }

                            }
                        });
                    }
        }

        function validateField()
        { 

            var salesperson=$("#salesperson").val();
            var target=$("#target").val();

           if(target.length == "" || target.length =='' || target.length == null)
            {
                alert("Please Enter Target");
                return false;
            }
            /*var startDate=$("#startDate").val();
            var endDate=$("#endDate").val();
            if(salesperson == "" || salesperson =='' || salesperson == null)
            {
                alert("Please Select Sales Person");
                return false;
            }
            else if(target.length == "" || target.length =='' || target.length == null)
            {
                alert("Please Enter Target");
                return false;
            }

            else if(startDate == "" || startDate =='' || startDate == null)
            {
                alert("Please Enter Start Date");
                return false;
            }
           
            else if(endDate.length == "" || endDate.length =='' || endDate.length == null)
            {
                alert("Please Enter End Date ");
                return false;
            }*/
            else
            {

            }
        }

function showSalesTarget(){
    var input_date = $("#datepicker").val();
    var salesperson_id = $("#assigned_user_id").val();
    /*alert(salesperson_id);*/
    
    $.ajax({
        url:"ajax_service/view_leads_ajax.php",
        data:"salesperson_id="+salesperson_id+"&s_date="+input_date+"&action=showSalesTarget",
        success:function(data){
           /* alert(data);*/
            console.log(data);
            $('#sales_target_data').empty();
            var oTable = $("#example-table").dataTable();
            oTable.fnDestroy();
            $('#sales_target_data').html(data);
            $("#example-table").dataTable();
        }
    });
}

function setValue123(idd){
    
    var id = idd.id;
    //alert(id);
        var v = $("#"+id).val();
       // alert(v);
       $("#assigned_user_id").val(v);
        if(id == "bde_name"){
            $("#bdm_name").val('');
        }

        if(id == "bdm_name"){
            $("#bde_name").val('');
        }
    
}

    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Sales Target 
                                <small>Target Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Sales Target</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                <div class="row">

                    <div class="col-md-2" <?php if($_SESSION['role']=='2' || $_SESSION['role']=='3' || $_SESSION['role']=='4'){echo "style='display:none'"; }?> >
                        <div>                                        
                            <b> Select BDM</b>                           
                            <select  class='form-control' id='bdm_name' name='bdm_name' onchange='setValue123(this);' required>
                                <option value='' >----Select BDM----</option>;
                                
                                <?php
                                    if($get_bdm_result > 0){
                                        echo "<option value='BDM'>All</option>;";
                                        foreach ($get_bdm_result as $key => $value){
                                            echo "<option value='".$get_bdm_result[$key]['user_id']."'>".$get_bdm_result[$key]['fname']." ".$get_bdm_result[$key]['lname']."</option>";
                                        }
                                    }                                    
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-2" <?php if($_SESSION['role']=='3' || $_SESSION['role']=='4'){echo "style='display:none'"; }?>>
                        <div>                                        
                            <b> Select BDE</b>                           
                            <select  class='form-control' id='bde_name' name='bde_name' onchange='setValue123(this);' required>
                                
                                <?php
                                    echo "<option value=''>----Select BDE----</option>";
                                    if($get_bde_result > 0){
                                        echo "<option value='BDE'  selected>All</option>";
                                    }
                                    
                                    if($_SESSION['role']=='2'){                                       
                                        echo "<option value='".$_SESSION['user_id']."' selected >Self Target</option>";
                                    }
                                    
                                    foreach ($get_bde_result as $key => $value) {
                                        echo "<option value='".$get_bde_result[$key]['user_id']."'>".$get_bde_result[$key]['fname']." ".$get_bde_result[$key]['lname']."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                     <div class="col-md-2">
                        <div>                                        
                            <b> Select Month</b>  
                                <input class='form-control' type="text" id="datepicker" name="startDate" placeholder="Select Month"/>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div>
                             <b>&nbsp;</b><br>
                             <?php
                                if($_SESSION['role']=='2' || $_SESSION['role']=='3'){
                                    echo '<input type="hidden" id="assigned_user_id" name="assigned_user_id" value="'.$_SESSION['user_id'].'">';
                                }else{
                                    echo '<input type="hidden" id="assigned_user_id" name="assigned_user_id" value="BDM">';
                                }
                             ?>
                                
                            <button class="btn btn-green btn-sm" onclick="showSalesTarget()">Submit</button>
                        </div>
                    </div>

                </div>
                <br>
                
                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
                         <!-- <div >
                                <label>Select Month</label>
                                <input type="text" id="datepicker" name="startDate" placeholder="Select Month"/>
                                <button class="btn btn-green btn-sm" onclick="showSalesTarget()">Submit</button>
                                <div class="clearfix"></div>
                        </div> -->
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Sales Target</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <?php
                                                if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"){
                                                    echo "<th>Person Name</th>";
                                                }
                                                ?>
                                                
                                                <th>Target Month</th>
                                                <th>Target</th>
                                                <th>Total Target</th>
                                                <th>Achieved</th>
                                                <th>Balance</th>
                                                
                                               <?php
                                                if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"){
                                                    echo "<th>Status</th>";
                                                    echo "<th>Action</th>";
                                                }
                                               ?> 
                                            </tr>
                                        </thead>
                                        <tbody id="sales_target_data">
                                           <?php
                                                if($result > 0){
                                                       foreach ($result as $key => $value) {
                                                        $sr = $key + 1;

                                                        $res =explode("-", $result[$key]['s_date']);
                                                        $month = $res[1];
                                                        $year = $res[0];
                                                        $mon="";

                                                        if($month == 1)
                                                        {
                                                            $mon = "January";
                                                        }else if($month == 2){
                                                            $mon = "February";
                                                        }else if($month == 3){
                                                            $mon = "March";
                                                        }else if($month == 4){
                                                            $mon = "April";
                                                        }else if($month == 5){
                                                            $mon = "May";
                                                        }else if($month == 6){
                                                            $mon = "June";
                                                        }else if($month == 7){
                                                            $mon = "July";
                                                        }else if($month == 8){
                                                            $mon = "August";
                                                        }else if($month == 9){
                                                            $mon = "September";
                                                        }else if($month == 10){
                                                            $mon = "October";
                                                        }else if($month == 11){
                                                            $mon = "November";
                                                        }else if($month == 12){
                                                            $mon = "December";
                                                        }


                                                        
                                                        $start_date = $mon." ".$year;

                                                        echo '<tr id="row'.$sr.'">';

                                                        echo "<td>".$sr."</td>";

                                                        if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"){
                                                            $role_name = "";
                                                            if($result[$key]['role'] == 1){
                                                                $role_name = "Admin";
                                                            }
                                                            if($result[$key]['role'] == 2){
                                                                $role_name = "BDM";
                                                            }
                                                            if($result[$key]['role'] == 3){
                                                                $role_name = "BDE";
                                                            }
                                                            if($result[$key]['role'] == 4){
                                                                $role_name = "TelleCaller";
                                                            }
                                                            if($result[$key]['role'] == 5){
                                                                $role_name = "RM";
                                                            }
                                                            if($result[$key]['role'] == 7){
                                                                $role_name = "AC";
                                                            }
                                                            echo "<td id='sales_name".$sr."'>".$result[$key]['fname']." ".$result[$key]['lname']." - ".$role_name."</td>";
                                                        }
                                                        
                                                        echo "<td>".$start_date."</td>";
                                                        echo "<td><i class='fa fa-inr'></i> ".$result[$key]['target_amount']."</td>";
                                                        echo "<td><i class='fa fa-inr'></i> ".$result[$key]['target_total_amount']."</td>";
                                                        //echo "<td>".$result[$key]['e_date']."</td>";
                                                        echo "<td><i class='fa fa-inr'></i> ".$result[$key]['achieved']."</td>";
                                                        echo "<td><i class='fa fa-inr'></i> ".$result[$key]['balance']."</td>";
                                                        
                                                       // echo "<td>".$result[$key]['flag']."</td>";
                                                        if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"){
                                                            if($result[$key]['flag'] == 1){
                                                                echo "<td>Active</td>";
                                                            }
                                                            if($result[$key]['flag'] == 0){
                                                                echo "<td>Inactive</td>";
                                                            }
                                                             echo '<td align="center"><a href="#" title="Edit Details" data-toggle="modal" data-target="#flexModal" onclick="return viewData('.$result[$key]["st_id"].','.$sr.');"><i class="fa fa-cut btn-green btn-sm"></i></a>  <a href="#" title="Delete" onclick="deleteData('.$result[$key]["st_id"].','.$sr.')"><i class="fa fa-trash-o btn-red btn-sm"></i></a></td>';
                                                        }
                                                        
                                                        echo "</tr>";
                                                    } 
                                                }                                                
                                                
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit Sales Target</h4>
                </div>
                <form id="updateSalesTarget" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                        <input type="hidden" id="st_id" name="st_id">  
                        <input type="hidden" id="achieved" name="achieved">                 
                            
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Sales Person</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="salesperson_name" name="salesperson_name" value="" placeholder="Placeholder Text" readonly>
                                    <input type="hidden" class="form-control" id="salesperson" name="salesperson" value="" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Balance Target(<i class='fa fa-inr'></i>)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="balance_target" name="balance_target" placeholder="Placeholder Text" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Target(<i class='fa fa-inr'></i>)</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="target" name="target" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Start Date</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="startDate" id="startDate" name="startDate"  required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">End Date</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="endDate" name="endDate"  required>
                                    <span class="help-block"></span>
                                </div>
                            </div> -->
                            <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-10">
                                <select  class="form-control" id="status" name="status" required>
                                    <option value="">Select One:</option>
                                    <option id="status1" value="1">Active</option>
                                    <option id="status0" value="0">Inactive</option>
                                </select>
                            </div>
                        </div>

                            
                </div>
                <div class="modal-footer">
                    <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                    <button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
                    <button type="button" id="update" class="btn btn-green">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
<?php
 include('footer_crm.php');
?>