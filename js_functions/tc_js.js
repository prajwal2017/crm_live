$(document).ready(function(){

    $("#addOpenLeadTCDetails").click(function(){
       
        var temp = validateOpenLeadTC();
           if(temp == false)
           {
             return false;
             
           }
        $(".close_model_div").hide();
        $("#addOpenLeadTCDetails").hide();
        $(".ajax-loader").show();
        $.ajax({
            url:"ajax_service.php",
            data:$("#openLeadTCFormData").serialize()+"&action=addClientDetails",
            success:function(data){
                /*alert(data);
                console.log(data);
                return false;*/
                if (data == "success") {
                    alert("Client data inserted.");
                    $(".close_model_div").show();                       
                    $(".ajax-loader").hide();
                    location.reload();
                }else{
                    alert("Client data inserted.");
                    $(".close_model_div").show();
                    $("#addOpenLeadTCDetails").show();
                    $(".ajax-loader").hide();
                }
            }
        });
    });


    $("#addRejectLeadTCDetails").click(function(){
        //alert();
        var temp =validateRejectLeadTC();
        if(temp == false)
        {
            return false;
        }
        $(".close_model_div").hide();
        $("#addRejectLeadTCDetails").hide();
        $(".ajax-loader").show();

        $.ajax({
            url:"ajax_service.php",
            data:$("#rejectLeadTCData").serialize()+"&action=updateRejectDetails&type=Call",
            success:function(data){
                //alert(data);
                console.log(data);
                if (data == "success") {
                    alert("Customer data Updated.");
                    $(".close_model_div").show();
                    $(".ajax-loader").hide();
                    location.reload();
                }else{
                    alert("Client data Updated.");
                    $(".close_model_div").show();
                    $("#addRejectLeadTCDetails").show();
                    $(".ajax-loader").hide();
                }
            }
        });
    });

    $("#addDelayLeadTCDetails").click(function(){
        //alert();
        var temp = validateDelayLeadTC();
           if(temp == false)
           {
             return false;
             
           }
        $(".close_model_div").hide();
        $("#addDelayLeadTCDetails").hide();
        $(".ajax-loader").show();
        $.ajax({
            url:"ajax_service.php",
            data:$("#delayLeadTCData").serialize()+"&action=updateDelayDetails&r_type=Call",
            success:function(data){
                //alert(data);
                console.log(data);
                if (data == 1) {
                    alert("Customer data Updated.");
                    $(".close_model_div").show();
                    $(".ajax-loader").hide();
                    location.reload();
                }else{
                    alert("Client data Updated.");
                    $(".close_model_div").show();
                    $("#addDelayLeadTCDetails").show();
                    $(".ajax-loader").hide();
                }
            }
        });
    });

});

function acceptTC(sr,cid,qid)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    $("#companyName").val(cname);
    $("#contactNumber").val(cno);
    $("#accept_cust_id").val(cid);
    $("#accept_q_id").val(qid);
    //$("#popup1").show();

}

function rejectTC(sr,cid,qid)
{
    
    $("#reject_cust_id").val(cid);
    $("#reject_q_id").val(qid);
    //$("#popup2").show();

}

function delayTC(sr,cid,qid)
{
    
    $("#delay_cust_id").val(cid);
    $("#delay_q_id").val(qid);
    //$("#popup3").show();

}

function validateOpenLeadTC()
{ 
   
    var companyName=$("#companyName").val();
    var contactNumber=$("#contactNumber").val().replace(/ /g,'');
    var contactPerson=$("#contactPerson").val();
    var emailId=$("#emailId").val();
    var address=$("#address").val();
    var assignLead=$("#assignLead").val();
    
//alert($("#contactNumber").length);
   
    if(companyName.length == "" || companyName.length =='' || companyName.length == null)
    {
        alert("Please Enter Company Name");
        return false;
    }
    else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null)
    {
        
    }
    else if(contactNumber.length < 10 || contactNumber.length > 12)
    {

            alert("Please Enter correct number");
            return false;
      
    }
    else if(contactPerson.length == "" ||contactPerson.length =='' || contactPerson.length == null)
    {
         alert("Please Enter Contact Person");
            return false;
    }
    else if(emailId.length == "" ||emailId.length =='' || emailId.length == null)
    {

        alert("Please Enter email Id");
            return false;
        
    }
    else if(address.length == "" ||address.length =='' || address.length == null)
    {
        alert("Please Enter Address");
        return false;
    }
   /* else if(assignLead =="" || assignLead == '' || assignLead == null)
    {
        alert("Please Select  Assign Lead");
        return false;
    }*/
    else
    {

    }
}