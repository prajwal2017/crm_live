

$(document).ready(function(){
    $(".close_client_model").click(function(){
        $('#addClientData').trigger("reset");
        $('#updateRejectDataCall').trigger("reset");
        $('#updateDelayDataCall').trigger("reset");
    });


    $("#addClientDetails").click(function(){
        var temp = validateFieldCall();
        if(temp == false){
           return false;
       }
       $.ajax({
        url:"ajax_service.php",
        data:$("#addClientData").serialize()+"&action=addClientDetails&action2=delete",
        success:function(data){
            console.log(data);
            if (data == "success") {
                alert("Client data inserted.");
                $("#popup1").hide();
                $(".overlay").remove();
                location.reload();
            }
        }
    });
   });



    $("#updateRejectDetailsCall").click(function(){
            //alert();
            var temp =validateRejectCall();
            if(temp == false)
            {
                return false;
            }
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateRejectData").serialize()+"&action=updateRejectDetails&action2=delete&type=Call",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup2").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });



    $("#updateDelayDetailsCall").click(function(){
        var temp = validateDelayCall();
        if(temp == false){
           return false;
       }
       $.ajax({
        url:"ajax_service.php",
        data:$("#updateDelayData").serialize()+"&action=updateDelayDetails&action2=delay",
        success:function(data){
            console.log(data);
            if (data == 1) {
                alert("Customer data Updated.");
                location.reload();
            }
        }
    });
   });



    $("#viewCustomers").click(function(){
        var user_id = $("#assigned_user_id").val();
            //alert(user_id);
            $.ajax({
                url:"ajax_service/customer_ajax.php",                
                data:"user_id="+user_id+"&action=viewCustomers",
                dataType:"json",
                //dataType:"json",
                success:function(data){
                    
                    //console.log(data);
                    $("#example-table").find("tr").remove();
                    //var thead = "<thead><th>Sr No.</th><th>company Name</th><th>Contact Number</th></thead>";
                   // $("#example-table").append(thead);
                   // alert(data[1].company_name);
                   if(data == "no"){
                      var tbody="";
                      var tr="<tr>";
                      var td0="<td colspan='3' align='center'>No Records Found</td></tr>";
                      tbody = tbody+tr+td0;
                      $('#example-table').append('<thead><th>Sr No.</th><th>company Name</th><th>Contact Number</th></thead>'+tbody);
                  }else{
                    var n=1;
                    var tbody="";
                    for(var i=0;i<data.length;i++)
                    {
                        
                        var tr="<tr id='row"+n+"'>";
                        var td0="<td>"+n+"</td>";
                        var td1="<td>"+data[i].company_name+"</td>";
                        var td2="<td>"+data[i].contact_number+"</td></tr>";
                        tbody = tbody+tr+td0+td1+td2;
                               //$("#example-table").append(tr+td0+td1+td2); 
                               n++;
                               
                           }   
                           
                           $('#example-table').append('<thead><th>Sr No.</th><th>company Name</th><th>Contact Number</th></thead>'+tbody);
                           
                       }
                   }
               });
        });

    

    $("#viewSelfCustomers").click(function(){
        location.reload();

    });



});
function acceptCall(sr,cid,r_id)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    $("#companyNameCall").val(cname);
    $("#contactNumberCall").val(cno);
    $("#acceptCall_cust_id").val(cid);
    $("#acceptCall_reminderId").val(r_id);

}

function rejectCall(sr,cid,r_id)
{
    
    $("#rejectCall_cust_id").val(cid);
    $("#rejectCall_reminderId").val(r_id);
    
}

function delayCall(sr,cid,r_id)
{
    
    $("#delayCall_cust_id").val(cid);
    $("#delayCall_reminderId").val(r_id);
    
}

function setValue123(idd){
    
    var id = idd.id;
    //alert(id);
    var v = $("#"+id).val();
       // alert(v);
       $("#assigned_user_id").val(v);
       if(id == "bde_name"){
        $("#tc_name").val('');
    }

    if(id == "tc_name"){
        $("#bde_name").val('');
    }
    
}


function validateRejectCall()
{
    var  remark=$("#remarkCall").val();
    if(remark.length == "" || remark.length == '' || remark.length == null)
    {
        alert("Please Enter Remark ");
        return false;
    }
    else
    {

    }

}
function validateDelayCall()
{
    var delay_remark=$("#delay_remarkCall").val();
    
    var rdate=$("#rdateCall").val();
    var rtime=$("#rtimeCall").val();
    if(delay_remark.length == "" || delay_remark.length == '' || delay_remark.length == null)
    {
        alert("Please Enter Delay Remark ");
        return false;
    }
    else if(rdate.length == "" || rdate.length == '' || rdate.length == null)
    {
        alert("Please Select  Date ");
        return false;
    }
    else if(rtime.length == "" || rtime.length == '' || rtime.length == null)
    {
        alert("Please Select  rtime ");
        return false;
    }
}
function validateFieldCall()
{ 
 
    var companyName=$("#companyNameCall").val();
    var contactNumber=$("#contactNumberCall").val().replace(/ /g,'');
    var contactPerson=$("#contactPersonCall").val();
    var emailId=$("#emailIdCall").val();
    var address=$("#addressCall").val();
    var assignLead=$("#assignLeadCall").val();
    
//alert($("#contactNumber").length);

if(companyName.length == "" || companyName.length =='' || companyName.length == null)
{
    alert("Please Enter Company Name");
    return false;
}
else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null)
{
    
}
else if(contactNumber.length < 10 || contactNumber.length > 12)
{

    alert("Please Enter correct number");
    return false;
    
}
else if(contactPerson.length == "" ||contactPerson.length =='' || contactPerson.length == null)
{
   alert("Please Enter Contact Person");
   return false;
}
else if(emailId.length == "" ||emailId.length =='' || emailId.length == null)
{

    alert("Please Enter email Id");
    return false;
    
}
else if(address.length == "" ||address.length =='' || address.length == null)
{
    alert("Please Enter Address");
    return false;
}
   /* else if(assignLead =="" || assignLead == '' || assignLead == null)
    {
        alert("Please Select  Assign Lead");
        return false;
    }*/
    else
    {

    }
}


/****************************************************************************************************************/
/****************************************************************************************************************/
/****************************************************************************************************************/


$(document).ready(function(){


    $("#updateRejectDetailsLead").click(function(){
            //alert();
            var temp=validateRejectLead();
            if(temp == false)
            {
              return false;
          }
          $.ajax({
            url:"ajax_service.php",
            data:$("#updateRejectDataLead").serialize()+"&action=updateRejectDetails&action2=delete&type=Lead",
            success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup2").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
      });

    $("#updateDelayDetailsLead").click(function(){
            //alert();
            var temp=validateDelayLead();
            if(temp == false)
            {
              return false;
          }
          $.ajax({
            url:"ajax_service.php",
            data:$("#updateDelayDataLead").serialize()+"&action=updateDelayDetails&action2=delay",
            success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == 1) {
                        alert("Customer data Updated.");
                        location.reload();
                    }
                }
            });
      });


});
function acceptLead(sr,cid,clid,r_id)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();
    var eid = $("#emailid"+sr).val();
    var cadd = $("#c_add"+sr).html();
    var cnm = $("#cno_person"+sr).html();
   // alert(eid);

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    
    $("#acceptLead_reminderId").val(r_id);
    $("#acceptLead_cust_id").val(cid);
    $("#acceptLead_client_id").val(clid);
    $("#acceptLead_email_id").val(eid);
    $("#acceptLead_client_add").val(cadd);
    $("#acceptLead_client_name").val(cnm);
    $("#acceptLead_company_name").val(cname);
    $("#acceptLead_contact_number").val(cno);
    

}

function rejectLead(sr,cid,r_id)
{
    
    $("#reject_cust_id").val(cid);
    $("#rejectLead_reminderId").val(r_id);
    
}

function delayLead(sr,cid,r_id)
{
    
    $("#delay_cust_id").val(cid);
    $("#delayLead_reminderId").val(r_id);
    
}

function reassign(sr,cid,clid,r_id){

  $("#reassign_client_id").val(clid);
  $("#reassignLead_reminderId").val(r_id);

}


function validateRejectLead()
{
    var  remark=$("#remark").val();
    if(remark.length == "" || remark.length == '' || remark.length == null)
    {
        alert("Please Enter Remark ");
        return false;
    }
    else
    {

    }

}
function validateDelayLead()
{
    var delay_remark=$("#delay_remark").val();
    
    var rdate=$("#rdateLead").val();
    var rtime=$("#rtimeLead").val();
    if(delay_remark.length == "" || delay_remark.length == '' || delay_remark.length == null)
    {
        alert("Please Enter Delay Remark ");
        return false;
    }
    else if(rdate.length == "" || rdate.length == '' || rdate.length == null)
    {
        alert("Please Select  Date ");
        return false;
    }
    else if(rtime.length == "" || rtime.length == '' || rtime.length == null)
    {
        alert("Please Select  rtime ");
        return false;
    }
}
function validateFieldLead()
{ 
 
    var product_name1=$("#product_name1").val();
    var product_quantity1=$("#product_quantity1").val();
    var product_rate1=$("#product_rate1").val();
    var total_amount1=$("#total_amount1").val();
    var paymode1=$("#paymode1").val();
    var check_number1=$("#check_number1").val();
    var amount_received1=$("#amount_received1").val();
    
    if(product_name1 == "" || product_name1 =='' || product_name1== null)
    {
        alert("Please Select  Product Name");
        return false;
    }
    else if(product_quantity1 == "" || product_quantity1 =='' || product_quantity1 == null)
    {
        alert("Please Select product quantity");
        return false;
    }
    
    else if(total_amount1.length == "" ||total_amount1.length =='' || total_amount1.length == null)
    {
       alert("Please Enter Total Amount");
       return false;
   }
   else if(paymode1 == "" ||paymode1 =='' || paymode1 == null)
   {

    alert("Please Select  Paymode");
    return false;
    
}
else if(paymode1 == "Check")
{
    
   if(check_number1.length == "" ||check_number1.length =='' || check_number1.length == null)
   {
      alert("Please Enter check number");
      return false;
  }
} 
else if(amount_received1.length =="" || amount_received1.length == '' || amount_received1.length == null)
{
    alert("Please enter amount received by");
    return false;
}
else
{

}
}


$(document).ready(function(){

 // sessionStorage.clear();

 $("#saveClientDetails").click(function(){
    //alert();
    var temp=validateFieldLead();
    if(temp == false)
    {
      return false;
  }
   // alert("in getData");
   var prodN="";
   var prodName="";
    //var smsT="";
   // var smsType="";
   var prodQ="";
   var prodQty="";
   var prodR="";
   var prodRate="";
   var totalA="";
   var totalAmount="";
   var payM="";
   var payMode="";
   var checkN="";
   var checkNumber="";
   var amtR="";
   var amtReceived="";
   var prodT="";
   var prodTax="";
   var prodTAmt="";
   var prodTaxAmt="";
   var prodHTAmt="";
   var prodHTaxAmt="";
   
    //var check=true;
    $("select[name=product_name]").each(function(){
      //alert();
      var prodid=this.id;
      prodN=$("#"+prodid).val();
      prodName=prodName+prodN+"__";
     // alert("product name: "+prodName);  
 });
    
    
    $("select[name=product_quantity]").each(function(){
      var qtyid=this.id;
      prodQ=$("#"+qtyid).val();
      prodQty=prodQty+prodQ+"__";
      //alert("product quantity: "+prodQty);  
      
  });
    
    $("input[name=product_rate]").each(function(){
      var rateid=this.id;
      prodR=$("#"+rateid).val();
      prodRate=prodRate+prodR+"__";
      //alert("product prodRate: "+prodRate);  
      
  });
    
    $("input[name=total_amount]").each(function(){
      var totalid=this.id;
      totalA=$("#"+totalid).val();
      totalAmount=totalAmount+totalA+"__";
     // alert("product totalAmount: "+totalAmount);  
     
 });

    $("select[name=paymode]").each(function(){
      var payid=this.id;
      payM=$("#"+payid).val();
      if(payM == "null" || payM == null || payM == "")
      {
          payM = "NO"
      }
      payMode=payMode+payM+"__";
      //alert("product quantity: "+prodQty);  
      
  });

    $("input[name=check_number]").each(function(){
        var checkid=this.id;
        checkN=$("#"+checkid).val();
        if(checkN == "null" || checkN == null || checkN == "")
        {
          checkN = "NO"
      }
      checkNumber=checkNumber+checkN+"__";
    //alert("product quantity: "+prodQty);  
    
});

    $("input[name=amount_received]").each(function(){
        var amtRid=this.id;
        amtR=$("#"+amtRid).val();    
        amtReceived=amtReceived+amtR+"__";
    //alert("product quantity: "+prodQty);  
    
});

    $("input[name=send_tax]").each(function(){
        var taxid=this.id;
        prodT=$("#"+taxid).val();
        if(prodT == "null" || prodT == null || prodT == "")
        {
          prodT = "NO"
      }
      prodTax=prodTax+prodT+"__";
    //alert("product quantity: "+prodQty);  
    
});

    $("input[name=tax_amount]").each(function(){
        var taxamtid=this.id;
        prodTAmt=$("#"+taxamtid).val();
        if(prodTAmt == "null" || prodTAmt == null || prodTAmt == "")
        {
          prodTAmt = "NO"
      }
      prodTaxAmt=prodTaxAmt+prodTAmt+"__";
    //alert("product quantity: "+prodQty);  
    
});

    $("input[name=hidden_total_amount]").each(function(){
        var hamtid=this.id;
        prodHTAmt=$("#"+hamtid).val();
        if(prodHTAmt == "null" || prodHTAmt == null || prodHTAmt == "")
        {
          prodHTAmt = "NO"
      }
      prodHTaxAmt=prodHTaxAmt+prodHTAmt+"__";
    //alert("product quantity: "+prodQty);  
    
});

    $("#appendProductName").val(prodName);
    //$("#appendSmsType").val(smsType);
    $("#appendProductQty").val(prodQty);
    $("#appendProductRate").val(prodRate);
    $("#appendTotalAmount").val(totalAmount);
    $("#appendPayMode").val(payMode);
    $("#appendCheckNumber").val(checkNumber);
    $("#appendAmountReceived").val(amtReceived);
    $("#appendCheckNumber").val(checkNumber);
    $("#appendAmountReceived").val(amtReceived);
    $("#appendSendTax").val(prodTax);
    $("#appendTaxAmount").val(prodTaxAmt);
    $("#appendhidden_total_amount").val(prodHTaxAmt);

    
    
    $("#ajax-loaderLead").show();
    $("#saveClientDetails").hide();
        //$("#add_more_div").hide();
        $("#close_model_div").hide();
        $.ajax({
            url:"ajax_service/view_leads_ajax.php",
            type: "POST",
            //dataType:"json",
            data:$("#saveClientData").serialize()+"&action=saveClientDetails&action2=delete",
            success:function(data){
                //console.log(data);
                
                alert(data);
               //return false;
                //alert();
                $('#close_model_div').prop('disabled', false);
                //$('#add_more_div').prop('disabled', false);
                $('#saveClientDetails').prop('disabled', false);

                if(data == 1){
                  
                   
                  $("#ajax-loaderLead").hide();
                  $("#saveClientDetails").show();
                  $("#close_model_div").show();
                  //$("#add_more_div").show();
                  alert("Data insert successfully!");
                  location.reload();
              }
              else
              {
                  alert("Data not insert");
              }
              
          }
      });
    });

   // alert();
   
   $('#product_quantity').on("change",function() {
       $("#product_rate").val('');
       $("#total_amount").val('');
   });
   
   
   var cnt=2;
   var pnmct=1;
   $("#add_more_div").click(function(){

    var pname = $("#product_name"+pnmct).val();
    var pqty = $("#product_quantity"+pnmct).val();
    var prate = $("#product_rate"+pnmct).val();
    var pmode = $("#paymode"+pnmct).val();

    if(pname == "" || pname == '' || pname == null){
      alert("Please fill previous details.");
      $("#product_name"+pnmct).focus();
      return false;
  }
  if(pqty == "" || pqty == '' || pqty == null){
      alert("Please fill previous details.");
      $("#product_quantity"+pnmct).focus();
      return false;
  }
  if(prate == "" || prate == '' || prate == null){
      alert("Please fill previous details.");
      $("#product_rate"+pnmct).focus();
      return false;
  }
  if(pmode == "" || pmode == '' || pmode == null){
      alert("Please fill previous details.");
      $("#paymode"+pnmct).focus();
      return false;
  }


  $("#product_div_main").append('<div id="divRow'+cnt+'"><hr><label>'+cnt+'.Product Details</label> <label style="float:right;font-size: 20px;"><a href="javascript:void()" style="color:red;text-decoration: none;" onclick="removeDiv('+cnt+');">X</a></label><div class="form-group">'
      +'<label class="col-sm-2 control-label">Product</label>'
      +'<div class="col-sm-10">'
      +'<select  class="form-control" id="product_name'+cnt+'" onchange="changeQty(this);" name="product_name"  required>'
      +' <option value="" selected disabled >--------Select Product---------</option>'
      
      +' </select>'
      +' </div>'
      +'</div>'
      +'<div class="form-group" >'
      +' <label class="col-sm-2 control-label">Quantity</label>'
      +'<div class="col-sm-10">'
      +' <select  class="form-control" id="product_quantity'+cnt+'" name="product_quantity" onchange="changeRate(this);" required>'
      +' <option value="" selected disabled >--------Select Quantity---------</option>'
      
      +'</select>'
      +'</div>'
      +'</div>'

      +'<div class="form-group" style="display:none;" id="div_code_validity'+cnt+'">'
      +' <label class="col-sm-2 control-label">Validity</label>'
      +' <div class="col-sm-10">'
      +'   <select  class="form-control" id="code_validity'+cnt+'" name="code_validity" required>'
      +'   <option value="" selected disabled >--------Select Month---------</option>'
      +'<option value="1 Month" >1 Month</option>'
      +'<option value="3 Months" >3 Months</option>'
      +'<option value="6 Months" >6 Months</option>'
      +'<option value="12 Months" >12 Months</option>'
      +' </select>'
      +' </div>'
      +'</div>'

      +'<div class="form-group has-success">'
      +'<label class="col-sm-2 control-label">Rate</label>'
      +' <div class="col-sm-10">'
      +' <input type="text" class="form-control" id="product_rate'+cnt+'" name="product_rate" onkeyup="calculaterate(this);" placeholder="Placeholder Text" required>'
      +'<span class="help-block"></span>'
      +'</div>'
      +'</div>'

      +'<div class="form-group has-success">'
      +' <label class="col-sm-2 control-label"><span style="margin: 0 10px 0 0;">Tax</span> <input type="checkbox" style="border: 2px solid #16A085;width: 15px;height: 15px;" id="send_tax'+cnt+'" name="send_tax" onclick="disableRadio(this);" value="N"></label>'
      +'<div class="col-sm-10">'
      +' <input type="text" class="form-control" id="tax'+cnt+'" name="tax" value="14.00"  onkeyup="calculatetax(this);" disabled required>'
      +'<input type="hidden" id="tax_amount'+cnt+'" name="tax_amount"  disabled >'
      +' <span class="help-block"></span>'
      +' </div>'
      +' </div>'
      
      +' <div class="form-group has-success">'
      +'<label class="col-sm-2 control-label">Total Amount</label>'
      +'<div class="col-sm-10">'
      +'<input type="hidden" id="hidden_total_amount'+cnt+'" name="hidden_total_amount" placeholder="Placeholder Text" readonly required>'
      +'<input type="text" class="form-control" id="total_amount'+cnt+'" name="total_amount" placeholder="0" readonly required>'
      +'<span class="help-block"></span>'
      +' </div>'
      +'</div>'
      +'<div class="form-group">'
      +'<label class="col-sm-2 control-label">Payment Mode</label>'
      +'<div class="col-sm-10">'
      +'<select  class="form-control" id="paymode'+cnt+'" name="paymode" onchange="checkNumber(this);" required>'
      +'<option value="" selected disabled >--------Select Payment Mode---------</option>'
      +'<option value="Cash" >Cash</option>'
      +'<option value="Check" >Check</option>'
      +'<option value="Online" >Online</option>'
      +'</select>'
      +'</div>'
      +'</div>'
      +'<div class="form-group has-success" style="display:none;" id="check_number_div'+cnt+'">'
      +'<label class="col-sm-2 control-label">Check Number</label>'
      +'<div class="col-sm-10">'
      +'<input type="text" class="form-control" id="check_number'+cnt+'" name="check_number" placeholder="Placeholder Text" required>'
      +'<span class="help-block"></span>'
      +'</div>'
      +'</div>'
      +'<div class="form-group has-success">'
      +'<label class="col-sm-2 control-label">Received By</label>'
      +'<div class="col-sm-10">'
      +'<input type="text" class="form-control" id="amount_received'+cnt+'" name="amount_received" placeholder="Placeholder Text" required>'
      +'<span class="help-block"></span>'
      +'</div>'
      +'</div>');

$.ajax({
  url:"ajax_service.php",
  data:"action=getProductList",
  contentType: "application/json",
  async: false,
  dataType: 'json',
  success:function(data){
    console.log(data);
    var p = cnt;
    
            //alert(pnm);
            $("#product_name"+cnt).find('option').remove();
            $("#product_name"+cnt).append("<option value='' selected disabled >--------Select Product---------</option>");
            for(var n=0;n<data.length;n++)
            {
             
                $("#product_name"+cnt).append("<option id='"+data[n].product_name+""+cnt+"' value='"+data[n].product_name+"'>"+data[n].product_name+"</option>");
                
            }
            
        }
    });


cnt++;
pnmct++;

});




$("#viewLeads").click(function(){
    var user_id = $("#assigned_user_id").val();
       // alert(user_id);
       $.ajax({
        url:"ajax_service/view_leads_ajax.php",                
        data:"user_id="+user_id+"&action=viewLeads",
        dataType:"json",
        success:function(data){
               // alert(data);
                //console.log(data);
                $("#example-table").find("tr").remove();
                if(data == "no"){
                  var tbody="";
                  var tr="<tr>";
                  var td0="<td colspan='6' align='center'>No Records Found</td></tr>";
                  tbody = tbody+tr+td0;
                  $('#example-table').append('<thead><th>Sr No.</th><th>Lead Open</th><th>company Name</th><th>Contact Number</th><th>Contact Person</th><th>Created Date</th></thead>'+tbody);
              }else{

                var n=1;
                var tbody="";
                for(var i=0;i<data.length;i++)
                {
                    
                    var tr="<tr id='row"+n+"'>";
                    var td0="<td>"+n+"</td>";
                    var td1="<td>"+data[i].fname+" "+data[i].lname+"</td>";
                    var td2="<td>"+data[i].company_name+"</td>";
                    var td3="<td>"+data[i].contact_number+"</td>";
                    var td4="<td>"+data[i].contact_person+"</td>";
                    var td5="<td>"+data[i].c_date+"</td></tr>";
                    tbody = tbody+tr+td0+td1+td2+td3+td4+td5;
                           //$("#example-table").append(tr+td0+td1+td2); 
                           n++;
                           
                       }   
                       
                       $('#example-table').append('<thead><th>Sr No.</th><th>Lead Open</th><th>company Name</th><th>Contact Number</th><th>Contact Person</th><th>Created Date</th></thead>'+tbody);
                       

                   }            
               }
           });
   });


$("#updateReassignDetails").click(function(){
  var client_id = $("#reassign_client_id").val();
  var user_id = $("#reassign_user_id").val();
  var reminder_id = $("#reassignLead_reminderId").val();

  $.ajax({
      url:"ajax_service/view_leads_ajax.php",                
      data:"client_id="+client_id+"&user_id="+user_id+"&reminder_id="+reminder_id+"&action=updateReassignDetails&action2=delete",
          //dataType:"json",
          success:function(data){

            if(data == 1)
            {
              alert("Lead Re-Assigned Successfully");
              location.reload();
          }else{
              alert("Lead Not Re-Assigned");
          }
      }
  });

});

});




function changeQty(idd) {
 
       //alert("id: "+idd.id);
       var id = idd.id;
      // alert(id);
      var product_name = $("#"+id).val();
      var encoded = encodeURIComponent(product_name);
      var res = id.substring(12, id.length);

      $("#total_amount"+res).val('');
      $("#product_rate"+res).val('');
      $('#tax_amount'+res).val('');


      if(product_name == "ShortCode" || product_name == "LongCode"){

          $("#div_code_validity"+res).show();

      }else{

        $("#div_code_validity"+res).hide();

    }
    
    
    $.ajax({
        url:"ajax_service/view_leads_ajax.php",
        data:"product_name="+encoded+"&action=getProductQty",              
       // dataType:'json',
       success:function(data){
          
          $("#product_quantity"+res).html(data);
      }
  });

}

function changeRate(idd) {
   var id = idd.id;    
   var res = id.substring(16, id.length);
   $("#total_amount"+res).val('');
   $("#product_rate"+res).val('');
   $("#tax_amount"+res).val('');
}


function calculaterate(idd)
{
   
    var id = idd.id;
    var res = id.substring(12, id.length);

    var str = $('#'+id).val();
    
    
    var checked = $("#send_tax"+res).attr('checked');
    
    var r = $('#product_rate'+res).val();
    var q = $('#product_quantity'+res).val();

    var t = (r * q);
    $('#total_amount'+res).val(t);
    $('#hidden_total_amount'+res).val(t);

    if(checked){ 
      
      //$("#tax_amount"+res).val('');
      var tax = $("#tax"+res).val();
      var total_amt = $('#hidden_total_amount'+res).val();

      var tax_amt = total_amt * tax / 100;
      $('#tax_amount'+res).val(tax_amt);
      var grand_total =  parseInt(total_amt) + parseInt(tax_amt);
      $('#hidden_total_amount'+res).val(t);
      $('#total_amount'+res).val(grand_total);

  }
  
}

function removeDiv(idd)
{
    
    $("#divRow"+idd).remove();
    
}

function checkNumber(idd)
{
    //alert("id: "+idd.id);
    var id = idd.id;
      //alert(id.length);
      var res = id.substring(7, id.length);
      //alert(res);
      if(idd.value == "Cheque")
      {
        $("#check_number_div"+res).show();
        $("#bank_name_div"+res).show();
    }
    else
    {
        $("#check_number_div"+res).hide();
        $("#bank_name_div"+res).hide();
    }
}

function setValue123(idd){
    
    var id = idd.id;
    //alert(id);
    var v = $("#"+id).val();
       // alert(v);
       $("#assigned_user_id").val(v);
       if(id == "bde_name"){
        $("#bdm_name").val('');
    }

    if(id == "bdm_name"){
        $("#bde_name").val('');
    }
    
}

function disableRadio(idd){
  var id = idd.id;
  //alert("id: "+id+" and Length: "+id.length);
  var res = id.substring(8, id.length);
  //alert(res);

  var checked = $("#"+id).attr('checked');
  
  if(checked){ 
    $("#"+id).attr('checked', false);
     //alert("in false");
     $("#"+id).val('N');
     $("#tax_amount"+res).val('');
     var htotal_amt = $('#hidden_total_amount'+res).val();
     
     if(htotal_amt == "" || htotal_amt == '' || htotal_amt == null){
      $("#total_amount"+res).val(0);

  }else{
      $("#total_amount"+res).val(htotal_amt);
  }
  
}
else{ 

  $("#"+id).attr('checked', true);
       //alert("in true");
       $("#"+id).val('Y');
       var total_amt = $('#hidden_total_amount'+res).val();
       
       if(total_amt == "" || total_amt == '' || total_amt == null){

        $("#total_amount"+res).val(0);
        $("#tax_amount"+res).val('');

    }else{

        var tax = $("#tax"+res).val();
        
        var tax_amt = total_amt * tax / 100;
        $('#tax_amount'+res).val(tax_amt);
        var grand_total =  parseInt(total_amt) + parseInt(tax_amt);
        $('#total_amount'+res).val(grand_total);

    }

    
      //document.getElementById("tax_amount"+res).disabled = false;
      
  }

}

function calculatetax(idd){
  var id = idd.id;
  //alert("id: "+id+" and Length: "+id.length);

  var res = id.substring(3, id.length);

  var tax = $("#tax"+res).val();
  var total_amt = $('#hidden_total_amount'+res).val();

  var tax_amt = total_amt * tax / 100;


  
  var grand_total =  parseInt(total_amt) + parseInt(tax_amt);
  //alert(grand_total);
 //var grand_total = 1 + 2;
  //alert("Tax: "+tax+"total_amt: "+total_amt+"tax_amt: "+tax_amt+"grand_total: "+grand_total);
  $('#total_amount'+res).val(grand_total);

}

function getReminder(){
    var sdateSearch = $("#sdateSearch").val();
    var edateSearch = $("#edateSearch").val();
    //alert(sdateSearch);
    $.ajax({
        url:"ajax_service/reminder_ajax.php",
        data:"sdateSearch="+sdateSearch+"&edateSearch="+edateSearch+"&action=getReminder",
        success:function(data){
            //alert(data);
            //console.log(data);
            $("#example-table").find("tr").remove();
            $('#example-table').append(data);
        }
    });
}