$(document).ready(function(){

        $("#updateRejectDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateRejectData").serialize()+"&action=updateRejectDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup2").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });

        $("#updateDelayDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateDelayData").serialize()+"&action=updateDelayDetails&r_type=Lead",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup3").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });


});

function accept(sr,cid,clid)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();
    var eid = $("#eid"+sr).html();
    var cadd = $("#c_add"+sr).html();
    var cnm = $("#cno_person"+sr).html();
    //alert(cadd);

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    $("#companyName").val(cname);
    $("#contactNumber").val(cno);
    $("#accept_cust_id").val(cid);
    $("#accept_client_id").val(clid);
    $("#accept_email_id").val(eid);
    $("#accept_client_add").val(cadd);
    $("#accept_client_name").val(cnm);
    $("#accept_company_name").val(cname);
    

}

function reject(sr,cid,clid)
{
    $("#reject_client_id").val(clid);
    $("#reject_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup2").show();

}

function delay(sr,cid,clid)
{
    $("#delay_client_id").val(clid);
    $("#delay_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup3").show();

}


$(document).ready(function(){

 // sessionStorage.clear();

 $(".close_client_model").click(function(){
    $('#saveClientData').trigger("reset");
  });

    $("#saveClientDetails").click(function(){
        alert();

       // alert("in getData");
        var prodN="";
        var prodName="";
        //var smsT="";
       // var smsType="";
        var prodQ="";
        var prodQty="";
        var prodR="";
        var prodRate="";
        var totalA="";
        var totalAmount="";
        var payM="";
        var payMode="";
        var checkN="";
        var checkNumber="";
        var amtR="";
        var amtReceived="";
        
        //var check=true;
        $("select[name=product_name]").each(function(){
          //alert();
          var prodid=this.id;
          prodN=$("#"+prodid).val();
          prodName=prodName+prodN+"__";
         // alert("product name: "+prodName);  
        });
        
         
        $("select[name=product_quantity]").each(function(){
          var qtyid=this.id;
          prodQ=$("#"+qtyid).val();
          prodQty=prodQty+prodQ+"__";
          //alert("product quantity: "+prodQty);  
          
        });
        
        $("input[name=product_rate]").each(function(){
          var rateid=this.id;
          prodR=$("#"+rateid).val();
          prodRate=prodRate+prodR+"__";
          //alert("product prodRate: "+prodRate);  
          
        });
        
        $("input[name=total_amount]").each(function(){
          var totalid=this.id;
          totalA=$("#"+totalid).val();
          totalAmount=totalAmount+totalA+"__";
         // alert("product totalAmount: "+totalAmount);  
          
        });

         $("select[name=paymode]").each(function(){
          var payid=this.id;
          payM=$("#"+payid).val();
          if(payM == "null" || payM == null || payM == "")
            {
              payM = "NO"
            }
           payMode=payMode+payM+"__";
          //alert("product quantity: "+prodQty);  
          
        });

        $("input[name=check_number]").each(function(){
        var checkid=this.id;
        checkN=$("#"+checkid).val();
        if(checkN == "null" || checkN == null || checkN == "")
            {
              checkN = "NO"
            }
        checkNumber=checkNumber+checkN+"__";
        //alert("product quantity: "+prodQty);  
          
        });

         $("input[name=amount_received]").each(function(){
        var amtRid=this.id;
        amtR=$("#"+amtRid).val();
        if(amtR == "null" || amtR == null || amtR == "")
            {

            amtR = "<?php echo  $result_get_name[0]['fname']." ".$result_get_name[0]['lname'] ?>";

              
            }
        amtReceived=amtReceived+amtR+"__";
        //alert("product quantity: "+prodQty);  
          
        });

        $("#appendProductName").val(prodName);
        //$("#appendSmsType").val(smsType);
        $("#appendProductQty").val(prodQty);
        $("#appendProductRate").val(prodRate);
        $("#appendTotalAmount").val(totalAmount);
        $("#appendPayMode").val(payMode);
        $("#appendCheckNumber").val(checkNumber);
        $("#appendAmountReceived").val(amtReceived);

        
        

            $.ajax({
                url:"ajax_service/view_leads_ajax.php",
                type: "POST",
                dataType:"json",
                data:$("#saveClientData").serialize()+"&action=saveClientDetails",
                success:function(data){
                    console.log(data.success);
                   // alert(data.success);
                    //alert();
                    $('#close_model_div').prop('disabled', false);
                    $('#add_more_div').prop('disabled', false);
                    $('#saveClientDetails').prop('disabled', false);

                    if(data.success == "yes"){
                      
                      $('#close_model_div').prop('disabled', true);
                      $('#add_more_div').prop('disabled', true);
                      $('#saveClientDetails').prop('disabled', true);
                      alert("Data insert successfully!");
                      location.reload();
                    }
                    else
                    {
                      alert("Data not insert");
                    }
                    
                }
            });
    });

   // alert();
            
    $('#product_quantity').on("change",function() {
         $("#product_rate").val('');
         $("#total_amount").val('');
    });
    
   
    var cnt=2;
    var pnmct=1;
    $("#add_more_div").click(function(){

     
        $("#product_div_main").append('<div id="divRow'+cnt+'"><hr><label>'+cnt+'.Product Details</label> <label style="float:right;font-size: 20px;"><a href="javascript:void()" style="color:red;text-decoration: none;" onclick="removeDiv('+cnt+');">X</a></label><div class="form-group">'
                                                  +'<label class="col-sm-2 control-label">Product</label>'
                                                  +'<div class="col-sm-10">'
                                                      +'<select  class="form-control" id="product_name'+cnt+'" onchange="changeQty(this);" name="product_name"  required>'
                                                        +' <option value="" selected disabled >--------Select Product---------</option>'
                                                       
                                                     +' </select>'
                                                 +' </div>'
                                              +'</div>'
                                               +'<div class="form-group" >'
                                                 +' <label class="col-sm-2 control-label">Quantity</label>'
                                                  +'<div class="col-sm-10">'
                                                     +' <select  class="form-control" id="product_quantity'+cnt+'" name="product_quantity" required>'
                                                        +' <option value="" selected disabled >--------Select Quantity---------</option>'
                                                        
                                                      +'</select>'
                                                  +'</div>'
                                              +'</div>'

                                              +'<div class="form-group has-success">'
                                                  +'<label class="col-sm-2 control-label">Rate</label>'
                                                 +' <div class="col-sm-10">'
                                                     +' <input type="text" class="form-control" id="product_rate'+cnt+'" name="product_rate" onkeyup="calculaterate(this);" placeholder="Placeholder Text" required>'
                                                      +'<span class="help-block"></span>'
                                                  +'</div>'
                                              +'</div>'
                                             
                                             +' <div class="form-group has-success">'
                                                  +'<label class="col-sm-2 control-label">Total Amount</label>'
                                                  +'<div class="col-sm-10">'
                                                      +'<input type="text" class="form-control" id="total_amount'+cnt+'" name="total_amount" placeholder="Placeholder Text" readonly required>'
                                                      +'<span class="help-block"></span>'
                                                 +' </div>'
                                              +'</div>'
                                              +'<div class="form-group">'
                                                +'<label class="col-sm-2 control-label">Payment Mode</label>'
                                                +'<div class="col-sm-10">'
                                                    +'<select  class="form-control" id="paymode'+cnt+'" name="paymode" onchange="checkNumber(this);" required>'
                                                        +'<option value="" selected disabled >--------Select Payment Mode---------</option>'
                                                        +'<option value="Cash" >Cash</option>'
                                                        +'<option value="Check" >Check</option>'
                                                        +'<option value="Online" >Online</option>'
                                                    +'</select>'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="form-group has-success" style="display:none;" id="check_number_div'+cnt+'">'
                                                +'<label class="col-sm-2 control-label">Check Number</label>'
                                                +'<div class="col-sm-10">'
                                                    +'<input type="text" class="form-control" id="check_number'+cnt+'" name="check_number" placeholder="Placeholder Text" required>'
                                                    +'<span class="help-block"></span>'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="form-group has-success">'
                                            +'<label class="col-sm-2 control-label">Received By</label>'
                                            +'<div class="col-sm-10">'
                                                +'<input type="text" class="form-control" id="amount_received'+cnt+'" name="amount_received" placeholder="Placeholder Text" required>'
                                                +'<span class="help-block"></span>'
                                            +'</div>'
                                        +'</div>');

      $.ajax({
        url:"ajax_service.php",
        data:"action=getProductList",
        contentType: "application/json",
        async: false,
        dataType: 'json',
        success:function(data){
          console.log(data);
          var p = cnt;
          
          //alert(pnm);
          $("#product_name"+cnt).find('option').remove();
          $("#product_name"+cnt).append("<option value='' selected disabled >--------Select Product---------</option>");
          for(var n=0;n<data.length;n++)
          {
           // var pct = n+1;
            //var pnm = $("#product_name"+pct).val();
           // var pnm = sessionStorage.getItem('prod_name'+pct)
          //  alert(pnm);
          //  var opt = pnm+cnt;
          
              $("#product_name"+cnt).append("<option id='"+data[n].product_name+""+cnt+"' value='"+data[n].product_name+"'>"+data[n].product_name+"</option>");
              

           // if(pnm == data[n].product_name)
          //  {
          //     alert(opt);
                //$("#"+opt).attr("disabled",true);
          //  }
            
          }
          //if (pnm == ) {};
        }
      });
            

     cnt++;
     pnmct++;

      });

});




function changeQty(idd) {
       
       //alert("id: "+idd.id);
       var id = idd.id;
      //alert(id.length);
      var res = id.substring(12, id.length);
      //alert(res);
       $("#product_rate"+res).val('');
       $("#total_amount"+res).val('');
       //$("#div_sms_type"+res).hide();

       var n = $('#product_name'+res).val();

       // sessionStorage.setItem('prod_name'+res, n);

      // alert("Session variable:"+sessionStorage.getItem('prod_name'+res));

        $('#product_quantity'+res).find('option').remove();
        if(n=="SMS - Promotional" || n=="SMS - Transactional")
        {
            //$("#div_sms_type"+res).show();
            $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option value='1000'>1000</option><option value='10000'>10,000</option>"
                +"<option value='20000'>20,000</option><option value='50000'>50,000</option><option value='100000'>1,000,00</option><option value='200000'>2,000,00</option>"
                +"<option value='500000'>5,000,00</option><option value='1000000'>10,000,00</option>");
        }
        if(n=="Email")
        {
            
            $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option value='100000'>1 Lac</option>"
                +"<option value='200000'>2 Lac</option><option value='500000'>5 Lac</option><option value='1000000'>10 Lac</option><option value='2000000'>20 Lac</option>");
        }
        if(n=="WhatsApp")
        {
            
             $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option value='100000'>1 Lac</option>"
                +"<option value='200000'>2 Lac</option><option value='500000'>5 Lac</option><option value='1000000'>10 Lac</option><option value='2000000'>20 Lac</option>");
        }
        if(n=="Website")
        {
            
            $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option>1 Page</option>"
                +"<option value='5'>5 Pages</option><option  value='10'>10 Pages</option><option  value='20'>20 Pages</option><option  value='30'>30 Pages</option>"
                +"<option  value='40'>40 Pages</option><option  value='50'>50 Pages</option><option  value='100'>100 Pages</option>");
        }
        if(n=="ShortCode")
        {
            
            $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option>1 Year</option>");
        }
        if(n=="Longcode")
        {
            
            $('#product_quantity'+res).append("<option selected disabled>----Select Quantity----</option><option>1 Year</option>");
        }
    }

   /* function changeQty123(idd)
    {
      alert(idd.id);
    }*/

function calculaterate(idd)
{
 // alert("id: "+idd.id);
       var id = idd.id;
     // alert(id.length);
    var res = id.substring(12, id.length);
    var n = $('#product_name'+res).val();
    var r = $('#product_rate'+res).val();
    var q = $('#product_quantity'+res).val();

    var comp_product = product_name.replace(/ /g,'');
    comp_product = comp_product.toLowerCase();

   if(comp_product == "shortcode" || comp_product == "longcode"){
        
        $('#total_amount'+res).val(r);
    }

    else
    {
        var t = (r * q);
        $('#total_amount'+res).val(t);
    }
    
 }

 function removeDiv(idd)
 {
    
    $("#divRow"+idd).remove();
    //var pname = $("product_name"+idd).val();
    //sessionStorage.setItem('prod_name'+idd, null);
 }

 function checkNumber(idd)
 {
    //alert("id: "+idd.id);
       var id = idd.id;
      //alert(id.length);
      var res = id.substring(7, id.length);
      //alert(res);
    if(idd.value == "Check")
    {
        $("#check_number_div"+res).show();
    }
    else
    {
        $("#check_number_div"+res).hide();
    }
 }