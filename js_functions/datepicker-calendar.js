$(function() {
        $('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            beforeShow: function(input, inst) {
                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(datestr.length - 4, datestr.length);
                    month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
            }
        });
         $('#startDate1').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'MM yy',
            onClose: function(dateText, inst) {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            beforeShow: function(input, inst) {
                if ((datestr = $(this).val()).length > 0) {
                    year = datestr.substring(datestr.length - 4, datestr.length);
                    month = jQuery.inArray(datestr.substring(0, datestr.length - 5), $(this).datepicker('option', 'monthNames'));
                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 1));
                    $(this).datepicker('setDate', new Date(year, month, 1));
                }
            }
        });
    });

$(document).ready(function(){

    var dateObj = new Date();
    var m = dateObj.getUTCMonth() + 1; //months from 1-12
    var y = dateObj.getUTCFullYear();

    var newdate = m + "-" + y ;
    //alert(newdate);

    var res = newdate.split("-");
    var month = res[0];
    var year = res[1];
    var mon="";

    if(month == 1)
    {
        mon = "January";
    }else if(month == 2){
        mon = "February";
    }else if(month == 3){
        mon = "March";
    }else if(month == 4){
        mon = "April";
    }else if(month == 5){
        mon = "May";
    }else if(month == 6){
        mon = "June";
    }else if(month == 7){
        mon = "July";
    }else if(month == 8){
        mon = "August";
    }else if(month == 9){
        mon = "September";
    }else if(month == 10){
        mon = "October";
    }else if(month == 11){
        mon = "November";
    }else if(month == 12){
        mon = "December";
    }
    
    var current_month_year = mon+" "+year;
    $("#startDate1").val(current_month_year);
    $("#datepicker").val(current_month_year);

});
    /*function setDate(idd){
        var id = idd.id;
        var date_time = $("#"+id).val();
        var res = date_time.split(" ");
        var month = res[0];
        var year = res[1];
        var mon="";

        if(month == "January")
        {
            mon = 1;
        }else if(month == "February"){
            mon = 2;
        }else if(month == "March"){
            mon = 3;
        }else if(month == "April"){
            mon = 4;
        }else if(month == "May"){
            mon = 5;
        }else if(month == "June"){
            mon = 6;
        }else if(month == "July"){
            mon = 7;
        }else if(month == "August"){
            mon = 8;
        }else if(month == "September"){
            mon = 9;
        }else if(month == "October"){
            mon = 10;
        }else if(month == "November"){
            mon = 11;
        }else if(month == "December"){
            mon = 12;
        }
        
        var start_date = year+"-"+mon;
        $("#datepicker").val(start_date);
        //alert(start_date);

    }*/