function json_option(json_data)
{
	
      var options ="<option value=''>----Select----</option>";
     for(var i=0;i<json_data.length;i++)
     {
     	opt="<option value='"+json_data[i].user_id+"'>"+json_data[i].fname+" "+json_data[i].lname+"</option>"
     	options = options+opt;
     }
	return options;

}

function json_option_role(json_data)
{
     
      var options ="<option value=''>----Select User----</option>";
     for(var i=0;i<json_data.length;i++)
     {
          var role = "";
          if(json_data[i].role == 1){
               role = "CEO";
          }else if(json_data[i].role == 2){
               role = "BDM";
          }else if(json_data[i].role == 3){
               role = "BDE";
          }else if(json_data[i].role == 4){
               role = "Telle Caller";
          }else if(json_data[i].role == 5){
               role = "RM";
          }
          opt="<option value='"+json_data[i].user_id+"'>"+json_data[i].fname+" "+json_data[i].lname+" - "+role+"</option>"
          options = options+opt;
     }
     return options;

}

function product_quantity_option(json_data)
{
	var options ="<option value ='' selected disabled>---Select---</option>";
     for(var i=0;i<json_data.length;i++)
     {
     	opt="<option value='"+json_data[i].user_id+"'>"+json_data[i].fname+" "+json_data[i].lname+"</option>"
     	options = options+opt;
     }
	return options;
}