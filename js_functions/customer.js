
   /*Add, Reject and Delay Customer */
$(document).ready(function(){

$("#addClientDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#addClientData").serialize()+"&action=addClientDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Client data inserted.");
                        $("#popup1").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });


        $("#updateRejectDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateRejectData").serialize()+"&action=updateRejectDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup2").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });

        $("#updateDelayDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateDelayData").serialize()+"&action=updateDelayDetails&r_type=Call",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup3").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });


});


function accept(sr,cid)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    $("#companyName").val(cname);
    $("#contactNumber").val(cno);
    $("#accept_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup1").show();

}

function reject(sr,cid)
{
    
    $("#reject_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup2").show();

}

function delay(sr,cid)
{
    
    $("#delay_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup3").show();

}