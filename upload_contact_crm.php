<?php
include('header_sidebar_crm.php');
$root = $_SERVER['DOCUMENT_ROOT'];
$path = "/crm/uploaded/Sample_Upload_CSV_Format.csv";
?>

<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Upload Customer
						<small>Form </small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
						</li>
						<li class="active">Upload Customer</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Customer</h4>
						</div>
						<div class="portlet-widgets">
							<a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
						</div>
						<div style="float:right;margin: -20px 15px 0 0;">
							<h1 align="center"><a href="<?php echo $path;?>" class="btn btn-green"> Sample Download</a></h1>
						</div>
						<div class="clearfix"></div>
					</div>
					<div id="validationStates" class="panel-collapse collapse in">
						<div class="portlet-body">
							<form class="form-horizontal" role="form"  method= "post" enctype="multipart/form-data" action="">
								<div class="form-group has-success">
									<label class="col-sm-2 control-label">Upload CSV</label>
									<div class="col-sm-10">
										<input type="file"  class="form-control" id="customer_csv" name="customer_csv" required>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label"></label>
									<div class="col-sm-10">
										<div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
										<h1 align="center"><input type="submit" class="btn btn-default" id="submit" name="submit" value="Submit" ></h1>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
		$("#submit").hide();
	});

	$("#customer_csv").change(function () {
		var fileExtension = ['csv', 'CSV'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			alert("Only formats are allowed : "+fileExtension.join(', '));
			$("#submit").hide();
		}else{
			$("#submit").show();
		}
	});
</script>


<?php
$con = new functions();
$conn1 = new mysqli("localhost", "root", "hmara@123", "mobisoft_crm");

if(isset($_FILES) && !empty($_FILES)){

	if(is_array($_FILES)) {
		if(is_uploaded_file($_FILES['customer_csv']['tmp_name'])) {
			$sourcePath = $_FILES['customer_csv']['tmp_name'];
			$user_id = $_SESSION['user_id'];
			$date = $con->get_datetime();

			$targetPath = "uploaded/contactList".$user_id.".csv";

			$i=0;
			if(move_uploaded_file($sourcePath,$targetPath)){
				$conn1->autocommit(FALSE);
				$file = fopen("uploaded/contactList".$user_id.".csv","r");          
				while (($line = fgetcsv($file)) !== FALSE) {
					$sql = "INSERT INTO customer_details (company_name, contact_number,customer_email,status,reject_in,remark,user_id,c_date,u_date,flag)
					VALUES ('".$line[0]."','".$line[1]."','".$line[2]."','Pending','NA','NA','$user_id','$date','$date',1)";
					$conn1->query($sql);
				}
				if (!$conn1->commit()) {
					print("Error!Transaction commit failed\n");
					exit();
				}
			}
		}
	}
	header("Location:view_customer_crm.php");
	unlink("uploaded/contactList".$user_id.".csv");
}
$conn1->autocommit(TRUE);
?>

<?php
include('footer_crm.php');
?>