 <?php 
$user_id = $_SESSION['user_id'];
 $current_date = date("Y-m-d");

$usersId = "SELECT user_id from user_details where reporting_id =".$_SESSION['user_id'];
$usersIdData = $con->data_select($usersId);
if(!empty($usersIdData)){
    foreach ($usersIdData as $key => $value) {
        # code...
        $arr[$key] = (int)$usersIdData[$key]['user_id'];
    }$key++;
    $arr[$key] = (int) $_SESSION['user_id'];
    $ids = join("','",$arr); 
}else{
    $ids = [0];
} 
  if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7"|| $_SESSION['role'] == "8" || $_SESSION['role'] == "9"){
        $qry = "SELECT cd.*,ud.fname,ud.lname FROM client_details as cd INNER JOIN user_details as ud on ud.user_id = cd.bde_user_id WHERE cd.client_status = 'L' and cd.flag = 1 and DATE(cd.c_date)='".$current_date."' ORDER BY client_id DESC";
        $result = $con->data_select($qry);
        
  }else if($_SESSION['role'] == "3"){ /*BDE*/
    $qry = "SELECT ud.fname,ud.lname, cd.cust_id,cd.client_id,cd.company_name,cd.address,cd.contact_number,cd.contact_person,cd.email_id, cd.address,cd.c_date FROM client_details as cd inner join user_details as ud on cd.lead_user_id = ud.user_id WHERE cd.bde_user_id = '".$user_id."' and cd.client_status = 'L' and cd.flag = 1 and DATE(cd.c_date)='".$current_date."' ORDER BY cd.client_id DESC";
    $result = $con->data_select($qry);
    
    
}else if( $_SESSION['role'] == "2"){
    $qry = "SELECT cd.*,ud.fname,ud.lname FROM client_details as cd INNER JOIN user_details as ud on ud.user_id = cd.bde_user_id WHERE cd.client_status = 'L' and cd.flag = 1 and DATE(cd.c_date)='".$current_date."' and cd.bde_user_id IN('$ids') ORDER BY cd.u_date DESC";
        $result = $con->data_select($qry);
       /* echo $qry;*/
}
   
   /* $sql = "SELECT * from sales_target where s_date ='".$current_date."' order by c_date desc";
    $result = $con->data_select($sql);
*/
    

 ?>


<div class="row">
                    <div class="col-lg-12 col-sm-12">
                    <div class="portlet portlet-default">
                        <div class="portlet-heading">
                            <div class="portlet-title">
                                <h4>Leads</h4>
                            </div>
                        <div class="clearfix"></div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered nowrap table-green" cellspacing="0" width="100%" >
                               <thead>
                                    <tr>
                                        <th>SrNo.</th>
                                        <?php
                                        if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
                                            echo"<th>Lead Open</th>";/*Telle Caller Name*/
                                        }
                                        if($_SESSION['role'] == "4"){
                                            echo"<th>Lead Assigned</th>";/*BDE Name*/
                                        }
                                        if($_SESSION['role'] == "1" ||  $_SESSION['role'] == "5"){
                                            echo"<th>Lead Open By</th>";/*Telle Caller Name*/
                                            echo"<th>Lead Assigned To</th>";/*BDE Name*/
                                        }
                                        ?>
                                        <th>Company Name</th>
                                        <th>Contact Number</th>
                                        <th>Contact Person</th>
                                        <th>Email Id</th>
                                        <th>Address</th>
                                        <th>Created date/time</th>
                                        <?php
                                        #if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
                                        #    echo"
                                        #   <th>Action_For_Lead </th>
                                        #    <th>Re-Assign</th>
                                        #    <th>Quotation</th>
                                        #    ";
                                        #}
                                        ?>
                                    </tr>
                                </thead>
                                <tfoot>
                                     <tr>
                                       <!--  <th>SrNo.</th> -->
                                        <?php
                                        #if($_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
                                        #    echo"<th>Lead Open</th>";/*Telle Caller Name*/
                                        #}
                                        #if($_SESSION['role'] == "4"){
                                        #    echo"<th>Lead Assigned</th>";/*BDE Name*/
                                        #}
                                        #if($_SESSION['role'] == "1" ||  $_SESSION['role'] == "5"){
                                        #    echo"<th>Lead Open By</th>";/*Telle Caller Name*/
                                        #    echo"<th>Lead Assigned To</th>";/*BDE Name*/
                                        #}
                                        ?>
                                       <!--  <th>Company Name</th>
                                        <th>Contact Number</th>
                                        <th>Contact Person</th>
                                        <th>Email Id</th>
                                        <th>Address</th>
                                        <th>Created date/time</th> -->
                                       
                                    </tr>
                                </tfoot>
                               <tbody id="lead_data">
                                    <?php
                                    if($result != 'no'){
                                        foreach ($result as $key => $value) {
                                            $sr = $key + 1;
                                            echo "<tr>";
                                            echo "<td>".$sr."</td>";
                                            if( $_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
                                                echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";/*Lead Open Name*/
                                            }
                                            if($_SESSION['role'] == "4"){
                                                echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";/*BDE Name*/
                                            }
                                            if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
                                                $get_user="SELECT fname,lname FROM user_details WHERE user_id = '".$result[$key]['lead_user_id']."' ";
                                                $get_user_result = $con->data_select($get_user);
                                                echo "<td>".$get_user_result[0]['fname']." ".$get_user_result[0]['lname']."</td>";

                                                $get_user1="SELECT fname,lname FROM user_details WHERE user_id = '".$result[$key]['bde_user_id']."' ";
                                                $get_user_result1 = $con->data_select($get_user1);
                                                echo "<td>".$get_user_result1[0]['fname']." ".$get_user_result1[0]['lname']."</td>";
                                            }
                                            echo "<td id='cnm".$sr."'>".$result[$key]['company_name']."</td>";
                                            echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
                                            echo "<td id='cno_person".$sr."'>".$result[$key]['contact_person']."</td>";
                                            echo "<td id='eid".$sr."'>".$result[$key]['email_id']."</td>";
                                            echo "<td id='c_add".$sr."'>".$result[$key]['address']."</td>";
                                            echo "<td>".$result[$key]['c_date']."</td>";
                                            if($_SESSION['role'] == "2#" || $_SESSION['role'] == "3#"){
                                                echo"<td align='center'><div class='btn-group btn-group-sm'>
                                                <a href='#'  data-toggle='modal' data-target='#flexModal' title='Make Payment' onclick='return accept(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#flexModal1' title='Reject Lead' onclick='return reject(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#flexModal2' title='Set Reminder' onclick='return delay(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
                                                </div></td>";
                                                echo"<td align='center'><a href='#'  data-toggle='modal' data-target='#flexModal3' title='Re-Assign Lead' onclick='return reassign(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'><i class='fa fa-mail-forward btn-green btn-sm'></i></a></td>";
                                                echo "<td align='center'><a title='Send Quotation' href='send_quotation_crm.php?name=".$result[$key]['contact_person']."&contactNumber=".$result[$key]['contact_number']."&companyName=".$result[$key]['company_name']."&emailId=".$result[$key]['email_id']."&address=".$result[$key]['address']."&cust_id=".$result[$key]['cust_id']."&q_type=Lead'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>"; 
                                            }
                                            echo "</tr>";
                                        }
                                    }                                           
                                    ?>

                                </tbody>
                            </table>
                         </div>
                        </div>
                    </div>
                    </div>
                
                <!-- END DASHBOARD -->
</div>