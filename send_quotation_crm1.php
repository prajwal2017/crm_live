<?php
    include('header_sidebar_crm.php');
    //include('class/functions.php');
    $con = new functions();

    $qry1 = "SELECT * FROM products WHERE flag = 1 ";

    $result1 = $con->data_select($qry1);
    /*echo "<pre>";
    echo  $qry;
    print_r($result); 
    exit;*/


       

?>
<!-- java script start -->

<script type="text/javascript">

$(document).ready(function(){

    $("#sendQuotation").click(function(){
    alert();
        $.ajax({
            url:"send_quotation_mail.php",
            type: "POST",
            data:$("#quotationDetails").serialize(),
            success:function(data){
                console.log(data);
            }
        });
    });

   // alert();
            
    $('#product_quantity').on("change",function() {
         $("#product_rate").val('');
         $("#total_amount").val('');
    });
    $('#product_name').on("change",function() {
       //alert();
       $("#product_rate").val('');
       $("#total_amount").val('');
       $("#div_sms_type").hide();
        var n = $('#product_name').val();
        $('#product_quantity').find('option').remove();
        if(n=="SMS")
        {
            $("#div_sms_type").show();
            $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option value='1000'>1000</option><option value='10000'>10,000</option>"
                +"<option value='20000'>20,000</option><option value='50000'>50,000</option><option value='100000'>1,000,00</option><option value='200000'>2,000,00</option>"
                +"<option value='500000'>5,000,00</option><option value='1000000'>10,000,00</option>");
        }
        if(n=="Email")
        {
            
            $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option value='100000'>1 Lac</option>"
                +"<option value='200000'>2 Lac</option><option value='500000'>5 Lac</option><option value='1000000'>10 Lac</option><option value='2000000'>20 Lac</option>");
        }
        if(n=="WhatsApp")
        {
            
             $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option value='100000'>1 Lac</option>"
                +"<option value='200000'>2 Lac</option><option value='500000'>5 Lac</option><option value='1000000'>10 Lac</option><option value='2000000'>20 Lac</option>");
        }
        if(n=="Website")
        {
            
            $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option>1 Page</option>"
                +"<option value='5'>5 Pages</option><option  value='10'>10 Pages</option><option  value='20'>20 Pages</option><option  value='30'>30 Pages</option>"
                +"<option  value='40'>40 Pages</option><option  value='50'>50 Pages</option><option  value='100'>100 Pages</option>");
        }
        if(n=="ShortCode")
        {
            
            $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option>1 Year</option>");
        }
        if(n=="Longcode")
        {
            
            $('#product_quantity').append("<option selected disabled>----Select Quantity----</option><option>1 Year</option>");
        }
    });

 $('#product_rate').on("keyup",function() {
    var n = $('#product_name').val();
    var r = $('#product_rate').val();
    var q = $('#product_quantity').val();

    if(n=="ShortCode" || n=="Longcode")
    {
        
        $('#total_amount').val(r);
    }

    else
    {
        var t = (r * q);
        $('#total_amount').val(t);
    }
    
 });
   

});


</script>
<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Create Quotation
                                <small>Form </small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                                </li>
                                <li class="active">Create Quotation</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <div class="row">

                    <!-- Validation States -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Create Quotation</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationStates" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="quotationDetails" role="form">
                                         <div class="form-group">
                                            <label class="col-sm-2 control-label">Product</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="product_name" name="product_name"  required>
                                                   <option value="" selected disabled >--------Select Product---------</option>
                                                    <?php

                                                    foreach ($result1 as $key => $value) {
                                                        echo "<option value='".$result1[$key]['product_name']."' onchange='changeSelectOption();'>".$result1[$key]['product_name']."</option>";
                                                    }


                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="form-group" style="display:none;" id="div_sms_type">
                                            <label class="col-sm-2 control-label">SMS Type</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="sms_type" name="sms_type" required>
                                                   <option value="" selected disabled >--------Select SMS Type---------</option>
                                                   <option value="Promotional" >Promotional</option>
                                                   <option value="Transactional" >Transactional</option>
                                                </select>
                                            </div>
                                        </div>
                                         <div class="form-group" >
                                            <label class="col-sm-2 control-label">Quantity</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="product_quantity" name="product_quantity" required>
                                                   <option value="" selected disabled >--------Select Quantity---------</option>
                                                  
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Rate</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="product_rate" name="product_rate" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                         <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Contact Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="contactNumber" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                         <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Company name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="compname" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Email Id</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="emailId" placeholder="Placeholder Text" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Total Amount</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="total_amount" name="total_amount" placeholder="Placeholder Text" readonly required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="sendQuotation" class="btn btn-default">Send</button></h1>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                  

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
<?php
 include('footer_crm.php');
?>