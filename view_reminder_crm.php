<?php
include('header_sidebar_crm.php');
error_reporting(E_ALL ^ E_NOTICE);
/*include('class/functions.php');*/
$con = new functions();
$user_id = $_SESSION['user_id'];
$cur_date = date("Y-m-d");

/* $qry = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id WHERE r.user_id= ".$user_id." and r_date = '".$cur_date."'";*/
$qry = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id WHERE r.user_id= ".$user_id;
/*$qry = "select * from customer_details cd inner join reminder r on cd.cust_id = r.cust_id inner join user_details ud on ud.user_id = r.user_id where r.user_id = ".$user_id." and r_date = '".$cur_date."'";*/
if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
	/*$qry = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id WHERE r_date = '".$cur_date."'";*/
	$qry = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id";
	/*$qry = "select * from customer_details cd inner join reminder r on cd.cust_id = r.cust_id inner join user_details ud on ud.user_id = r.user_id where r_date = '".$cur_date."'";*/
}
$result = $con->data_select($qry);
/*echo "<pre>";
print_r($result);
exit();*/

$qry1 = "SELECT user_id,fname,lname,role FROM user_details WHERE role !=1 AND flag = '1' ";
$result1 = $con->data_select($qry1);

$qry2 = "SELECT * FROM products WHERE flag = 1 ";
$result2 = $con->data_select($qry2);

$reassign_user = "SELECT user_id,fname,lname,role FROM user_details WHERE role !='1' AND user_id != $user_id AND flag = 1 ";
$reassign_user_result = $con->data_select($reassign_user);

?>

<script src="js_functions/reminder.js"></script>

<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>View Reminder 
						<small>Reminder Details</small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
						</li>
						<li class="active">View Reminder</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div>
					<b> From</b> 
					<input type="date" class="form-control" id="sdateSearch" name="sdateSearch" placeholder="Placeholder Text" value="<?php echo $cur_date;?>" required>
				</div>
			</div>
			<div class="col-md-3">
				<div>
					<b> To</b> 
					<input type="date" class="form-control" id="edateSearch" name="edateSearch" placeholder="Placeholder Text" value="<?php echo $cur_date;?>" required>
				</div>
			</div>
			<div class="col-md-3">
				<div>
					<b> &nbsp;</b><br>
					<button type="button" class="btn btn-default" onclick="getReminder()">Search</button> 
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>View Reminder</h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table id="example-table" class="table table-striped table-bordered table-hover table-green">
								<thead>
									<tr>
										<th>Sr.No</th>
										<?php
										if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
											echo "<th>Reminder Set By</th>";
										}
										?>
										<th>Company Name</th>
										<th>Contact No.</th>                                        
										<th>Remark</th>
										<th>Reminder Date</th>
										<th>Reminder Time</th>
										<?php
										echo " <th>Reminder Type</th>";
										if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "4"){
											echo " <th>Action_For_Reminder</th>";
											echo " <th>Re-Assign</th>";
											echo " <th>Quotation</th>";
										}
										?>
									</tr>
								</thead>
								<tbody>
									<?php
									if($result != 'no'){
										foreach ($result as $key => $value) {
											$get_reminder_details = "";
											if($result[$key]['r_type'] == "Call"){
												$get_reminder_details = "SELECT * FROM customer_details WHERE user_id = '".$result[$key]['user_id']."' AND cust_id = '".$result[$key]['cust_id']."' ";                                               
											}
											if($result[$key]['r_type'] == "Lead"){
												$get_reminder_details = "SELECT * FROM client_details WHERE bde_user_id = '".$result[$key]['user_id']."' AND cust_id = '".$result[$key]['cust_id']."' ";                                               
											}
											$get_reminder_result = $con->data_select($get_reminder_details);
											/*echo "<pre>";
											print_R($get_reminder_details);
											exit();*/

											$sr = $key + 1;
											echo '<tr id="row'.$sr.'">';
											echo "<td>".$sr."</td>";
											if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
												echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
											}
											echo "<td id='cnm".$sr."'>".$get_reminder_result[0]['company_name']."</td>";
											echo "<td id='cno".$sr."'>".$get_reminder_result[0]['contact_number']."</td>";
											echo "<td>".$result[$key]['remark']."</td>";
											echo "<td>".$result[$key]['r_date']."</td>";
											echo "<td>".$result[$key]['r_time']."</td>";
											echo "<td>".$result[$key]['r_type']."</td>";  
											if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "4"){
												if($result[$key]['r_type'] == "Call"){
													/*echo "<td align='center'><a href='#' data-toggle='modal' data-target='#flexModal' title='Make Lead' onclick='return accept(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].");'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#'  data-toggle='modal' data-target='#flexModal1' title='Reject Lead' onclick='return reject(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].",1);'><i class='fa fa-times btn-red btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#flexModal2' title='Set Reminder' onclick='return delay(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].",1);'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>  <a title='Send Quotation' href='send_quotation_crm.php?contactNumber=".$result[$key]['contact_number']."&companyName=".$result[$key]['company_name']."&cust_id=".$result[$key]['cust_id']."&q_type=Customer'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>";*/
													echo "<td align='center'><div class='btn-group btn-group-sm'>
													<a href='#' data-toggle='modal' data-target='#acceptCall' title='Make Lead' onclick='return acceptCall(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].");'><i class='fa fa-thumbs-o-up btn-green btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#rejectCall' title='Reject Call' onclick='return rejectCall(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#delayCall' title='Set Reminder' onclick='return delayCall(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
													</div></td>";
													echo "<td>---</td>";
													echo "<td align='center'><a title='Send Quotation' href='send_quotation_crm.php?contactNumber=".$get_reminder_result[0]['contact_number']."&companyName=".$get_reminder_result[0]['company_name']."&emailId=".$get_reminder_result[0]['customer_email']."&cust_id=".$result[$key]['cust_id']."&r_id=".$result[$key]['r_id']."&q_type=Customer'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>"; 
												}
												if($result[$key]['r_type'] == "Lead"){
													/*echo "<td align='center'><a href='#' data-toggle='modal' data-target='#flexModalLead' title='Make Payment' onclick='return acceptLead(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#'  data-toggle='modal' data-target='#flexModal1' title='Reject Lead' onclick='return reject(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-times btn-red btn-sm'></i></a>  <a href='#'  data-toggle='modal' data-target='#flexModal2' title='Set Reminder' onclick='return delay(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>  <a title='Send Quotation' href='send_quotation_crm.php?name=".$result[$key]['contact_person']."&contactNumber=".$result[$key]['contact_number']."&companyName=".$result[$key]['company_name']."&emailId=".$result[$key]['email_id']."&address=".$result[$key]['address']."&cust_id=".$result[$key]['cust_id']."&q_type=Lead'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>"; */
													/*echo "<td align='center'><a href='#'  data-toggle='modal' data-target='#flexModal3' title='Re-Assign Lead' onclick='return reassign(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'><i class='fa fa-mail-forward btn-green btn-sm'></i></a></td>";*/
													echo"<td align='center'><div class='btn-group btn-group-sm'><input type='hidden' id='emailid".$sr."' value='".$get_reminder_result[0]['email_id']."'>
													<a href='#'  data-toggle='modal' data-target='#acceptLead' title='Make Payment' onclick='return acceptLead(".$sr.",".$result[$key]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#rejectLead' title='Reject Lead' onclick='return rejectLead(".$sr.",".$result[$key]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#delayLead' title='Set Reminder' onclick='return delayLead(".$sr.",".$result[$key]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
													</div></td>";
													echo"<td align='center'><a href='#'  data-toggle='modal' data-target='#reassign' title='Re-Assign Lead' onclick='return reassign(".$sr.",".$result[$key]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$key]['r_id'].",2);'><i class='fa fa-mail-forward btn-green btn-sm'></i></a></td>";
													echo "<td align='center'><a title='Send Quotation' href='send_quotation_crm.php?name=".$result[$key]['contact_person']."&contactNumber=".$result[$key]['contact_number']."&companyName=".$result[$key]['company_name']."&emailId=".$result[$key]['email_id']."&address=".$result[$key]['address']."&cust_id=".$result[$key]['cust_id']."&r_id=".$result[$key]['r_id']."&q_type=Lead'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>"; 
												}
											}
											echo "</tr>";
										} 
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /#page-wrapper -->
<!-- end MAIN PAGE CONTENT -->

<div class="modal modal-flex fade" id="acceptCall" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Assign Lead </h4>
			</div>
			<form id="addClientData" class="form-horizontal" role="form">
				<div class="modal-body">
					<input type="hidden" id="acceptCall_cust_id" name="cust_id"> 
					<input type="hidden" id="acceptCall_reminderId" name="reminder_id">             
					<div class="form-group">
						<label class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="companyNameCall" name="companyName" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Contact No</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contactNumberCall" name="contactNumber" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Contact Person</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contactPersonCall" name="contactPerson" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Email ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="emailIdCall" name="emailId" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="addressCall" name="address" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<?php
					echo '<div class="form-group">';
					echo '<label class="col-sm-2 control-label">Assign To</label>';
					echo '<div class="col-sm-10">';
					echo '<select  class="form-control" id="assignLead" name="assignLead" required>';
					echo '<option value="" selected disabled >--------Select User---------</option>';
					foreach ($result1 as $key => $value) {
						$role_name = "";
						if($result1[$key]['role'] == 1){
							$role_name = "Admin";
						}
						if($result1[$key]['role'] == 2){
							$role_name = "BDM";
						}
						if($result1[$key]['role'] == 3){
							$role_name = "BDE";
						}
						if($result1[$key]['role'] == 4){
							$role_name = "TelleCaller";
						}
						if($result1[$key]['role'] == 5){
							$role_name = "RM";
						}
						echo "<option value='".$result1[$key]['user_id']."'>".$result1[$key]['fname']." ".$result1[$key]['lname']." - ".$role_name."</option>";
					}
					echo '</select>';
					echo '</div>';
					echo '</div>';
					?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
					<button type="button" id="addClientDetails" class="btn btn-green">Save changes</button>
				</div>
			</form> 
		</div>
	</div>
</div>


<div class="modal modal-flex fade" id="rejectCall" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Reject Customer Form</h4>
			</div>
			<form id="updateRejectData" class="form-horizontal" role="form">
				<div class="modal-body">

					<input type="hidden" id="rejectCall_cust_id" name="cust_id">              
					<input type="hidden" id="rejectCall_reminderId" name="reminder_id"> 
					<div class="form-group">
						<label for="textArea" class="col-sm-2 control-label">Remark</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="remarkCall" name="remark" cols="100" rows="10" placeholder="Placeholder Text" required></textarea>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
					<button type="button" id="updateRejectDetailsCall" class="btn btn-green">Save changes</button> 
				</div>
			</form> 
		</div>
	</div>
</div>

<div class="modal modal-flex fade" id="delayCall" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Delay Customer Form </h4>
			</div>
			<form id="updateDelayData" class="form-horizontal" role="form">
				<div class="modal-body">
					<input type="hidden" id="delayCall_cust_id" name="cust_id">           
					<input type="hidden" id="delayCall_reminderId" name="reminder_id"> 
					<div class="form-group">
						<label for="textArea" class="col-sm-2 control-label">Remark</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="delay_remarkCall" name="remark" placeholder="Placeholder Text" required></textarea>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Date</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="rdateCall" name="rdate" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Time</label>
						<div class="col-sm-10">
							<input  class="form-control" type="time" id="rtimeCall" name="rtime" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
					<button type="button" id="updateDelayDetailsCall" class="btn btn-green">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- ************************************************************************************************************* -->
<!-- ************************************************************************************************************* -->
<!-- ************************************************************************************************************* -->

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="acceptLead" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="saveClientData" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="flexModalLabel">Save Client Details</h4>
				</div>
				<div class="modal-body">
					<div id="product_div_main">
						<label>1.Product Details </label>              
						<div class="form-group">
							<label class="col-sm-2 control-label">Product</label>
							<div class="col-sm-10">
								<select  class="form-control" id="product_name1" name="product_name" onchange="changeQty(this);"  required>
									<option value="" selected disabled >--------Select Product---------</option>
									<?php
									foreach ($result2 as $key => $value) {
										echo "<option value='".$result2[$key]['product_name']."'>".$result2[$key]['product_name']."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group" >
							<label class="col-sm-2 control-label" id="label1">Quantity</label>
							<div class="col-sm-10">
								<select  class="form-control" id="product_quantity1" name="product_quantity" onchange="changeRate(this);" required>
									<option value="" selected disabled >--------Select Quantity---------</option>
								</select>
							</div>
						</div>
						<div class="form-group" style="display:none;" id="div_code_validity1">
							<label class="col-sm-2 control-label">Validity</label>
							<div class="col-sm-10">
								<select  class="form-control" id="code_validity1" name="code_validity" required>
									<option value="" selected disabled >--------Select Month---------</option>
									<option value="1 Month" >1 Month</option>
									<option value="3 Months" >3 Months</option>
									<option value="6 Months" >6 Months</option>
									<option value="12 Months" >12 Months</option>
									<option value="Unlimited" >Unlimited</option>
								</select>
							</div>
						</div>
						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Rate per unit</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="product_rate1" name="product_rate" onkeyup="calculaterate(this);" placeholder="Placeholder Text" required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group has-success">
							<label class="col-sm-2 control-label"><span style="margin: 0 10px 0 0;">Tax</span> <input type="checkbox" style="border: 2px solid #16A085;width: 15px;height: 15px;" id="send_tax1" name="send_tax" onclick="disableRadio(this);" value="N"> </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="tax1" name="tax" value="14.00"  onkeyup="calculatetax(this);" disabled required>
								<input type="hidden" id="tax_amount1" name="tax_amount"  disabled >
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Total Amount</label>
							<div class="col-sm-10">
								<input type="hidden" id="hidden_total_amount1" name="hidden_total_amount" placeholder="Placeholder Text" readonly>
								<input type="text" class="form-control" id="total_amount1" name="total_amount" placeholder="0" readonly required>
								<span class="help-block"></span>
							</div>
						</div>

                            <!-- <div class="form-group">
                                <label class="col-sm-2 control-label">Payment Mode</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="paymode1" name="paymode" onchange="checkNumber(this);" required>
                                        <option value="" selected disabled >--------Select Payment Mode---------</option>
                                        <option value="Cash" >Cash</option>
                                        <option value="Check" >Check</option>
                                        <option value="Online" >Online</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-success" style="display:none;" id="check_number_div1">
                                <label class="col-sm-2 control-label">Check Number</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="check_number1" name="check_number" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div> -->
                            <div class="form-group">
                            	<label class="col-sm-2 control-label">Payment Mode</label>
                            	<div class="col-sm-10">
                            		<select  class="form-control" id="paymode1" name="paymode" onchange="checkNumber(this);" required>
                            			<option value="" selected disabled >--------Select Payment Mode---------</option>
                            			<option value="Cash" >Cash</option>
                            			<option value="Cheque" >Cheque</option>
                            			<option value="Online" >Online</option>
                            		</select>
                            	</div>
                            </div>
                            <div class="form-group has-success" style="display:none;" id="check_number_div1">
                            	<label class="col-sm-2 control-label">Cheque Number</label>
                            	<div class="col-sm-10">
                            		<input type="text" class="form-control" id="check_number1" name="check_number" placeholder="Placeholder Text" required>
                            		<span class="help-block"></span>
                            	</div>
                            </div>
                            <div class="form-group has-success" style="display:none;" id="bank_name_div1">
                            	<label class="col-sm-2 control-label">Bank Name</label>
                            	<div class="col-sm-10">
                            		<input type="text" class="form-control" id="bank_name1" name="bank_name" placeholder="Placeholder Text" required>
                            		<span class="help-block"></span>
                            	</div>
                            </div>
                            <div class="form-group has-success">
                            	<label class="col-sm-2 control-label">Received By</label>
                            	<div class="col-sm-10">
                            		<input type="text" class="form-control" id="amount_received1" name="amount_received" value="<?php echo  $result_get_name[0]['fname']." ".$result_get_name[0]['lname'] ?>" placeholder="Placeholder Text" required>
                            		<span class="help-block"></span>
                            	</div>
                            </div>
                        </div> 
                        <div class="col-sm-10" style="width: 145px;float: right;">
                        	<div class="checkbox">
                        		<label>
                        			<strong><input type="checkbox" id="send_invoice" name="send_invoice" value="send_invoice" style="border: 2px solid #16A085;width: 15px;height: 15px;">Send Invoice</strong>
                        		</label>
                        	</div>                          
                        </div>
                        <input type="hidden" id="acceptLead_reminderId" name="reminder_id">
                        <input type="hidden" id="acceptLead_cust_id" name="cust_id">
                        <input type="hidden" id="acceptLead_client_id" name="client_id">  
                        <input type="hidden" id="acceptLead_email_id" name="email_id">
                        <input type="hidden" id="acceptLead_client_add" name="client_add">
                        <input type="hidden" id="acceptLead_client_name" name="client_name">
                        <input type="hidden" id="acceptLead_company_name" name="company_name">
                        <input type="hidden" id="acceptLead_contact_number" name="contact_number">
                        <input type="hidden" id="appendProductName" name="appendProductName">
                        <!-- <input type="hidden" id="appendSmsType" name="appendSmsType"> -->
                        <input type="hidden" id="appendProductQty" name="appendProductQty">
                        <input type="hidden" id="appendProductRate" name="appendProductRate">
                        <input type="hidden" id="appendTotalAmount" name="appendTotalAmount">
                        <input type="hidden" id="appendPayMode" name="appendPayMode">
                        <input type="hidden" id="appendCheckNumber" name="appendCheckNumber">
                        <input type="hidden" id="appendAmountReceived" name="appendAmountReceived">
                        <input type="hidden" id="appendTaxAmount" name="appendTaxAmount">
                        <input type="hidden" id="appendSendTax" name="appendSendTax">
                        <input type="hidden" id="appendhidden_total_amount" name="appendhidden_total_amount">
                    </div>
                    <div class="modal-footer">
                    	<div  align="center" id="ajax-loaderLead" style="display:none" ><img src="img/ajax-loader.gif"></div>
                    	<div >
                    		<br>
                    		<button type="button" class="btn btn-default close_client_model" id="close_model_div" data-dismiss="modal">Close</button>
                    		<!-- <button type="button" class="btn btn-default" id="add_more_div">Add Another Product</button> -->
                    		<button type="button" class="btn btn-green" id="saveClientDetails" >Save changes</button>
                    	</div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal modal-flex fade" id="rejectLead" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title" id="flexModalLabel">Reject Remark</h4>
    			</div>
    			<form id="updateRejectData" class="form-horizontal" role="form">
    				<div class="modal-body">
    					<input type="hidden" id="rejectLead_cust_id" name="cust_id">              
    					<input type="hidden" id="rejectLead_client_id" name="client_id"> 
    					<input type="hidden" id="rejectLead_reminderId" name="reminder_id">
    					<div class="form-group">
    						<label for="textArea" class="col-sm-2 control-label">Remark</label>
    						<div class="col-sm-10">
    							<textarea class="form-control" rows="5" id="remarkLead" name="remark" placeholder="Placeholder Text" required></textarea>
    						</div>
    					</div>                            
    				</div>
    				<div class="modal-footer">
    					<button type="button" class="btn btn-default close_client_model" data-dismiss="modal">Close</button> 
    					<button type="button" class="btn btn-green" id="updateRejectDetailsLead">Submit</button>
    				</div>
    			</form> 
    		</div>
    	</div>
    </div>



    <div class="modal modal-flex fade" id="delayLead" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-title" id="flexModalLabel">Set Reminder</h4>
    			</div>
    			<form id="updateDelayData" class="form-horizontal" role="form">
    				<div class="modal-body">
    					<input type="hidden" id="delayLead_cust_id" name="cust_id">
    					<input type="hidden" id="delayLead_client_id" name="client_id">  
    					<input type="hidden" id="delayLead_reminderId" name="reminder_id">         
    					<div class="form-group">
    						<label for="textArea" class="col-sm-2 control-label">Remark</label>
    						<div class="col-sm-10">
    							<textarea class="form-control" rows="5" id="delay_remarkLead" name="remark" placeholder="Placeholder Text" required></textarea>
    						</div>
    					</div>
    					<div class="form-group has-success">
    						<label class="col-sm-2 control-label">Date</label>
    						<div class="col-sm-10">
    							<input type="date" class="form-control" id="rdateLead" name="rdate" placeholder="Placeholder Text" required>
    							<span class="help-block"></span>
    						</div>
    					</div>
    					<div class="form-group has-success">
    						<label class="col-sm-2 control-label">Time</label>
    						<div class="col-sm-10">
    							<input  class="form-control" type="time" id="rtimeLead" name="rtime" placeholder="Placeholder Text" required>
    							<span class="help-block"></span>
    						</div>
    					</div>
                            <!--  <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="updateDelayDetails" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div> -->
                        </div>
                        <div class="modal-footer">
                        	<button type="button" class="btn btn-default close_client_model" data-dismiss="modal">Close</button>
                        	<button type="button" class="btn btn-green" id="updateDelayDetailsLead">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal modal-flex fade" id="reassign" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
        				<h4 class="modal-title" id="flexModalLabel">Re-Assign Lead</h4>
        			</div>
        			<form id="updateReassignData" class="form-horizontal" role="form">
        				<div class="modal-body">
        					<input type="hidden" id="reassign_client_id" name="client_id"> 
        					<input type="hidden" id="reassignLead_reminderId" name="reminder_id">          
        					<div class="form-group" >
        						<label class="col-sm-2 control-label" id="label1">Re-Assign To</label>
        						<div class="col-sm-10">
        							<select  class="form-control" id="reassign_user_id" name="reassign_user_id" required>
        								<option value="" selected disabled >--------Select User---------</option>
        								<?php
        								foreach ($reassign_user_result as $key => $value) {
        									$role_name = "";
        									if($reassign_user_result[$key]['role'] == 1){
        										$role_name = "Admin";
        									}
        									if($reassign_user_result[$key]['role'] == 2){
        										$role_name = "BDM";
        									}
        									if($reassign_user_result[$key]['role'] == 3){
        										$role_name = "BDE";
        									}
        									if($reassign_user_result[$key]['role'] == 4){
        										$role_name = "TelleCaller";
        									}
        									if($reassign_user_result[$key]['role'] == 5){
        										$role_name = "RM";
        									}
        									echo "<option value='".$reassign_user_result[$key]['user_id']."'>".$reassign_user_result[$key]['fname']." ".$reassign_user_result[$key]['lname']." - ".$role_name."</option>";
        								}
        								?>
        							</select>
        						</div>
        					</div>
        				</div>
        				<div class="modal-footer">
        					<button type="button" class="btn btn-default close_client_model" data-dismiss="modal">Close</button>
        					<button type="button" class="btn btn-green" id="updateReassignDetails">Submit</button>
        				</div>
        			</form>
        		</div>
        	</div>
        </div>

        <script type="text/javascript">
        	$(function(){
        		$('#example-table').DataTable();
        	});
        </script>

        <?php
        include('footer_crm.php');
        ?>