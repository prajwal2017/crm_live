   <!-- begin SIDE NAVIGATION -->
        <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
                       <!--  <img class="img-circle" src="img/profile-pic.jpg" alt=""> -->
                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                        <p class="name tooltip-sidebar-logout">
                            kundan
                            <span class="last-name">Kumar</span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->
                  
                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a href="index-2.html">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <!-- end DASHBOARD LINK -->
                    <!-- begin CHARTS DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#charts">
                            <i class="fa fa-bar-chart-o"></i> Sample <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="charts">
                            <li>
                               <!--  <a href="assign.php"> -->
                               <a href="#">
                                    <i class="fa fa-angle-double-right"></i>Assign Sample
                                </a>
                            </li>
                            <li>
                                <a href="samp_entry.php">
                                    <i class="fa fa-angle-double-right"></i>Sample Entry
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="product.php">
                            <i class="fa fa-dashboard"></i> Product
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="fa fa-dashboard"></i> Department
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <i class="fa fa-dashboard"></i> Lab Test
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-dashboard"></i> Equipment
                        </a>
                    </li>
                   
                
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->