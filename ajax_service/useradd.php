<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Facades\Hash;
use Excel;
use DB;
use Mail;
DB::enableQuerylog();


class Users extends Model{
	public $table="users";


	public static function viewUsers($product_id,$role,$status){

		if($product_id !=0)
		{
			$data = \DB::table('vw_user_detail')
			->where('status','=',$status)
			->where('role','=',$role)
			->where('product_id','=',$product_id)
			->paginate(10);
		}
		else
		{
			$data = \DB::table('vw_user_detail')
			->select('vw_user_detail.*',DB::raw('group_concat(product_id) as products'),DB::raw('group_concat(current_amount) as balance'))
			->where('status','=',$status)
			->where('role','=',$role)
			->groupBy('id')
			->paginate(10); 
		}

		return $data;
	}

    //For get Uesrs at auto complete field. 
	public static function getUsersName($role,$status,$product_id,$request)
	{
		if($product_id !=0)
		{
			$data = DB::table('vw_user_detail')->select(DB::raw('distinct(user_name) AS LABEL'),'id AS VALUE')
			->where('status','=',$status)
			->where('role','=',$role)
			->where('product_id','=',$product_id)
			->where('user_name', 'like', '%'.$request.'%')
			->get();
		}
		else
		{
			$data = DB::table('vw_user_detail')->select(DB::raw('distinct(user_name) AS LABEL'),'id AS VALUE')
			->where('status','=',$status)
			->where('role','=',$role)
			->where('user_name', 'like', '%'.$request.'%')
			->get();
		}

		return $data;
	}

	public static function userSearch($mobile,$user_id,$role,$status,$product_id)
	{
		//return $user_id.$role.$status.$product_id;
		if($product_id !=0)
		{
			if($mobile !='' && $user_id =='')
			{
				$data = \DB::table('vw_user_detail')
				->where('status','=',$status)
				->where('role','=',$role)
				->where('product_id','=',$product_id)
				->where('mobile','=',$mobile)
				->paginate(10); 
			} 

			if($user_id !='' && $mobile =='')
			{
				$data = \DB::table('vw_user_detail')
				->where('status','=',$status)
				->where('role','=',$role)
				->where('product_id','=',$product_id)
				->where('id','=',$user_id)
				->paginate(10); 
			} 

			if($mobile !='' && $user_id !='')
			{
				$data = \DB::table('vw_user_detail')
				->where('status','=',$status)
				->where('role','=',$role)
				->where('product_id','=',$product_id)
				->where('mobile','=',$mobile)
				->where('id','=',$user_id)
				->paginate(10); 
			} 

			if($mobile =='' && $user_id =='')
			{
				$data = \DB::table('vw_user_detail')
				->where('status','=',$status)
				->where('role','=',$role)
				->where('product_id','=',$product_id)
				->paginate(10); 
			}
		}
		else
		{
			if($mobile !='' && $user_id =='')
			{
				$data = \DB::table('vw_user_detail')
				->select('vw_user_detail.*',DB::raw('group_concat(product_id) as products'),DB::raw('group_concat(current_amount) as balance'))
				->where('status','=',$status)
				->where('role','=',$role)
				->where('mobile','=',$mobile)
				->groupBy('id')
				->paginate(10); 
			} 

			if($user_id !='' && $mobile =='')
			{
				$data = \DB::table('vw_user_detail')
				->select('vw_user_detail.*',DB::raw('group_concat(product_id) as products'),DB::raw('group_concat(current_amount) as balance'))
				->where('status','=',$status)
				->where('role','=',$role)
				->where('id','=',$user_id)
				->groupBy('id')
				->paginate(10); 
			} 

			if($mobile !='' && $user_id !='')
			{
				$data = \DB::table('vw_user_detail')
				->select('vw_user_detail.*',DB::raw('group_concat(product_id) as products'),DB::raw('group_concat(current_amount) as balance'))
				->where('status','=',$status)
				->where('role','=',$role)
				->where('mobile','=',$mobile)
				->where('id','=',$user_id)
				->groupBy('id')
				->paginate(10); 
			} 

			if($mobile =='' && $user_id =='')
			{
				$data = \DB::table('vw_user_detail')
				->select('vw_user_detail.*',DB::raw('group_concat(product_id) as products'),DB::raw('group_concat(current_amount) as balance'))
				->where('status','=',$status)
				->where('role','=',$role)
				->groupBy('id')
				->paginate(10); 
			}
		}

		return $data;
	}


	public static function addUsers($input)
	{
		DB::beginTransaction();
		$inputDataUser['address']=$input['address'];
		$inputDataUser['state']=$input['state'];
		$inputDataUser['user_name']=$input['user_name'];
		$inputDataUser['company_name']=$input['company_name'];
		$inputDataUser['city']=$input['city'];
		$inputDataUser['country']=$input['country'];
		$inputDataUser['reseller_id']=$input['reseller_id'];
		$inputDataUser['email']=$input['email'];
		$inputDataUser['mobile']=$input['mobile'];
		$inputDataUser['name']=$input['name'];
		$inputDataUser['role']=$input['role'];
		$inputDataUser['password']=$input['autopassword'];
		//$inputDataUser['sales_id']=$input['sales_id'];
		$inputDataUser['status']=1;

	//$inputDataAuth['percentage']=$input['percentage'];
		$inputDataAuth['dnd_check']=$input['dndCheck'];
		$inputDataAuth['spam_check']=$input['spamCheck']; 
		$inputDataAuth['product_id']=$input['productId'];

		$inputDataProduct['product_id']=$input['productId'];
		$inputDataProduct['route_id']=$input['route_id'];
		$inputData['credit']=$input['creditBalnce'];
		$inputData['current_amount']=$input['creditBalnce'];
		$inputData['previous_amount']=0;
		$inputData['credit_by']=$input['creditBy'];
		$inputData['credit_type']=1;
		$inputData['remark']="INITIAL ADD";
		$inputData['product_id']=$input['productId'];
		$inputDataSender['sender_id']=$input['senderId'];
		try {
			$data = DB::table('sms_balance')
			->where('user_id','=',$inputDataUser['reseller_id'])
			->where('product_id','=',$inputDataProduct['product_id'])->get();
			if(sizeof($data) > 0 ){
				if($inputData['credit'] < $data[0]->balance){
					$newCreditBalance['balance'] = $data[0]->balance - $inputData['credit'];
					$updateSmsBalance = DB::table('sms_balance')
					->where('user_id','=',$inputDataUser['reseller_id'])
					->where('product_id','=',$inputDataProduct['product_id'])
					->update($newCreditBalance);/*deduct sms_balance*/

					$debitTable['current_amount'] = $newCreditBalance['balance'];
					$debitTable['previous_amount'] = $data[0]->balance;
					$debitTable['debit'] = $inputData['credit'];
					$debitTable['debit_by'] = $inputDataUser['reseller_id'];
					$debitTable['debit_type'] = 1;
					$debitTable['remark'] = "Debit by Admin for".$inputDataUser['reseller_id'];
					$debitTable['product_id'] = $inputDataProduct['product_id'];
					$debitTable['user_id'] = $inputDataUser['reseller_id'];
					$insertDebitTable = DB::table('debit')->insert($debitTable);
					$data = DB::table('user')->where('user_name','=',$inputDataUser['user_name'])->get();/*insert new debit*/
					if(sizeof($data) > 0 )
					{
						$message['code'] = 409 ;
						$message['status'] = "Username found";
						$message['message'] = $inputDataUser['user_name']. ", Username already exist.";
						return $message;
					}

					$data= DB::table('user')->insertGetId($inputDataUser);
					$inputDataAuth['user_id']=$data;
					$data = DB::table('user_authrization')->insert($inputDataAuth);
					$inputDataProduct['user_id'] = $inputDataAuth['user_id'];
					$data = DB::table('user_product')->insert($inputDataProduct);
					$inputData['user_id'] = $inputDataProduct['user_id'];
					$data = DB::table('credit')->insert($inputData);
					$inputDataSender['user_id']=$inputData['user_id'];
					$data = DB::table('sender_id')->insert($inputDataSender);

					$smsBalance['balance'] = $inputData['credit'];
					$smsBalance['product_id'] = $debitTable['product_id'] ;
					$smsBalance['user_id'] = $inputData['user_id'] ;
					$data = DB::table('sms_balance')->insert($smsBalance);

					if($input['productId'] == 1){
						$product_name = 'Transactional';
					}elseif($input['productId'] == 2){
						$product_name = 'Promotional';
					}else{
						$product_name = 'Scrub';
					}

					$userMailInfo = array();
					$userMailInfo['name'] = $input['name'];
					$userMailInfo['username'] = $input['user_name'];
					$userMailInfo['password'] = $input['autopassword'];
					$userMailInfo['balance'] = $input['creditBalnce'];
					$userMailInfo['account_type'] = $product_name;
					$userMailInfo['email'] = $input['email'];
					$mail = Mail::send('email.user_registrationMail',['userMailInfo' => $userMailInfo], function($message) use ($userMailInfo){
						$message->from('support@mobisofttech.co.in', 'MakeMySMS');
						$message->to($userMailInfo['email'])->subject('MakeMySMS - Account Created');
						$message->cc('support@mobisofttech.co.in');
						$message->cc('aakash@mobisofttech.co.in');
						$message->cc('priya@mobisofttech.co.in');
						$message->cc('hr@mobisofttech.co.in');
					//$message->bcc('prajwal.p@mobisofttech.co.in');
					});

					/*$username = env('SMS_USERNAME');
					$password = env('SMS_PASSWORD');
					$sender = env('SMS_SENDERID');
					$message = "Hello ".$username.",<br>Your account has been created successsfully.";
					$message=urlencode($message);
					$api_url = "http://makemysms.in/api/sendsms.php?username=".$username."&password=".$password."&sender=".$sender."&mobile=".$number."&type=1&message=".$message;

					$output = file_get_contents($api_url);*/

					/*$supportMailInfo = array();
					$supportMailInfo['username'] = $input['user_name'];
					$supportMailInfo['password'] = $input['autopassword'];
					$supportMailInfo['balance'] = $input['creditBalnce'];
					$supportMailInfo['account_type'] = $product_name;
					$supportMailInfo['email'] = $input['email'];
					$mail = Mail::send('email.support_registrationMail',['supportMailInfo' => $supportMailInfo], function($message) use ($supportMailInfo){
						$message->from('no-reply@mobisofttech.co.in', 'Make My SMS');
						$message->to('prajwal.p@mobisofttech.co.in')->subject('MakeMySMS - Registration Mail');
					});*/

					DB::commit();
				}else{
					$message['code'] = 409 ;
					$message['status'] = "Low Balance";
					$message['message'] = "Not Enought Balance";
					return $message;
				}

			}else{
				$message['code'] = 409 ;
				$message['status'] = "Data not Found";
				$message['message'] = "Reseller not found.";
				return $message;
			}
		} catch (\Exception $e) {
			DB::rollback();
			$message['code'] = 409 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
		return $data;
	}


	public static function viewActivePromotionalUsers(){

		/*$data = \DB::table('user')
		->join('user_product', 'user.id', '=', 'user_product.user_id')
		->join('credit', 'user.id', '=', 'credit.user_id')
		->join('user_authrization','user_authrization.u_id','=','user.id')
		->select('user.*','user_product.route_id','user_product.product_id','credit.current_amount','credit.created','user_authrization.dnd_check','user_authrization.spam_check')->where('user.status','=',1)->where('user_product.product_id','=',2)->paginate();
		return $data;*/

		$data = \DB::table('vw_user_detail')
		->where('status','=',1)
		->where('role','=',2)
		->where('product_id','=',2)
		->paginate();
		return $data;
	}


	public static function viewInactivePromotionalUsers(){

		/*$data = \DB::table('user')
		->join('user_product', 'user.id', '=', 'user_product.user_id')
		->join('credit', 'user.id', '=', 'credit.user_id')
		->join('user_authrization','user_authrization.u_id','=','user.id')
		->select('user.*','user_product.route_id','user_product.product_id','credit.current_amount','credit.created','user_authrization.dnd_check','user_authrization.spam_check')->where('user.status','=',1)->where('user_product.product_id','=',2)->paginate();
		return $data;*/

		$data = \DB::table('vw_user_detail')
		->where('status','=',2)
		->where('role','=',2)
		->where('product_id','=',2)
		->paginate();
		return $data;
	}


	public static function viewActiveTranactionalUsers(){

		$data = \DB::table('vw_user_detail')
		->where('status','=',1)
		->where('role','=',2)
		->where('product_id','=',1)
		->paginate();
		return $data;


	}


	public static function viewInactiveTransactionalUsers(){

		$data = \DB::table('vw_user_detail')
		->where('status','=',2)
		->where('role','=',2)
		->where('product_id','=',1)
		->paginate();
		return $data;


	}


	public static function getProduct()
	{
        //dd($formData);
		return DB::table('product')->where('status','=',1)->get();
	}

	public static function getUserDetails($id,$p_id,$role){
     // $data= DB::table('vw_user_detail')
     //  ->where('id','=',$id)
     //  ->where('product_id','=',$p_id)
     //  ->get();
		$data= DB::table('user')
		->join('user_product', 'user.id', '=', 'user_product.user_id')
		->join('user_authrization', 'user.id', '=', 'user_authrization.user_id')   
		->select('user.*','user_authrization.*','user_product.route_id','user_product.product_id')->where('user.role','=',$role)->where('user.id','=',$id)->where('user_product.product_id','=',$p_id)->get();
		return $data;
	}


	public static function addUser($formData)
	{
		return DB::table('users')->insert($formData);
	}


	public static function getUser()
	{
		return DB::table('users')->get();
	}


	public static function getUserById($id)
	{
		return DB::table('user')->where("id","=",$id)->get();
	}


	public static function editUser($input)
	{
		DB::beginTransaction();
		$inputDataUser['address']=$input['address'];
		$inputDataUser['state']=$input['state'];
		$inputDataUser['company_name']=$input['company_name'];
		$inputDataUser['city']=$input['city'];
		$inputDataUser['country']=$input['country'];
		$inputDataUser['reseller_id']=$input['reseller_id'];
		$inputDataUser['email']=$input['email'];
		$inputDataUser['mobile']=$input['mobile'];
		$inputDataUser['name']=$input['name'];

		//$userProduct['product_id'] = $input['product_id'];
		$userProduct['route_id'] = $input['route'];

		$userAuthrization['dnd_check']=$input['dnd_check'];
		//$userAuthrization['percentage']=$input['percentage'];
		$userAuthrization['spam_check']=$input['spam_check'];
        //return $inputDataUser;
		try {
			$data =  DB::table('user')->where("id","=",$input['u_id'])->update($inputDataUser);

			/*$data =  DB::table('user_product')
			->where("user_id","=",$input['u_id'])
			->where('product_id','=',$input['product_id'])
			->update($userProduct);
            
			$data =  DB::table('user_authrization')->where("user_id","=",$input['u_id'])->update($userAuthrization);*/
			DB::commit();
			$message['code'] = 200 ;
			$message['status'] = "Success";
			$message['message'] = "Update Success";
			return $message;

		} catch (\Exception $e) {
			DB::rollback();
			$message['code'] = 409 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
	}


	public static function deleteUserById($id){
		$status['status'] = 6;
		return DB::table('users')->where("id","=",$id)->update($status);
	}

	public static function deactivateUser($id){
		$status['status'] = 2;
		return DB::table('user')->where("id","=",$id)->update($status);
	}


	public static function activateUser($id){
		$status['status'] = 1;
		return DB::table('user')->where("id","=",$id)->update($status);
	}


	public static function deactivateAlert($id){
		$status['smsbalance_alert'] = 0;
		return DB::table('user')->where("id","=",$id)->update($status);
	}


	public static function activateAlert($id){
		$status['smsbalance_alert'] = 1;
		return DB::table('user')->where("id","=",$id)->update($status);
	}


	public static function change_password($id)
	{
		$password = bin2hex(openssl_random_pseudo_bytes(4));
		$status['password'] =$password;
		$data =  DB::table('user')
		->where("id","=",$id)
		->update($status);
		if($data > 0){
			$message['code'] = 200;
			$message['data'] = $password;
		}else{
			$message['code'] = 400;
			$message['data'] = "";
		}
		return $message;
	}

	public static function debitDetails($id,$pid)
	{
		$data = DB::table('user')
		->select('sms_balance.balance','user.mobile','user.email','user.id','user.user_name','user.reseller_id')
		->join('sms_balance','sms_balance.user_id','=','user.id')
		->where('sms_balance.product_id','=',$pid)
		->where('user.id','=',$id)->get();
		return $data;
	}

	public static function getResellerName($id)
	{
		$data = DB::table('user')
		->select('user_name')
		->where('id','=',$id)->get();
		return $data;
	}

	public static function DebitCreditSmsBalance_Debit($input){

		DB::beginTransaction();

		try {
			$userBalance = DB::table('sms_balance')->where('user_id','=',$input['uid'])->where('product_id','=',$input['pro'])->get();

			if($userBalance[0]->balance >= $input['debit'] && $input['debit'] > 0)
			{
				$smsBalance['balance'] = $userBalance[0]->balance - $input['debit'];
				$userDebit = DB::table('sms_balance')
				->where('user_id','=',$input['uid'])
				->where('product_id','=',$input['pro'])
				->update($smsBalance);
				$getResellerDetails = Users::getUserById($input['uid']);
				$insertDebit['current_amount'] = $smsBalance['balance'];
				$insertDebit['previous_amount'] = $userBalance[0]->balance;
				$insertDebit['debit'] = $input['debit'];
				$insertDebit['remark'] ="Debit from UserId:".$input['uid']."Credit to ResellerID:".$getResellerDetails[0]->reseller_id;
				$insertDebit['debit_by'] =$input['uid'];
            //$insertDebit['debit_by'] =$input['uid'];
				$insertDebit['debit_type'] =1;
				$insertDebit['product_id'] =$input['pro'];
				$insertDebit['user_id'] =$input['uid'];
            //$insertDebit['user_id'] =$input['resel_id'];

				$debit = DB::table('debit')->insert($insertDebit);
				$resellerBalance = DB::table('sms_balance')
				->where('user_id','=',$getResellerDetails[0]->reseller_id)
				->where('product_id','=',$input['pro'])->get();

				$resellerSmsBalance['balance'] = $resellerBalance[0]->balance + $input['debit'];

				$resellerDebit = DB::table('sms_balance')
				->where('user_id','=',$getResellerDetails[0]->reseller_id)
				->where('product_id','=',$input['pro'])
				->update($resellerSmsBalance);

				$insertCredit['current_amount'] = $resellerSmsBalance['balance'];
				$insertCredit['previous_amount'] = $resellerBalance[0]->balance;
				$insertCredit['credit'] = $input['debit'];
				$insertCredit['remark'] ="Credit to ResellerID:".$getResellerDetails[0]->reseller_id.",Debit from UserId:".$input['uid'];
				$insertCredit['credit_by'] =$input['uid'];
				$insertCredit['credit_type'] =1;
				$insertCredit['product_id'] =$input['pro'];
				$insertCredit['user_id'] =$getResellerDetails[0]->reseller_id;
				$credit = DB::table('credit')->insert($insertCredit);

				DB::commit();
				$message['code'] = 200 ;
				$message['status'] = "Success";
				$message['message'] = "Update Success";
				return $message;
			}
			else
			{
				$message['code'] = 406 ;
				$message['status'] = "Success";
				$message['message'] = "Your balance is less from debit value, Please enter right value.";
				return $message;
			}
		} catch (\Exception $e) {
			DB::rollback();
			$message['code'] = 400 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
	}

	public static function DebitCreditSmsBalance_Credit($input){
		DB::beginTransaction();

		try {
			$getUserDetails = Users::getUserById($input['user_id']);

			if($input['debitto'] == '11'){
				$resellerBalance = DB::table('sms_balance')
				->where('user_id','=',$input['debitto'])
				->where('product_id','=',$input['cpro'])->get();
			}else{
				$resellerBalance = DB::table('sms_balance')
				->where('user_id','=',$getUserDetails[0]->reseller_id)
				->where('product_id','=',$input['cpro'])->get();
			}

			if($resellerBalance[0]->balance >= $input['credit'] && $input['credit'] > 0)
			{
				$resellerSmsBalance['balance'] = $resellerBalance[0]->balance - $input['credit'];

				$resellerDebit = DB::table('sms_balance')
				->where('user_id','=',$resellerBalance[0]->user_id)
				->where('product_id','=',$input['cpro'])
				->update($resellerSmsBalance);

				$insertCredit['current_amount'] = $resellerSmsBalance['balance'];
				$insertCredit['previous_amount'] = $resellerBalance[0]->balance;
				$insertCredit['debit'] = $input['credit'];
				$insertCredit['remark'] = $input['cremark'];
				$insertCredit['debit_by'] =$resellerBalance[0]->user_id;
				$insertCredit['debit_type'] =1;
				$insertCredit['product_id'] =$input['cpro'];
				$insertCredit['user_id'] =$resellerBalance[0]->user_id;
				$credit = DB::table('debit')->insert($insertCredit);

				$userBalance = DB::table('sms_balance')->where('user_id','=',$input['user_id'])->where('product_id','=',$input['cpro'])->get();
				$smsBalance['balance'] = $userBalance[0]->balance + $input['credit'];
				$userDebit = DB::table('sms_balance')
				->where('user_id','=',$input['user_id'])
				->where('product_id','=',$input['cpro'])
				->update($smsBalance);

				$insertDebit['current_amount'] = $smsBalance['balance'];
				$insertDebit['previous_amount'] = $userBalance[0]->balance;
				$insertDebit['credit'] = $input['credit'];
				$insertDebit['remark'] =$input['cremark'];
				$insertDebit['credit_by'] =$input['cresel_id'];
				$insertDebit['credit_type'] =1;
				$insertDebit['product_id'] =$input['cpro'];
				$insertDebit['user_id'] =$input['user_id'];
				$debit = DB::table('credit')->insert($insertDebit);
				DB::commit();
				$message['code'] = 200 ;
				$message['status'] = "Success";
				$message['message'] = "Balance credited successfully";
				return $message;
			}
			else
			{
				$message['code'] = 406 ;
				$message['status'] = "Success";
				$message['message'] = "Your Reseller balance is less from credit value, Please enter right value.";
				return $message;
			}
		} catch (\Exception $e) {
			DB::rollback();
			$message['code'] = 409 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
	}


	public static function transaction($id,$pid)
	{
		try {

			$data = DB::table('credit')
			->where('credit.product_id','=',$pid)
			->where('credit.user_id','=',$id)
			->orderBy('id','desc')
			->get();

			$message['code'] = 200;
			$message['status'] = "Success";
			$message['message'] ="data found";
			if(sizeof($data) > 0){

				$message['data'] = $data;

			}else{

				$message['data'] = 400; 

			}
			$data2 = DB::table('debit')
			->where('product_id','=',$pid)
			->where('user_id','=',$id)
			->orderBy('id','desc')->get();
			if(sizeof($data2) > 0){

				$message['data2'] = $data2;

			}else{

				$message['data2'] = 400; 

			}
			if(sizeof($data) <= 0 && sizeof($data2) <= 0){
				$message['code'] = 400;
				$message['status'] = "Failed";
				$message['message'] ="no data found";
			}
			return $message;

		} catch (\Exception $e) {
			$message['code'] = 409 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
		return $data;
	}


	public static function getRouteByPid($uid,$pid){
		
		$data = DB::table('vw_route_detail')->where('user_id','=',$uid)->where('product_id','=',$pid)->get();
		return $data;
	}


	public static function getDndCheck($uid,$pid){

		$data = DB::table('vw_user_detail')->where('id','=',$uid)->where('product_id','=',$pid)->get();
		return $data;
	}

	public static function updateRoute($uid,$pid,$route){
		//return $route;
		$update['route_id'] = $route;
		$data = DB::table('user_product')->where('user_id','=',$uid)->where('product_id','=',$pid)->update($update);
		return $data;
	}


	public static function updateDND($uid,$pid,$dnd){

		try {
			$update['dnd_check'] = $dnd;

			$route = DB::table('user_authrization')->where('user_id','=',$uid)->update($update);
			if($route > 0 ){
				$message['code'] = 200;
				$message['status'] = "Success";
				$message['message'] ="DND Updated";
				$message['data'] = $route;
				return $message;
			}else{
				$message['code'] = 304;
				$message['status'] = "Success";
				$message['message'] ="DND already updated";
				$message['data'] = $route;
				return $message;
			}
		} catch (\Exception $e) {
			$message['code'] = 400 ;
			$message['status'] = "Something When Wrong";
			$message['message'] = $e->getMessage();
			return $message;
		}
	}



	public static function userGroup($uid){

		$data = DB::table('vw_user_group')->where('user_id','=',$uid)->get();
		return $data;
	}



	public static function getResellersUser($uid){

		/*$data = DB::table('user')
		->join('sms_balance','sms_balance.user_id','=','user.id')
		->select('user.user_name','sms_balance.product_id','sms_balance.balance')
		->where('user.reseller_id','=',$uid)
		->where('user.status',1)
		->get();*/

		$data = DB::table('vw_user_detail')
		->where('reseller_id','=',$uid)
		->where('status',1)
		->get();

		return $data;

	}



	public static function exportGroupContact($group_id,$user_id){

		$data = DB::table('contact')
		->select('name as Name','designation as Designation','mobile as Mobile','email_id as Email')
		->where('group_id','=',$group_id)
		->where('user_id','=',$user_id)
		->get();

		return $data;
	}



	public static function deleteGroupContact($group_id,$user_id){

		$data = DB::table('group_details')
		->where('id','=',$group_id)
		->where('user_id','=',$user_id)
		->delete();

		return $data;
	}



	public static function checkProduct($data){
        //dd($formData);
		return DB::table('user_product')
		->where("product_id","=",$data['product_id'])
		->where("user_id","=",$data['user_id'])
		->get();
	}



	public static function checkResellerProduct($data){
        //dd($formData);
		return DB::table('sms_balance')
		->where("product_id","=",$data['product_id'])
		->where("user_id","=",$data['reseller_id'])
		->get();
	}



	public static function deductResellerBalance($data){

		$data = DB::table('sms_balance')
		->where("product_id","=",$data['product_id'])
		->where("user_id","=",$data['user_id'])
		->update($data);
		return $data;
	}

	public static function inserInDebit($data)
	{
		$result = DB::table('debit')->insert($data);
		return $result;
	}

	public static function addProduct($data)
	{
		$result = DB::table('user_product')->insert($data);
		return $result;
	}

	public static function addSmsBalance($data)
	{
		$result = DB::table('sms_balance')->insert($data);
		return $result;
	}

	public static function inserInCredit($data)
	{
		$result = DB::table('credit')->insert($data);
		return $result;
	}

	public static function inserUserAuth($data)
	{
		$result = DB::table('user_authrization')->insert($data);
		return $result;
	}

	public static function excel($data) {

		$excelExport = []; 

		foreach ($data as $result) {
			$excelExport[] = (array) $result;
		}
      //return $excelExport;

		$file = date("Ymdhis");
		Excel::create($file, function($excel) use ($excelExport) {
			$excel->sheet('sheet1', function($sheet) use ($excelExport) {
				$sheet->fromArray($excelExport);
			});

		})->export('xlsx');
	}

    //Emial Function

	public static function sendUserLoginDetails($userInfo)
	{
		$res = Mail::send('email.registration',['userInfo' => $userInfo], function($message) use ($userInfo){

			$message->from('no-reply@mobisofttech.co.in', 'Mobisoft Technology');
			$message->to($userInfo['email'])->subject('Create User');
			$message->cc('ziaurrahman.a@mobisofttech.co.in');     
		});

		return $res;
	}



	public static function userDetails($user_id){
		$userDetails = DB::table('user')
		->where('id',$user_id)
		->get();

		return $userDetails;
	}

}
