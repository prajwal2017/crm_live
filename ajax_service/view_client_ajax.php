 <?php

	$root = $_SERVER['DOCUMENT_ROOT'];


    

	$dom_path = $root."/crm/dompdf/dompdf_config.inc.php";



	include('../class/functions.php');
	//include('../class/fpdf.php');

	require_once($dom_path);
	
	$smtp_path = $root."/crm/smtpmail/class.phpmailer.php";

	$con = new functions();
	session_start(); 
	$date = $con->get_datetime();
	$cur_date = date("Y-m-d");


	//----------------------Conert Number to Word--------------------

	function convert_number($number) 
	{ 
	    if (($number < 0) || ($number > 999999999)) 
	    { 
	    throw new Exception("Number is out of range");
	    } 

	    $Gn = floor($number / 1000000);  /* Millions (giga) */ 
	    $number -= $Gn * 1000000; 
	    $kn = floor($number / 1000);     /* Thousands (kilo) */ 
	    $number -= $kn * 1000; 
	    $Hn = floor($number / 100);      /* Hundreds (hecto) */ 
	    $number -= $Hn * 100; 
	    $Dn = floor($number / 10);       /* Tens (deca) */ 
	    $n = $number % 10;               /* Ones */ 

	    $res = ""; 

	    if ($Gn) 
	    { 
	        $res .= convert_number($Gn) . " Million"; 
	    } 

	    if ($kn) 
	    { 
	        $res .= (empty($res) ? "" : " ") . 
	            convert_number($kn) . " Thousand"; 
	    } 

	    if ($Hn) 
	    { 
	        $res .= (empty($res) ? "" : " ") . 
	            convert_number($Hn) . " Hundred"; 
	    } 

	    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
	        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
	        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
	        "Nineteen"); 
	    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
	        "Seventy", "Eigthy", "Ninety"); 

	    if ($Dn || $n) 
	    { 
	        if (!empty($res)) 
	        { 
	            $res .= " and "; 
	        } 

	        if ($Dn < 2) 
	        { 
	            $res .= $ones[$Dn * 10 + $n]; 
	        } 
	        else 
	        { 
	            $res .= $tens[$Dn]; 

	            if ($n) 
	            { 
	                $res .= "-" . $ones[$n]; 
	            } 
	        } 
	    } 

	    if (empty($res)) 
	    { 
	        $res = "zero"; 
	    } 

	    return $res; 
	} 

	//---------------------------------------------------------------
	
	if($_REQUEST['action'] == 'generateInvoice')
	{
		/*echo "<pre>";
		print_r($_REQUEST);
		exit;*/
		
		$client_id = $_REQUEST['client_id'];
		$invoice_number = $_REQUEST['invoice_number'];

		$get_invoice = "SELECT p.product_name,cd.contact_person,cd.address,cd.company_name,pp.p_id,pp.package,pp.amount,pp.rate,pp.pay_mode,pp.invoice_number,pp.c_date FROM product_payment as pp inner join client_details as cd on pp.client_id=cd.client_id inner join products as p on pp.p_id = p.p_id WHERE pp.client_id = $client_id and pp.invoice_number = '".$invoice_number."' ";
		$result_get_invoice = $con->data_select($get_invoice);
		/*echo "<pre>";
		print_r($result_get_invoice);*/

						
		$tr = "";
		
		$srno = 1;
		$total_amt=0;
		$total_actual_amt=0;
		$total_tax=0;
		$pnm_count = count($result_get_invoice);
		$extra_tr = "<tr>
						<td id='table_td_border'>&nbsp;</td>
						<td id='table_td_border'>&nbsp;</td>
						<td id='table_td_border'>&nbsp;</td>
						<td id='table_td_border'>&nbsp;</td>
						</tr>";
	
		$pamt = 0;
		
		foreach ($result_get_invoice as $key => $value) {

			$pamt = $result_get_invoice[$key]['package'] * $result_get_invoice[$key]['rate'];

			$total_tax=$total_tax + $result_get_invoice[$key]['rate'];
			
			$total_actual_amt = $total_actual_amt + $result_get_invoice[$key]['amount'];
			$total_amt = $total_amt + $pamt;
			
			$sms_td = "<td id='table_td_border'>".$result_get_invoice[$key]['product_name']."</td>";

			$tr = $tr."<tr>
				<td id='table_td_border'>".$srno."</td>
				".$sms_td."
				<td id='table_td_border'>".$result_get_invoice[$key]['package']."</td>
				<td id='table_td_border'>".$pamt."</td>
				</tr>";
		
						
			$srno++;
		}
		
		$tax_amt = $total_actual_amt - $total_amt;
		/*echo "Tax Amount: ".$tax_amt;
		echo "<br>";
		echo "Actual Amount: ".$total_amt;
		echo "<br>";
		echo "Total Amount: ".$total_actual_amt;
		exit;*/
		
		for ($j=0; $j < 5 - $pnm_count; $j++) { 
			$tr = $tr."".$extra_tr;
		}
		
		$grand_total = $total_amt + $tax_amt;

		$gt_word = convert_number($grand_total);



		$st = "AAHCM1232CSD001";
		$pan = "AAHCM1232C";
		$invoice_no = $invoice_number;
		//$invoice_no = "0".$invoice_nm."/".$f_month."/MOBSFT";
		
		$client_add = $result_get_invoice[0]['address'];
		$client_name = $result_get_invoice[0]['contact_person'];

		$acc_name = "Mobisoft Technology India Pvt Ltd";
		$bill_date = $result_get_invoice[0]['c_date'];
		$due_date = "Immediate";

		$comp_name = "Mobisoft Technology India Pvt Ltd";
		$bank_name = "ICICI Bank";
		$acc_no = "015105012883";
		$ifcs_code = "ICICI0000151";
		
		require_once($dom_path);
		$html = '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

		  <head>
		  <style>
			  #table_css, #table_td {
			      border: 1px solid black;
			      border-collapse: collapse;

			  }
			   #table_td_border{
			    border-right:1px solid black;
			    border-left:1px solid black;
			  }
			 

		</style>
		  </head>


		<body><br>
		<img src="mobisoft_logo.jpg" alt="" style="margin-left:30px" height ="200px" width ="1200px"/>
		<table border="1" cellspacing="10" cellpadding="5" style="margin:auto;"  width="90%">



			<tr>
				<td colspan="2" rowspan="2" width="35%"><strong>Mobisoft Technology India Pvt Ltd.</strong><br>
					Office No. H-004, P Level, Tower-3,<br>
					Railway Station Complex, Sector 11,<br />
					CBD Belapur, Navi Mumbai 400 614.<br>
					India.
				</td>

				<td width="20%">Invoice no:-'.$invoice_no.'</td>

				<td rowspan="2" width="24%"><br><br>ST:-'.$st.'<br>
					PAN:-'.$pan.'</td>
			</tr>

		<tr><td><br>Account number:- 01</td></tr>

		<tr>
		<td colspan="2" rowspan="2">
		<strong>Requisitioner:- '.$client_name.' </strong><br>
		'.$client_add.'
		</td>
		<td rowspan="2">Account name:-<br>
		'.$acc_name.'
		</td>
		<td><br>Bill Date:- &nbsp;&nbsp;&nbsp;'.$bill_date.'</td>
		</tr>

		<tr>
		
		<td>Due Date:-&nbsp;&nbsp;&nbsp;'.$due_date.'</td>
		</tr>

		</table>


		<br>


		<table id="table_css" cellpadding="5" style="margin:auto;"  width="87%">
		<tr>
		<td width="10%" id="table_td">Sr. No.</td>
		<td width="40%" id="table_td">Product Name</td>
		<td width="25%" id="table_td">Qty</td>
		<td width="25%" id="table_td">Amt</td>
		</tr>


		'.$tr.'


		<tr>
		<td id="table_td" colspan="3" align="right">Service Tax 15%</td>
		<td id="table_td" colspan="3">'.$tax_amt.'</td>
		</tr>
		<tr>
		<td id="table_td" colspan="3" align="right">Total Amount</td>
		<td id="table_td" colspan="3" >'.$grand_total.'</td>
		</tr>

		<tr>
		<td colspan="4" id="table_td" align="left"> Rupees: '.$gt_word.' Only/-</td>
		</tr>

		</table>


		<br /><br />


		<table frame="box" style="margin:auto;"  width="87%" align="center">
		<tr>
		<td style="border-right:1px solid #000;" rowspan="4" colspan="2" width="50%">
		Company Name<br />
		Bank Name<br />
		Account No<br />
		RTGS/NEFT/IFSC/CODE
		</td>

		<td rowspan="4" colspan="2" width="50%">
		'.$comp_name.'<br>
		'.$bank_name.'<br>
		'.$acc_no.'<br>
		'.$ifcs_code.'
		</td>

		</tr>
		</table>
		<br>
		<center><i>This is Computer Generated Proforma Invoice.</i></center>


		<br><br><br>
		<table>
		<tr>
		<td>
		<hr>
		
			<center><font size="2"><b>Register Office : 694/4 BMC Colony Malad(W) Mumbai 400095.<br>
			Corporate Office : Office No. H-004, P Level, Tower-3, Railway Station Complex, Sector 11, CBD Belapur, Navi Mumbai, Maharashtra 400 614 India.</b></font><br>
			Phone : +91 2756 4515 / 4127 4515. Mobile : +91 8108004545<br>
			www.mobisofttech.co.in</center>

			
		</td>
		</tr>
		</table>
		</body>
		</html>';


		if ( get_magic_quotes_gpc() )
		   
		$old_limit = ini_set("memory_limit", "16M");		  
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		//$dompdf->set_paper("letter", "landscape");
		$dompdf->render();
		$output = $dompdf->output();
		//file_put_contents($pdf_path, $output);//to save pdf
		// $dompdf->stream( 'Invoice.pdf' , array( 'Attachment'=>0 ) );//for generating pdf in browser
		$dompdf->stream("Invoice_".rand(10,1000).".pdf", array("Attachment" => false));//For generating pdf in browser
		//$dompdf->stream("Invoice_".rand(10,1000).".pdf");//For Direct Download		
		exit(0);
		 //--------------------------------Invoice End-------------------------------------------------

	}

	if($_REQUEST['action'] == 'generateReceipt')
	{
		$client_id = $_REQUEST['client_id'];
		$invoice_number = $_REQUEST['invoice_number'];

		$get_receipt = "SELECT p.product_name,cd.contact_person,cd.address,cd.company_name,pp.p_id,pp.package,pp.amount,pp.rate,pp.pay_mode,pp.invoice_number,pp.c_date FROM product_payment as pp inner join client_details as cd on pp.client_id=cd.client_id inner join products as p on pp.p_id = p.p_id WHERE pp.client_id = '".$client_id."' and pp.invoice_number = '".$invoice_number."' ";
		//echo $get_receipt;
		$result_get_receipt = $con->data_select($get_receipt);
		
		
		$client_name = $result_get_receipt[0]['contact_person'];
		$client_comp_name = $result_get_receipt[0]['company_name'];
		$bill_date = $result_get_receipt[0]['c_date'];

		$pname_cash_recpt="";
		$cash_total_amt=0;


		foreach ($result_get_receipt as $key => $value) { 

			$pamt = $result_get_receipt[$key]['package'] * $result_get_receipt[$key]['rate'];	
			$cash_total_amt = $cash_total_amt + $pamt;
			$pname_cash_recpt = $pname_cash_recpt.",".$result_get_receipt[$key]['product_name'].": ".$result_get_receipt[$key]['package'];
					
		}

		
		$pname_cash_recpt = substr($pname_cash_recpt, 1);

		$gt_cash_word = convert_number($cash_total_amt);

		/*echo $pname_cash_recpt;
		echo "<br>";
		echo $gt_cash_word;
		exit;*/

		$receipt_html = '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

				    <head>
				    <style>
					  #table_css, #table_td {
					      border: 1px solid black;
					      border-collapse: collapse;

					  }
					   #table_td_border{
					    border-right:1px solid black;
					    border-left:1px solid black;
					  }
						 

					</style>
					</head>


					<body>
					

					<table frame="box" style="margin: 250px 150px 0 150px;"  width="100%">
					<tr>
					<td  align="left"><b>Cash Receipt</b></td>
					<td align="right">Date:'.$bill_date.'</td><br>
					</tr>

					<tr>
					<td align="center" colspan="2">
					<b><font size="3">Mobisoft Technology India Pvt Ltd</font></b><br>
					<font size="2">
						Office No. H-004, P Level, Tower-3,<br>
						Railway Station Complex, Sector 11,<br />
						CBD Belapur, Navi Mumbai 400 614.<br>
						India.<br>
					</font><br>
					</td>
					</tr>

					<tr>
					<td >&nbsp;</td>
					<td >&nbsp;</td>		
					</tr>

					<tr>
					<td  align="left" colspan="2">Cash Received From <i><u>' .$client_name.'('.$client_comp_name.')</u></i></td>
					
					</tr>

					<tr>
					<td align="left" colspan="2">Of <b>Rupees</b> <i><u>' .$gt_cash_word.' Only/-</u></i> </td>
					</tr>

					<tr>
					<td align="left" colspan="2">For <i><u>' .$pname_cash_recpt.' </u></i></td>
					</tr>

					</table>
					<br /><br />
					<center><i>This is Computer Generated Proforma Receipt.</i></center>

					
					</body>
					</html>';

		
		if ( get_magic_quotes_gpc() )		   
		$old_limit = ini_set("memory_limit", "16M");		  
		$dompdf = new DOMPDF();
		$dompdf->load_html($receipt_html);
		$dompdf->render();
		$output = $dompdf->output();
		$dompdf->stream("Invoice_".rand(10,1000).".pdf", array("Attachment" => false));		
		exit(0);
	}


?>