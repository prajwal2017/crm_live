<?php
$root = $_SERVER['DOCUMENT_ROOT'];
include('../class/functions.php');
	//include('../class/fpdf.php');

$con = new functions();
session_start(); 
$date = $con->get_datetime();
	//echo date("d/m/Y");

if($_REQUEST['action'] == "getClientDetails"){
	$get_client_details = "SELECT client_id,contact_person,email_id,address FROM client_details WHERE cust_id = ".$_REQUEST['cust_id']." ";
	$get_client_details_result = $con->data_select($get_client_details);
		//$result["json"] = json_encode($get_client_details_result);
	echo json_encode($get_client_details_result);
	exit;
}

if($_REQUEST['action'] == "getReminder"){
	$user_id = $_SESSION['user_id'];
	$tr = "";
	if($_SESSION['role'] == 1 || $_SESSION['role'] == 5){
		$get_reminder = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id WHERE Date(r.r_date) >='".$_REQUEST['sdateSearch']."' and Date(r.r_date) <= '".$_REQUEST['edateSearch']."' ";			
		$result = $con->data_select($get_reminder);
		if($result != "no"){
			for($i=0; $i < count($result) ; $i++){
				$get_reminder_details = "";
				if($result[$i]['r_type'] == "Call"){
					$get_reminder_details = "SELECT * FROM customer_details WHERE user_id = '".$result[$i]['user_id']."' AND cust_id = '".$result[$i]['cust_id']."' ";
				}
				if($result[$i]['r_type'] == "Lead"){
					$get_reminder_details = "SELECT * FROM client_details WHERE bde_user_id = '".$result[$i]['user_id']."' AND cust_id = '".$result[$i]['cust_id']."' ";
				}
				$get_reminder_result = $con->data_select($get_reminder_details);
				$sr = $i + 1;
				$tr = $tr."<tr>
				<td>".$sr."</td><td>".$result[$i]['fname']." ".$result[$i]['lname']."</td><td id='cnm".$sr."'>".$get_reminder_result[0]['company_name']."</td><td id='cno".$sr."'>".$get_reminder_result[0]['contact_number']."</td><td>".$result[$i]['remark']."</td><td>".$result[$i]['r_date']."</td><td>".$result[$i]['r_time']."</td><td>".$result[$i]['r_type']."</td>
				</tr>";
			}
		}else{
			$tr = $tr."<tr><td colspan='8' align='center'>No Records Found</td></tr>";
		}			
	}else{
		$get_reminder = "SELECT ud.fname,ud.lname,ud.role,r.* FROM reminder AS r INNER JOIN user_details AS ud ON ud.user_id = r.user_id WHERE r.user_id= ".$user_id." and Date(r.r_date) >='".$_REQUEST['sdateSearch']."' and Date(r.r_date) <= '".$_REQUEST['edateSearch']."' ";
		$result = $con->data_select($get_reminder);
		if($result > 0){
			for($i=0; $i < count($result) ; $i++){
				$get_reminder_details = "";
				if($result[$i]['r_type'] == "Call"){
					$get_reminder_details = "SELECT * FROM customer_details WHERE user_id = '".$result[$i]['user_id']."' AND cust_id = '".$result[$i]['cust_id']."' ";
				}
				if($result[$i]['r_type'] == "Lead"){
					$get_reminder_details = "SELECT * FROM client_details WHERE bde_user_id = '".$result[$i]['user_id']."' AND cust_id = '".$result[$i]['cust_id']."' ";
				}
				$get_reminder_result = $con->data_select($get_reminder_details);
				$sr = $i + 1;					
				if($result[$i]['r_type'] == "Call"){
					$tr = $tr."<tr>
					<td>".$sr."</td><td id='cnm".$sr."'>".$get_reminder_result[0]['company_name']."</td><td id='cno".$sr."'>".$get_reminder_result[0]['contact_number']."</td><td>".$result[$i]['remark']."</td><td>".$result[$i]['r_date']."</td><td>".$result[$i]['r_time']."</td><td>".$result[$i]['r_type']."</td><td align='center'><div class='btn-group btn-group-sm'>
					<a href='#' data-toggle='modal' data-target='#acceptCall' title='Make Lead' onclick='return acceptCall(".$sr.",".$result[$i]['cust_id'].",".$result[$i]['r_id'].");'><i class='fa fa-thumbs-o-up btn-green btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#rejectCall' title='Reject Call' onclick='return rejectCall(".$sr.",".$result[$i]['cust_id'].",".$result[$i]['r_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#delayCall' title='Set Reminder' onclick='return delayCall(".$sr.",".$result[$i]['cust_id'].",".$result[$i]['r_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
					</div></td><td>---</td><td align='center'><a title='Send Quotation' href='send_quotation_crm.php?contactNumber=".$get_reminder_result[0]['contact_number']."&companyName=".$get_reminder_result[0]['company_name']."&cust_id=".$result[$i]['cust_id']."&r_id=".$result[$i]['r_id']."&q_type=Customer'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>
					</tr>";
				}
				if($result[$i]['r_type'] == "Lead"){
					$tr = $tr."<tr>
					<td>".$sr."</td><td id='cnm".$sr."'>".$get_reminder_result[0]['company_name']."</td><td id='cno".$sr."'>".$get_reminder_result[0]['contact_number']."</td><td>".$result[$i]['remark']."</td><td>".$result[$i]['r_date']."</td><td>".$result[$i]['r_time']."</td><td>".$result[$i]['r_type']."</td>
					<td align='center'><div class='btn-group btn-group-sm'><input type='hidden' id='emailid".$sr."' value='".$get_reminder_result[0]['email_id']."'>
					<a href='#'  data-toggle='modal' data-target='#acceptLead' title='Make Payment' onclick='return acceptLead(".$sr.",".$result[$i]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$i]['r_id'].",2);'><i class='fa fa-money btn-green btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#rejectLead' title='Reject Lead' onclick='return rejectLead(".$sr.",".$result[$i]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$i]['r_id'].",2);'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i></a>  <a href='#' data-toggle='modal' data-target='#delayLead' title='Set Reminder' onclick='return delayLead(".$sr.",".$result[$i]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$i]['r_id'].",2);'><i class='fa fa-clock-o btn-blue btn-sm'></i></a>
					</div></td><td align='center'><a href='#'  data-toggle='modal' data-target='#reassign' title='Re-Assign Lead' onclick='return reassign(".$sr.",".$result[$i]['cust_id'].",".$get_reminder_result[0]['client_id'].",".$result[$i]['r_id'].",2);'><i class='fa fa-mail-forward btn-green btn-sm'></i></a></td>
					<td align='center'><a title='Send Quotation' href='send_quotation_crm.php?name=".$result[$i]['contact_person']."&contactNumber=".$result[$i]['contact_number']."&companyName=".$result[$i]['company_name']."&emailId=".$result[$i]['email_id']."&address=".$result[$i]['address']."&cust_id=".$result[$i]['cust_id']."&r_id=".$result[$i]['r_id']."&q_type=Lead'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>
					</tr>";
				}
			}
		}else{
			$tr = $tr."<tr><td colspan='10' align='center'>No Records Found</td></tr>";
		}
	}
	if($_SESSION['role'] == 1 || $_SESSION['role'] == 5){
		echo $tr = "<thead><tr>
		<th>Sr.No</th><th>Reminder Set By</th><th>Company Name</th><th>Contact No.</th><th>Remark</th><th>Reminder Date</th><th>Reminder Time</th><th>Reminder Type</th>
		</tr></thead>".$tr;
		exit;
	}else{
		echo $tr = "<thead><tr>
		<th>Sr.No</th><th>Company Name</th><th>Contact No.</th><th>Remark</th><th>Reminder Date</th><th>Reminder Time</th><th>Reminder Type</th><th>Action_For_Reminder</th><th>Re-Assign</th><th>Quotation</th>
		</tr></thead>".$tr;
		exit;
	}
}
?>