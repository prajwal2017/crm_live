<?php


$root = $_SERVER['DOCUMENT_ROOT'];    

$dom_path = $root."/crm/dompdf/dompdf_config.inc.php";

include('../class/functions.php');
include('../class/fpdf.php');

require_once($dom_path);

$smtp_path = $root."/crm/smtpmail/class.phpmailer.php";

$con = new functions();
session_start(); 
$date = $con->get_datetime();
$cur_date = date("Y-m-d");



$getQuotation = "SELECT * FROM quotation WHERE quotation_id = '".$_REQUEST['quotation_id']."' ";
$getQuotation_result = $con->data_select($getQuotation);

$get_user = "SELECT * FROM user_details WHERE user_id = '".$getQuotation_result[0]['user_id']."' ";	
$get_user_result = $con->data_select($get_user);

$user_name = $get_user_result[0]['fname']." ".$get_user_result[0]['lname'];
$user_emailId = $get_user_result[0]['email_id'];
$phone_number = "+91 22 27564515";
$mobile_number = $get_user_result[0]['contact_number'];
$reply_to = $_SESSION['email_id'];
$email_id = $getQuotation_result[0]['email_id'];

$role = "";
if($get_user_result[0]['role'] == 1){
    $role = "CEO";
}else if($get_user_result[0]['role'] == 2){
    $role = "BDM";
}else if($get_user_result[0]['role'] == 3){
    $role = "BDE";
}else if($get_user_result[0]['role'] == 4){
    $role = "Telle Caller";
}else if($get_user_result[0]['role'] == 5){
    $role = "RM";
}

$pname = unserialize($getQuotation_result[0]['product_name']);   
$pqty = unserialize($getQuotation_result[0]['product_quantity']);
$prate = unserialize($getQuotation_result[0]['product_rate']);
$codeValidity = unserialize($getQuotation_result[0]['product_validity']);


class PDF extends FPDF
{
// Current column
var $col = 0;
// Ordinate of column start
var $y0;
	
	function Header()
{
    // Page header
    //global $title;

    //$this->SetFont('Arial','B',15);
    //$w = $this->GetStringWidth($title)+6;
   // $this->SetX((210-$w)/2);//we are align
   // $this->SetDrawColor(0,80,100);
   // $this->SetFillColor(230,230,0);
   // $this->SetTextColor(220,50,50);
   // $this->SetLineWidth(1);
   // $this->Cell($w,9,$title,1,1,'C',true);
   // $this->Ln(10);
    // Save ordinate
   // $this->y0 = $this->GetY();
}

function Footer()
{
    // Page footer
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->SetTextColor(128);
    //$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

function SetCol($col)
{
    // Set position at a given column
    $this->col = $col;
    $x = 6+$col*100;//columns
    $this->SetLeftMargin($x);
    $this->SetX($x);
}



function ChapterTitle1($label)
{
    // Title
    $this->SetFont('Arial','',12);
    $this->SetFillColor(200,220,255);
	$this->Cell(0,6,"$label",0,1,'L',true);
       $this->Ln(4);
    // Save ordinate
    $this->y0 = $this->GetY();
}



function AcceptPageBreak()
{
    // Method accepting or not automatic page break
    if($this->col<2)
    {
        // Go to next column
        $this->SetCol($this->col+1);
        // Set ordinate to top
        $this->SetY($this->y0);
        // Keep on page
        return false;
    }
    else
    {
        // Go back to first column
        $this->SetCol(0);
        // Page break
        return true;
    }
}

function ChapterTitle($label)
{
    // Title
    $this->SetFont('Arial','',12);
    $this->SetFillColor(200,220,255);
	//$this->Cell(90,6,"$label",0,1,'L',true);
       $this->Ln(4);
    // Save ordinate
    $this->y0 = $this->GetY();
}

function ChapterBody($file)
{
    // Read text file
    $txt = file_get_contents($file);
    // Font
    $this->SetFont('Times','',12);
    // Output text in a 6 cm width column
    $this->MultiCell(90,6,$txt);
    $this->Ln();
    // Mention
    $this->SetFont('','I');
    
    // Go back to first column
    $this->SetCol(0);
}

function PrintChapter($title, $file)
{
    // Add chapter
    $this->AddPage();
    $this->ChapterTitle($title);
    $this->ChapterBody($file);
}
}
	

// fpdf object
$pdf = new PDF();
$pdf->AddPage();

$pdf->Image('../img/img1.jpg',203,30,4);

$pdf->Image('../img/img1.jpg',203,90,4);

/*$pdf->SetFont('Arial','BI',12);
$pdf->SetTextColor(200,139,47);
$pdf->text(160,165,'PROPOSAL');
$pdf->text(126,175,$getQuotation_result[0]['contact_person']);
$pdf->text(126,185,$getQuotation_result[0]['company_name']);
			*/

$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(12,20,47);
$pdf->text(170,20,'DATE: '. date("d/m/Y"));

$pdf->SetFont('Arial','BU',12);
$pdf->SetTextColor(12,20,47);
$pdf->text(93,30,'PROPOSAL');
$pdf->SetFont('Arial','BI',12);
$pdf->SetTextColor(200,139,47);
$pdf->text(12,60,'To,');
$pdf->text(12,70,$getQuotation_result[0]['contact_person']);
$pdf->text(12,80,$getQuotation_result[0]['company_name']);

$pdf->Image('../img/proposal.jpg',12,100,185);   


$pdf->Image('../img/mobisoft.png',120,260,80);
  
  
  
 
$pdf->SetTextColor(20,39,47);
$pdf->PrintChapter( '','../mis.txt');

$pdf->Image('../img/img1.jpg',4,30,4);

$pdf->Image('../img/img1.jpg',4,90,4);



$pdf->Image('../img/img1.jpg',203,30,4);

$pdf->Image('../img/img1.jpg',203,90,4);

$pdf->Image('../img/weare.jpg',16,5,180);//We are
$pdf->SetTextColor(220,50,50);
$pdf->text(11,34,'About Us');

$pdf->SetTextColor(220,50,50);
$pdf->text(11,160,'Our Mission');

$pdf->SetTextColor(220,50,50);
$pdf->text(11,191,'Vision');

$pdf->SetTextColor(220,50,50);
$pdf->text(11,219,'Services We Offer');


$pdf->SetTextColor(220,50,50);
$pdf->text(110,100,'Some Of Key Services Are:');
$pdf->SetFont('Arial','BI',11);
$pdf->SetTextColor(50,160,230);
$pdf->text(112,108,' 1 .Bulk SMS');
$pdf->text(112,114,' 2 .SHORT code');

$pdf->text(112,120,' 3 .LONG code');
$pdf->text(112,126,' 4 .Email');
$pdf->text(112,132,' 5 .Web Development');
$pdf->text(112,138,' 6 .SEO');
$pdf->text(112,144,' 7 .S/W Development');

$pdf->text(112,161,' 1 .Travel Portal ');
$pdf->text(112,167,' 2 .Mobile Recharge Portal ');
$pdf->text(112,173,'3 .E-commerce ');
$pdf->text(112,179,'4 .ERP');
$pdf->text(112,184,'5 .CRM');

$pdf->SetTextColor(220,50,50);
$pdf->text(110,155,'Our other custom made applications.');
$pdf->Image('../img/mobisoft.png',120,260,80);

$pdf->AddPage();
$pdf->Image('../img/img1.jpg',10,30,5);

$pdf->Image('../img/img1.jpg',10,90,5);


$pdf->Image('../img/img1.jpg',200,30,5);

$pdf->Image('../img/img1.jpg',200,60,5);



$pdf->SetFont('Arial','B',15);
	
//$pdf->Image('img/img.jpg',18,5,180);//Greeting image


$pdf->SetFont('Arial','',11);

$pdf->SetTextColor(20,39,47);
$pdf->text(30,50,'To,');
$pdf->SetFont('Arial','',11);
$pdf->SetTextColor(220,39,47);



$pdf->text(30,56,$getQuotation_result[0]['contact_person']);
$pdf->text(30,62,$getQuotation_result[0]['company_name']);
$pdf->text(30,68,$getQuotation_result[0]['contact_number']);
$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(20,39,47);
$pdf->text(30,83,'Sub :- ');
$pdf->text(43,83,"Quotation");

$pdf->SetFont('Arial','',11);
$pdf->text(30,95,'Dear Sir,');
$pdf->text(30,101,'As  Per your Conversation here under given the rate for required services');

 
$pdf->SetFont('Arial','',11);
$pdf->SetFillColor(0, 255,0);
	


$pdf->SetFont( "Arial","B",10);
	

$pdf->setXY(28,112);
$pdf->SetFillColor(128,128,128);


/*$pdf->Cell (41,9,"Product",1,0,"C");
$pdf->Cell( 35,9,"Qty",1,0,"C");
$pdf->Cell (40,9,"Amount",1,0,"C");
$pdf->Cell (31,9,"Validity",1,0,"C");*/

$pdf->Cell (41,9,"Product",1,0,"C");
$pdf->Cell( 25,9,"Qty",1,0,"C");
$pdf->Cell( 25,9,"Rate",1,0,"C");
$pdf->Cell (35,9,"Amount",1,0,"C");
$pdf->Cell (31,9,"Validity",1,0,"C");

$pdf->SetFont( "Arial","",10);
$n = 125;
$c = 121;
for ($i=0; $i < count($pname); $i++) { 

	$pdf->setXY(28,$c);
	$pdf->SetFillColor(128,128,128);

	/*$pdf->Cell (41,5,"",1,0,"C");
	$pdf->Cell( 35,5,"",1,0,"C");
	$pdf->Cell (40,5,"",1,0,"C");
	$pdf->Cell (31,5,"",1,0,"C");

	$pdf->text(31,$n,$pname[$i]);
	$pdf->text(73, $n,$pqty[$i]);
	$pdf->text(107, $n,$pqty[$i] * $prate[$i]);
    $pdf->text(150, $n,$codeValidity[$i]);*/

    $pdf->Cell (41,5,"",1,0,"C");
    $pdf->Cell( 25,5,"",1,0,"C");
    $pdf->Cell( 25,5,"",1,0,"C");
    $pdf->Cell (35,5,"",1,0,"C");
    $pdf->Cell (31,5,"",1,0,"C");

    $pdf->text(31,$n,$pname[$i]);
    $pdf->text(76, $n,$pqty[$i]);
    $pdf->text(103, $n,$prate[$i]);
    $pdf->text(132, $n,$pqty[$i] * $prate[$i]);
    $pdf->text(162, $n,$codeValidity[$i]);
	
	$n = $n+5;
	$c = $c+5;
	
}

$pdf->SetFont( "Arial","B",10);
$pdf->text(30, $n+5,'* NOTE');
$pdf->text(35, $n+10,'1) Service Tax (15%)EXTRA');
$pdf->text(35, $n+15,'2) Quotation valid for 7 days only');

$pdf->Image('../img/img2.jpg',20,182,69);//$n+25
$pdf->Image('../img/img3.jpg',20,182,2);//$n+25
$pdf->Image('../img/img3.jpg',85,182,2);//$n+25

$pdf->Image('../img/img2.jpg',20,244,69);//$n+87


$pdf->Image('../img/mobi.png',23,185,60);//$n+28
 //$pdf->Image('img/profile-pic.jpg',20,166,69);

$pdf->text(106, 190,'Payment'); 
$pdf->text(132, 190,':  Prepaid Mode');

$pdf->text(106, 195,'Bank Details'); 
$pdf->text(132, 195,':  ICICI Bank');

$pdf->text(106, 200,'Account Name'); 
$pdf->text(132, 200,':  Mobisoft Technology India Pvt Ltd');

$pdf->text(106, 205,'Account No'); 
$pdf->text(132, 205,':  015105012883');

$pdf->text(106, 210,'IFSC'); 
$pdf->text(132, 210,':  ICIC0000151');

$pdf->text(106, 215,'Bank Branch'); 
$pdf->text(132, 215,':  VASHI');



$pdf->SetFont('Arial','',10);
$pdf->text(106, 230,'Terms & Conditions : -');
$pdf->text(106, 235,'1. Rate are subject to change as per TRAI');

$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(150,39,47);
$pdf->text(112, 240,'www.mobisofttech.co.in');

/*
$pdf->Image('../img/img2.jpg',20,166,69);
$pdf->Image('../img/img3.jpg',20,166,2);
$pdf->Image('../img/img3.jpg',85,166,2);

$pdf->Image('../img/img2.jpg',20,228,69);


$pdf->Image('../img/mobi.png',23,169,60);
 //$pdf->Image('img/profile-pic.jpg',20,166,69);

$pdf->text(106, 180,'Payment      : Prepaid Mode');
$pdf->text(106, 186,'Bank Details : ICICI Bank');

$pdf->text(106, 192,'Account Name : Mobisoft Technology India Pvt Ltd');
$pdf->text(106, 198,'Account No   : 015105012883');
$pdf->text(106, 204,'IFSC         : ICIC0000151');
$pdf->text(106, 210,'Bank Branch  : VASHI');*/



/*$pdf->SetFont('Arial','',10);
$pdf->text(106, 216,'Terms & Conditions : -');
$pdf->text(106, 222,'1. Rate are subject to change as per TRAI');

$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(150,39,47);
$pdf->text(112, 230,'www.mobisofttech.co.in');
*/
$pdf->Image('../img/mobisoft.png',120,270,80);






$pdf->AddPage();

$pdf->Image('../img/ourclient.jpg',18,5,180);//Our Client List

$pdf->Image('../img/cl1.jpg',30,55,70);
$pdf->Image('../img/new_client.jpg',120,55,70);
//$pdf->SetTextColor(220,50,50);
//$pdf->text(24,30,'OUR CLIENT LIST.');


   

//$pdf->Image('images/lable1.png',22,170,170);
$pdf->SetFont('Arial','',14);

$pdf->text(110, 210,'Mobisoft Technology India Pvt. Ltd.');

$pdf->SetFont('Arial','',10);

$pdf->text(110, 216,$user_name);
$pdf->text(110, 222,$user_emailId);
$pdf->text(110, 228,'Tel:- '.$phone_number);
$pdf->text(110, 234,'Mob:- '.$mobile_number);
$pdf->text(110, 240,'www.mobisofttech.co.in');


$pdf->Image('../img/mobisoft.png',120,267,80);



$pdf->Image('../img/img1.jpg',10,30,5);

$pdf->Image('../img/img1.jpg',10,60,5);

$pdf->Image('../img/img1.jpg',200,30,5);

$pdf->Image('../img/img1.jpg',200,60,5);


 	$filename = "Mobisoft Business Proposal.pdf";

   // encode data (puts attachment in proper format)
    $pdfdoc = $pdf->Output('../PDF_File/Mobisoft Business Proposal.pdf', "F");
    //$attachment = chunk_split(base64_encode($pdfdoc));
 

	$to = $email_id;
	$subject = "MobiSoft Technology";
	$attachment_name = array('../PDF_File/Mobisoft Business Proposal.pdf','../PDF_File/Presentation_Mobisoft Technology India Pvt Ltd [Autosaved].ppsx',);
	//$attachment_name = array('PDF_File/Mobisoft Business Proposal.pdf');
	//$attachment_name = "doc/Mobisoft Business Proposal.pdf","doc/MobiSoftppt.ppt";
	$body = "

	<font color='#500050'><b>Dear Sir/Madam, </b><br><br>
	           
	Greetings from Mobisoft Technology India Pvt. Ltd.<br><br>
 
    Hope my email finds you well & doing great. <br><br>
     
    Mobisoft  is India's Top Growing <b>Bulk SMS Service Provider when it comes to Bulk SMS Solution</b>.<br><br>
     
    At current we cater over 2000+ Customers with 100+ Enterprise across Industries in India varying from major Banks to retails by end to end. Our messaging routes are being used by major Enterprises, varying from  Religare, India Bulls, Angle Brokings  and More.<br><br>
     
    If you are looking for sms service routes, we can cater your <b>Indian</b> termination requirements for both <b>Promotional & Transactional Traffic</b>.<br><br><br>
                                                                                                                                                                                       
     
    <font size='3'>1:- Promotional SMS :–</font><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Features:</b>
    <ul>
    <li> Promo on NON-DND Numbers</li>
    <li> DND Numbers list can be extracted and will not be charged.</li>
    <li> Delivery Time – Instant Delivery</li>
    <li> Delivery Report - Instant Delivery Report, 100% of Guaranteed Delivery</li>
    <li> To get activate it takes maximum 10 Minutes of time</li>
    </ul><br>
    <font size='3'>2:- Transactional SMS :-</font><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Features:</b>
    <ul>
    <li> Six Character Sender ID will be provided (ONLY Alphabets).</li>
    <li> Web Interface/HTTP API will be given.</li>
    <li> Messages gets delivered to DND Numbers also.</li>
    <li> It works on template basis. (Provide your sample to support@mobisofttech.co.in) . The templates with 70% of Static Content and 30% Variables.</li>
    <li> We would require details on how your customers are subscribing or getting registered and one sample of subscription form is mandatory.</li>
    <li> To get activate it takes 30 – 45 Minutes of time.</li>
    </ul><br>
    To know our exciting pricing and other features, kindly reply to the mail so that our sales team call back to you instantly.<br><br>
     
    We are also dealing in Mobile Applications and Products as below:<br><br>
     
    <b>
    1. Short Code & Long Code<br>
    2. Virtual Number which is Ten digit Mobile number<br>
    3. IVR, Voice services (Click to Call & Missed Call)<br>
    4. Website Design, Application development ( customize)<br>
    5. Software development.
    </b><br><br><br>
     
     
    Looking forward to hear from you soon .<br><br>

    Regards, <br><br></font>

    <font size='3' color='#CC0000'><b>".$user_name." // MobiSoft (".$role.")</b></font><br>
    <hr>
    <font color='#365F91'><b>MobisoftTechnology India Pvt. Ltd.</b><br>
    
        Office No. H-004, P Level, Tower-3,<br><br>

        Railway Station Complex, Sector 11,<br><br>

        CBD Belapur, Navi Mumbai 400 614.<br><br>

        India.<br><br><br>

    Mobile : + 91 ".$mobile_number."<br><br>
     
    Phone : +91 22 27574515<br><br><br>
     

     
    Email: ".$user_emailId."<br><br>

    www.mobisofttech.co.in | www.makemysms.in<br></font>

    ------------------------------------------------------------------------------------------------------------------------------------------------------
    <br><br>
    This email, and any attachments, may be confidential and also privileged. If you are not the
    intended recipient, please notify the sender and delete all copies of this transmission along with
    any attachments immediately. You should not copy or use it for any purpose, nor disclose its
    contents to any other person.All rights reserved Mobisofttechnology India Pvt Ltd 2011
    <br><br>
    ------------------------------------------------------------------------------------------------------------------------------------------------------

    ";


	

	$result2 = $con->send_quotation_mail($subject,$body,$to,$attachment_name,$reply_to,$user_name);
    echo $result2;
    unlink('../PDF_File/Mobisoft Business Proposal.pdf');
	
	

?>

<?php
    ob_end_flush(); // Flush the output from the buffer
?>