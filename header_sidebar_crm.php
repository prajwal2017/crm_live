<?php
include('class/functions.php');
session_start();

if(!isset($_SESSION['user_id'])){
	header('Location: index.php');
}

$con = new functions();
$userid = $_SESSION['user_id'];
$cur_date = date("Y-m-d");
$qry = "select * from customer_details cd inner join reminder AS r on cd.cust_id = r.cust_id inner join user_details ud on ud.user_id = r.user_id where r.user_id = ".$userid." and r.r_date = '".$cur_date."'";
if($_SESSION['role'] == "1" || $_SESSION['role'] == "5")
{
	$qry = "select * from customer_details cd inner join reminder AS r on cd.cust_id = r.cust_id inner join user_details ud on ud.user_id = r.user_id where r.r_date = '".$cur_date."'";
}else if($_SESSION['role'] == "6"){
	$qry = "SELECT id FROM confirm_user_temp WHERE status = 0 ";

}else if($_SESSION['role'] == "7"){
	$qry = "SELECT id FROM credit_request WHERE status = 0 ";

}

$result = $con->data_select($qry);

$reminder_count = "";

if($result != 'no')
{
	$_SESSION['reminder'] = sizeof($result);
	$reminder_count = sizeof($result);

}
else
{
	$_SESSION['reminder'] = "0";        
}

?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Mobisoft CRM</title>

	<!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->
	<link href="css/plugins/pace/pace.css" rel="stylesheet">
	<script src="js/plugins/pace/pace.js"></script>
	<script src="js/plugins/ajaxcall/jquery-1.11.1.min.js"></script>
	<script src="js_functions/common_js_functions.js"></script>

	<!-- GLOBAL STYLES - Include these on every page. -->
	<link href="css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
	<link href="icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<!-- PAGE LEVEL PLUGIN STYLES -->
	<link href="css/plugins/messenger/messenger.css" rel="stylesheet">
	<link href="css/plugins/messenger/messenger-theme-flat.css" rel="stylesheet">
	<link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	<!--    <link href="css/plugins/morris/morris.css" rel="stylesheet"> -->
	<link href="css/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet">
	<link href="vendor/datatable/css/datatables.css" rel="stylesheet">
	<link rel="stylesheet" href="vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css" />

    <!-- <link href="css/plugins/bootstrap-tokenfield/tokenfield-typeahead.css" rel="stylesheet">
    	<link href="css/plugins/bootstrap-tokenfield/bootstrap-tokenfield.css" rel="stylesheet"> -->
    	<link href="css/plugins/tokenfield/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">

    	<!-- PAGE LEVEL PLUGIN STYLES -->
    	<link href="css/plugins/summernote/summernote.css" rel="stylesheet">
    	<link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    	<!-- THEME STYLES - Include these on every page. -->
    	<link href="css/style.css" rel="stylesheet">
    	<link href="css/plugins.css" rel="stylesheet">

    	<!-- THEME DEMO STYLES - Use these styles for reference if needed. Otherwise they can be deleted. -->
    	<link href="css/demo.css" rel="stylesheet">
    	<link href="css/main.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
  <![endif]-->




</head>
<?php
    //include('class/functions.php');
    //$con=new functions();
    //session_start();

if((!isset($_SESSION['user_id'])) && empty($_SESSION['user_id'])) {
       //echo 'Set and not empty, and no undefined index error!');
	header('Location:index.php');
	exit;
}
$session_qry = "SELECT * FROM user_details WHERE user_id = '".$_SESSION['user_id']."' ";
$session_result = $con->data_select($session_qry);

    /*echo "<pre>";
    print_r($result);
    exit;*/
    ?>

    <script type="text/javascript">
    	$(function(){

       // getReminderCount(); 

       setInterval(function(){

       	getReminderCount(); 

       }, 60000 * 5);

   });

    	function  getReminderCount(){

    		$.ajax({
    			url:"ajax_service/mks_customer_ajax.php",
    			data:"action=getReminderCount",
    			dataType:"html",
    			success:function(data){
                //alert("inside ajax");
                //alert(data);
                //console.log(data);
                $('.reminder_count').html(data);


            }
        });

    	}
    </script>
    <body>

    	<div id="wrapper">

    		<!-- begin TOP NAVIGATION -->
    		<nav class="navbar-top" role="navigation">

    			<!-- begin BRAND HEADING -->
    			<div class="navbar-header">
    				<button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
    					<i class="fa fa-bars"></i> Menu
    				</button>
    				<div class="navbar-brand">
    					<a href="main_report_dashboard_crm.php">
    						<img src="img/mobisoftlogo12.png" data-1x="img/mobisoftlogo12.png" data-2x="img/mobisoftlogo12.png" class="hisrc img-responsive" alt="">
    					</a>
    				</div>
    			</div>
    			<!-- end BRAND HEADING -->

    			<div class="nav-top">

    				<!-- begin LEFT SIDE WIDGETS -->
    				<ul class="nav navbar-left">
    					<li class="tooltip-sidebar-toggle">
    						<a href="#" id="sidebar-toggle" data-toggle="tooltip" data-placement="right" title="Sidebar Toggle">
    							<i class="fa fa-bars"></i>
    						</a>
    					</li>
    					<!-- You may add more widgets here using <li> -->
    					</ul>
    					<ul class="nav navbar-right">

    						<!-- begin MESSAGES DROPDOWN -->
    						<li class="">
    							<?php
    							if($_SESSION['role'] == "6"){
    								echo '<a href="view_new_user.php" class="alerts-link " >';
    							}else if($_SESSION['role'] == "7"){
    								echo '<a href="credit_request.php" class="alerts-link " >';
    							}else{
    								echo '<a href="view_reminder_crm.php" class="alerts-link " >';
    							}
    							?>

    							<i class="fa fa-bell"></i>
    							<?php
    							if($reminder_count > 0){
    								echo '<span class="number reminder_count">'.$reminder_count.'</span>';
    							}else{
    								echo '<span class="number reminder_count">0</span>';
    							}

    							?> 
    							<!-- <span class="number">9</span> --><!-- <i class="fa fa-caret-down"></i> -->
    						</a>
    						<!-- /.dropdown-menu -->
    					</li>
    					<!-- /.dropdown -->
    					<!-- end MESSAGES DROPDOWN -->

    				</ul>

    			</div>
    			<!-- /.nav-top -->
    		</nav>
    		<!-- /.navbar-top -->
    		<!-- end TOP NAVIGATION -->

    		<!-- begin SIDE NAVIGATION -->
    		<nav class="navbar-side" role="navigation">
    			<div class="navbar-collapse sidebar-collapse collapse">
    				<ul id="side" class="nav navbar-nav side-nav">
    					<!-- begin SIDE NAV USER PANEL -->
    					<li class="side-user hidden-xs">
    						<!-- <img class="img-circle" src="" alt=""> -->
    						<p class="welcome">
    							<i class="fa fa-key"></i> Logged in as
    						</p>
    						<p class="name tooltip-sidebar-logout">
    							<?php 
    							echo $session_result[0]['fname'];
    							?>
    							<span class="last-name"><?php echo $session_result[0]['lname'];?></span> <a style="color: inherit" class="logout_open" href="logout_crm.php" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
    						</p>
    						<div class="clearfix"></div>
    					</li>
    					<!-- end SIDE NAV USER PANEL -->
    					<!-- begin SIDE NAV SEARCH -->

    					<!-- end SIDE NAV SEARCH -->
    					<!-- begin DASHBOARD LINK -->
    					<li>
    						<a class="active" href="main_report_dashboard_crm.php">
    							<i class="fa fa-dashboard"></i> Dashboard
    						</a>
    					</li>
    					<!-- end DASHBOARD LINK -->

    					<?php 

    					if($_SESSION['role'] == "5" ){
    						echo ' <li class="panel">
    						<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#Mail">
    						<i class="fa fa-user"></i> E-Mail <i class="fa fa-caret-down"></i>
    						</a>
    						<ul class="collapse nav" id="Mail">

    						<li>
    						<a href="compose_mail.php">
    						<i class="fa fa-angle-double-right"></i> Send Mail
    						</a>
    						</li>                          

    						</ul>
    						</li>';

    						echo ' <li class="panel">
    						<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#SMS">
    						<i class="fa fa-user"></i> SMS <i class="fa fa-caret-down"></i>
    						</a>
    						<ul class="collapse nav" id="SMS">

    						<li>
    						<a href="compose_sms.php">
    						<i class="fa fa-angle-double-right"></i> Send SMS
    						</a>
    						</li>                          

    						</ul>
    						</li>';
    					}

    					?>
    					<!-- begin CHARTS DROPDOWN -->
    					<?php
    					if($_SESSION['role'] != "6" && $_SESSION['role'] != "7"){
    						?>
    						<li class="panel">
    							<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#charts">
    								<i class="fa fa-user"></i> Customer <i class="fa fa-caret-down"></i>
    							</a>
    							<ul class="collapse nav" id="charts">



    								<?php

    								if($_SESSION['role'] != "1"){
    									?>
    									<li>
    										<a href="add_customer_crm.php">
    											<i class="fa fa-angle-double-right"></i> Add Customer
    										</a>
    									</li>
    									<?php  
    								}
    								?>

    								<li>
    									<a href="view_customer_crm.php">
    										<i class="fa fa-angle-double-right"></i> View Customer
    									</a>
    								</li>
    								<?php
    								if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5" ){
    									echo "<li>
    									<a href='view_reject_call.php'>
    									<i class='fa fa-angle-double-right'></i> Reject Calls
    									</a>
    									</li>";
    								}
    								?>

    							</ul>
    						</li>
    						<?php
    					}
    					?>
    					<!-- end CHARTS DROPDOWN -->
    					<!-- begin FORMS DROPDOWN -->

    					<?php
    					if($_SESSION['role'] != "6" && $_SESSION['role'] != "7"){
    						?>
    						<li class="panel">
    							<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#tables">
    								<i class="fa fa-thumbs-o-up"></i> Leads<i class="fa fa-caret-down"></i>
    							</a>
    							<ul class="collapse nav" id="tables">
    								<li>
    									<a href="view_leads_crm.php">
    										<i class="fa fa-angle-double-right"></i>View Leads
    									</a>
    								</li>

    								<?php
    								if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5" ){
    									echo "<li>
    									<a href='view_reject_leads.php'>
    									<i class='fa fa-angle-double-right'></i> Reject Leads
    									</a>
    									</li>";
    								}
    								?>

                            <!-- <li>
                                <a href="view_reject_leads.php">
                                    <i class="fa fa-angle-double-right"></i>Reject Leads
                                </a>
                            </li> -->

                        </ul>
                    </li>
                    <?php
                }
                ?>
                <!-- end TABLES DROPDOWN -->

                <?php
                if($_SESSION['role'] == "9" ||$_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "5" || $_SESSION['role'] == "8" ){
                	echo "<li class='panel'>
                	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#message-center'>
                	<i class='fa fa-users'></i>Client  <i class='fa fa-caret-down'></i>
                	</a>
                	<ul class='collapse nav' id='message-center'>
                	<li>
                	<a href='pending_client_crm.php'>
                	<i class='fa fa-angle-double-right'></i> Pending Client
                	</a>
                	</li>
                	<li>
                	<a href='pending_approval_crm.php'>
                	<i class='fa fa-angle-double-right'></i> Pending Approval
                	</a>
                	</li>
                	<li>
                	<a href='view_client_crm.php'>
                	<i class='fa fa-angle-double-right'></i> Confirm Client
                	</a>
                	</li>
                	<li>
                	<a href='view_client_balance.php'>
                	<i class='fa fa-angle-double-right'></i> Client Balance
                	</a>
                	</li>
                	";
                	if($_SESSION['role'] == "1" || $_SESSION['role'] == "5")
                	{
                                /*echo "<li>
                                        <a href='client_history_crm.php'>
                                            <i class='fa fa-angle-double-right'></i> Client History
                                        </a>
                                    </li>

                                    <li>
                                        <a href='view_all_client_crm.php'>
                                            <i class='fa fa-angle-double-right'></i> All Client
                                        </a>
                                    </li>
                                    ";*/
                                }

                                echo "</ul></li>";   

                            }

                            if($_SESSION['role'] == "1" || $_SESSION['role'] == "2"  || $_SESSION['role'] == "3" || $_SESSION['role'] == "5")
                            {
                            	echo "<li class='panel'>
                            	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#sales-elements'>
                            	<i class='fa fa-bullseye'></i>Sales Target<i class='fa fa-caret-down'></i>
                            	</a>
                            	<ul class='collapse nav' id='sales-elements'>";

                            	if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5")
                            	{
                            		echo "<li>
                            		<a href='add_salestarget_crm.php'>
                            		<i class='fa fa-angle-double-right'></i>Add Sales Target
                            		</a>
                            		</li>"; 
                            	}

                            	if($_SESSION['role'] == "1" || $_SESSION['role'] == "2"  || $_SESSION['role'] == "3" || $_SESSION['role'] == "5")
                            	{
                            		echo " <li>
                            		<a href='view_salestarget_crm.php'>
                            		<i class='fa fa-angle-double-right'></i>View Sales Target
                            		</a>
                            		</li>

                            		</ul>
                            		</li>";
                            	}

                            }


                            if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "4" || $_SESSION['role'] == "5")
                            {

                            	echo "<li class='panel'>
                            	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#target-elements'>
                            	<i class='fa fa-bullseye'></i>Lead Target<i class='fa fa-caret-down'></i>
                            	</a>
                            	<ul class='collapse nav' id='target-elements'>";

                            	if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){

                            		echo " <li>
                            		<a href='add_leadtarget_crm.php'>
                            		<i class='fa fa-angle-double-right'></i>Add Lead Target
                            		</a>
                            		</li> ";
                            	}

                            	if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "4" || $_SESSION['role'] == "5"){

                            		echo " <li>
                            		<a href='view_leadtarget_crm.php'>
                            		<i class='fa fa-angle-double-right'></i>View Lead Target
                            		</a>
                            		</li></ul></li>";       

                            	}


                            }

                            #if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "5")
                            if($_SESSION['role'] == "1")
                            {
                            	echo "<li>
                            	<a href='view_user_crm.php'>
                            	<i class='fa fa-angle-double-right'></i> View User
                            	</a>
                            	</li>
                            	";
                            }
                            if($_SESSION['role'] == "9" || $_SESSION['role'] == "2" || $_SESSION['role'] == "8")
                            {
                                echo "<li>
                                <a href='credit_request_test.php'>
                                <i class='fa fa-angle-double-right'></i> Credit Request
                                </a>
                                </li>
                                ";
                            }
                            ?> 

                            <?php
                            if($_SESSION['role'] != "6" && $_SESSION['role'] != "7"){
                            	?>
                            	<li>
                            		<a href='view_reminder_crm.php'>
                            			<i class='fa  fa-bell'></i> View Reminder
                            		</a>
                            	</li>
                            	<li>
                            		<a href='proforma_invoice.php'>
                            			<i class='fa fa-file-text-o'></i> Send Proforma Invoice
                            		</a>
                            	</li>
                                <li>
                                    <a href='final_invoice.php'>
                                        <i class='fa fa-file-text-o'></i> Send Final Invoice
                                    </a>
                                </li>
                            	<li class="panel">
                            		<a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#Quotation">
                            			<i class="fa fa-briefcase"></i>Quotation<i class="fa fa-caret-down"></i>
                            		</a>
                            		<ul class="collapse nav" id="Quotation">
                            			<li>
                            				<a href="send_quotation_crm.php">
                            					<i class="fa fa-angle-double-right"></i>Send Quotation
                            				</a>
                            			</li>
                            			<li>
                            				<a href="view_quotation.php">
                            					<i class="fa fa-angle-double-right"></i>View Quotation
                            				</a>
                            			</li>
                            		</ul>
                            	</li>
                            	<?php
                            }
                            ?>

                            <?php
                            if($_SESSION['role'] == "1"){

                            	echo "
                            	<li class='panel'>
                            	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#mail'>
                            	<i class='fa fa-inbox'></i>Mail<i class='fa fa-caret-down'></i>
                            	</a>
                            	<ul class='collapse nav' id='mail'>
                            	<li>
                            	<a href='compose_mail.php'>
                            	<i class='fa fa-angle-double-right'></i>Send Mail
                            	</a>
                            	</li>

                            	</ul>
                            	</li>";

                            	echo "
                            	<li class='panel'>
                            	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#forms'>
                            	<i class='fa fa-shopping-cart'></i>Product<i class='fa fa-caret-down'></i>
                            	</a>
                            	<ul class='collapse nav' id='forms'>
                            	<li>
                            	<a href='add_product_crm.php'>
                            	<i class='fa fa-angle-double-right'></i>Add Product
                            	</a>
                            	</li>
                            	<li>
                            	<a href='view_product_crm.php'>
                            	<i class='fa fa-angle-double-right'></i>View Product
                            	</a>
                            	</li>

                            	</ul>
                            	</li>";

                            	echo "                        
                            	<li class='panel'>
                            	<a href='javascript:;' data-parent='#side' data-toggle='collapse' class='accordion-toggle' data-target='#ui-elements'>
                            	<i class='fa fa-user'></i>User <i class='fa fa-caret-down'></i>
                            	</a>
                            	<ul class='collapse nav' id='ui-elements'>
                            	<li>
                            	<a href='user_registration_crm.php'>
                            	<i class='fa fa-angle-double-right'></i>User Registration
                            	</a>
                            	</li>
                            	<li>
                            	<a href='view_user_crm.php'>
                            	<i class='fa fa-angle-double-right'></i> View User
                            	</a>
                            	</li>

                            	</ul>
                            	</li>";

                            }
                            ?> 

                            <?php

                            if($_SESSION['role'] == "6"){
                            	?>
                            	<li>
                            		<a href="view_new_user.php">
                            			<i class="fa fa-angle-double-right"></i> New Users
                            		</a>
                            	</li>
                            	<?php  
                            }
                            if($_SESSION['role'] == "7" || $_SESSION['role'] == "1"){
                            	?> 
                            	<li>
                            		<a href="credit_request.php">
                            			<i class="fa fa-angle-double-right"></i> Credit Request
                            		</a>
                            	</li>
                                <li>
                                   <!--  <a href="credit_request_test.php">
                                        <i class="fa fa-angle-double-right"></i> Credit Request New
                                    </a> -->
                                </li>
                            	<?php  
                            } 
                            if($_SESSION['role'] == "3"){
                            	?> 
                            	<!-- <li>
                            		<a href="credit_request_new.php">
                            			<i class="fa fa-angle-double-right"></i> Credit Request New
                            		</a>
                            	</li> -->
                                <li class="panel">
                                    <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ClientRequest">
                                        <i class="fa fa-briefcase"></i>Credit Request<i class="fa fa-caret-down"></i>
                                    </a>
                                    <ul class="collapse nav" id="ClientRequest">
                                        <li>
                                            <a href="credit_request_new.php">
                                                <i class="fa fa-angle-double-right"></i>Credit Request Made
                                            </a>
                                        </li>
                                        <li>
                                            <a href="client_balance.php">
                                                <i class="fa fa-angle-double-right"></i>Client Balance
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php  
                            }                          
                            ?> 
                            <?php 
                            if($_SESSION['role'] == "9" || $_SESSION['role'] == "1" || $_SESSION['role'] == "7" ||$_SESSION['role'] == "2" ||$_SESSION['role'] == "8"  ){
                            ?>
                             <li>
                                <a href='accounts_approval.php'>
                                    <i class='fa fa-wrench'></i>Accounts Approval
                                </a>
                            </li>
                            <?php } ?>
                            <li>
                            	<a href='change_password.php'>
                            		<i class='fa fa-wrench'></i>Change Password
                            	</a>
                            </li>

                            <!-- end MESSAGE CENTER DROPDOWN -->
                            <!-- begin PAGES DROPDOWN -->
                    <!-- <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages">
                            <i class="fa fa-files-o"></i> Pages <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages">
                            <li>
                                <a href="profile.html">
                                    <i class="fa fa-angle-double-right"></i> User Profile
                                </a>
                            </li>
                            <li>
                                <a href="invoice.html">
                                    <i class="fa fa-angle-double-right"></i> Invoice
                                </a>
                            </li>
                            <li>
                                <a href="pricing.html">
                                    <i class="fa fa-angle-double-right"></i> Pricing Tables
                                </a>
                            </li>
                            <li>
                                <a href="faq.html">
                                    <i class="fa fa-angle-double-right"></i> FAQ Page
                                </a>
                            </li>
                            <li>
                                <a href="search-results.html">
                                    <i class="fa fa-angle-double-right"></i> Search Results
                                </a>
                            </li>
                            <li>
                                <a href="login.html">
                                    <i class="fa fa-angle-double-right"></i> Login Basic
                                </a>
                            </li>
                            <li>
                                <a href="login-social.html">
                                    <i class="fa fa-angle-double-right"></i> Login Social
                                </a>
                            </li>
                            <li>
                                <a href="404.html">
                                    <i class="fa fa-angle-double-right"></i> 404 Error
                                </a>
                            </li>
                            <li>
                                <a href="500.html">
                                    <i class="fa fa-angle-double-right"></i> 500 Error
                                </a>
                            </li>
                            <li>
                                <a href="blank.html">
                                    <i class="fa fa-angle-double-right"></i> Blank Page
                                </a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- end PAGES DROPDOWN -->
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->