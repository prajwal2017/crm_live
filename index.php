<?php  
    include('class/functions.php');
    $con=new functions();
    session_start();
    if(isset($_POST['submit']))
    {
        $email_id = $_POST['emailid'];
        $password = $_POST['password'];
       
        /*$qry="SELECT * FROM user_details WHERE email_id='$email_id' AND password='$password' AND flag=1";
        
        $result=$con->data_select($qry);
        echo "<pre>";
        print_r($result);
        echo $qry;
        exit;*/
        // hmara@123
         $conn = new mysqli('localhost', 'root', '', 'mobisoft_crm');
          if($conn->connect_error){
            die("Fatal Error: Can't connect to database: ". $conn->connect_error);
          }
         $stmt = $conn->prepare("SELECT lname,fname,email_id,user_id,role FROM `user_details` WHERE email_id = ? && `password` = ?") or die(mysqli_error());
         $stmt->bind_param('ss', $email_id, $password);
         $stmt->execute();
         $stmt->bind_result($lname,$fname,$email_id,$user_id,$role);
         $stmt->store_result();
         if($stmt->num_rows == 1)  //To check if the row exists
            {
                if($stmt->fetch()) //fetching the contents of the row
                {
                    $_SESSION['user_full_name']= $fname. " ".$lname;
                    $_SESSION['email_id']=$email_id;
                    $_SESSION['user_id']=$user_id;
                    $_SESSION['role']=$role;
                    $conn->close();
                    
                   // header('Location: main_report_dashboard_crm.php');
                    if($role == 6){
                        header('Location: main_report_dashboard_crm.php');
                    }else if($role== 7){
                        header('Location: main_report_dashboard_crm.php');
                    }else{
                         header('Location: main_report_dashboard_crm.php');
                    }
            
            
               }

        }
        else {
            echo '<div class="col-md-6">';                
               echo  '<div class="alert alert-danger"><strong>Login fail:</strong> Invalid User Name OR Password!</div>';
               echo '</div>';
        }
    $stmt->close();
     
     
    }elseif(isset($_SESSION['user_full_name'])){
         header('Location: main_report_dashboard_crm.php');
    }
    
?>


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mobisoft CRM </title>

    <!-- GLOBAL STYLES -->
    <link href="css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- PAGE LEVEL PLUGIN STYLES -->

    <!-- THEME STYLES -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/plugins.css" rel="stylesheet">

    <!-- THEME DEMO STYLES -->
    <link href="css/demo.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
     <script src="js/plugins/ajaxcall/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#forgetpass").click(function(){
                alert("Please Contact To Admin");
                
                
            });
        });
    </script>

</head>

<body class="login">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                    <h1><i class="fa fa-gears"></i>Mobisoft Admin</h1>
                </div>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Admin Login!</strong>
                            </h4>
                        </div>
                        <!-- <div class="portlet-widgets">
                            <button class="btn btn-white btn-xs"><i class="fa fa-plus-circle"></i>New User</button>
                        </div> -->
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <form  role="form" method="post" action="">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="user name" name="emailid" id="emailid" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <br>
                                
                                <input type="submit" class="btn btn-lg btn-green btn-block" id="submit" name="submit" value="Sign In"/>
                               </fieldset>
                            <br>
                            <p class="small">
                                <a href="#" id="forgetpass">Forgot your password?</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- GLOBAL SCRIPTS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- HISRC Retina Images -->
    <script src="js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->

    <!-- THEME SCRIPTS -->
    <script src="js/flex.js"></script>

</body>
</html>
