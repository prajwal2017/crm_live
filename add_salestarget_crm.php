<?php
include('header_sidebar_crm.php');
include_once("class/functions.php");
$obj=new functions();
$user_id = $_SESSION['user_id'];

if($_SESSION['role'] == "1")
{
    $qry = "select user_id,fname,lname,role from user_details where role= '2' or role= '3' order by role asc";
    $result = $obj -> data_select($qry);
}else if($_SESSION['role'] == "2"){
    $qry = "select user_id,fname,lname from user_details where role= '3' and reporting_id='".$user_id."' ";
    $result = $obj -> data_select($qry);
}

?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="js_functions/datepicker-calendar.js"></script>
<style>
.ui-datepicker-calendar {
    display: none;
}
</style>

<!-- java script start -->

<script type="text/javascript">

    $(document).ready(function(){

        $("#addSalesTargetSubmit").click(function(){
            /*var term=validateField();
                if(term == false)
                {
                    return false;
                }*/
             /* alert($("#addSalesTarget").serialize());
             return false;*/
             $.ajax({

                url:"ajax_service.php",
                data:$("#addSalesTarget").serialize()+"&action=addsalestarget",
                success:function(data){
                   /* alert(data);
                    console.log(data);
                    return false;*/
                    /*if(data == "wrong_date"){
                        alert("Please Do Not Select Previous Month.");
                    }*/
                    /*alert(data);
                    return false;*/
                    if(data == "0"){
                        alert("Sales Target Already Assigned To This User.");
                        window.location.href = "view_salestarget_crm.php";
                    }else if(data == "1"){
                        alert("Sales Target Assign Successfully");
                        //location.reload();
                        window.location.href = "view_salestarget_crm.php";
                    }else{
                        alert("Data not inserted");
                    }
                }
            });
         });

    });

    function validateField()
    { 
        var salesperson=$("#salesperson").val();
        var target=$("#target").val();

        
        var startDate=$("#startDate").val();

        var endDate=$("#endDate").val();
        if(salesperson == "" || salesperson =='' || salesperson == null)
        {
            alert("Please Select Sales Person");
            return false;
        }
        else if(target.length == "" || target.length =='' || target.length == null)
        {
            alert("Please Enter Target");
            return false;
        }

        else if(startDate == "" || startDate =='' || startDate == null)
        {
            alert("Please Select Month");
            return false;
        }
        
   /* else if(endDate.length == "" || endDate.length =='' || endDate.length == null)
    {
        alert("Please Enter End Date ");
        return false;
    }*/
    else
    {

    }
}

function get_previous_balance(){
    var user_id = $("#salesperson").val();
    var s_date = $("#startDate1").val();
    /* alert(user_id+","+s_date);*/
    
    $.ajax({
        url:"ajax_service/check_sales_target.php",
        data:"user_id="+user_id+"&action=get_previous_balance&s_date="+s_date+"",
        dataType:"json",
        success:function(response){
            console.log(response);
            
            //alert(data[0].balance);
            if(response != "no"){
                alert("Previous Month Balance is RS:"+response[0]['balance']);
                $("#balance_amt").val(response[0].balance);
            }else{
                alert("No data from previous month");
                $("#balance_amt").val('0');
            }
            
        }
    });
}

</script>
<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
<div id="page-wrapper">

    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Add Sales Target
                        <small>Form </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                        </li>
                        <li class="active">Add Sales Target</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE ROW -->

        <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Sales Target</h4>
                        </div>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="validationStates" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="addSalesTarget" role="form">
                             
                               <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Target Month</label>
                                <div class="col-sm-10">
                                    <!-- <input type="hidden" id="startDate" name="startDate"  required>  --> 
                                                <!--  <select class="form-control" id="startDate1" name="startDate" onchange="">
                                                        <option value="">---Select Month---</option>;
                                                        <option value="1">1</option>;
                                                        <option value="2">2</option>;
                                                        <option value="3">3</option>;
                                                        <option value="4">4</option>;
                                                    </select>    -->                                         
                                                    <input  class="form-control" type="text" id="startDate1" name="startDate" placeholder="Select Start Date" value="" />
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-sm-2 control-label">Sales Person</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" id="salesperson" name="salesperson"  onchange="get_previous_balance()">
                                                        
                                                        <?php
                                                        if($result != 'no')
                                                        {
                                                            echo '<option value="">---Select Sales Person---</option>';
                                                            foreach ($result as $key => $value) {
                                                                $role = "";
                                                                if($result[$key]['role'] == 1){
                                                                    $role = "CEO";
                                                                }else if($result[$key]['role'] == 2){
                                                                    $role = "BDM";
                                                                }else if($result[$key]['role'] == 3){
                                                                    $role = "BDE";
                                                                }else if($result[$key]['role'] == 4){
                                                                    $role = "Telle Caller";
                                                                }else if($result[$key]['role'] == 5){
                                                                    $role = "RM";
                                                                }
                                                                echo "<option value=".$result[$key]['user_id'].">".$result[$key]['fname']." ".$result[$key]['lname']." - ".$role."</option>";
                                                            }
                                                        }else{
                                                            echo "<option value=''>--No Result Found--</option>";
                                                        }     
                                                        ?>
                                                    </select>
                                                    <span class="help-block"></span>
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-sm-2 control-label">Target( <i class='fa fa-inr'></i> )</label>
                                                <div class="col-sm-10">
                                                 
                                               <!--  <input type="text" class="form-control" id="balance_amt"  pattern="[0-9]" name="balance_amt" placeholder="Enter Amount" required style="width: 427px;">
                                               -->
                                               <div class="input-group">
                                                <span class="input-group-addon">Target Balance <i class='fa fa-inr'></i></span>
                                                <input type="text" class="form-control" id="balance_amt" name="balance_amt" placeholder="Previous Month Balance" style="width: 247px;" readonly>&nbsp;<i class='fa fa-plus btn-xs'></i>&nbsp;
                                                <span class="input-group-addon"><i class='fa fa-inr'></i></span>
                                                <input type="text" class="form-control" id="target"  pattern="[0-9]" name="target" placeholder="Enter Amount" required style="width: 410px;float: inherit;">
                                                
                                            </div>

                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    
                                       <!--  <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">End Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" id="endDate" name="endDate"  placeholder="Select End Date" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div> -->                                      
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="addSalesTargetSubmit" class="btn btn-default">Submit</button></h1>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
        <?php
        include('footer_crm.php');
        ?>