$(function(){
	
 	
	var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
	
	
	$('#h_date').val(today);
	$('#date').text(today);
	$.validator.addMethod("CheckUrl", function(value, element) {
		var letters = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
		return this.optional(element) || letters.test(value);  
	});
	$('#submitInvoice').click(function(){

		submitInvoice();
	});
	$("#invoiceForm").validate({
		rules: {    
			company:{
				required: true,
			},
			address:{
				required: true,
				
			},
			name:{
				required: true,
				//gstin:true,
			},
			user_email:{
				required:true,
				CheckUrl:true,
			}
		},
		messages: {    
			user_email:{
				CheckUrl:"Provide valid email id."
			},
			phone:{
				maxlength:"Must be max 10 digit ",
				minlength:"Must be max 10 digit ",
			}
			
		}
	});
	$("#company").keyup(function(){

		$.ajax({
		type: "POST",
		url: "search.php",
		data:'action=real&keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			console.log(data);
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
	});
	$("#company").focusout(function(){
		/*$("#suggesstion-box").fade();*/
		setTimeout(function() {$('#suggesstion-box').hide()}, 250);

	})
	$("#company1").focusout(function(){
		/*$("#suggesstion-box").fade();*/
		$("#company").attr('readonly','readonly');

	})
});
function selectCountry(val,name,email,address,contact_person) {
	alert(contact_person);
$("#address").val(address);
$("#suggesstion-box").hide();
$("#name").val(contact_person);
$("#company").val(name);
$("#user_email").val(email);
}
function placeOfSup(str){
		

}
function calculateTotalRate(rate){
	var id = rate.id;
	// alert(id);
	var value= parseFloat(rate.value);
	var tokenId = id.substr(id.length - 1);
	var quantity = parseInt($('#quantity'+tokenId).val());
	// alert(quantity);
	var amount = (quantity * value).toFixed(2);
	$('#amount'+tokenId).val(amount)
	/*alert(amount);*/
	calcTax(amount);
}
function calcTax(amount){
	amount = parseFloat(amount);
	
	var amount1 = parseFloat($('#amount1').val());
	var amount2 = parseFloat($('#amount2').val());
	var amount3 = parseFloat($('#amount3').val());
	var amount4 = parseFloat($('#amount4').val());
	var amount5 = parseFloat($('#amount5').val());
	var amount = amount1 + amount2 + amount3 +amount4 +amount5;

	amount = parseFloat(amount);
	// alert(amount);
	amount = amount.toFixed(2);

	$('#h_Sub_amount').val(amount);
	$('#Sub_amount').text(amount);
	var POS = $('#place_of_supply').val();
	var Split_string = POS.split("_");
	
	var placeoforder = Split_string[1];
	if(placeoforder == "Maharashtra"){
		var cgst = 9;
	    // var CGST_Amount = Math.round(amount*(cgst/100));
	    var CGST_Amount =(amount*(cgst/100)).toFixed(2);
	    var sgst = 9;
	    var SGST_Amount =(amount*(sgst/100)).toFixed(2);
	  	// var SGST_Amount = Math.round(amount*(sgst/100));
		$('#cgst').text(CGST_Amount);
		$('#sgst').text(SGST_Amount);
		var total =parseFloat(CGST_Amount) + parseFloat(SGST_Amount);
		alert(parseFloat(CGST_Amount));
		var total =Math.round(total);
		var GT = parseFloat(total) + parseFloat(amount);
		/*alert(GT);*/
		$('#h_total_tax').val(total);
		$('#total_tax').text(total);
		$('#h_total_amount').val(Math.round(GT));
		$('#total_amount').text(Math.round(GT));
		$('#h_cgst').val(CGST_Amount);
		$('#h_sgst').val(SGST_Amount);
		$('#igst').text("");
		$('#h_igst').val("");
		var Towords = GT;
		var inwords = toWords(Towords);
		$('#inWords').text(inwords);
		

	}else{
		$('#cgst').text("");
		$('#sgst').text("");
		$('#h_cgst').val("");
		$('#h_sgst').val("");
		var igst = 18;
		var IGST_Amount = amount*(igst/100);
		var igst =IGST_Amount.toFixed(2);
		$('#h_total_tax').val(igst);
		$('#total_tax').text(igst);

		var totalAmount = Math.round(parseFloat(igst) + parseFloat(amount));
		var round_igst=Math.round(IGST_Amount);
		$('#h_total_amount').val(totalAmount);
		$('#total_amount').text(totalAmount);
		$('#igst').text(igst);
		$('#h_igst').val(igst);
		// alert(totalAmount);
		var inwords = toWords(totalAmount);
		$('#inWords').text(inwords);
		// var inwords = toWords(totalAmount);
		// alert(inwords);
	}

}
function EditcalcTax(amount){
	amount = parseFloat(amount);
	
	var amount1 = parseFloat($('#amount1').val());
	var amount2 = parseFloat($('#amount2').val());
	var amount3 = parseFloat($('#amount3').val());
	var amount4 = parseFloat($('#amount4').val());
	var amount5 = parseFloat($('#amount5').val());
	if(isNaN(amount1))
	{
		
		amount1 = 0 ;
	}
	if(isNaN(amount2))
	{
		amount2 = 0 ;
	}
	if(isNaN(amount3))
	{
		amount3 = 0 ;
	}
	if(isNaN(amount4))
	{
		amount4 = 0 ;
	}
	if(isNaN(amount5))
	{
		amount5 = 0 ;
	}
	var amount = amount1 + amount2 + amount3 +amount4 +amount5;
	amount = parseFloat(amount);
	//alert(amount);
	amount = roundOff(amount);
	//alert(t);

	$('#sub_total').val(amount);
	
	var placeoforder = $('#place_of_supply').val();
	if(placeoforder == "Maharashtra"){
		var cgst = 9;
	    var CGST_Amount = Math.round(amount*(cgst/100));

	    var sgst = 9;
	  	var SGST_Amount = Math.round(amount*(sgst/100));
		$('#cgst').val(CGST_Amount);
		$('#sgst').val(SGST_Amount);
		var total =CGST_Amount+SGST_Amount;
		
		
		$('#total_tax').val(total);
		
		$('#total_amount').val(Math.round(CGST_Amount+SGST_Amount+amount));
		
		$('#igst').val(0);
		var inwords = toWords(totalAmount);
		alert(inwords);
	}else{
		$('#cgst').val(0);
		$('#sgst').val(0);
		
		var igst = 18;
		var IGST_Amount = amount*(igst/100);
		var igst = IGST_Amount;
		
		$('#total_tax').val(igst);

		var totalAmount = igst + amount;
		var round_igst=Math.round(IGST_Amount);
		
		$('#total_amount').val(totalAmount);
		var inwords = toWords(totalAmount);
		alert(inwords);
		$('#igst').val(igst);

		
	}
}

function submitInvoice(){

	var checkSelect = '';
	flag = $("#invoiceForm").valid();
		if(flag==false){
			alert('All Mandatory Fields are required.');
			return false;
		}
	$(".mySelect option:selected").each(function(i) {
		
	    checkSelect += this.value;	
	    var checkClass = $(this).data('amt');		   
	    var amount_check = $('.quantity'+checkClass).val();
	    if( amount_check == '' || amount_check == 0 ){
	    	alert('Please enter quantity');
	    	return false;
	    }
	    var amount_check = $('.product'+checkClass).val();
	    if( amount_check == '' || amount_check == 0 ){
	    	alert('Please enter amount');
	    	return false;
	    }
	   
	});
	
	if(checkSelect == ''){
		alert('Please select product');
	}
	//alert(checkSelect);
	//return false;

	var flag = true;
	if(!$('#product1').val() == ""){
		
	}
	else {
	  alert('Please Select atleast one product');
	    return false;
	}
	
	
	var data = $('#invoiceForm').serialize();

	$.ajax({
		
		"url":"ajax_service/final_invoice.php",
		
		
		"data":data,
		"dataType":"JSON",
		beforeSend:function(){
		
		 $(".bodyLoaderWithOverlay").show();
		},
		success:function(response){
			
			/*console.log(response);
			console.log(response.code);
			console.log(response['code']);*/
			
			if(response.code == 1){
				alert("Invoice Send");
				location.reload();
				
			}else{
				alert("Invoice not send");
			}
		},
		complete:function(){
			$(".bodyLoaderWithOverlay").hide();
			// location.reload();
		
		}
	});
}

function pdfGenerate(id){
       
        $.ajax({
		
			"url":"/api/pdfgen/"+id,
			"method":"GET",
			
			
			"dataType":"JSON",
			beforeSend:function(){
			
			},
			success:function(response){
				
				console.log(response);
				if(response.code == 200){
					alert(response.message);
					location.reload();
				}else{
					alert(response.message);
				}
			},
			complete:function(){
				
			
			}
		});
        
}
function getCompanyAddress(id){
	
	$.ajax({
		
		"url":"/api/getCompanyDetails/"+id,
		"method":"GET",
		
		
		"dataType":"JSON",
		beforeSend:function(){
		
		},
		success:function(response){
			
			console.log(response);
			if(response.code == 200){
				$('#company').val(response.data[0].company_name);
				$('#name').val(response.data[0].name);
				$('#id').val(response.data[0].id);
				$('#address').val(response.data[0].company_address);
				$('#GSTIN').val(response.data[0].gistin);
				$('#address').val(response.data[0].company_address);
				
			}else{
				
			}
		},
		complete:function(){
			
		
		}
	});
}

function getCompanyDetails(){
	var data = $('#searchForm').serialize();
	$.ajax({
		
		"url":"/api/getCompanyDetails",
		"method":"POST",
		
		"data":data,
		"dataType":"JSON",
		beforeSend:function(){
		console.log(data);
		},
		success:function(response){
			
			console.log(response);
			if(response.code == 200){
				alert(response.message);
				location.reload();
			}else{
				alert(response.message);
			}
		},
		complete:function(){
			
		
		}
	});
}

function editInvoice(){
	var data = $('#invoiceForm').serialize();

	
	$.ajax({
		
		"url":"/api/editInvoice",
		"method":"POST",
		
		"data":data,
		"dataType":"JSON",
		beforeSend:function(){
		console.log(data);
		},
		success:function(response){
			
			console.log(response);
			if(response.code == 200){
				alert(response.message);
				 window.location.href = "/viewInvoice";
			}else{
				alert(response.message);
			}
		},
		complete:function(){
			
		
		}
	});
}

function displayAmount(){
 	if(!$('#product1').val() == ""){
		$('#amount1').css('visibility','visible');
		$('#quantity1').css('visibility','visible');
		$('#rate1').css('visibility','visible');
	   
	   
	}
	
	if(!$('#product2').val() == ""){
		$('#amount2').css('visibility','visible');
	   	$('#quantity2').css('visibility','visible');
	   	$('#rate2').css('visibility','visible');
	   
	}
	if(!$('#product3').val() == ""){
		$('#amount3').css('visibility','visible');
	   	$('#quantity3').css('visibility','visible');
	   	$('#rate3').css('visibility','visible');
	   
	}
	if(!$('#product4').val() == ""){
		$('#amount4').css('visibility','visible');
	   	$('#quantity4').css('visibility','visible');
	   	$('#rate4').css('visibility','visible');
	   
	}
	if(!$('#product5').val() == ""){
		$('#amount5').css('visibility','visible');
	   	$('#quantity5').css('visibility','visible');
	   	$('#rate5').css('visibility','visible');
	   
	}
	
}

function mailInvoice(id){
 	
 	$.ajax({
		
		"url":"/api/mail/"+id,
		"method":"GET",
		
		
		"dataType":"JSON",
		beforeSend:function(){
			$('.bodyLoaderWithOverlay').show();
		},
		success:function(response){
			
			
			if(response.code == 200){
				alert(response.message);
				 window.location.href = "/viewInvoice";
			}else{
				alert(response.message);
			}
		},
		complete:function(){
			$('.bodyLoaderWithOverlay').hide();
		
		}
	});
}

/*function calcQuanty(amount){
	amount = parseInt(amount);
	
	var amount1 = parseInt($('#quantity1').val());
	var amount2 = parseInt($('#quantity2').val());
	var amount3 = parseInt($('#quantity3').val());
	var amount4 = parseInt($('#quantity4').val());
	var amount5 = parseInt($('#quantity5').val());
	if(isNaN(amount1))
	{
		
		amount1 = 0 ;
	}
	if(isNaN(amount2))
	{
		amount2 = 0 ;
	}
	if(isNaN(amount3))
	{
		amount3 = 0 ;
	}
	if(isNaN(amount4))
	{
		amount4 = 0 ;
	}
	if(isNaN(amount5))
	{
		amount5 = 0 ;
	}
	var amount = amount1 + amount2 + amount3 +amount4 +amount5;
	
	
}*/

var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function toWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

function roundOff(total){
	grand_total = total;
	tostring = grand_total.toString();
	new_grand_total = '';
	if(tostring % 1 != 0){
		decimal = tostring.split('.')[1];
		digit = tostring.split('.')[0];
		if(decimal > 50){
			new_grand_total = parseFloat(digit) + 1;
			return new_grand_total;
		}else{
			return digit;
		}
	}else{
		return grand_total;
	}
}