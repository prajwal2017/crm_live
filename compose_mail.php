 <?php
 include('header_sidebar_crm.php');

 include('class2/functions2.php');
$con2 = new functions2();
/*session_start(); */

$sql = "SELECT DISTINCT email_id FROM user WHERE u_status = 1 AND user_type = 2 AND email_id IS NOT NULL AND email_id != '' ";

$result = $con2->data_select($sql);

/*echo "<pre>";
print_r($result);*/
?>
<div class="bodyLoaderWithOverlay">
    <div class="loaderImage text-center">
        <img src="img/loading-bar.gif" width="100%" height="100%">
        <p><center>Please Wait</center></p>
    </div>
</div>

<script src="ckeditor/ckeditor.js"></script>
 <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Compose Message
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">Compose Message</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <div class="row">
                    <div class="col-lg-3">
                        <div class="col-lg-12">
                            <div class="col-sm-2 nopadding">
                                <div class="checkbox">
                                    <label>
                                        <input value="" type="checkbox" id="selectAll" onchange="selectAll(this);">All
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-10">
                                <div class="searchMailDiv form-group">
                                    <input type="text" class="form-control" id="searchMailValue">
                                </div>
                            </div>
                        </div>
                        
                        <div class="emailList" id="list">                            
                            <div class="col-sm-12">
                                <?php 
                                    if($result !='no')
                                    {
                                        foreach ($result as $key => $value) {
                                            echo '<div class="checkbox">
                                                    <label>
                                                        <input name="'.$result[$key]["email_id"].'" class="checkbox_email" value="'.$result[$key]["email_id"].'" type="checkbox">'.$result[$key]["email_id"].'
                                                    </label>
                                                </div>';
                                        }
                                    }
                                ?>
                                <!-- <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="tushar.k@mobisofttech.co.in" type="checkbox">tushar.k@mobisofttech.co.in
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="kundan.k@mobisofttech.co.in" type="checkbox">kundan.k@mobisofttech.co.in
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="pramod.v@mobisofttech.co.in" type="checkbox">pramod.v@mobisofttech.co.in
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="zia.a@mobisofttech.co.in" type="checkbox">zia.a@mobisofttech.co.in
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="bhakti.m@mobisofttech.co.in" type="checkbox">bhakti.m@mobisofttech.co.in
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="checkbox_email" value="harshada.s@mobisofttech.co.in" type="checkbox">harshada.s@mobisofttech.co.in
                                    </label>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>New Message</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <form id="mailContent">
                                    <div class="form-group usersEmailList">
                                        <div class="tokenfield input-group">
                                           <span class="input-group-addon">To:</span>
                                            <input type="text" class="form-control usersEmailId" name="mailTo" id="tokenfield-1" data-tokens="" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Cc:</span>
                                            <input type="text" id="tokenfield-2" name="mailCC" data-tokens="" class="form-control CC">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Bcc:</span>
                                            <input type="text" id="tokenfield-3" name="mailBcc" data-tokens="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">From:</span>
                                            <input type="text" class="form-control" name="fromEmail" value="alert@mobisofttech.co.in">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">Subject:</span>
                                            <input type="text" name="subject" class="form-control">
                                        </div>
                                    </div>
                                    <hr>
                                    <textarea name="editor1"><p>Dear <strong>Sir/Madam</strong>,</p>
                                        <p><span style="color:#cc0000"><span style="font-size:16px"><strong>Support Team // Mobisoft Technology</strong></span></span></p>
                                        <hr />
                                        <p><span style="color:#365f91"><strong>MobisoftTechnology India Pvt. Ltd.</strong></span></p>
                                        <p><span style="color:#365f91">Office No. H-004, P Level, Tower-3,</span></p>
                                        <p><span style="color:#365f91">Railway Station Complex, Sector 11,</span></p>
                                        <p><span style="color:#365f91">CBD Belapur, Navi Mumbai 400 614.</span></p>
                                        <p><span style="color:#365f91">India.</span></p>
                                        <p><span style="color:#365f91">Mobile : + 91 8108004545</span></p>
                                        <p><span style="color:#365f91">Phone : +91 22 27574515</span></p>
                                        <p><span style="color:#365f91">Email:</span><span style="color:#0066ff"> </span><a href="mailto:support@mobisofttech.co.in"><span style="color:#0066ff">support@mobisofttech.co.in</span></a></p>
                                        <p><a href="http://www.mobisofttech.co.in"><span style="color:#0066ff">www.mobisofttech.co.in</span></a> | <a href="http://www.makemysms.in"><span style="color:#0066ff">www.makemysms.in</span></a></p>
                                        <p>------------------------------------------------------------------------------------------------------------------------------------------------------</p>
                                        <p>This email, and attachments, may be confidential and also privileged. If you are not the intended recipient, please notify the sender and delete all copies of this transmission along with any attachments immediately. You should not copy or use it for any purpose, nor disclose its contents to any other person. All rights reserved Mobisoft Technology India Pvt Ltd 2017.</p>
                                        <p>-------------------------------------------------------------------------------------------------------------------------------------------------------</p>
                                        <p>&nbsp;</p>                                        
                                    </textarea>
                                    <div class="form-group">
                                        
                                        <div class="attachmentDiv">
                                            <!-- <div class="col-lg-12">
                                                <div class="col-lg-6 attachmentName">                                                
                                                    <div class="col-lg-10 fileHeight"><input type="file" class="getFile" ><label></label></div>
                                                    <div class="col-lg-2 text-center"><span><i class="fa fa-times"></i></span></div>                                                
                                                </div>
                                            </div> -->
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="portlet-footer">
                                <div class="btn-toolbar" role="toolbar">
                                    <button class="btn btn-default" id="sendMail"><i class="fa fa-envelope"></i> Send</button>
                                    <button class="btn btn-red pull-right"><i class="fa fa-times"></i> Discard</button>
                                    <button class="btn btn-green pull-right" id="addAttachment"><i class="fa fa-paperclip"></i> Add Attachment</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <div>
            <div class="checkbox_hidden hidden hiddenChecks" id="hiddenChecks">
                    <input value="" type="checkbox">
               
               
            </div>'
        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

<style type="text/css">
    .attachmentName{
        background-color: #eaeaea;
        padding: 2px;
        margin-top: 5px;
    }
    .attachmentName span i{
        cursor: pointer;
        color: red;                
    }
    .uploadMultipleDoc{
        visibility: hidden;
        width: 1px;
        height: 1px;
    }
    .fileHeight{
        height: 25px;
    }

    .loaderImage{
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        margin: auto; 
        width: 15%;
        height: 10%;
        z-index: 9999;
        position: fixed;
        color: #fff; 
        
    }
    .bodyLoaderWithOverlay{
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 9999;
        background-color: #000;
        filter: alpha(Opacity=80);
        opacity: .8;
        display: none;
    }
</style>

<script type="text/javascript">
    var fileSize = 0;
    $(".bodyLoaderWithOverlay").hide();
    $(function(){  
        var arr = [];
        var count = 0;
        $("#tokenfield-2").tokenfield(function(){alert();});
        $("#addAttachment").click(function(){
            //$('#uploadDoc').trigger('click');
            var randomNumber = Math.floor(1000 + Math.random() * 9000);
            var html = '<div class="col-lg-12"><div class="col-lg-6 attachmentName '+randomNumber+'">'                                                
                            +'<div class="col-lg-10 fileHeight"><input type="file" class="getFile" ><label></label></div>'
                            +'<div class="col-lg-2 text-center"><span onclick="removeAttachment('+randomNumber+');"><i class="fa fa-times"></i></span></div></div></div>';

            $(".attachmentDiv").append(html);
        });


       /* $(".checkbox_email").on('change',function(){
            alert();
            addMailId();
        });*/
        $("#list").on('change','input',function(){
           var dataCheck =  $(this).val();
            if($(this).is(':checked')){
             
             arr[count++] = $(this).val();
             
             $('#tokenfield-1').tokenfield('setTokens', arr);
            }else{
            // alert(arr);
            $.each(arr,function(i){
               console.log(dataCheck);
               if(dataCheck == arr[i]){
               arr[i] ="";
               }
              /* console.log(arr[i]);*/
            })
             count--;
             addMailId(arr); 
            }
           
            
        });
        $(".attachmentDiv").on('change','.attachmentName .getFile',function(e){
            //20971520
            var size = e.target.files[0].size;
            
            fileSize = fileSize + size;

            if(fileSize >= 20971520){
                alert('Maximum 20 MB is allowed');
            }

        });

        //CKEDITOR.replace( 'editor1' );
        CKEDITOR.replace( 'editor1', {});

        $("#sendMail").click(function(){

           /* console.log(fileArray);
            return false;*/
            var message = CKEDITOR.instances.editor1.getData();

            //var formdata = $("#mailContent").serialize();//new FormData($("#mailContent")[0]);
            var formdata = new FormData($("#mailContent")[0]);
            formdata.append('message',message);

            $.each($("input[type=file]"), function(i, obj) {
                $.each(obj.files,function(i,file){
                    formdata.append('file[]', file);
                })
            });
        
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "mailgun/sendMail.php",
                "method": "POST",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "5d6d42d9-9cdb-e834-6366-d217b8e77f59"
                },   
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",         
                "data":formdata,
                "dataType":"JSON",
                beforeSend:function(){
                     
                    $(".bodyLoaderWithOverlay").show();
                    //return false;
                },
                success:function(response){
                    console.log(response);
                    return false;
                    if(response.status == 200){
                        alert("Mail sent successfully.");
                        $('#mailContent')[0].reset();
                        location.reload();
                    }else{
                        alert(response.message+", "+response.data);
                    }
                },
                complete:function(){
                    $(".bodyLoaderWithOverlay").hide();
                }
            });
        });

        $("#searchMailValue").on('keyup',function(){
                var i= 0;
                var token=[];
           

             $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "ajax_service.php",
                "method": "POST",
                "headers": {
                    "cache-control": "no-cache",
                    "postman-token": "5d6d42d9-9cdb-e834-6366-d217b8e77f59"
                },   
                "processData": false,                
                "data":"email="+this.value+"&action=getUserEmailId",
                "dataType":"JSON",
                beforeSend:function(){
                    //$(".bodyLoaderWithOverlay").show();
                    //return false;
                },
                success:function(response){
                    //console.log(response);
                    //return false;
                    if(response.status == 200){
                        console.log(response.data);
                        var data = response.data;

                        var html = '';
                        $.each(data, function(i, item) {                        
                            
                            html += '<div class="checkbox">'
                                        +'<label>'
                                            +'<input name="'+data[i].email_id+'" class="checkbox_email" value="'+data[i].email_id+'" type="checkbox">'+data[i].email_id
                                        +'</label>'
                                    +'</div>';

                        }); 

                        console.log(html);
                        $(".emailList div").html('');
                        $(".emailList div").html(html);

                    }else{
                        console.log(response.message);
                    }
                },
                complete:function(){
                    //$(".bodyLoaderWithOverlay").hide();
                }
            });
        });

    });

    function addMailId(arr){
       /* var arr = [];*/
        var i=0;
        var j = 0;
        var html ="";
        var token ="";
       /* $.each(arr,function(i){

        })*/
        /* $('.checkbox_email:checked').each(function () {
            arr[i++] = $(this).val();
         });*/
        $('#tokenfield-1').tokenfield('setTokens', arr);
       
       
    }

    function selectAll(obj){
        if(obj.checked) {
            //alert('checked');
            $(".emailList .checkbox_email").prop("checked", true);
        } else {
            //alert('not checked');
            $(".emailList .checkbox_email").prop("checked", false);
        }
        addMailId();
    }

    function unCheckedCheckbox(value) {
        $(".checkbox_email").each(function(){
            if($(this).val() == value)
            {
                $("#selectAll").attr("checked", false);
                $("input:checkbox[value='"+value+"']").attr("checked", false);
                return;
            }
        });

        
    }

    function removeAttachment(randomNumber)
    {       
        var size =  $('.attachmentDiv .'+randomNumber+' .getFile')[0].files[0].size;
        fileSize = fileSize - size; 
        $( "."+randomNumber ).remove();
    }
    function fillField(res){
        var html="";
    $('.hiddenChecks  > input').each(function () {
      

    });
     // var  html ='<input type="checkbox" value="'+res.name+'">';
    $('#hiddenChecks').append(html);
   
    }

</script>

<?php
 include('footer_crm.php');
?>        