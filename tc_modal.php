<!------------------------------------------------------------------>
<!------------------------------------------------------------------>
<!--------------                                      -------------->
<!-------------- THIS PAGE IS ONLY FOR VIEW QUOTATION -------------->
<!--------------                                      -------------->
<!------------------------------------------------------------------>
<!------------------------------------------------------------------>


<!-- Flex Modal -->
<div class="modal modal-flex fade" id="openLeadTCModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Assign Lead </h4>
            </div>
            <form id="openLeadTCFormData" class="form-horizontal" role="form">
            <div class="modal-body">
                    
                    <input type="hidden" name="action_temp" value="deleteQuotation">   
                    <input type="hidden" id="accept_cust_id" name="cust_id">
                    <input type="hidden" id="accept_q_id" name="q_id">               

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Company Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Contact No</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Contact Person</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="contactPerson" name="contactPerson" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Email ID</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                         <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="address" name="address" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <?php

                                $qry1 = "SELECT user_id,fname,lname,role FROM user_details WHERE role =2 OR role =3 AND flag = '1' ORDER BY role ASC";
                                $result1_details_user = $con->data_select($qry1);                      
                                echo '<div class="form-group">';
                                echo '<label class="col-sm-2 control-label">Assign To</label>';
                                echo '<div class="col-sm-10">';
                                echo '<select  class="form-control" id="assignLead" name="assignLead" required>';
                                echo '<option value="" selected disabled >--------Select User---------</option>';
                                foreach ($result1_details_user as $key => $value) {
                                    $role_name = "";
                                    if($result1_details_user[$key]['role'] == 1){
                                        $role_name = "Admin";
                                    }
                                    if($result1_details_user[$key]['role'] == 2){
                                        $role_name = "BDM";
                                    }
                                    if($result1_details_user[$key]['role'] == 3){
                                        $role_name = "BDE";
                                    }
                                    if($result1_details_user[$key]['role'] == 4){
                                        $role_name = "TelleCaller";
                                    }
                                    if($result1_details_user[$key]['role'] == 5){
                                        $role_name = "RM";
                                    }
                                            echo "<option value='".$result1_details_user[$key]['user_id']."'>".$result1_details_user[$key]['fname']." ".$result1_details_user[$key]['lname']." - ".$role_name."</option>";
                                           
                                }
                                echo '</select>';
                                echo '</div>';
                                echo '</div>';
                            

                        ?>
                       
                      
            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                 <button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
                 <button type="button" id="addOpenLeadTCDetails" class="btn btn-green">Save changes</button>
            </div>
            </form> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="rejectLeadTCModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Reject Remark</h4>
            </div>
            <form id="rejectLeadTCData" class="form-horizontal" role="form">
            <div class="modal-body">

                <input type="hidden" name="action_temp" value="deleteQuotation"> 
                <input type="hidden" id="reject_q_id" name="q_id">
                <input type="hidden" id="reject_cust_id" name="cust_id">              
                <input type="hidden" id="reject_client_id" name="client_id"> 
                <div class="form-group">
                    <label for="textArea" class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="remark" name="remark" placeholder="Placeholder Text" required></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="addRejectLeadTCDetails">Submit</button>
            </div>
            </form> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Flex Modal -->
<div class="modal modal-flex fade" id="delayLeadTCModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Set Reminder</h4>
            </div>
            <form id="delayLeadTCData" class="form-horizontal" role="form">
            <div class="modal-body">
               
                <input type="hidden" name="action_temp" value="deleteQuotation"> 
                <input type="hidden" id="delay_q_id" name="q_id">
                <input type="hidden" id="delay_cust_id" name="cust_id">
                <input type="hidden" id="delay_client_id" name="client_id">           

                <div class="form-group">
                    <label for="textArea" class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="delay_remark" name="remark" placeholder="Placeholder Text" required></textarea>
                    </div>
                </div>
                <div class="form-group has-success">
                    <label class="col-sm-2 control-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="rdate" name="rdate" placeholder="Placeholder Text" required>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group has-success">
                    <label class="col-sm-2 control-label">Time</label>
                    <div class="col-sm-10">
                        <input  class="form-control" type="time" id="rtime" name="rtime" placeholder="Placeholder Text" required>
                        <span class="help-block"></span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
               <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="addDelayLeadTCDetails">Submit</button>
            </div>
              </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->