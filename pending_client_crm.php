<?php

    include('header_sidebar_crm.php');
    $con = new functions();

    if($_SESSION['role'] == "4")//TC
    {
        header("Location:index.php");
    }
    
    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3")//BDE AND BDM
    {
        $user_id = $_SESSION['user_id'];
        $qry = "SELECT c.company_name, c.contact_number, c.contact_person, c.email_id, c.address, c.lead_user_id, c.u_date FROM client_details AS c WHERE c.mks_pending = 'P' AND c.bde_user_id ='".$user_id."' ORDER BY c.u_date  DESC";        
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "8" || $_SESSION['role'] == "9")
    {
        //Admin,RM
        $qry = "SELECT c.company_name, c.contact_number, c.contact_person, c.email_id, c.address, c.lead_user_id, c.u_date,u.fname,u.lname FROM client_details AS c INNER JOIN user_details AS u ON c.bde_user_id=u.user_id WHERE c.mks_pending = 'P'  ORDER BY c.u_date  DESC";
        $result = $con->data_select($qry);
    
    }
       

?>
<script type="text/javascript">

/*$(function(){
    $("#viewSelfCustomers").click(function(){
        location.reload();            
    });

    $("#viewCustomers").click(function(){
        var user_id = $("#bde_name").val();
        if(user_id == ''){
            alert('Please select BDE');
            return false;
        }
        //alert(user_id);
        $.ajax({
            url:"ajax_service/mks_customer_ajax.php",                
            data:"user_id="+user_id+"&action=getMksUserData",
            async: false,
            //dataType:"json",
            success:function(data){                
                //console.log(data);
                $('#cust_tbody').empty();
                
                var oTable = $("#example-table").DataTable();
                oTable.fnDestroy();
                $('#cust_tbody').html(data);
                $("#example-table").DataTable();
            }
        
        });
    });


});*/
</script>


 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Client 
                                <small>Pending Client Details</small>

                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Client</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            /*if( $_SESSION['role'] == "2"){

                                echo "<div class='col-md-3'>
                                        <div>
                                            
                                               <b> Select BDE</b>
                                           
                                            <select  class='form-control' id='bde_name' name='bde_name' required>
                                                <option value='' selected disabled >--------Select BDE---------</option>";

                                                foreach ($get_bde_result as $key => $value) {
                                                    echo "<option value='".$get_bde_result[$key]['user_id']."'>".$get_bde_result[$key]['fname']." ".$get_bde_result[$key]['lname']."</option>";
                                                }

                                            echo"</select>
                                        </div>
                                    </div>";

                                 echo"<div class='col-md-1'>
                                            <div>
                                                &nbsp;&nbsp;&nbsp;
                                                <input type='hidden' id='assigned_user_id' name='assigned_user_id'>
                                                <button type='button' id='viewCustomers' class='btn btn-green'>Search</button>
                                            </div>
                                        </div>";
                               
                                echo"<div class='col-md-2'>
                                    <div>
                                        </br>
                                        <button type='button' id='viewSelfCustomers' class='btn btn-green'>Self Customer</button>
                                    </div>
                                </div>";
                                   
                                
                            }*/
                        ?>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Pending Client Confirmation</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>
                                                <th>Company Name</th>
                                                <th>Cont. Number</th>
                                                <th>Cont. Person</th>
                                                <th>Email ID</th>
                                                <?php
                                                if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
                                                    echo '<th>Sales Person</th> ';
                                                }
                                                ?>
                                                <th>Created Date</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cust_tbody">
                                        <?php 
                                            /*echo "<pre>";
                                            print_r($result);
                                            exit;*/
                                            if($result !='no')
                                            {
                                                foreach ($result as $key => $value) {
                                                
                                                    $sr = $key +1;
                                                    echo "<tr>";
                                                    echo "<td>".$sr."</td>";
                                                    echo "<td>".$result[$key]['company_name']."</td>";
                                                    echo "<td>".$result[$key]['contact_number']."</td>";
                                                    echo "<td>".$result[$key]['contact_person']."</td>";
                                                    echo "<td>".$result[$key]['email_id']."</td>";
                                                    
                                                    if($_SESSION['role'] == "1" || $_SESSION['role'] == "5"){
                                                        echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
                                                    }                                                   
                                                    
                                                    echo "<td>".$result[$key]['u_date']."</td>";
                                                    
                                                    echo "</tr>";
                                                }
                                                
                                            }
                                            
                                            
                                            
                                        ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
                </div>
                <div class="modal-body">
                   <form id="updateUserData" class="form-horizontal" role="form">
                        <input type="hidden" id="user_id" name="user_id">                   

                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Contact No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="role" name="role" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="status" name="status" required>
                                        <option value="">Select One:</option>
                                        <option id="status1" value="1">Active</option>
                                        <option id="status0" value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="update" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
<?php
 include('footer_crm.php');
?>