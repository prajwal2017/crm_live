<?php
 include('header_sidebar_crm.php');
?>

<!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/plugins/bootstrap-tokenfield/tokenfield-typeahead.css" rel="stylesheet">
    <link href="css/plugins/bootstrap-tokenfield/bootstrap-tokenfield.css" rel="stylesheet">


<script type="text/javascript">

$(document).ready(function(){
  $("#addProduct").click(function(){
            //alert();
        var term=validateField();
            if(term == false)
            {
                return false;
            }
        var qty = $("#tokenfield").val();
        $("#append_qty").val(qty);
       // var serialize_data = $("#saveAddProduct").serialize();
        //var decode_serialize_data = decodeURIComponent(serialize_data.replace(/%2C/,"__"))
        //alert(decode_serialize_data);
       // return false;
        $.ajax({
        url:"ajax_service.php",
        data:$("#saveAddProduct").serialize()+"&action=addProduct",
        success:function(data){
            console.log(data);
            //alert(data);
            $("#addProduct").hide();
            $("#ajax-loader").show();
            if (data == "success") {
                $("#addProduct").show();
                $("#ajax-loader").hide();
                alert("Product added successfully");
                location.reload();
            }
            else
            {
                $("#addProduct").show();
                $("#ajax-loader").hide();
                alert("Product not added");
            }
        }

        });
    });
    });

function validateField()
{ 
   
    var product=$("#product").val();
    var tokenfield=$("#tokenfield").val();
    if(product.length == "" || product.length =='' || product.length == null)
    {
        alert("Please Enter Product Name");
        return false;
    }
    else if(tokenfield.length == "" || tokenfield.length =='' || tokenfield.length == null)
    {
        
        alert("Please Enter Quantity");
        return false;
    }
    else
    {
         
        
        
    }
}

</script>    

<!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Add Product
                                <small>Form </small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                                </li>
                                <li class="active">Add Product</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED FORM COMPONENTS MAIN ROW -->
                <div class="row">

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-12">

                               
                                <!-- /.portlet -->

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Add Product</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <form class="form-horizontal" id="saveAddProduct" role="form">
                                    <div class="portlet-body">
                                        <h4>Product Name</h4>
                                        <input type="text" class="form-control" name="product" style="text-transform:capitalize;" id="product" placeholder="Enter Product Name" />
                                       
                                       <h4>Product Quantity</h4>
                                        <input type="text" class="form-control" id="tokenfield" pattern="[0-9]"  placeholder="Enter Quantity And Hit Enter For Multiple Quantity" />
                                       <input type="hidden" name="product_quantity" id="append_qty">
                                        
                                    </div>

                                     <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="addProduct" class="btn btn-default">Submit</button></h1>
                                            </div>
                                    </div>
                                </form>
                                </div>
                                <!-- /.portlet -->

                            </div>
                            <!-- /.col-lg-12 (nested) -->

                        </div>
                        <!-- /.row (nested) -->

                    </div>
                    <!-- /.col-lg-6 -->

                   
                    <!-- /.col-lg-6 -->

                </div>
                <!-- /.row -->
                <!-- end ADVANCED FORM COMPONENTS MAIN ROW -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper --> 
   
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/scrollspy.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/affix.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/typeahead.min.js"></script>
    <script src="js/plugins/bootstrap-maxlength/bootstrap-maxlength.js"></script>
    <script src="js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <script src="js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <script src="js/demo/advanced-form-demo.js"></script>

<?php
 include('footer_crm.php');
?>