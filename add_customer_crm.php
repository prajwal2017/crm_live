<?php
include('header_sidebar_crm.php');
?>
<!-- java script start -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		/* $("#contactNumber").keypress(function(event){
        
	       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
	           event.preventDefault(); //stop character from entering input
	       }

	    });*/
		/*$("#companyName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90) && !(inputValue >= 97 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    	});*/
    	/*$('#companyName').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z\\s\b]*$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }*/
        $('#contactNumber').on('input', function (event) { 
	    this.value = this.value.replace(/[^0-9]/g, '');
		});
		/*$('#companyName').on('input', function (event) { 
	    this.value = this.value.replace(/[^a-zA-Z0-9\s]/g, '');
		});*/
		$.validator.addMethod("CheckUrl", function(value, element) {
		var letters = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
		return this.optional(element) || letters.test(value);  
	});
		/*$('#customerEmail').on('input', function (event) { 
	    this.value = this.value.replace(/[^a-zA-Z\s]/g, '');
		});*/
		
	    /*});*/
	    $("#saveCustomer").validate({
		rules: {    
			companyName:{
				required: true,
			},
			contactNumber:{
				required: true,
				minlength:10,
			},
		
			customerEmail:{
				required:true,
				CheckUrl:true,
			}
		},
		messages: {    
			customerEmail:{
				CheckUrl:"Provide valid email id."
			},
			phone:{
				maxlength:"Must be max 10 digit ",
				minlength:"Must be max 10 digit ",
			}
			
		}
	}); 
		$("#addCustomer").click(function(){
			/*var temp = validateField();
			if(temp == false){
				return false;
			}*/
			var contact = $("#contactNumber").val();
			if(contact.length !='10'){
				alert('Contact number should be exactly 10 numbers');
				return false;
			}
			var flag = $("#saveCustomer").valid();
			if(flag==false){
				alert('All Mandatory Fields are required.');
				return false;
			}
			$.ajax({
				url:"ajax_service.php",
				data:$("#saveCustomer").serialize()+"&action=addCustomer",
				dataType: "JSON",
				beforeSend:function(){
					$("#addCustomer").hide();
					$("#ajax-loader").show();
				},
				success:function(data){
                //console.log(data);
                if (data.status == 1) {                    
                	alert(data.message);
                }else if(data.status == 2){                    
                	alert(data.message);
                	location.reload();
                }
            },
            complete:function(){
            	$("#addCustomer").show();
            	$("#ajax-loader").hide();
            }
        });
		});
	});



	function validateField(){ 
		var companyName=$("#companyName").val();
		var contactNumber=$("#contactNumber").val();
		if(companyName.length == "" || companyName.length =='' || companyName.length == null){
			alert("Please Enter Company Name");
			return false;
		}else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null){
			alert("Please Enter contact Number");
			return false;
		}else{
			if(contactNumber.length < 10 || contactNumber.length > 12){
				alert("Please Enter correct format");
				return false;
			}
		}
	}

</script>
<!-- javascript end -->
<style type="text/css">
	.error{
		 color:red!important;
	}
	
	input.error {
   	border:1px dotted red!important;
	}
</style>

<!-- begin MAIN PAGE CONTENT -->
<div id="page-wrapper">
	<div class="page-content">

		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Add Customer
						<small>Form </small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
						</li>
						<li class="active">Add Customer</li>
					</ol>
				</div>
			</div>
		</div>
		<!-- end PAGE TITLE ROW -->

		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Customer</h4>
						</div>
						<div class="portlet-widgets">
							<a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
						</div>
						<div style="float: right;margin: 2px 20px 0 0;">
							<a href="upload_contact_crm.php"><input type="button" class="btn btn-green" id="button" value="Import Excel" ></a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div id="validationStates" class="panel-collapse collapse in">
						<div class="portlet-body">
							<form class="form-horizontal" id="saveCustomer" role="form">
								<div class="form-group has-success">
									<label class="col-sm-2 control-label">Company Name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="companyName" id="companyName"  style=" text-transform: capitalize;" placeholder="Enter Company Name" required>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group has-success">
									<label class="col-sm-2 control-label">Contact Number</label>
									<div class="col-sm-10">
										<input type="text" class="form-control contact_validate" name="contactNumber" pattern="[0-9]"  id="contactNumber" placeholder="Enter Contact Number" required>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group has-success">
									<label class="col-sm-2 control-label">Email id</label>
									<div class="col-sm-10">
										<input type="text" class="form-control email_validate" name="customerEmail" id="customerEmail" placeholder="Enter Email Id" >
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<h1 align="center"><button type="button"  id="addCustomer" class="btn btn-default">Submit</button></h1>
										<div align="center" id="ajax-loader" style="display:none"><img src="img/ajax-loader.gif"></div>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.page-content -->

</div>

<!-- /#page-wrapper -->
<!-- end MAIN PAGE CONTENT -->
<?php
include('footer_crm.php');
?>
