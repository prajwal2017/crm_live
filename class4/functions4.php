<?php

/**
 * Description of functions
 *
 * @author sms
 */

$root = $_SERVER['DOCUMENT_ROOT'];
ini_set('memory_limit', '2048M');

$smtp_path = $root."/crm/smtpmail/class.phpmailer.php";

include_once 'db_connect4.php';
require_once ($smtp_path);
class functions4 extends db_connect4 {

	public function send_attached_mail($subject,$body,$to,$attachment_name,$reply_to,$from_name){
      $mail = new PHPMailer(); // create a new object
      $mail->IsSMTP(); // enable SMTP
      $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
      $mail->SMTPAuth = true; // authentication enabled
      $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
      $mail->Host = "smtp.gmail.com";
      $mail->Port = 465; // or 587
      $mail->IsHTML(true);
      $mail->Username = "contact@mobisofttech.co.in";
      $mail->Password = "mobi0240";

      $mail->SetFrom("contact@mobisofttech.co.in",$from_name);
      $mail->Subject = $subject;
      $mail->Body = $body;
      $mail->AddAddress($to);
     /* $mail->AddCC("aakash@mobisofttech.co.in", "Recepient 1");
      $mail->AddCC($reply_to, "Recepient 2");*/
      //$mail->AddBCC("mail1@domain.com", "Recepient 1");
      $mail->AddReplyTo($reply_to, "Reply-To"); // indicates ReplyTo headers
      $arrlength=count($attachment_name);
      for($x=0;$x<$arrlength;$x++)
      {
      	$mail->AddAttachment($attachment_name[$x]);
      }
      //$mail->AddAttachment($attachment_name);
      
      if(!$mail->Send()){
      	return json_encode("Mailer Error: " . $mail->ErrorInfo);
      }
      else{
      	return 1;
      }
      
  }
	
	public function test_input($data)
	{
      //$data = trim(strtolower($data));
		$data = strip_tags($data);
		$data = htmlspecialchars($data);
		$data = mysqli_real_escape_string($this->mysqli,$data);
		return $data;
	}
	/* Test_Input Function closed Here */
	
	public function get_datetime()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date1=date( "Y-m-d H:i:s");
		return $date1;
	}
	/* Function get_datetime closed here*/
	
	public function data_insert($sql)
	{
		$result= $this->mysqli->query($sql) or die($this->mysqli->error);
		return $result;
	}
	
	public function data_insert_return_id($sql)
	{
		$result= $this->mysqli->query($sql) or die($this->mysqli->error);
		return $id=$this->mysqli->insert_id;
	}
	/* Insert Function Closed here  */
	
	public function data_select($sql)
	{ 
		$select=$this->mysqli->query($sql) or die($this->mysqli->error);      
		if($select->num_rows==0){
			return 'no';
		}  else {
			while ($row = $select->fetch_array(MYSQLI_ASSOC)) {
				$data[]=$row;
			}
			return $data;
		}   
	}
	/* Select function closed here */
	
	public function data_update($sql)
	{
		$update=  $this->mysqli->query($sql) or die($this->mysqli->error);
		return $update;
	} 
	
	public function data_delete($sql)
	{
		$update=  $this->mysqli->query($sql) or die($this->mysqli->error);
		return $update;
	} 
    // Update Function Closed Here
	function mobile_validate($mobile)
	{
		if(preg_match('/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/', $mobile,$matches)){
          //  print_r($matches);
			return true;
		}
		return false;
	}
	
	public function send_mail_sms_password($name,$to,$mobile_no,$password){
		$subject = "makemysms.in password";
		$message = "Dear ".$name.", You are successfully registered with makemysms.in and password is ".$password.".";
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: support@mobisofttech.co.in' . "\r\n";
  //$headers .= 'Cc: kundan.k@mobisofftech.co.in' . "\r\n";

		mail($to,$subject,$message,$headers);

  //------sms code pending----------
		
	}

	public function send_mail_sms($type,$array){
		$subject = "makemysms.in balance ".$type."ed";
		
		if($type == "credit"){
			$message = "Dear ".$array['user_name'].", Your makemysms.in account ".$type."ed by ".$array['sms_credit']." and total balance is ".$array['sms_balance'].".";
		}else{
			$message = "Dear ".$array['user_name'].", Your makemysms.in account ".$type."ed by ".$array['sms_debit']." and total balance is ".$array['sms_balance'].".";
		}
		$to = $array['email_id'];
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: support@mobisofttech.co.in' . "\r\n";
  //$headers .= 'Cc: kundan.k@mobisofftech.co.in' . "\r\n";

		mail($to,$subject,$message,$headers);

  //------sms code pending----------
		
	}


	public function send_mail($mail_info){

		$subject   = $mail_info['subject'];
		$send_from = $mail_info['from'];
		$send_to   = $mail_info['to'];

		$msg_content =  $mail_info['msg_content'];

		$message = "Dear Sir/Madam ,<br><br>";

		$message = $message.$msg_content."<br><br><br><br>";

		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: support@mobisofttech.co.in' . "\r\n";
  //$headers .= 'Cc: kundan.k@mobisofftech.co.in' . "\r\n";


		$footer =  '<pre><strong>Thanks Regards
		Mobisofttechnology India Pvt LTd
		SMS Support
		107,1st Floor, Shiv Chamber
		Sec-11, Plot No-21, Next to MTNL Office
		Belapur, Navi Mumbai: 400614.

		www.mobisofttech.co.in
		Email Id : support@mobisofttech.co.in
		: support@makemysms.co.in
		Mobile   : +91 810800 4545 
		Phone    : +91 27564515.</strong><pre>';



		$message = $message.$footer;






		$res = 0;

		if(is_array($send_to)){

			foreach ($send_to as $key => $value) {

				$res =   mail($send_to[$key],$subject,$message,$headers);

			}

		}else{

			$res = mail($send_to,$subject,$message,$headers);

		}

		

		return $res;

  //------sms code pending----------
		
	}



}

?>
