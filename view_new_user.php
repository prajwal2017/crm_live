<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();

    
    if($_SESSION['role'] == "6"){
        $qry = "SELECT cu.id, cu.crm_client_id, cu.name, cu.company_name, cu.mob_no, cu.email_id, cu.user_name, cu.sender_id, cu.product, cu.quantity,cu.rate, cu.created,ud.fname,ud.lname,ud.role FROM confirm_user_temp AS cu INNER JOIN user_details AS ud ON cu.sales_id=ud.user_id WHERE cu.status = 0 ORDER BY cu.id DESC";
        $result = $con->data_select($qry);
    }
   

?>

<script type="text/javascript" src="js_functions/admin_main.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $("#addMksClient").click(function(){
            //alert("inside submit");
            /*var temp= validateField()
            if(temp == false)
            {
                return false;
            }*/
            $.ajax({

                url:"ajax_service/mks_customer_ajax.php",
                data:$("#addMksClientData").serialize()+"&action=addMksClient",
                beforeSend:function(){
                    $(".ajax-loader").show();
                    $("#addMksClient").hide();
                    $(".close-btn").hide();
                },
                success:function(data){
                    console.log(data);
                    if(data == "1")
                    {
                        alert("Data updated successfully");
                        $("#viewUserModal").modal('toggle');
                       
                        location.reload();
                    }
                    else
                    {
                        alert("Data not updated");
                        
                    }
                },
                complete:function(){
                    $("#addMksClient").show();
                    $(".close-btn").show();
                    $(".ajax-loader").hide();
                }
            });
        });

    });
    function viewData(id)
    {
        //alert("Inside edit "+user_id);
        $.ajax({
            url:"ajax_service/mks_customer_ajax.php",
            data:"id="+id+"&action=viewCustomer",
            dataType:"json",
            success:function(data){
                //alert("inside ajax");
                console.log(data);
                
                $("#id").val(data[0].id);
                $("#crm_client_id").val(data[0].crm_client_id);
                $("#sales_id").val(data[0].sales_id);
                $("#rate").val(data[0].rate);
                $("#name").val(data[0].name);
                $("#company_name").val(data[0].company_name);
                $("#mob_no").val(data[0].mob_no);
                $("#user_name").val(data[0].user_name);
                //$("#role").val(data[0].role);
                $("#email_id").val(data[0].email_id);
                $("#sender_id").val(data[0].sender_id);
                $("#product").val(data[0].product);
                $("#quantity").val(data[0].quantity);
                                   
            }
        });
        
    }

    
    function deleteData(id,sr)
    {
        var confirm_delete = confirm("Are you sure you want to delete?");
        if(confirm_delete == true){
             $.ajax({
                url:"ajax_service/mks_customer_ajax.php",
                data:"id="+id+"&action=deleteUserData",
                success:function(data){
                    console.log(data);
                    if(data == 1)
                    {
                        alert("Data deleted successfully");
                        $("#row"+sr).fadeOut();
                        location.reload();
                    }
                    else
                    {
                        alert("Data not deleted");
                        
                    }

                }
            });
        }
    }

    function checkuserName(){
    
        var userName = $("#user_name").val();
         $.ajax({
            url:"ajax_service/mks_customer_ajax.php",
            data:"userName="+userName+"&action=checkuserName",
            //dataType:"json",
            success:function(data){
                //console.log(data);
                //return false;
                if(data == "no")
                {
                    //alert(data);
                    $("#errMsgUser").css("color","green");
                    $("#errMsgUser").html("User Name is available.");
                }
                else
                {
                    //alert(data);
                    $("#errMsgUser").css("color","red");
                    $("#errMsgUser").html("User Name is already registered.");
                    return false;
                    
                }
            }
        });
    }

     function onlyAlphabets(e, t) {
        try {
            if (window.event) {
                var charCode = window.event.keyCode;
            }
            else if (e) {
                var charCode = e.which;
            }
            else { return true; }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) )
                return true;
            else
                return false;
        }
        catch (err) {
            alert(err.Description);
        }
    }
</script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View User 
                                <small>User Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View User</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View User</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>

                                                <th>Sr.No</th>
                                                <th>Sales Person</th>
                                                <th>Client Name</th>
                                                <th>Company Name</th>
                                                <th>Contact Number</th>
                                                <th>User Name</th>
                                                <th>Email Id</th>
                                                <th>Sender Id</th>
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>                                              

                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php

                                                foreach ($result as $key => $value) {

                                                    $role = "";
                                                    if($result[0]['role'] == 1){
                                                        $role = "CEO";
                                                    }else if($result[0]['role'] == 2){
                                                        $role = "BDM";
                                                    }else if($result[0]['role'] == 3){
                                                        $role = "BDE";
                                                    }else if($result[0]['role'] == 4){
                                                        $role = "Telle Caller";
                                                    }else if($result[0]['role'] == 5){
                                                        $role = "RM";
                                                    }

                                                    $sr = $key + 1;
                                                    echo '<tr id="row'.$sr.'">';

                                                    echo "<td>".$sr."</td>";
                                                    echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']." (".$role.")</td>";
                                                    echo "<td>".$result[$key]['name']."</td>";
                                                    echo "<td>".$result[$key]['company_name']."</td>";
                                                    echo "<td>".$result[$key]['mob_no']."</td>";
                                                    echo "<td>".$result[$key]['user_name']."</td>";
                                                    echo "<td>".$result[$key]['email_id']."</td>";
                                                    echo "<td>".$result[$key]['sender_id']."</td>";
                                                    echo "<td>".$result[$key]['product']."</td>";
                                                    echo "<td>".$result[$key]['quantity']."</td>";
                                                    echo '<td><div class="btn-group" role="group"><a href="#" class="btn btn-green btn-xs" data-toggle="modal" data-target="#viewUserModal" onclick="return viewData('.$result[$key]["id"].');">View</a><a href="#" class="btn btn-red btn-xs ladda-button" onclick="deleteData('.$result[$key]["id"].','.$sr.')">Delete</a></div></td>';
                                                    echo "</tr>";
                                                }
                                                
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="viewUserModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit User</h4>
                </div>
                <form id="addMksClientData" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                    <input type="hidden" id="crm_client_id" name="crm_client_id">
                    <input type="hidden" id="sales_id" name="sales_id">
                     <input type="hidden" id="rate" name="rate">
                    <input type="hidden" id="id" name="id">                       
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">User Name</label></label><span id="errMsgUser"></span>
                                    <input type="text" class="form-control" id="user_name" name="user_name" onkeyup="return checkuserName();" onkeypress="return alphaNumeric(event,this);" placeholder="Enter First Name" required>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Sender Id</label>
                                    <input type="text" class="form-control" id="sender_id" name="sender_id" onkeypress="return onlyAlphabets(event,this);" placeholder="Enter First Name" required>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter First Name" readonly>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Company Name</label>
                                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter First Name" readonly>                                                                      
                                </div>                                
                            </div>                            
                        </div>

                        <div class="col-md-6">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Contact Number</label>
                                    <input type="text" class="form-control" id="mob_no" name="mob_no" placeholder="Enter First Name" readonly>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Email Id</label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" placeholder="Enter First Name" readonly>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Product Name</label>
                                    <input type="text" class="form-control" id="product" name="product" placeholder="Enter First Name" readonly>                                                                      
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="control-label">Quantity</label>
                                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Enter First Name" readonly>                                                                      
                                </div>
                            </div>
                        </div>
                    </div>
                    
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button> 
                    <button type="button" id="addMksClient" class="btn btn-green">Add Client</button>
                    <div align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
<?php
 include('footer_crm.php');
?>