<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();

   
    if($_SESSION['role'] == "7" || $_SESSION['role'] == "2" || $_SESSION['role'] == "8" || $_SESSION['role'] == "9"){
        $qry = "SELECT cr.remark,cr.status,cr.id, cr.u_id, cr.company_name,cr.user_name, cr.product, cr.quantity,cr.rate,cr.type, cr.created,ud.fname,ud.lname,ud.contact_number FROM credit_request AS cr INNER JOIN user_details AS ud ON cr.sales_id = ud.user_id WHERE cr.status!=1 ORDER BY cr.created desc";
        $result = $con->data_select($qry);
        
    }
   

?>

<script type="text/javascript" src="js_functions/admin_main.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $("#addMksCredit").click(function(){
            
            $.ajax({

                url:"ajax_service/mks_customer_ajax.php",
                data:$("#addMksCreditData").serialize()+"&action=addMksCredit",
                beforeSend:function(){
                    $(".ajax-loader").show();
                    $("#addMksCredit").hide();
                    $(".close-btn").hide();
                },
                success:function(data){
                    console.log(data);
                    //alert(data);
                    if(data == "1")
                    {
                        $("#notifyCreditModal .close").click();
                        alert("Credited successfully");                       
                        location.reload();
                    }
                    else
                    {
                        alert("Not Credited");
                        location.reload();
                    }
                },
                complete:function(){
                    $("#addMksCredit").show();
                    $(".close-btn").show();
                    $(".ajax-loader").hide();
                }
            });
        });
         $("#appMksCredit").click(function(){
            
            $.ajax({

                url:"ajax_service/mks_customer_ajax.php",
                data:$("#appMksCreditData").serialize()+"&action=appMksCredit_BH",
                dataType:"JSON",
                beforeSend:function(){
                   /* console.log($("#appMksCreditData").serialize());
                    return false;*/
                   /* alert($("#appMksCreditData").serialize());
                    return false;*/
                    // $(".ajax-loader").show();
                    $("#appMksCredit").hide();
                    $(".close-btn").hide();
                },
                success:function(data){
                    console.log(data);
                    //alert(data);
                   /* return false;*/
                    if(data.code == 200)
                    {
                        $("#approveCreditModal .close").click();
                        alert("Credit Approved successfully");                       
                        location.reload();
                    }
                    else
                    {
                        alert(data.message);
                        // location.reload();
                    }
                },
                complete:function(){
                    $("#appMksCredit").show();
                    $(".close-btn").show();
                    $(".ajax-loader").hide();
                }
            });
        });
        $("#appMksCredit_hsbu").click(function(){
            
            $.ajax({

                url:"ajax_service/mks_customer_ajax.php",
                data:$("#appMksCreditData_hsbu").serialize()+"&action=appMksCredit",
                beforeSend:function(){
                   /* alert($("#appMksCreditData").serialize());
                    return false;*/
                    // $(".ajax-loader").show();
                    $("#appMksCredit").hide();
                    $(".close-btn").hide();
                },
                success:function(data){
                    console.log(data);
                    //alert(data);
                   /* return false;*/
                    if(data == "1")
                    {
                        $("#approveCreditModal_hsbu .close").click();
                        alert("Credit Approved successfully");                       
                        location.reload();
                    }
                    else
                    {
                        alert("Credit Not Approved ");
                        // location.reload();
                    }
                },
                complete:function(){
                    $("#appMksCredit").show();
                    $(".close-btn").show();
                    $(".ajax-loader").hide();
                }
            });
        });
    });

    function notifyCredit(id,pkg,u_id,type)
    {
        //alert("Inside edit "+user_id);
        $("#hidden_id").val(id);
        $("#hidden_package").val(pkg);
        $("#hidden_u_id").val(u_id);
        $("#hidden_mode").val(type);
    }
    function notifyCredit_HSBU(id,pkg,u_id,type)
    {
        //alert("Inside edit "+user_id);
        $("#hidden_id_hsbu").val(id);
        $("#hidden_package_hsbu").val(pkg);
        $("#hidden_u_id_hsbu").val(u_id);
        $("#hidden_mode_hsbu").val(type);
    }

</script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View User 
                                <small>User Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View User</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View User</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>

                                                <th>Sr.No</th>                                                
                                                <th>Company Name</th>
                                                <th>User Name</th>                                                
                                                <th>Product</th>
                                                <th>Quantity</th>
                                                <th>Rate per SMS</th>
                                                <th>Sales Person</th>
                                                <th>Sales Number</th>
                                                <th>Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                <th>Remark</th>                                              

                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php

                                                foreach ($result as $key => $value) {
                                                  
                                                    $sr = $key + 1;
                                                    echo '<tr id="row'.$sr.'">';

                                                    echo "<td>".$sr."</td>"; 
                                                    echo "<td>".$result[$key]['company_name']."</td>";
                                                    echo "<td>".$result[$key]['user_name']."</td>";                                                    
                                                    echo "<td>".$result[$key]['product']."</td>";
                                                    echo "<td>".$result[$key]['quantity']."</td>";
                                                    echo "<td>".$result[$key]['rate']."</td>";
                                                    echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
                                                    echo "<td>".$result[$key]['contact_number']."</td>";
                                                    if($result[$key]['status'] == 2){
                                                        echo '<td  class="text-center">
                                                                <div class="btn-group disable" role="group" >
                                                                    <a href="#" class="btn btn-green btn-xs" disabled data-toggle="modal" data-target="#notifyCreditModal" onclick="return notifyCredit('.$result[$key]["id"].','.$result[$key]['quantity'].','.$result[$key]['u_id'].',\''.$result[$key]['type'].'\');">HSBU approved</a>
                                                                </div></td>';
                                                    }else if($result[$key]['status'] == 3){
                                                       if($_SESSION['role'] == "9"){
                                                            echo '<td  class="text-center">
                                                                <div class="btn-group" role="group">
                                                                    <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#approveCreditModal_hsbu" onclick="return notifyCredit_HSBU('.$result[$key]["id"].','.$result[$key]['quantity'].','.$result[$key]['u_id'].',\''.$result[$key]['type'].'\');">BH approved</a>
                                                                </div></td>';
                                                            }else{
                                                                echo '<td  class="text-center">
                                                                <div class="btn-group" role="group">
                                                                    <a href="#" class="btn btn-green btn-xs" disabled data-toggle="modal" data-target="#approveCreditModal" onclick="return notifyCredit('.$result[$key]["id"].','.$result[$key]['quantity'].','.$result[$key]['u_id'].',\''.$result[$key]['type'].'\');">BH approved</a>
                                                                </div></td>';
                                                            }
                                                    }else if($result[$key]['status'] == 0){
                                                        echo '<td class="text-center">
                                                                <div class="btn-group hidden" role="group">
                                                                    <a href="#" class="btn btn-green btn-xs" data-toggle="modal" data-target="#notifyCreditModal" onclick="return notifyCredit('.$result[$key]["id"].','.$result[$key]['quantity'].','.$result[$key]['u_id'].',\''.$result[$key]['type'].'\');">Credit</a>
                                                                </div>
                                                                <div class="btn-group" role="group">
                                                                    <a href="#" class="btn btn-green btn-xs" data-toggle="modal" data-target="#approveCreditModal" onclick="return notifyCredit('.$result[$key]["id"].','.$result[$key]['quantity'].','.$result[$key]['u_id'].',\''.$result[$key]['type'].'\');">Approve</a>
                                                                </div></td>';
                                                   
                                                    }
                                                      echo "<td>".$result[$key]['remark']."</td>";
                                                     echo "</tr>";
                                                   
                                                }
                                                
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
  <div class="modal modal-flex fade" id="approveCreditModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Approve User</h4>
                </div>
                <form id="appMksCreditData" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                    <input type="hidden" id="hidden_u_id" name="hidden_u_id">
                    <input type="hidden" id="hidden_id" name="hidden_id">
                    <input type="hidden" id="hidden_mode" name="mode">
                    <input type="hidden" id="hidden_package" name="hidden_package">
                    <div class="row">
                        <div class="col-md-12">
                            please add a remark
                            <input type="text" id="hidden_remark" name="hidden_remark">
                        </div>
                    </div>
                    
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button> 
                    <button type="button" id="appMksCredit" class="btn btn-green">Approve Credit</button>
                    <div align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-flex fade" id="approveCreditModal_hsbu" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Approve User()</h4>
                </div>
                <form id="appMksCreditData_hsbu" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                    <input type="hidden" id="hidden_u_id_hsbu" name="hidden_u_id">
                    <input type="hidden" id="hidden_id_hsbu" name="hidden_id">
                    <input type="hidden" id="hidden_mode_hsbu" name="mode">
                    <input type="hidden" id="hidden_package_hsbu" name="hidden_package">
                    <div class="row">
                        <div class="col-md-12">
                            please add a remark
                            <input type="text" id="hidden_remark" name="hidden_remark">
                        </div>
                    </div>
                    
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button> 
                    <button type="button" id="appMksCredit_hsbu" class="btn btn-green">Approve Credit</button>
                    <div align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal modal-flex fade" id="notifyCreditModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit User</h4>
                </div>
                <form id="addMksCreditData" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                    <input type="hidden" id="hidden_u_id" name="hidden_u_id">
                    <input type="hidden" id="hidden_id" name="hidden_id">
                    <input type="hidden" id="hidden_mode" name="mode">
                    <input type="hidden" id="hidden_package" name="hidden_package">
                    <div class="row">
                        <div class="col-md-12">
                            Are you sure want to credit?
                        </div>
                    </div>
                    
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button> 
                    <button type="button" id="addMksCredit" class="btn btn-green">Add Credit</button>
                    <div align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- /.modal -->
    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
<?php
 include('footer_crm.php');
?>