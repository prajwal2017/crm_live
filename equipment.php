<?php
include("header.php");
include("side_navigation.php");
include("./class/functions.php");
$obj1 = new functions();

$status[1]="Active";
$status[0]="Inactive";

$select_option = "select id ,name from euip_master where status = 1";
$data_select  = $obj1->data_select($select_option);
if($data_select !="no"){
   $options='<option value="">Select One:</option>';
    foreach($data_select as $key => $vallue){
      $options.='<option value="'.$data_select[$key]['id'].'">'.$data_select[$key]['name'].'</option>';

    }

}


if(isset($_POST['equip_action']) && ($_POST['equip_action'] == "insert") ){    
    $sql = "insert into equipment(equip_id,purchase,expiry,description,status,created) values('".$_POST['name']."','".date('Y-m-d',strtotime($_POST['purchase']))."','".date('Y-m-d',strtotime($_POST['expiry']))."','".$_POST['desc']."',".$_POST['status'].",'".date("Y-m-d h:i:s")."')";
    $obj1->data_insert($sql);
    //echo "data insreted";

}


if(isset($_POST['equip_action']) && ($_POST['equip_action'] == "product_update") ){    
    $sql = "update equipment set equip_id ='".$_POST['name']."',description ='".$_POST['desc']."',purchase ='".date('Y-m-d',strtotime($_POST['purchase']))."',expiry ='".date('Y-m-d',strtotime($_POST['expiry']))."',  status =".$_POST['status']." where id=".$_POST['product_id'];
  $obj1->data_update($sql);

    //echo "data insreted";

}


$sql_product ="select e.id,m.name as equip_id,purchase,expiry,description,e.status from equipment e left join  euip_master m on m.id=e.equip_id";
$data = $obj1->data_select($sql_product);
//print_r($data);
$obj1->__destruct();


?>

    <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Equipment
                                <small>Equipment Management</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">Equipment</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->



                       <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12" id ="product_info">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title" style ="float:none">
                                    <span>Equipment Info</span>
                                    <span class="pull-right"><button class="btn btn-white" onclick="add_product();">Add Equipment</button></span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Purchase</th>
                                                <th>Expiry</th>
                                                <th>Status</th>                                                                                            
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php

                                               if($data !="no"){

                                                          foreach($data as $key =>$value){
                                                                echo "<tr class='gradeA'>";
                                                                echo "<td>".$data[$key]['id']."</td>";
                                                                echo "<td>".$data[$key]['equip_id']."</td>";
                                                                echo "<td>".$data[$key]['description']."</td>";
                                                                 echo "<td>".$data[$key]['purchase']."</td>";
                                                                  echo "<td>".$data[$key]['expiry']."</td>";
                                                                echo "<td>".$status[$data[$key]['status']]."</td>";                                                                

                                                                echo ' <td> 
                                                                        <div class="btn-group">
                                                                              <a class="btn btn-green btn-xs" role="button" onclick="edit_product(\''.$data[$key]['id'].'\');">Edit</a>
                                                                              <a class="btn btn-red btn-xs" role="button"   onclick="delete_product(\''.$data[$key]['id'].'\');">Inactive</a>
                                                                               <a class="btn btn-green btn-xs" role="button" onclick="active_product(\''.$data[$key]['id'].'\');">Active</a>

                                                                              
                                                                         </div>
                                                                  </td>';
                                                                echo "</tr>";

                                                        }
                                               }
                                    
                                        ?>
                                           
                                           
                                          
                                        </tbody>
                                    </table>
                                </div>
                                 <!-- /.table-responsive -->
                                
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                     <!-- ************Add user div  ***************  -->
                                 <div class="col-lg-12 add_user"  id ="add_user">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>ADD Equipment</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationExamples"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form"  method= "post">
                                        <div class="form-group">
                                            <label for="textInput" class="col-sm-2 control-label">Equipment Name:</label>
                                            <div class="col-sm-10">
                                                 <select name="name"  id ="name" class="form-control" required>
                                                  <?php
                                                     echo $options;
                                                  ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Parchase date:</label>
                                            <div class="col-sm-10">
                                                <div id="sandbox-container">
                                                    <input class="form-control" type="text" name="purchase" id="purchase"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Expiry  date:</label>
                                            <div class="col-sm-10">
                                               <div id="sandbox-container">
                                                    <input class="form-control" type="text" name="expiry" id="expiry"/>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Note:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="desc" name="desc" placeholder="Add Description" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Status</label>
                                            <div class="col-sm-10">
                                                <select name="status"  id ="status" class="form-control" required>
                                                    <option value="">Select One:</option>
                                                    <option value="1">Active</option>
                                                    <option  value="0">Inactive</option>
                                                  
                                                </select>
                                            </div>
                                        </div>
                               
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <input type="hidden" class="form-control" id="action" name="equip_action" value="insert">
                                                 <input type="hidden" class="form-control" id="product_id" name="product_id" value="">
                                                <button type="submit" class="btn btn-default" value="save">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <!-- **********  Add user div  ******************--> 

                </div>
                <!-- /.row -->

                <!-- end PAGE TITLE ROW -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->

  <script>
      $(function() {
         //alert("kundan");
         $("#add_user").hide();

});

      function add_product(){
         $("#product_info").hide();
          $("#add_user").show();
      }

      function edit_product(id){     

        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=equip_edit",
               dataType: "json",
              success:function(data){
               
                  $("#add_user").show();
                  $("#product_info").hide();
                  $("#name").val(data[0].equip_id);
                  $("#purchase").val(data[0].purchase)
                  $("#expiry").val(data[0].expiry)
                  $("#desc").val(data[0].description);
                  $("#product_id").val(data[0].id);
                  $("#action").val("product_update");
                 // alert('#status option[value="'+data[0].status+'"]');
                  //$('#status option[value="'+data[0].status+'"]');
                  $('#status').val(data[0].status);


               
              }
            });


      }

      function delete_product(id){      

       //alert(id);
        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=equip_Inactive",
              success:function(data){
               // alert(data);
                 location.reload();
              }
            });

      }

      function active_product(id){
        

        $.ajax({
              url: "ajax.php",
              data : "id="+id+"&action=equip_active",
              success:function(data){
               // alert(data);
                 location.reload();
              }
            });
      }
  </script>      


<?php
include("footer.php");