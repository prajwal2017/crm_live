<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();
    $c_date = date("Y-m-d");
    if($_SESSION['role'] == "1"){
        $qry = "SELECT * FROM user_details WHERE role!='1' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "5"){
        $qry = "SELECT * FROM user_details WHERE role='3' AND reporting_id = '".$_SESSION['user_id']."' ORDER BY user_id DESC";
        $result = $con->data_select($qry);

        $get_all_client = "SELECT u.u_id,u.name,u.user_name,u.company_name,u.email_id,u.product_name,u.smsc_cred,u.sender_id FROM user as u INNER JOIN client_details AS cd ON u.crm_client_id=cd.client_id ";
        $get_all_client_result = $con->data_select($get_all_client);
    }
    

?>

<script type="text/javascript">

    $(document).ready(function(){
        //alert();

    	$.ajax({
        url:"ajax_service/client_history_ajax.php",
        data:"action=getBDEData",
        dataType:"json",
        success:function(data){
               // console.log(data);
                //alert(data);
                if (data == "no") {
                   
                   // alert("Please add Resource Manager first.");
                   $("#bde_list").append("<option value ='' selected disabled>---No BDE Available---</option>");
                }
                else
                {
                    $("#bde_list").find('option').remove();
                    var options = json_option_role(data);
                   // alert(options);
                    $("#bde_list").append(options);
                    
                }
            }
        });


    });
      
function getClientList(idd)
{
    //alert();
    $("#example-table").find("tr:gt(0)").remove();
	  var cid = idd.value;
		$.ajax({
        url:"ajax_service/client_history_ajax.php",
        data:"bde_id="+idd.value+"&action=getClientData",
        dataType:"json",
        success:function(data){
            //console.log(data);
          //  alert();
              $('#tbody_data').empty();
	            if (data == "no" || data == "" || data==null) {

                            var tr ="<tr id='row1'>";
                            tr += "<td colspan='5' align='center'>No Records Found</td></tr>";
                            
                           $("#tbody_data").html(tr); 
                }
	            else
	            {

                var tr = "";
                for(var i=0;i<data.length;i++)
  					    {
  					    	
  					    	var srno=i+1;
  				        tr += "<tr id='row"+srno+"'>";
  				        tr += "<td>"+srno+"</td>";
                  tr += "<td><input type='checkbox' name='comp_emailid' value='"+data[i].email_id+"'> "+data[i].email_id+"</td>";
  				        tr += "<td>"+data[i].company_name+"</td>";
  				        /*var td2="<td>"+data[i].contact_number+"</td>";
  				        var td3="<td>"+data[i].email_id+"</td>";*/
  				        tr += "<td>"+data[i].product_name+"</td>";
  				        //var td5="<td>"+data[i].c_date+"</td>";
  				        tr += "<td><center><a data-modal-id='popup2'><input class='btn btn-primary btn-sm' type='button' data-toggle='modal' data-target='#flexModal' onclick=\"viewClientData('"+data[i].u_id+"')\" value='View Details' id='datatable'></a></center></td></tr>";				
  					    }
                $("#tbody_data").html(tr); 
                
              }
            }
        });
} 

function viewClientData(cid,bdeid,pid,mksuid)
{
    //alert();
    
    $("#mksuid").val(mksuid);
    $("#cid").val(cid);
    $("#bdeid").val(bdeid);
    $("#pid").val(pid);

    $("#example-table1").find("tr:gt(0)").remove();
    $.ajax({
        url:"ajax_service/client_history_ajax.php",
        data:"client_id="+cid+"&bde_id="+bdeid+"&p_id="+pid+"&mks_u_id="+mksuid+"&action=viewClientData",
        dataType:"json",
        success:function(data){
            //alert("Data: "+data);
            //console.log(data);
                 if (data == "no") {
                        //alert(data);
                            var tr="<tr id='row"+srno+"'>";
                            var td="<td colspan='4' align='center'>No Records Found</td></tr>";
                            
                           $("#example-table1").append(tr+td); 
                }
                else
                {
                   
                    for(var i=0;i<data.length;i++)
                        {
                            var srno=i+1;
                            var tr="<tr id='row"+srno+"'>";
                            var td="<td>"+srno+"</td>";
                            var td1="<td>"+data[i].sender_id+"</td>";
                            var td2="<td>"+data[i].sms_status+"</td>";
                            var td3="<td>"+data[i].ct+"</td></tr>";
                            
                           $("#example-table1").append(tr+td+td1+td2+td3); 
                                          
                        }

                }
                 
        }
     });   
}

$(document).ready(function(){

$("#smscount").click(function(){
var startdate = $("#startDate").val();
var enddate = $("#endDate").val();
var mksuid = $("#mksuid").val();
var cid = $("#cid").val();
var bdeid = $("#bdeid").val();
var pid = $("#pid").val();
alert(startdate+"--"+enddate+"--"+mksuid);
$("#example-table1").find("tr:gt(0)").remove();
$.ajax({
        url:"ajax_service/client_history_ajax.php",
        data:"mksuid="+mksuid+"&cid="+cid+"&bdeid="+bdeid+"&pid="+pid+"&startdate="+startdate+"&enddate="+enddate+"&action=viewClientData",
        dataType:"json",
        success:function(data)
        {
           console.log(data); 
           alert(data);
           if(data == "no")
           {
            var tr="<tr id='row1'>";
            var td="<td colspan='4' align='center'>No Records Found</td></tr>";
            $("#example-table1").append(tr+td); 
           }
           else
           {
                for(var i=0;i<data.length;i++)
                {
                    var srno=i+1;
                    var tr="<tr id='row"+srno+"'>";
                    var td="<td>"+srno+"</td>";
                    var td1="<td>"+data[i].sender_id+"</td>";
                    var td2="<td>"+data[i].sms_status+"</td>";
                    var td3="<td>"+data[i].ct+"</td></tr>";
                    $("#example-table1").append(tr+td+td1+td2+td3);
                }
           }
        }

});
});

/*$("#smscountclose").click(function(){
    $("#startDate").val("");
    $("#endDate").val("");
});*/
});


function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
 }

function getValue(){
  var checkboxes = document.getElementsByName('comp_emailid');
  var vals = "";
  for (var i=0, n=checkboxes.length;i<n;i++) {
    if (checkboxes[i].checked) 
    {
    vals += ","+checkboxes[i].value;
    }
  }
  if (vals) vals = vals.substring(1);

  alert(vals);
  console.log(vals);
}

</script>

 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Client History 
                                <small>SMS Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Client History</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                <div class="row">
                  <div class="col-md-2">
                    <div>                                        
                        <b> User List</b>  
                          <select  class="form-control" id="bde_list" name="bde_list" onchange="getClientList(this);" required>
                              <option value="">---Select---</option>                              
                          </select>
                    </div>
                  </div>
                </div>
                <br>
                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Client History</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">                            	
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                               <th>Sr.</th>
                                               <th><input type="checkbox" onclick="checkAll(this)" > All</th>
                                               <th>Company Name</th>
                                               <!-- <th>Contact Number</th>
                                               <th>Email Id</th> -->
                                               <th>Product</th>
                                               <!-- <th>Created date</th> -->
                                               <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody id="tbody_data">
                                           
                                           <?php
                                                $sr = 1;
                                                foreach ($get_all_client_result as $key => $value) {
                                                  echo "<tr>";
                                                  echo "<td>".$sr."</td>";
                                                  echo "<td><input type='checkbox' name='comp_emailid' value='".$get_all_client_result[$key]['email_id']."'>".$get_all_client_result[$key]['company_name']."</td>";
                                                  echo "<td>".$get_all_client_result[$key]['company_name']."</td>";
                                                  echo "<td>".$get_all_client_result[$key]['product_name']."</td>";
                                                  echo "<td><center><a data-modal-id='popup2'><input class='btn btn-primary btn-sm' type='button' data-toggle='modal' data-target='#flexModal' onclick=\"viewClientData('".$get_all_client_result[$key]['u_id']."')\" value='View Details' id='datatable'></a></center></td>";
                                                  echo "</tr>";
                                                  $sr++;
                                                }
                                           ?>
             
                                      </tbody>
                                    </table>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <input onclick="getValue()" type="button" value="E-Mail" class="btn btn-sm btn-green"> <input type="button" value="SMS" class="btn btn-sm btn-info">
                                      </div>                                      
                                    </div>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

<!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="smscountclose" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-6">
                      <div class="form-group">
                      <label>Start Date</label>
                      <input type="date" class="form-control" id="startDate" name="startDate" value="<?php echo $c_date;?>">
                      </div>
                      </div>
                      <div class="col-sm-6">
                      <div class="form-group">
                      <label>End Date</label>
                      <input type="date" class="form-control" id="endDate" id="endDate" name="startDate" value="<?php echo $c_date;?>">
                      </div>
                      </div>
                    </div>
                    <div class="row">
                        <center>
                            <input type="hidden" id="mksuid">
                            <input type="hidden" id="cid">
                            <input type="hidden" id="bdeid">
                            <input type="hidden" id="pid">
                            <button class="btn btn-block btn-info" id="smscount" style="width: 125px;">Get Details</button><br>
                        </center>
                    </div>
                  <div class="table-responsive">
                                    <table id="example-table1" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                               <th>Sr.</th>
                                               <th>Sender Id</th>
                                               <th>SMS Status</th>
                                               <th>Count</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                           
                                      </tbody>
                                    </table>
                                </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<!-- /.modal -->
<?php
 include('footer_crm.php');
?>