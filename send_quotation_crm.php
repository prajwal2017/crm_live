<?php
include('header_sidebar_crm.php');
$con = new functions();
$qry1 = "SELECT * FROM products WHERE flag = 1 ";
$result1 = $con->data_select($qry1);
?>

<style type="text/css">
hr{
	background-color: #16a085;
	height: 5px;
	color:#16a085;
}
</style>

<script type="text/javascript">
	function getData(){
	}

	$(document).ready(function(){

		$("#sendQuotation").click(function(){

			var prodN="";
			var prodName="";
			var prodQ="";
			var prodQty="";
			var codeV="";
			var codeValidity="";

			var prodR="";
			var prodRate="";
			var totalA="";
			var totalAmount="";

			$("select[name=product_name]").each(function(){
				/*alert('asd');*/
				var prodid=this.id;
				prodN=$("#"+prodid).val();
				prodName=prodName+prodN+"__";
			});

			$("input[name=product_quantity]").each(function(){
				var qtyid=this.id;
				/*alert('qtyid');*/
				prodQ=$("#"+qtyid).val();
				prodQty=prodQty+prodQ+"__";
			});

			$("select[name=code_validity]").each(function(){
				var codeValidityid=this.id;
				codeV=$("#"+codeValidityid).val(); 
				if(codeV == "null" || codeV == null || codeV == ""){
					codeV = "Unlimited";
				}
				codeValidity=codeValidity+codeV+"__";
			});

			$("input[name=product_rate]").each(function(){
				var rateid=this.id;
				prodR=$("#"+rateid).val();
				prodRate=prodRate+prodR+"__";
			});

			$("input[name=total_amount]").each(function(){
				var totalid=this.id;
				totalA=$("#"+totalid).val();
				totalAmount=totalAmount+totalA+"__";
			});

			/*alert(prodQty);
			return false;*/

			$("#appendProductName").val(prodName);
			$("#appendCodeValidity").val(codeValidity);
			$("#appendProductQty").val(prodQty);
			$("#appendProductRate").val(prodRate);
			$("#appendTotalAmount").val(totalAmount);

			$(".ajax-loader").show();
			$("#sendQuotation").hide();
			$("#add_more_div").hide();
			var fake = $("#quotationDetails").serialize();
			/*alert(fake);
			alert("THASHIS");*/
			
			$.ajax({
				url:"send_quotation_mail.php",
				type: "POST",
				data:$("#quotationDetails").serialize(),
				success:function(data){
					// $("#ajax-loader").show();
					console.log(data);
					if(data == 1){
						$(".ajax-loader").hide();
						alert("Quotation Send Successfully!!!");
						$("#sendQuotation").show();
						$("#add_more_div").show();
						/*console.log(data);
						return false;*/
					}else{
						$("#sendQuotation").show();
						$("#add_more_div").show();
						$(".ajax-loader").hide();
						/*console.log(data);
						return false;*/
						alert("Quotation not Send");
					}
				}
			});
		});

		$('#product_quan1').on("change",function() {
			$("#product_rate").val('');
			$("#total_amount").val('');
		});

		var cnt=2;
		var pnmct=1;
		$("#add_more_div").click(function(){

			$("#product_div_main").append('<div id="divRow'+cnt+'"><hr><label>'+cnt+'.Product Details</label> <label style="float:right;font-size: 20px;"><a href="javascript:void()" style="color:red;text-decoration: none;" onclick="removeDiv('+cnt+');">X</a></label><div class="form-group">'
				+'<label class="col-sm-2 control-label">Product</label>'
				+'<div class="col-sm-10">'
				+'<select  class="form-control" id="product_name'+cnt+'" onchange="changeQty(this);" name="product_name"  required>'
				+' <option value="" selected disabled >--------Select Product---------</option>'

				+' </select>'
				+' </div>'
				+'</div>'
				+'<div class="form-group" >'
				+' <label class="col-sm-2 control-label">Quantity</label>'
				+'<div class="col-sm-10">'
				+' <input type="text" class="form-control" id="product_quan'+cnt+'" name="product_quantity" onkeyup="calculaterate(this);" placeholder="Enter Quantity" required>'
				+'</div>'
				+'</div>'
				+'<div class="form-group" style="display:none;" id="div_code_validity'+cnt+'">'
				+' <label class="col-sm-2 control-label">Validity</label>'
				+' <div class="col-sm-10">'
				+'   <select  class="form-control" id="code_validity'+cnt+'" name="code_validity" required>'
				+'   <option value="" selected disabled >--------Select Month---------</option>'
				+'<option value="1 Month" >1 Month</option>'
				+'<option value="3 Months" >3 Months</option>'
				+'<option value="6 Months" >6 Months</option>'
				+'<option value="12 Months" >12 Months</option>'
				+'<option value="Unlimited" >Unlimited</option>'
				+' </select>'
				+' </div>'
				+'</div>'
         /* +'<div class="form-group" style="display:none;" id="div_sms_validity'+cnt+'">'
             +' <label class="col-sm-2 control-label">Validity</label>'
             +' <div class="col-sm-10">'
               +'   <select  class="form-control" id="sms_validity'+cnt+'" name="sms_validity" required>'
                  +'   <option value="" selected disabled >--------Select Month---------</option>'
                      +'<option value="3 Months" >3 Months</option>'
                      +'<option value="6 Months" >6 Months</option>'
                      +'<option value="12 Months" >12 Months</option>'
                 +' </select>'
             +' </div>'
             +'</div>'*/
             +'<div class="form-group has-success">'
             +'<label class="col-sm-2 control-label">Rate</label>'
             +' <div class="col-sm-10">'
             +' <input type="text" class="form-control" id="product_rate'+cnt+'" name="product_rate" onkeyup="calculaterate(this);" placeholder="Enter rate" required>'
             +'<span class="help-block"></span>'
             +'</div>'
             +'</div>'

             +' <div class="form-group has-success">'
             +'<label class="col-sm-2 control-label">Total Amount</label>'
             +'<div class="col-sm-10">'
             +'<input type="text" class="form-control" id="total_amount'+cnt+'" name="total_amount" readonly required>'
             +'<span class="help-block"></span>'
             +' </div>'
             +'</div>');

			$.ajax({
				url:"ajax_service.php",
				data:"action=getProductList",
				contentType: "application/json",
				async: false,
				dataType: 'json',
				success:function(data){
					var productArray = new Array();
					$("select[name=product_name]").each(function(i){
						if($("#"+this.id).val() != null){
							productArray[i] = $("#"+this.id).val();
						}        
					});

					var p = cnt;      

					$("#product_name"+cnt).find('option').remove();
					$("#product_name"+cnt).append("<option value='' selected disabled >--------Select Product---------</option>");
					for(var n=0;n<data.length;n++){      
						$("#product_name"+cnt).append("<option id='"+data[n].product_name+""+cnt+"' value='"+data[n].product_name+"'>"+data[n].product_name+"</option>");        
						$(productArray).each(function(j){
							var productName = data[n].product_name; 
							if(productName.localeCompare(productArray[j]) == 0){
								$("#"+data[n].product_name+""+cnt).remove();
							}    
						});
					}
				}
			});
			cnt++;
			pnmct++;
		});
});



function changeQty(idd) {
	var id = idd.id;
	var product_name = $("#"+id).val();
	var encoded = encodeURIComponent(product_name);
	var res = id.substring(12, id.length);

	var comp_product = product_name.replace(/ /g,'');
	comp_product = comp_product.toLowerCase();
	var sms_name = comp_product.replace(/[^a-zA-Z ]/g, "");

	if(comp_product == "shortcode" || comp_product == "longcode"){
		$("#div_code_validity"+res).show();
	}else if(sms_name == "smstransactional" || sms_name == "smspromotional" || "missedcall"){
		$("#div_code_validity"+res).show();
	}else{
		$("#div_code_validity"+res).hide();
	}
	$("#product_rate"+res).val('');
	$("#total_amount"+res).val('');
	$.ajax({
		url:"ajax_service/view_leads_ajax.php",
		data:"product_name="+encoded+"&action=getProductQty",              
		success:function(data){
			$("#product_quan"+res).html(data);
		}
	});
}



function calculaterate(idd){
	var id = idd.id;
	var res = id.substring(12, id.length);
	var r = $('#product_rate'+res).val();
	var q = $('#product_quan'+res).val();
	var t = (r * q);
	$('#total_amount'+res).val(t);
}



function removeDiv(idd){
	$("#divRow"+idd).remove();
}
</script>


<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Send Quotation
						<small>Form </small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
						</li>
						<li class="active">Send Quotation</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg-12">
						<form class="form-horizontal" id="quotationDetails" role="form">
							<div class="portlet portlet-default">
								<div class="portlet-heading">
									<div class="portlet-title">
										<h4>Send Quotation</h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="portlet-body">
									<div class="form-group has-success">
										<label class="col-sm-2 control-label">Name</label>
										<div class="col-sm-10">
											<?php 
											if(isset($_REQUEST['name']) && $_REQUEST['name'] !=''){
												?>
												<input type="text" class="form-control" name="name" value="<?php echo $_REQUEST['name']?>" placeholder="Name" readonly required>
												<?php  
											}else{
												?>
												<input type="text" class="form-control" name="name" value="" placeholder="Name"  required>
												<?php 
											} 
											?>
											<span class="help-block"></span>
										</div>
									</div>
									<div class="form-group has-success">
										<label class="col-sm-2 control-label">Contact Number</label>
										<div class="col-sm-10">
											<?php 
											if(isset($_REQUEST['name']) && $_REQUEST['name'] !=''){
												?>
												<input type="text" class="form-control" name="contactNumber" value="<?php echo $_REQUEST['contactNumber']?>" placeholder="Contact Number" required>
												<?php  
											}else{
												?>
												<input type="text" class="form-control" name="contactNumber" placeholder="Contact Number" required>
												<?php 
											} 
											?>
											<span class="help-block"></span>
										</div>
									</div>
									<div class="form-group has-success">
										<label class="col-sm-2 control-label">Company name</label>
										<div class="col-sm-10">
											<?php 
											if(isset($_REQUEST['name']) && $_REQUEST['name'] !=''){
												?>
												<input type="text" class="form-control" name="compname" value="<?php echo $_REQUEST['companyName']?>" placeholder="Company Name" required>
												<?php  
											}else{
												?>
												<input type="text" class="form-control" name="compname" placeholder="Company Name" required>
												<?php 
											} 
											?>
											<span class="help-block"></span>
										</div>
									</div>
									<div class="form-group has-success">
										<label class="col-sm-2 control-label">Email Id</label>
										<div class="col-sm-10">
											<?php 
											if(isset($_REQUEST['name']) && $_REQUEST['name'] !=''){
												?>
												<input type="text" class="form-control" name="emailId" value="<?php echo $_REQUEST['emailId']?>" placeholder="Email ID" required>
												<?php  
											}else{
												?>
												<input type="text" class="form-control" name="emailId" placeholder="Email ID" required>
												<?php 
											} 
											?>
											<span class="help-block"></span>
										</div>
									</div>

									<?php
									if (isset($_REQUEST["address"])) {
										echo '<div class="form-group has-success">
										<label class="col-sm-2 control-label">Address</label>
										<div class="col-sm-10">
										<input type="text" class="form-control" name="address" value="'.$_REQUEST["address"].'" placeholder="Placeholder Text" required>
										<span class="help-block"></span>
										</div>
										</div>';
									}else{
										echo '<div class="form-group has-success">
										<label class="col-sm-2 control-label">Address</label>
										<div class="col-sm-10">
										<input type="text" class="form-control" name="address" placeholder="Placeholder Text" required>
										<span class="help-block"></span>
										</div>
										</div>';
									}                                            
									?>                                      

								</div>
							</div>

							<div class="portlet portlet-default">
								<div class="portlet-heading">
									<div class="portlet-title">
										<h4>Product Details</h4>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="portlet-body">
									<div id="product_div_main">
										<label>1.Product Details </label>
										<div class="form-group">
											<label class="col-sm-2 control-label">Product</label>
											<div class="col-sm-10">
												<select  class="form-control" id="product_name1" name="product_name" onchange="changeQty(this);"  required>
													<option value="" selected disabled >--------Select Product---------</option>
													<?php
													foreach ($result1 as $key => $value) {
														echo "<option id='".$result1[$key]['product_name']."1' value='".$result1[$key]['product_name']."'>".$result1[$key]['product_name']."</option>";
													}
													?>
												</select>
											</div>
										</div>
										<div class="form-group" >
											<label class="col-sm-2 control-label">Quantity</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="product_quan1" name="product_quantity" onkeyup="calculaterate(this);" placeholder="Enter Quantity" required>
											</div>
										</div>
										<div class="form-group" style="display:none;" id="div_code_validity1">
											<label class="col-sm-2 control-label">Validity</label>
											<div class="col-sm-10">
												<select  class="form-control" id="code_validity1" name="code_validity" required>
													<option value="" selected disabled >--------Select Month---------</option>
													<option value="1 Month" >1 Month</option>
													<option value="3 Months" >3 Months</option>
													<option value="6 Months" >6 Months</option>
													<option value="12 Months" >12 Months</option>
													<option value="Unlimited" >Unlimited</option>
												</select>
											</div>
										</div>

                                        <!-- <div class="form-group" style="display:none;" id="div_sms_validity1">
                                            <label class="col-sm-2 control-label">Validity</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="sms_validity1" name="sms_validity" required>
                                                  <option value="" selected disabled >-Select Month-</option>
                                                  <option value="3 Months" >3 Months</option>
                                                  <option value="6 Months" >6 Months</option>
                                                  <option value="12 Months" >12 Months</option>
                                                      
                                                </select>
                                            </div>
                                        </div> -->

                                        <div class="form-group has-success">
                                        	<label class="col-sm-2 control-label">Rate</label>
                                        	<div class="col-sm-10">
                                        		<input type="text" class="form-control" id="product_rate1" name="product_rate" onkeyup="calculaterate(this);" placeholder="Enter rate" required value="0">
                                        		<span class="help-block"></span>
                                        	</div>
                                        </div>

                                        <div class="form-group has-success">
                                        	<label class="col-sm-2 control-label">Total Amount</label>
                                        	<div class="col-sm-10">
                                        		<input type="text" class="form-control" id="total_amount1" name="total_amount" readonly required>
                                        		<span class="help-block"></span>
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="margin: 0 90px 0 0;">
                            	<label class="col-sm-2 control-label"></label>
                            	<div class="col-sm-10">
                            		<?php
                            		$q_type = "";
                            		if (isset($_REQUEST["q_type"])) {
                            			$q_type = $_REQUEST["q_type"];
                            		}else{  
                            			$q_type = 'Direct';
                            		}
                            		echo '<input type="hidden" name="quotationType" value="'.$q_type.'">';
                            		if (isset($_REQUEST["cust_id"])) {
                            			echo '<input type="hidden" name="cust_id" value="'.$_REQUEST["cust_id"].'">';
                            		}
                            		if (isset($_REQUEST["r_id"])) {
                            			echo '<input type="hidden" name="reminder_id" value="'.$_REQUEST["r_id"].'">';
                            		}
                            		?>

                            		<input type="hidden" id="appendProductName" name="appendProductName">
                            		<input type="hidden" id="appendCodeValidity" name="appendCodeValidity">
                            		<!-- <input type="hidden" id="appendSmsValidity" name="appendSmsValidity"> -->
                            		<input type="hidden" id="appendProductQty" name="appendProductQty">
                            		<input type="hidden" id="appendProductRate" name="appendProductRate">
                            		<input type="hidden" id="appendTotalAmount" name="appendTotalAmount">
                            		<h1 align="center"><button type="button" id="add_more_div" class="btn btn-default">Add Another Product</button>
                            			<button type="button"  id="sendQuotation"  class="btn btn-green">Send</button></h1>
                            			<div align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                            		</div>
                            	</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include('footer_crm.php');
        ?>