<?php

    include('header_sidebar_crm.php');

    $role_qry = "SELECT role_name, role_id FROM role WHERE status = 1";
    $role_qry_result = $con->data_select($role_qry);
 
?>
<!-- java script start -->

<script type="text/javascript" src="js_functions/admin_main.js"></script>

<script type="text/javascript">

$(document).ready(function(){

        $("#addUserSubmit").click(function(){
           var temp=validateField();
           if(temp == false)
           {
              return false;
           }
           var r = $("#reporting").val();
           var data_fields;
           if(r == null || r == "" || r == "null")
           {
                data_fields = $("#addUser").serialize()+"&reporting=0&action=addUser";
           }
           else
           {
                data_fields = $("#addUser").serialize()+"&action=addUser";
           }
            $.ajax({

                url:"ajax_service/user_registration_ajax.php",
                data:data_fields,
                success:function(data){
                    console.log(data);
                    if(data == "success")
                    {
                        alert("Data inserted successfully");
                        location.reload();
                       // location.assign("viewUser.php");
                    }
                    else
                    {
                        alert("Data not inserted");
                        
                    }
                }
            });
        });



    });




function validUser()
{

    var emailId=$("#emailId").val();
    $.ajax({
        url:"ajax_service/change_password_ajax.php",
        data:"email_id="+emailId+"&action=valid_user",
        dataType:"html",
        success:function(data){
            console.log(data);
            if (data == "yes") {
               
               $("#user_email").html("User Name already exit");              
               $("#user_email").css("color","red");
               $("#user_email").fadeIn();
               $("#user_email").fadeOut(3000);
            }
            else
            {
               $("#user_email").html("Valid  User Name");
               $("#user_email").css("color", "green");
                $("#user_email").fadeIn();
               $("#user_email").fadeOut(3000);

               //alert("Please add Resource Manager first");
            }
            }
        });
}
function validateField()
{ 
    var fname=$("#fname").val();
    var lname=$("#lname").val();
    var contactNumber=$("#contactNumber").val();
    var emailId=$("#emailId").val();
    var role=$("#role").val();
    if(fname.length == "" || fname.length =='' || fname.length == null)
    {
        alert("Please Enter Product Name");
        return false;
    }
    else if(lname.length == "" || lname.length =='' || lname.length == null)
    {
        alert("Please Enter Last Name");
        return false;
    }
    else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null)
    {
        alert("Please Enter Contact Number");
        return false;
    }
    else if(contactNumber.length != 10)
    {
        alert("Please Enter Correct format Number");
        return false;
    }
    else if(emailId.length == "" || emailId.length =='' || emailId.length == null)
    {
        alert("Please Enter Email Id");
        return false;
    }
    else if(role == "" || role =='' || role == null)
    {
        alert("Please Select Role");
        return false;
    }
    else
    {

    }
}

</script>
<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Add User
                                <small>Form </small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                                </li>
                                <li class="active">Add User</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <div class="row">

                    <!-- Validation States -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>User</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationStates" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="addUser" role="form">
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">First Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" pattern="[A-Za-z]" class="form-control" name="fname" id="fname" style="text-transform:capitalize;" placeholder="Enter First Name" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Last Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" pattern="[A-Za-z]" class="form-control" name="lname" id="lname" placeholder="Enter Last Name" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Contact Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" pattern="[0-9]" class="form-control" name="contactNumber" id="contactNumber" placeholder="Enter Contact Number" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                             <span id="user_email"></span>
                                            <label class="col-sm-2 control-label">Email Id</label>
                                            <div class="col-sm-10">
                                                <input type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" class="form-control" name="emailId" id="emailId" onkeyup="return validUser();" placeholder="Enter Email Id" required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Role</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="role" name="role" required onchange="showReportingManager(this);">
                                                    <option value="" selected disabled>---Select role---</option>
                                                   
                                                    <?php
                                                    if ($role_qry_result !='no') {
                                                        
                                                        foreach ($role_qry_result as $key => $value) {
                                                            echo '<option value="'.$role_qry_result[$key]['role_id'].'">'.$role_qry_result[$key]['role_name'].'</option>';
                                                        }

                                                    }                                                        
                                                    ?>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div id="rm_div" class="form-group" style="display:none;">
                                            <label class="col-sm-2 control-label">Reporting To</label>
                                            <div class="col-sm-10">
                                                <select  class="form-control" id="reporting" name="reporting" required>
                                                    
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="addUserSubmit" class="btn btn-default">Submit</button></h1>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                  

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
<?php
 include('footer_crm.php');
?>