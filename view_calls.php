<?php

	include('header_sidebar_crm.php');   
	//include("class/functions.php"); 

	$con = new functions();
    $user_id = $_SESSION['user_id'];

	$qry = "SELECT cd.company_name,cd.contact_number,cd.status,cd.remark,cd.c_date,ud.fname,ud.lname FROM customer_details as cd inner join user_details as ud on cd.user_id = ud.user_id WHERE cd.user_id = '".$user_id."' AND status != 'Pending' ORDER BY cd.c_date desc ";

	$result = $con->data_select($qry);

	if($_SESSION['role'] == "1" || $_SESSION['role'] == "5" || $_SESSION['role'] == "7")
    {
        //Admin,RM
        $qry = "SELECT cd.company_name,cd.contact_number,cd.status,cd.remark,cd.c_date,ud.fname,ud.lname FROM customer_details as cd inner join user_details as ud on cd.user_id = ud.user_id WHERE  cd.status != 'Pending' ORDER BY cd.cust_id desc ";

        $result = $con->data_select($qry);
    }

    
?>


 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View All Call
                                <small>View All Call</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View All Call</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View All Call</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                        <tr>
                                           <th>Sr No.</th>
                                            <th>Company Name</th>
                                            <th>Contact Number</th>
                                            <th>Status</th>
                                            <th>Remark</th>
                                            <th>Sales User Name</th>
                                            <th>Created Date</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if($result > 0){
                                            foreach ($result as $key => $value) {
                                                $sr = $key + 1;
                                                echo "<tr>";

                                                echo "<td>".$sr."</td>";
                                                echo "<td id='cnm".$sr."'>".$result[$key]['company_name']."</td>";
                                                echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
                                                echo "<td id='cno".$sr."'>".$result[$key]['status']."</td>";
                                                echo "<td id='cno".$sr."'>".$result[$key]['remark']."</td>";
                                                echo "<td id='cnm".$sr."'>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
                                                echo "<td id='cnm".$sr."'>".$result[$key]['c_date']."</td>";
                                                //echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
                                                //echo "<td><a href='#' class='btn btn-green btn-xs' data-toggle='modal' data-target='#flexModal' onclick='return accept(".$sr.",".$result[$key]['cust_id'].");'>Accept<a> | <a href='#' class='btn btn-green btn-xs' data-toggle='modal' data-target='#flexModal1' onclick='return reject(".$sr.",".$result[$key]['cust_id'].");'>Reject<a> | <a href='#' class='btn btn-green btn-xs' data-toggle='modal' data-target='#flexModal2' onclick='return delay(".$sr.",".$result[$key]['cust_id'].");'>Delay<a></td>";

                                                echo "</tr>";
                                            }
                                        }else{
                                            echo "<td colspan='7' align='center'>No Data Found</td>";                                                                                       
                                        }
                                        ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

        <script type="text/javascript">
            $(function(){
                $('#example-table').DataTable();
            });
        </script>

<?php
 include('footer_crm.php');
?>