<?php
 include('header_sidebar_crm.php');
?>

<!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="css/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/plugins/bootstrap-tokenfield/tokenfield-typeahead.css" rel="stylesheet">
    <link href="css/plugins/bootstrap-tokenfield/bootstrap-tokenfield.css" rel="stylesheet">


<script type="text/javascript">

var flag = true;
function checkPassword(idd){

    var id = idd.id;
    var password = $("#"+id).val();
   
    //alert(password);
    $.ajax({
        type: "POST",
        async: false,
        url:"ajax_service/change_password_ajax.php",
        data:"password="+password+"&action=checkPassword",
        success:function(data){
            //alert(data);
            if(data == "no"){
                $("#errormsg1").show();
                $("#errormsg1").css("color","red");
                $("#errormsg1").html("Incorrect Password!");
                $("#"+id).focus();
                $("#changePassword").attr("disabled",true);
                flag = false;
                return false;
            }
            if(data == "yes"){
                $("#errormsg1").hide();
                flag = true;
            }
        }

    });
}

function checkDuplicatePass(idd){
    var id = idd.id;
    var lastpass = $("#lastpass").val();
    var pass1 = $("#"+id).val();
    var pass2 = $("#pass2").val();
    if (lastpass == pass1) {
        $("#errormsg2").show();
        $("#errormsg2").css("color","red");
        $("#errormsg2").html("New password should not match with old password!");
        $("#"+id).focus();
        $("#changePassword").attr("disabled",true);
        return false;

    }else{
        $("#errormsg2").hide();
    }

    if(pass2 == "" || pass2 == '' || pass2 == null)
    {

    }else{
        if (pass1 == pass2) {
            $("#changePassword").attr("disabled",false);
            $("#errormsg3").hide();
        }else{
            $("#changePassword").attr("disabled",true);
            $("#errormsg3").show();
            $("#errormsg3").css("color","red");
            $("#errormsg3").html("Password does not match!");
            $("#"+id).focus();
            return false;
        }
    }

    if(flag == false){
        $("#changePassword").attr("disabled",true);
    }

}


function checkSamePass(idd){
    var id = idd.id;
    var pass1 = $("#pass1").val();
    var pass2 = $("#"+id).val();
    if (pass1 == pass2) {
        $("#changePassword").attr("disabled",false);
        $("#errormsg3").hide();
    }else{
        $("#changePassword").attr("disabled",true);
        $("#errormsg3").show();
        $("#errormsg3").css("color","red");
        $("#errormsg3").html("Password does not match!");
        $("#"+id).focus();
    }

    if(flag == false){
        $("#changePassword").attr("disabled",true);
    }

}







$(document).ready(function(){


    $("#changePassword").attr("disabled",true);

    $("#lastpass").click(function(){
        //alert();
        $("#pass2").val('');
    });

    $("#changePassword").click(function(){
        var lastpassword = $("#lastpass").val();
        var password = $("#pass2").val();
        $.ajax({
            type: "POST",
            url:"ajax_service/change_password_ajax.php",
            data:"password="+password+"&lastpassword="+lastpassword+"&action=changePassword",
            success:function(data){
                //alert(data);
                if(data == "yes"){
                    alert("Password updated successfully!");
                    location.reload();
                }

                if(data == "no"){
                    alert("Password not updated!");
                }
                
            }

        });
    });

});



</script>    

<!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Change Password
                                <small>Form </small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                                </li>
                                <li class="active">Change Password</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED FORM COMPONENTS MAIN ROW -->
                <div class="row">

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-12">

                               
                                <!-- /.portlet -->

                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Change Password</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <form class="form-horizontal" id="saveAddProduct" role="form">
                                    <div class="portlet-body">
                                        <h4>Current Password</h4><label id="errormsg1" style="display:none"></label>
                                        <input type="password" class="form-control" name="product" id="lastpass"  onchange="checkPassword(this);" placeholder="Type something and hit enter!" />
                                       
                                       <h4>New Password</h4><label id="errormsg2" style="display:none"></label>
                                        <input type="password" class="form-control" name="product" id="pass1"  onkeyup="checkDuplicatePass(this);" placeholder="Type something and hit enter!" />
                                       
                                       <h4>Re-enter Password</h4><label id="errormsg3" style="display:none"></label>
                                        <input type="password" class="form-control" name="user_password" id="pass2" onkeyup="checkSamePass(this);"  placeholder="Type something and hit enter!" />
                                       
                                        
                                    </div>

                                     <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="changePassword" class="btn btn-default">Submit</button></h1>
                                            </div>
                                    </div>
                                </form>
                                </div>
                                <!-- /.portlet -->

                            </div>
                            <!-- /.col-lg-12 (nested) -->

                        </div>
                        <!-- /.row (nested) -->

                    </div>
                    <!-- /.col-lg-6 -->

                   
                    <!-- /.col-lg-6 -->

                </div>
                <!-- /.row -->
                <!-- end ADVANCED FORM COMPONENTS MAIN ROW -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
    <!-- /#wrapper --> 
   
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/scrollspy.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/affix.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/typeahead.min.js"></script>
    <script src="js/plugins/bootstrap-maxlength/bootstrap-maxlength.js"></script>
    <script src="js/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <script src="js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

    <script src="js/demo/advanced-form-demo.js"></script>

<?php
 include('footer_crm.php');
?>