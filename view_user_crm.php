<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();
    
    if($_SESSION['role'] == "1" ){ 
        $qry = "SELECT * FROM user_details WHERE role!='1' and flag = '1' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "2"){
        $qry = "SELECT * FROM user_details WHERE flag = '1' AND reporting_id = '".$_SESSION['user_id']."' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "3"){
        $qry = "SELECT * FROM user_details WHERE flag = '1' AND reporting_id = '".$_SESSION['user_id']."' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "5"){
        $qry = "SELECT * FROM user_details WHERE flag = '1' AND reporting_id = '".$_SESSION['user_id']."' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }
    if($_SESSION['role'] == "7"){
        $qry = "SELECT * FROM user_details WHERE flag = '1' AND reporting_id = '".$_SESSION['user_id']."' ORDER BY user_id DESC";
        $result = $con->data_select($qry);
    }
    

    

       

?>

<script type="text/javascript" src="js_functions/admin_main.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $("#update").click(function(){
            alert($("#updateUserData").serialize());
            var temp= validateField()
            if(temp == false)
            {
                return false;
            }
            $.ajax({

                url:"ajax_service.php",
                data:$("#updateUserData").serialize()+"&action=updateUser",
                success:function(data){
                    console.log(data);
                    if(data == "success")
                    {
                        alert("Data updated successfully");
                        $("#popup1").hide();
                        $(".overlay").remove();
                        location.reload();
                    }
                    else
                    {
                        alert("Data not updated");
                        
                    }
                }
            });
        });

    });
        function viewData(user_id)
        {
            //alert("Inside edit "+user_id);
            $.ajax({
                url:"ajax_service.php",
                data:"user_id="+user_id+"&action=userData",
                dataType:"json",
                success:function(data){
                    //alert("inside ajax");
                    console.log(data);
                    
                    $("#user_id").val(data[0].user_id); 
                    $("#firstName").val(data[0].fname);
                    $("#lastName").val(data[0].lname);
                    $("#contactNumber").val(data[0].contact_number);
                    $("#emailId").val(data[0].email_id);
                    //$("#role").val(data[0].role);
                    $("#role"+data[0].role).attr("selected",true);
                    


                    if(data[0].flag == "1")
                    {
                        $("#status1").attr("selected",true);
                    }
                    else
                    {
                        $("#status0").attr("selected",true);
                    }

                    $("body").append("<div class='overlay js-modal-close'></div>");
                    $(".overlay").fadeTo(500, 0.9);
                    $("#popup1").show();


                }
            });
            
        }

        
        function deleteData(user_id,sr)
        {
            var confirm_delete = confirm("Are you sure you want to delete?");
            if(confirm_delete == true){
                 $.ajax({
                    url:"ajax_service.php",
                    data:"user_id="+user_id+"&action=deleteUserData",
                    success:function(data){
                        console.log(data);
                        if(data == "success")
                        {
                            alert("Data deleted successfully");
                            $("#row"+sr).fadeOut();
                            location.reload();
                        }
                        else
                        {
                            alert("Data not deleted");
                            
                        }

                    }
                });
            }
        }
           

    function validateField()
    { 
        var firstName=$("#firstName").val();
        var lastName=$("#lastName").val();
        var contactNumber=$("#contactNumber").val();
        var emailId=$("#emailId").val();
        var role=$("#role").val();
        var status=$("status").val();
        if(firstName.length == "" || firstName.length =='' || firstName.length == null)
        {
            alert("Please Enter Product Name");
            return false;
        }
        else if(lastName.length == "" || lastName.length =='' || lastName.length == null)
        {
            alert("Please Enter Last Name");
            return false;
        }
        else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null)
        {
            alert("Please Enter Contact Number");
            return false;
        }
        else if(contactNumber.length != 10)
        {
            alert("Please Enter Correct format Number");
            return false;
        }
        else if(emailId.length == "" || emailId.length =='' || emailId.length == null)
        {
            alert("Please Enter Email Id");
            return false;
        }
        else if(role == "" || role =='' || role == null)
        {
            alert("Please Select Role");
            return false;
        }
       /* else if(status == "" || status == '' || status == null)
        {
            alert("Please Select Status");
            return false;
        }*/
        else
        {

        }
    }

    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View User 
                                <small>User Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View User</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View User</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <?php
                                                    echo "<th>Sr.No</th>";
                                                    echo "<th>Name</th>";
                                                    echo "<th>Contact Number</th>";
                                                    if($_SESSION['role'] == "1"){
                                                        echo "<th>User Name</th>";
                                                        echo "<th>Password</th>";
                                                    }
                                                    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "5"){
                                                        echo "<th>Email Id</th>";
                                                    }
                                                    echo "<th>Role</th>";
                                                    if($_SESSION['role'] == "1"){
                                                        echo "<th>Created Date</th>
                                                              <th>Flag Status</th>";
                                                        echo "<th>Action</th>";
                                                    }
                                                ?>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php

                                                if($result=='no'){
                                                    echo 'NO DATA FOUND!';
                                                }else{

                                              
                                                foreach ($result as $key => $value) {
                                                    $sr = $key + 1;
                                                    echo '<tr id="row'.$sr.'">';

                                                    echo "<td>".$sr."</td>";
                                                    echo "<td>".$value['fname']." ".$value['lname']."</td>";
                                                    echo "<td>".$value['contact_number']."</td>";
                                                    echo "<td>".$value['email_id']."</td>";
                                                    if($_SESSION['role'] == "1"){
                                                       echo "<td>".$value['password']."</td>";
                                                    }
                                                    //echo "<td>".$result[$key]['role']."</td>";
                                                    if($value['role'] == 2)
                                                    {
                                                        echo "<td>BDM</td>";
                                                    }
                                                    if($value['role'] == 3)
                                                    {
                                                        echo "<td>BDE</td>";
                                                    }
                                                    if($value['role'] == 4)
                                                    {
                                                        echo "<td>Telle Caller</td>";
                                                    }
                                                    if($value['role'] == 5)
                                                    {
                                                        echo "<td>Resource Manager</td>";
                                                    }
                                                    if($value['role'] == 6)
                                                    {
                                                        echo "<td>Support</td>";
                                                    }
                                                    if($value['role'] == 7)
                                                    {
                                                        echo "<td>Accounts</td>";
                                                    }
                                                     if($value['role'] == 8)
                                                    {
                                                        echo "<td>Business Head</td>";
                                                    }
                                                     if($value['role'] == 9)
                                                    {
                                                        echo "<td>Head-Strategic Business Unit</td>";
                                                    }

                                                   
                                                    //echo "<td>".$result[$key]['flag']."</td>";
                                                    if($_SESSION['role'] == "1"){
                                                         echo "<td>".$value['c_date']."</td>";

                                                        if($value['flag'] == 1)
                                                        {
                                                            echo "<td>Active</td>";
                                                        }
                                                        if($value['flag'] == 0)
                                                        {
                                                            echo "<td>Deactive</td>";
                                                        }
                                                            echo '<td><div class="btn-group" role="group"><a href="#" class="btn btn-green btn-xs" data-toggle="modal" data-target="#flexModal" onclick="return viewData('.$value["user_id"].');">Edit</a><a href="#" class="btn btn-red btn-xs ladda-button" onclick="deleteData('.$value["user_id"].','.$sr.')">Delete</a></div></td>';
                                                    }
                                                    echo "</tr>";
                                                }
                                               }   
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit User</h4>
                </div>
                <form id="updateUserData" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                        <input type="hidden" id="user_id" name="user_id">                   

                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter First Name" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Contact No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Enter Contact Number" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <!-- <input type="text" class="form-control" id="role" name="role" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span> -->
                                    <select  class="form-control" id="role" name="role" required onchange="showReportingManager(this);">
                                        <option value="" selected disabled>---Select role---</option>
                                        <option id="role5" value="5">RM</option>
                                        <option id="role2" value="2">BDM</option>
                                        <option id="role3" value="3">BDE</option>
                                        <option id="role4" value="4">Telle Caller</option>
                                        <option id="role4" value="6">Supports</option>
                                        <option id="role4" value="7">Accounts</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div id="rm_div" class="form-group" style="display:none;">
                                <label class="col-sm-2 control-label">Reporting To</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="reporting" name="reporting" required>
                                        
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="status" name="status" required>
                                        <option value="">Select One:</option>
                                        <option id="status1" value="1">Active</option>
                                        <option id="status0" value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>
                             
                     
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                    <button type="button" id="update" class="btn btn-green">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <script type="text/javascript">
    $(function(){
        $('#example-table').DataTable();
    });
    </script>
    
<?php
 include('footer_crm.php');
?>