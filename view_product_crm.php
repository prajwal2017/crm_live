<?php
    include('header_sidebar_crm.php');

  // include("class/functions.php");

    $con = new functions();

    $qry = "SELECT * FROM products";

    $result = $con->data_select($qry);

    /*echo "<pre>";
    print_r($result);*/
       

?>
<script type="text/javascript">

    $(document).ready(function(){

       $("#updateProductDetails").click(function(){

            $.ajax({
            url:"ajax_service.php",
            data:$("#updateProductData").serialize()+"&action=updateProductDetails",
            success:function(data){
               $("#updateProductDetails").hide();
               $("#ajax-loader").show();
                if (data == "success") {
                    $("#updateProductDetails").show();
                    $("#ajax-loader").hide();
                    alert("Product updated successfully");
                    
                    location.reload();
                }
                else
                {
                    $("#updateProductDetails").show();
                    $("#ajax-loader").hide();
                    alert("Product not updated");
                }
            }
            });
        });

    });
    function deleteProduct(pid,sr)
    {

        //alert();
        var confirm_delete = confirm("Are you sure you want to delete?");
        if(confirm_delete == true){

             $.ajax({
                url:"ajax_service.php",
                data:"pid="+pid+"&action=deleteProduct",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == 1) {
                        alert("Product deleted successfully");
                        $("#row"+sr).fadeOut();
                        location.reload();
                    }
                    else
                    {
                        alert("Product not deleted");
                    }
                }
            });


        }
                
       
    }

    function updateProduct(pid,sr,flag)
    {
      
        $("#editProduct").val($("#pnm"+sr).html());
        $("#product_p_id").val(pid);
        if (flag == 1) {
            $("#status1").attr("selected",true);
        }
        if (flag == 0) {
            $("#status0").attr("selected",true);
        }

        

    }
            

    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Product 
                                <small>Product Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Product</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Product</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>SrNo.</th>
                                                <th>Product Name</th>
                                                <th>Created Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                            foreach ($result as $key => $value) {
                                                $sr = $key + 1;
                                                echo "<tr id='row".$sr."'>";
                                                echo "<td>".$sr."</td>";
                                                echo "<td id='pnm".$sr."'>".$result[$key]['product_name']."</td>";
                                                echo "<td>".$result[$key]['c_date']."</td>";
                                                if ($result[$key]['flag'] == 1) {
                                                    echo "<td>Active</td>";
                                                }
                                                if ($result[$key]['flag'] == 0) {
                                                    echo "<td>Deactive</td>";
                                                }
                                                echo "<td><a href='#' class='btn btn-green btn-xs' data-toggle='modal' data-target='#flexModal' onclick='updateProduct(".$result[$key]['p_id'].",".$sr.",".$result[$key]['flag'].");'>Edit</a> | <a href='#' class='btn btn-red btn-xs' onclick='deleteProduct(".$result[$key]['p_id'].",".$sr.");'>Delete</a></td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit Product</h4>
                </div>
                <form id="updateProductData" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                        <input type="hidden" id="product_p_id" name="p_id">                 

                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Product</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="editProduct" name="editProduct" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="status" name="status" required>
                                       <option value="" disabled selected>--------Change Status---------</option>
                                       <option id="status1" value="1">Active</option>
                                       <option id="status0" value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>
                           
                    
                </div>
                <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                   <button type="button" id="updateProductDetails" class="btn btn-green">Save changes</button> 
                </div>
                </form> 
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



    
<?php
 include('footer_crm.php');
?>