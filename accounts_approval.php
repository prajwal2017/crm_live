<?php
error_reporting(E_ALL ^ E_NOTICE);
include('header_sidebar_crm.php');   
$con = new functions();
$sql = "SELECT ud.fname,ud.lname,pp.pay_mode,cut.* from confirm_user_temp as cut INNER JOIN product_payment as pp on pp.client_id = cut.crm_client_id INNER JOIN user_details as ud on ud.user_id = cut.sales_id ORDER BY cut.updated desc ";
$customerData = $con->data_select($sql); 

?>

<div id="page-wrapper">
	<div class="page-content page-content-ease-in">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>View Accounts
						<small>Customer Accounts Details</small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
						</li>
						<li class="active">View Customer Accounts</li>
					</ol>
				</div>
			</div>
		</div>

		
		<br>

		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>View Customer Accounts</h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered table-hover table-green">
								<thead id="cust_thead">
									<tr>
										<th>Sr No.</th>
										<th>Company Name</th>
										<th>Contact Number</th>
										<th>Customer Name</th>
										<th>Email Id</th>
										<th>SENDER ID</th>
										<th>qty</th>
										<th>rate</th>
										<th>Sales Person</th>
										<?php
										if($_SESSION['role'] == "1" ||$_SESSION['role'] == "2"||$_SESSION['role'] == "7"||$_SESSION['role'] == "8" || $_SESSION['role'] == "9"){
											echo "
											
											<th>Client Status</th>
											<th>Approved by</th>
											<th>Pay-Mode</th>
											<th>Remark</th>

											
											
											";
										}
										
										?>
									</tr>
								</thead>
								<tbody id="cust_data">
									<?php
									if($customerData > 0){
										foreach ($customerData as $key => $value) {
											$sr = $key + 1;
											echo "<tr id='row".$sr."'>";
											echo "<td>".$sr."</td>";
											echo "<td id='cnm".$sr."'>".$customerData[$key]['company_name']."</td>";
											echo "<td id='cno".$sr."'>".$customerData[$key]['mob_no']."</td>";
											echo '<td id="custName'.$sr.'"><a href="#" title="Click here to edit name" class="customer_name" data-type="text" data-pk="'.$customerData[$key]['cust_id'].'" data-placement="right" data-placeholder="Required" data-title="Enter customer name" class="editable editable-click editable-empty">'.$customerData[$key]['name'].'</a></td>';
											echo "<td id='custEmail".$sr."'>".$customerData[$key]['email_id']."</td>";
											echo "<td id='custEmail".$sr."'>".$customerData[$key]['sender_id']."</td>";
											echo "<td  id='cqt".$sr."'>".$customerData[$key]['quantity']."</td>";
											echo "<td  id='crt".$sr."'>".$customerData[$key]['rate']."</td>";
											echo "<td  id='snm".$sr."'>".$customerData[$key]['fname']." ".$customerData[$key]['lname']."</td>";
											if($_SESSION['role'] == 2 || $_SESSION['role'] == 7 || $_SESSION['role'] == 8){
												
												if($customerData[$key]['status']==3 ||$customerData[$key]['status']==2 || $customerData[$key]['status']==1 || $customerData[$key]['status']==8)
												{
													if($customerData[$key]['status']==2 || $customerData[$key]['status']==8){
														echo "<td class='text-center'><a class='btn btn-xs  btn-success')>Approved by HSBU</a></td>";
														echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
													}elseif($customerData[$key]['status']==1 && $_SESSION['role'] == 8){
														echo "<td class='text-center'><a class='btn btn-xs  btn-success disabled')>Approved</a></td>";
														echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
													}elseif($customerData[$key]['status']==3 && $_SESSION['role'] == 8){
														echo "<td class='text-center'><a class='btn btn-xs  btn-success disabled')>Approved by BH</a></td>";
														echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
													}
												}else{
													echo "<td class='text-center'><a class='btn btn-xs btn-danger' onClick='approve2(".$customerData[$key]['crm_client_id'].");'>Not Approved</a></td>";
													echo "<td align='center'><a title=''><a></td>";
												}
											}elseif($_SESSION['role']==1){
												if($customerData[$key]['status']==1){
													echo "<td class='text-center'><a class='btn btn-xs  btn-success disabled')>Approved</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
												}elseif($customerData[$key]['status'] == 3 || $customerData[$key]['status'] == 0){
													echo "<td class='text-center'><a class='btn btn-xs btn-danger');'>Approved by BH</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
												}else{
													echo "<td class='text-center'><a class='btn btn-xs btn-danger' onClick='approve(".$customerData[$key]['crm_client_id'].");'>Not Approved</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
												}
											}
											elseif($_SESSION['role']==9){
												if($customerData[$key]['status']==1){
													echo "<td class='text-center'><a class='btn btn-xs  btn-success disabled')>Approved</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
												}elseif($customerData[$key]['status']==2)
												{	
													echo "<td class='text-center'><a class='btn btn-xs  btn-success disabled')>Approved by HSBU</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";

												}elseif($customerData[$key]['status']==3)
												{
													echo "<td class='text-center'><a class='btn btn-xs btn-danger' onClick='approveHead(".$customerData[$key]['crm_client_id'].");'>Not Approved</a></td>";
													echo "<td align='center'><a title=''>".$customerData[$key]['approved_by']."<a></td>";
												}else{
													echo "<td class='text-center'><a class='btn btn-xs btn-danger' onClick='approve(".$customerData[$key]['crm_client_id'].");'>Not Approved</a></td>";
													echo "<td align='center'><a title=''><a></td>";
												}
											}
											echo "<td>".$customerData[$key]['pay_mode']."</td>";
											echo "<td>".$customerData[$key]['remark']."</td>";	
											echo "</tr>";
										}
									}                                           
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	/*$(function(){
		$('#accountTable').DataTable();
		
	});*/
	$(document).ready(function() {
		var table = $('#example').DataTable( {
			responsive: true
		} );


	} )
	function approve(id){
		var con = confirm("Are you Sure!");
		if(con == false){
			return false;
		}else{
			var remark=prompt("Please add a remark",'');
			if(remark == null){

				alert("Remark cannot be blank");
				return false;
			}else{
				alert(remark);
			}
		}

			// return false;
			$.ajax({
				url:'ajax_service/approve.php',
				data:'remark='+remark+'&id='+id+'&action=businessApprove',
				dataType:'JSON',
				beforeSend:function(i){

				},
				success:function(response){
					console.log(response);
					if(response.code ==200){
						alert(response.message);
						location.reload();
					}else{
						alert(response.message);
					}
				}
			})
			
		}
function approve2(id){
	var con = confirm("Are you Sure!");
	if(con == false){
		return false;
	}else{
		var remark=prompt("Please add a remark",'');
		if(remark == null){
			
			alert("Remark cannot be blank");
			return false;
		}else{
			alert(remark);
		}
	}


	$.ajax({
		url:'ajax_service/approve.php',
		data:'remark='+remark+'&id='+id+'&action=adminApprove',
		dataType:'JSON',
		beforeSend:function(i){

		},
		success:function(response){
			console.log(response);
			if(response.code == 200){
				alert(response.message);
				location.reload();
			}else{
				alert(response.message);
			}
		}
	})
}

function approveHead(id){
	var con = confirm("Are you Sure!");
	if(con == false){
		return false;
	}else{
		var remark=prompt("Please add a remark",'');
		if(remark == null){
			
			alert("Remark cannot be blank");
			return false;
		}else{
			alert(remark);
		}
	}


	$.ajax({
		url:'ajax_service/approve.php',
		data:'remark='+remark+'&id='+id+'&action=adminApproveFinal',
		dataType:'JSON',
		beforeSend:function(i){

		},
		success:function(response){
			console.log(response);
			if(response.code == 200){
				alert(response.message);
				location.reload();
			}else{
				alert(response.message);
			}
		}
	})
}
</script>
<?php
include('footer_crm.php');
?>
