<?php
include('header_sidebar_crm.php');

$con = new functions();
$user_id = $_SESSION['user_id'];

if($_SESSION['role'] == "3"){
	// $qry = "SELECT * FROM credit_request_new WHERE sales_id = '".$user_id."' AND status = '0'";
	$qry = "SELECT * FROM credit_request WHERE sales_id = '".$user_id."' AND status = '0'";
	$result = $con->data_select($qry);
	
}
?>

<script type="text/javascript" src="js_functions/admin_main.js"></script>

<script type="text/javascript">

	$(document).ready(function(){

		$("#requestCreditBtn").click(function(){

			$.ajax({
				url:"ajax_service.php",
				data:$("#requestCreditData").serialize()+"&action=creditRequestNewUpdate",
				beforeSend:function(){
					$(".ajax-loader").show();
					$("#addMksCredit").hide();
					$(".close-btn").hide();
				},
				success:function(data){
					
                    if(data == "1"){
                    	$("#creditRequestModal .close").click();
                    	alert("Request sent successfully.");                       
                    	location.reload();
                    }else{
                    	alert("Not Credited");
                    }
                },
                complete:function(){
                	$("#addMksCredit").show();
                	$(".close-btn").show();
                	$(".ajax-loader").hide();
                }
            });
		});

	});
</script>


<div id="page-wrapper">
	<div class="page-content">
		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>Credit Request 
						<small>Request Details</small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
						</li>
						<li class="active">Credit Request</li>
					</ol>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<!-- end PAGE TITLE ROW -->

		<!-- begin ADVANCED TABLES ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>Credit Request</h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table id="example-table" class="table table-striped table-bordered table-hover table-green">
								<thead>
									<tr>
										<th>Sr.No</th>
										<th>User Name</th>                    
										<th>Product</th>
										<th>Quantity</th>
										<th>Rate per SMS</th>
										<th>Action</th>   
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($result as $key => $value) {
										$sr = $key + 1;
										echo '<tr id="row'.$sr.'">';

										echo "<td>".$sr."</td>"; 
										echo "<td>".$result[$key]['user_name']."</td>";
										echo "<td>".$result[$key]['product']."</td>";
										echo "<td>".$result[$key]['quantity']."</td>";
										echo "<td>".$result[$key]['rate']."</td>";
										$get_credit_status = "SELECT id FROM credit_request WHERE u_id ='".$result[$key]['u_id']."' AND product = '".$result[$key]['product']."' AND status = 0 ";
										$result_credit_status = $con->data_select($get_credit_status);
											if($result_credit_status =='no'){
												echo "<td class='text-center'><a class='btn btn-xs btn-success' onclick='creditRequest(\"".$result[$key]['company_name']."\",\"".$result[$key]['user_name']."\",".$result[$key]['balance'].",".$result[$key]['u_id'].",\"".$result[$key]['product_name']."\")';>Credit Request</a></td>";
											}else{
												echo "<td class='text-center'><span class='btn btn-xs btn-danger'>Request sent</span></td>";
											}
											echo "</tr>";
										/*echo '<td class="text-center"><div class="btn-group" role="group"><a href="#" class="btn btn-green btn-xs credit_modal" data-toggle="modal" data-target="#creditRequestModal" data-id="'.$result[$key]["id"].'" data-quantity="'.$result[$key]["quantity"].'" data-product="'.$result[$key]["product"].'" data-balance="'.$result[$key]["quantity"].'">Credit</a></div></td>';
										echo "</tr>";*/
									}
									?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.portlet-body -->
				</div>
				<!-- /.portlet -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.page-content -->
</div>
<!-- /#page-wrapper -->
<!-- end MAIN PAGE CONTENT -->

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="creditRequestModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="requestCreditData" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="flexModalLabel">Credit Allocation For : <span id="user_name"></span></h4>
				</div>
				<div class="modal-body">
					<div id="product_div_main">
						<label>1.Product Details </label> <br><br>
						<input type="hidden" id="id" name="id">
						<!-- <div class="form-group has-success">
							<label class="col-sm-2 control-label">Balance</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="balance" name="balance" placeholder="Placeholder Text" readonly required>
								<span class="help-block"></span>
							</div>
						</div>  -->            
						<div class="form-group">
							<label class="col-sm-2 control-label">Product</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="product" name="product" placeholder="Placeholder Text" readonly required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" >
							<label class="col-sm-2 control-label" id="label1">Quantity</label>
							<div class="col-sm-10">
                                <!-- <select  class="form-control" id="product_quantity" name="product_quantity" onchange="changeRate(this);" required>
                                    <option value="" selected disabled ></option>
                                </select> -->
                                <input type="text" class="form-control" id="product_quantity" name="product_quantity" placeholder="Enter Quantity" required readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group has-success">
                        	<label class="col-sm-2 control-label">Rate per unit</label>
                        	<div class="col-sm-10">
                        		<input type="text" class="form-control" id="product_rate" name="product_rate" onkeyup="calculaterate(this);" placeholder="Enter rate per unit" required>
                        		<span class="help-block"></span>
                        	</div>
                        </div>
                        <div class="form-group has-success">
                        	<label class="col-sm-2 control-label"><span style="margin: 0 10px 0 0;">Tax</span> <input type="checkbox" style="border: 2px solid #16A085;width: 15px;height: 15px;" id="send_tax" name="send_tax" onclick="includeTax(1);"> </label>
                        	<div class="col-sm-10">
                        		<input type="text" class="form-control" id="tax" name="tax" value="18%"  readonly required>
                        		<span class="help-block"></span>
                        	</div>
                        </div>
                        <div class="form-group has-success">
                        	<label class="col-sm-2 control-label">Total Amount</label>
                        	<div class="col-sm-10">
                        		<input type="hidden" id="hidden_total_amount" name="hidden_total_amount" placeholder="Placeholder Text" readonly>
                        		<input type="text" class="form-control" id="total_amount" name="total_amount" placeholder="0" readonly required>
                        		<span class="help-block"></span>
                        	</div>
                        </div>
                        <div class="form-group">
                        	<label class="col-sm-2 control-label">Payment Mode</label>
                        	<div class="col-sm-10">
                        		<select  class="form-control" id="paymode" name="paymode" required>
                        			<option value="" selected disabled>--------Select Payment Mode---------</option>
                        			<option value="Cash" >Cash</option>
                        			<option value="Check" >Check</option>
                        			<option value="Online" >Online</option>
                        		</select>
                        	</div>
                        </div>
                        <div class="form-group has-success">
                        	<label class="col-sm-2 control-label">Received By</label>
                        	<div class="col-sm-10">
                        		<input type="text" class="form-control" id="amount_received" name="amount_received" value="<?php echo  $session_result[0]['fname']." ".$session_result[0]['lname']; ?>" placeholder="Placeholder Text" required>
                        		<span class="help-block"></span>
                        	</div>
                        </div>
                    </div> 
                </div>
                <div class="modal-footer">
                	<div >
                		<br>
                		<button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                		<button type="button" class="btn btn-green" id="requestCreditBtn" >Send Request</button>
                	</div>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<script type="text/javascript">
	$(function(){
		$('#example-table').DataTable();
	});

	

	$(document).on("click", ".credit_modal", function () {
		var id = $(this).data('id');
		$(".modal-body #id").val( id );
		/* var product_quantity = $(this).data('quantity'); */
		var balance = $(this).data('balance');
		$(".modal-body #balance").val( balance );
		$(".modal-body #product_quantity").val( balance );
		var product = $(this).data('product');
		$(".modal-body #product").val( product );
	});


	$(document).on('hide.bs.modal','#creditRequestModal', function () {
		$('#requestCreditData').trigger("reset");
	});



	function calculaterate(e){
		var rate = $("#product_rate").val();
		if(rate == ''){
			rate = '0';
		}else{
			rate = e.value; 
		}

		var balance = $("#product_quantity").val();
		
		var amount = '';
		if($('#send_tax').is(":checked")){
			var total = parseFloat(rate) * parseFloat(balance);
			var percentage_val = (total / 100) * 18;
			amount = total + percentage_val;
		}else{
			amount = parseFloat(rate) * parseFloat(balance);
		}
		$("#total_amount").val(amount);
	}



	function includeTax(e){
		var rate = $("#product_rate").val();
		var new_rate = '';
		if(rate == ''){
			new_rate = '0';
		}else{
			new_rate = rate;
		}

		var balance = $("#product_quantity").val();
		
		var amount = '';
		if($('#send_tax').is(":checked")){
			var total = parseFloat(new_rate) * parseFloat(balance);
			
			var percentage_val = (total / 100) * 18;
			amount = total + percentage_val;
		}else{
			amount = parseFloat(rate) * parseFloat(balance);
		}
		$("#total_amount").val(amount);
	}
</script>

<?php
include('footer_crm.php');
?>