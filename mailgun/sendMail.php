<?php

	$root = $_SERVER['DOCUMENT_ROOT'];

	include('mailFunction.php');

	$obj = new mailFunction();

	$responseBody = array();
	/*echo json_encode($_REQUEST);
	exit;*/
	$responseBody['status'] = 0;
	$responseBody['message'] = "";

	$mailTo = $_REQUEST['mailTo'];
	$fromEmail = $_REQUEST['fromEmail'];
	$subject = $_REQUEST['subject'];
	$message = $_REQUEST['message'];

	$html = '<html>';
	$html .= '<body>';
	$html .= $message;
	$html .= '</body>';
	$html .= '</html>';

	$data = array();

	$data['mailTo'] = explode(',', $mailTo);
	$data['attachment'][0] = 'no';
	
	$count = count($_FILES['file']['name']);
	for ($i = 0; $i < $count; $i++) {

		$tmpFilePath = $_FILES['file']['tmp_name'][$i];

	  	if ($tmpFilePath != ""){
	    	
	    	$newFilePath = "attachments/" . $_FILES['file']['name'][$i];
	   	 	
	    	if(move_uploaded_file($tmpFilePath, $newFilePath)) {
	      		$data['attachment'][$i] = $root.'/crm/mailgun/attachments/'.$_FILES['file']['name'][$i];
	    	}else{
	    		$responseBody['status'] = 400;
				$responseBody['message'] = "Sorry, there was an error uploading your file.".$_FILES['file']['name'][$i];
	    		echo json_encode($responseBody);
	    		exit;
	    	}
	  	}
	}

	/*echo "<pre>";
	print_r($data['attachment']);
	exit;*/

	if(isset($_REQUEST['mailCC']))
	{
		$data['mailCC'] = explode(',', $_REQUEST['mailCC']);
	}else
	{
		$data['mailCC'][0] = 'no';
	}

	if(isset($_REQUEST['mailBcc']))
	{
		$data['mailBcc'] = explode(',', $_REQUEST['mailBcc']);
	}else
	{
		$data['mailBcc'][0] = 'no';
	}


	$data['fromEmail'] = $fromEmail;
	$data['subject'] = $subject;
	$data['message'] = $html;

	/*echo count($data['mailTo']);
	exit;*/

	/*echo "<pre>";
	print_r($data);
	exit;*/

	$result =  $obj->sendMail($data);

	echo json_encode($result);

?>