<?php 

require '../../mail-gun/phpmailer/vendor/autoload.php';

include('Mailgun\Mailgun');

# Instantiate the client.
$mgClient = new Mailgun('YOUR_API_KEY');
$domain = "YOUR_DOMAIN_NAME";

# Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => 'Excited User <YOU@YOUR_DOMAIN_NAME>',
    'to'      => 'bob@example.com, alice@example.com',
    'subject' => 'Hey %recipient.first%',
    'text'    => 'If you wish to unsubscribe,
                          click http://mailgun/unsubscribe/%recipient.id%',
            'recipient-variables' => '{"bob@example.com": {"first":"Bob", "id":1},
                                       "alice@example.com": {"first":"Alice", "id": 2}}'
));

?>