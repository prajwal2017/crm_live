<?php 

// require '../../mail-gun/phpmailer/vendor/autoload.php';
$root = $_SERVER['DOCUMENT_ROOT'];
ini_set('memory_limit', '2048M');

$smtp_path = $root."/crm/smtpmail/class.phpmailer.php";
include ($smtp_path); 
class mailFunction {

	public function sendMail($data)
	{
		$mail = new PHPMailer;

		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'contact@mobisofttech.co.in';
		$mail->Password = 'mobi0240';
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465; // or 587
		$mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
	    $mail->From = $data['fromEmail'];
		$mail->FromName = 'Makemysms.in';
		
		$mail->isHTML(true);

		$mail->Subject = $data['subject'];
		$mail->Body    = $data['message'];	

		$responseBody = array();
		$responseBody['status'] = 0;
		$responseBody['message'] = "";	
		
		//Attachments
		if($data['attachment'][0] != 'no'){
			$AddAttachmentLength=count($data['attachment']);
			for($x=0;$x<$AddAttachmentLength;$x++)
	      	{
	        	$mail->AddAttachment($data['attachment'][$x]);
	      	}
		}

		//AddCC
		if($data['mailCC'][0] != 'no'){
    		$AddCCLength=count($data['mailCC']);
    		for($j=0;$j<$AddCCLength;$j++)
	      	{
	        	$mail->AddCC($data['mailCC'][$j]);
	      	}
    	}
    	
    	//AddBCC
      	if($data['mailBcc'][0] != 'no'){
      		$AddBCCLength=count($data['mailBcc']);
      		for($k=0;$k<$AddBCCLength;$k++)
	      	{
	        	$mail->AddBCC($data['mailBcc'][$k]);
	      	}
      	} 

      	//AddAddressess
		if($data['mailTo'][0] != 'no'){
			$addAddressLength=count($data['mailTo']);
			for($i=0;$i<$addAddressLength;$i++)
	      	{
	        	$mail->AddAddress($data['mailTo'][$i]);

		      	if(!$mail->send()) {  
		      		$responseBody['status'] = 400;
					$responseBody['message'] = "Message hasn't been sent.";
					$responseBody['data'] = $mail->ErrorInfo. "\n";
					return $responseBody;				   
				} else {
					$responseBody['status'] = 200;
					$responseBody['message'] = "Message has been sent.";
				}

				$mail->ClearAddresses();
	      	}
		}      	

		return $responseBody;
	}

}

?>