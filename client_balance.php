<?php

include('header_sidebar_crm.php');
include('class2/functions2.php');
include('class5/functions5.php');

$con = new functions();
$con2 = new functions2();
$con5 = new functions5();
    if($_SESSION['role'] == "4")//TC
    {
    	header("Location:index.php");
    }
    
    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3")//BDE AND BDM
    {
    	$user_id = $_SESSION['user_id'];
    	/*$qry = "SELECT u.u_id,u.company_name, u.name, u.user_name,u.product_name,sb.balance,sb.sent_sms FROM user AS u INNER JOIN sms_balance sb ON u.u_id = sb.u_id WHERE u.sales_id = '".$user_id."' ORDER BY u.u_status ASC ";*/
        //$qry = "SELECT * FROM client_details WHERE client_status = 'C' AND bde_user_id = '".$user_id."' ";
       /* $qry = "SELECT cd.user_id as sales_id,u.u_id,u.company_name, u.name, u.user_name,u.product_name,sb.balance,sb.sent_sms FROM user AS u INNER JOIN sms_balance sb ON u.u_id = sb.u_id INNER JOIN customer_details cd ON sb.u_id =cd.cust_id  WHERE cd.user_id = '".$user_id."' ORDER BY u.u_status ASC ";
    	$result = $con2->data_select($qry);*/
    	 $qry = "SELECT u.id as u_id,u.company_name, u.name, u.user_name,p.name as product_name,sb.balance,sb.sent_sms FROM user AS u INNER JOIN sms_balance sb ON u.id = sb.user_id INNER JOIN product as p on sb.product_id = p.id WHERE u.salesperson_id = '".$user_id."' and u.status = 1 ORDER BY u.created ASC ";


    	$result = $con5->data_select($qry);

    	$get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND reporting_id = '".$user_id."' AND flag = '1' ";
    	$get_bde_result = $con->data_select($get_bde);
    }

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "5")
    {
        //Admin,RM
        //$qry = "SELECT * FROM client_details WHERE client_status = 'C' ";
    	/*$qry = "SELECT u.u_id,u.company_name, u.name, u.user_name,u.product_name,sb.balance,sb.sent_sms FROM user AS u INNER JOIN sms_balance sb ON u.u_id = sb.u_id ORDER BY u.u_status ASC ";
    	$result = $con2->data_select($qry);*/

    	$qry = "SELECT u.id as u_id,u.company_name, u.name, u.user_name,p.name as product_name,sb.balance,sb.sent_sms FROM user AS u INNER JOIN sms_balance sb ON u.id = sb.user_id  INNER JOIN product as p on sb.product_id = p.id where u.status=1 ORDER BY u.created DESC ";
    	$result = $con5->data_select($qry);

    }


    ?>

    <style type="text/css">
    .modal-body{
    	max-height: 370px !important;
    	overflow-y: auto !important;
    }
</style>
<script type="text/javascript">

	$(function(){
		$("#viewSelfCustomers").click(function(){
			location.reload();            
		});

		$("#viewCustomers").click(function(){
			var user_id = $("#bde_name").val();
			if(user_id == ''){
				alert('Please select BDE');
				return false;
			}
        //alert(user_id);
        $.ajax({
        	url:"ajax_service/mks_customer_ajax.php",                
        	data:"user_id="+user_id+"&action=getMksUserBalanceData",
            //async: false,
            //dataType:"json",
            success:function(data){                
                //console.log(data);
                //alert(data);
                var oTable = $("#example-table").dataTable();
                oTable.fnDestroy();                    
                $("#cust_tbody").html(data);
                $("#example-table").dataTable();
            }

        });
    });

		$("#requestCreditBtn").click(function(){

        //alert(user_id);
        var formData = $("#requestCreditData").serialize();
        alert(formData);
        // return false;
        $.ajax({
        	url:"ajax_service/mks_customer_ajax.php",
        	data:$("#requestCreditData").serialize()+"&action=getCreditRequestData",  
        	async: false,
            dataType:"json",
            success:function(data){                
                console.log(data);
                /*return false;*/
                if(data == "1")
                {
                	$("#creditRequestModal .close").click();
                	alert("Credited requested successfully");                       
                	location.reload();
                }
                else
                {
                	alert("Not Credited");

                }
                
            }

        });
    });


	});

	function creditRequest(compName,userName,balance,uid,pName){
		$('#creditRequestModal').modal({
			show: 'false'
		});
		// getQty(pName);
		$('#user_id').val(uid);
		$('#balance').val(balance);
		$('#product').val(pName);
		$('#user_name').html(userName);

		$('#userName').val(userName);
		$('#compName').val(compName);
	}


	function getQty(pname) {

		var product_name = pname;
		var encoded = encodeURIComponent(product_name);       

		$("#total_amount").val('');
		$("#product_rate").val('');
		$('#tax_amount').val('');

		var comp_product = product_name.replace(/ /g,'');
		comp_product = comp_product.toLowerCase();

		if(comp_product == "shortcode" || comp_product == "longcode"){

			$("#div_code_validity").show();

		}else{

			$("#div_code_validity").hide();

		}


		$.ajax({
			url:"ajax_service/view_leads_ajax.php",
			data:"product_name="+encoded+"&action=getProductQty",              
       // dataType:'json',
       success:function(data){

       	$("#product_quantity").html(data);
       }
   });

	}

	function calculaterate(idd)
	{

		var id = idd.id;
		var res = id.substring(12, id.length);

		var str = $('#'+id).val();


		var checked = $("#send_tax").attr('checked');

		var r = $('#product_rate').val();
		var q = $('#product_quantity').val();

		var t = (r * q);
		/*alert(t);*/
		/*alert(Math.round(t));*/
		$('#total_amount').val(Math.round(t));
		$('#hidden_total_amount').val(Math.round(t));

		if(checked){ 

      //$("#tax_amount").val('');
      var tax = $("#tax").val();
      var total_amt = $('#hidden_total_amount').val();

      var tax_amt = total_amt * tax / 100;
      $('#tax_amount').val(tax_amt);
      var grand_total =  parseInt(total_amt) + parseInt(tax_amt);
      $('#hidden_total_amount').val(t);
      $('#total_amount').val(grand_total);

  }

}
function changeRate(idd) {

	$("#total_amount").val('');
	$("#product_rate").val('');
	$("#tax_amount").val('');
}

function includeTax(idd){
	var id = idd.id;
	
	var checked = $("#"+id).attr('checked');
	
	if(checked){ 
		$("#"+id).attr('checked', false);

		$("#"+id).val('N');
		$("#tax_amount").val('');
		var htotal_amt = $('#hidden_total_amount').val();

		if(htotal_amt == "" || htotal_amt == '' || htotal_amt == null){
			$("#total_amount").val(0);

		}else{
			$("#total_amount").val(htotal_amt);
		}

	}
	else{ 

		$("#"+id).attr('checked', true);
       //alert("in true");
       $("#"+id).val('Y');
       var total_amt = $('#hidden_total_amount').val();

       if(total_amt == "" || total_amt == '' || total_amt == null){

       	$("#total_amount").val(0);
       	$("#tax_amount").val('');

       }else{

       	var tax = $("#tax").val();

       	var tax_amt = total_amt * tax / 100;
       	$('#tax_amount').val(tax_amt);
       	var grand_total =  parseInt(total_amt) + parseInt(tax_amt);
       	$('#total_amount').val(grand_total);

       }


   }

}

function checkNumber(idd)
{
    //alert("id: "+idd.id);
    var id = idd.id;

    if(idd.value == "Check")
    {
    	$("#check_number_div").show();
    }
    else
    {
    	$("#check_number_div").hide();
    }
}

function setValue123(idd){

	var id = idd.id;
    //alert(id);
    var v = $("#"+id).val();
       // alert(v);
       $("#assigned_user_id").val(v);
       if(id == "bde_name"){
       	$("#bdm_name").val('');
       }

       if(id == "bdm_name"){
       	$("#bde_name").val('');
       }

   }

</script>


<div id="page-wrapper">

	<div class="page-content">

		<!-- begin PAGE TITLE ROW -->
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>View Balance 
						<small>Confirm Client Balance</small>

					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
						</li>
						<li class="active">View Client Balance</li>
					</ol>
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
		<!-- end PAGE TITLE ROW -->

		<!-- begin ADVANCED TABLES ROW -->
		<div class="row">
			<div class="col-lg-12">
				<?php
				if( $_SESSION['role'] == "2"){

					echo "<div class='col-md-3'>
					<div>

					<b> Select BDE</b>

					<select  class='form-control' id='bde_name' name='bde_name' required>
					<option value='' selected disabled >--------Select BDE---------</option>";

					foreach ($get_bde_result as $key => $value) {
						echo "<option value='".$get_bde_result[$key]['user_id']."'>".$get_bde_result[$key]['fname']." ".$get_bde_result[$key]['lname']."</option>";
					}

					echo"</select>
					</div>
					</div>";

					echo"<div class='col-md-1'>
					<div>
					&nbsp;&nbsp;&nbsp;
					<input type='hidden' id='assigned_user_id' name='assigned_user_id'>
					<button type='button' id='viewCustomers' class='btn btn-green'>Search</button>
					</div>
					</div>";

					echo"<div class='col-md-2'>
					<div>
					</br>
					<button type='button' id='viewSelfCustomers' class='btn btn-green'>Self Customer</button>
					</div>
					</div>";


				}
				?>
			</div>
		</div>
		<div class="row">

			<div class="col-lg-12">

				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>View Balance</h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table id="example-table" class="table table-striped table-bordered table-hover table-green">
								<thead>
									<tr>
										<th>SrNo</th>
										<th>User Name</th>
										<th>Contact Person</th>
										<th>Balance</th>
										<th>Sent SMS</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody id="cust_tbody">
									<?php 

									if($result !='no')
									{
										foreach ($result as $key => $value) {

											$sr = $key +1;
											echo "<tr>";
											echo "<td>".$sr."</td>";
											echo "<td>".$result[$key]['user_name']."</td>";
											echo "<td>".$result[$key]['name']."</td>";
											echo "<td>".$result[$key]['balance']."</td>";
											echo "<td>".$result[$key]['sent_sms']."</td>";
											$get_credit_status = "SELECT id FROM credit_request WHERE u_id ='".$result[$key]['u_id']."' AND product = '".$result[$key]['product_name']."' AND status = 0 ";
											$result_credit_status = $con->data_select($get_credit_status);
											if($result_credit_status =='no'){
												echo "<td class='text-center'><a class='btn btn-xs btn-success' onclick='creditRequest(\"".$result[$key]['company_name']."\",\"".$result[$key]['user_name']."\",".$result[$key]['balance'].",".$result[$key]['u_id'].",\"".$result[$key]['product_name']."\")';>Credit Request</a></td>";
											}else{
												echo "<td class='text-center'><span class='btn btn-xs btn-danger'>Request sent</span></td>";
											}
											echo "</tr>";
										}
									}
									?>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					<!-- /.portlet-body -->
				</div>
				<!-- /.portlet -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.page-content -->
</div>
<!-- /#page-wrapper -->
<!-- end MAIN PAGE CONTENT -->

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="creditRequestModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="requestCreditData" class="form-horizontal" role="form">
				<div class="modal-header">
					<button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="flexModalLabel">Credit Allocation For : <span id="user_name"></span></h4>
				</div>

				<div class="modal-body">

					<div id="product_div_main">
						<label>1.Product Details </label> 
						<input type="hidden" id="user_id" name="user_id">
						<input type="hidden" id="userName" name="userName">
						<input type="hidden" id="compName" name="compName">
						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Balance</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="balance" name="balance" placeholder="Placeholder Text" readonly required>
								<span class="help-block"></span>
							</div>
						</div>             

						<div class="form-group">

							<label class="col-sm-2 control-label">Product</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="product" name="product" placeholder="Placeholder Text" readonly required>
								<span class="help-block"></span>
							</div>
						</div>

						<div class="form-group" >
							<label class="col-sm-2 control-label" id="label1">Quantity</label>
							<div class="col-sm-10">
								<input  class="form-control" type="number" id="product_quantity" name="product_quantity" onchange="changeRate(this);" required placeholder="Enter Quantity">
								<!-- <select  class="form-control" id="product_quantity" name="product_quantity" onchange="changeRate(this);" required>
									<option value="" selected disabled >--------Select Quantity---------</option>

								</select> -->
							</div>
						</div>

						<div class="form-group" style="display:none;" id="div_code_validity">
							<label class="col-sm-2 control-label">Validity</label>
							<div class="col-sm-10">
								<select  class="form-control" id="code_validity" name="code_validity" required>
									<option value="" selected disabled >--------Select Month---------</option>
									<option value="1 Month" >1 Month</option>
									<option value="3 Months" >3 Months</option>
									<option value="6 Months" >6 Months</option>
									<option value="12 Months" >12 Months</option>

								</select>
							</div>
						</div>

						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Rate per unit</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="product_rate" name="product_rate" onkeyup="calculaterate(this);" placeholder="Placeholder Text" required>
								<span class="help-block"></span>
							</div>
						</div>

						<div class="form-group has-success">
							<label class="col-sm-2 control-label"><span style="margin: 0 10px 0 0;">Tax</span> <input type="checkbox" style="border: 2px solid #16A085;width: 15px;height: 15px;" id="send_tax" name="send_tax" onclick="includeTax(this);" value="N"> </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="tax" name="tax" value="18.00" disabled required>
								<input type="hidden" id="tax_amount" name="tax_amount"  disabled >
								<span class="help-block"></span>
							</div>
						</div>


						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Total Amount</label>
							<div class="col-sm-10">
								<input type="hidden" id="hidden_total_amount" name="hidden_total_amount" placeholder="Placeholder Text" readonly>
								<input type="text" class="form-control" id="total_amount" name="total_amount" placeholder="0" readonly required>
								<span class="help-block"></span>
							</div>
						</div>



						<div class="form-group">
							<label class="col-sm-2 control-label">Payment Mode</label>
							<div class="col-sm-10">
								<select  class="form-control" id="paymode" name="paymode" onchange="checkNumber(this);" required>
									<option value="" selected disabled >--------Select Payment Mode---------</option>
									<option value="Cash" >Cash</option>
									<option value="Check" >Check</option>
									<option value="Online" >Online</option>
								</select>
							</div>
						</div>
						<div class="form-group has-success" style="display:none;" id="check_number_div">
							<label class="col-sm-2 control-label">Check Number</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="check_number" name="check_number" placeholder="Placeholder Text" required>
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group has-success">
							<label class="col-sm-2 control-label">Received By</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="amount_received" name="amount_received" value="<?php echo  $session_result[0]['fname']." ".$session_result[0]['lname']; ?>" placeholder="Placeholder Text" required>
								<span class="help-block"></span>
							</div>
						</div>




					</div> 

					<div class="col-sm-10" style="width: 145px;float: right;">
						<div class="checkbox">
							<label>
								<strong><input type="checkbox" id="send_invoice" name="send_invoice" value="Y" style="border: 2px solid #16A085;width: 15px;height: 15px;">Send Invoice</strong>
							</label>
						</div>                          
					</div>                     

				</div>
				<div class="modal-footer">

					<div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
					<div >
						<br>
						<button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>

						<button type="button" class="btn btn-green" id="requestCreditBtn" >Send Request</button>

					</div>

				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	$(function(){
		$('#example-table').DataTable();
	});
</script>

<?php
include('footer_crm.php');
?>