<?php

    include('header_sidebar_crm.php');
    include('class2/functions2.php');
   
    $con = new functions();
    $con2 = new functions2();

    if($_SESSION['role'] == "4")//TC
    {
        header("Location:index.php");
    }
    
    if($_SESSION['role'] == "2" || $_SESSION['role'] == "3")//BDE AND BDM
    {
        $user_id = $_SESSION['user_id'];
        $qry = "SELECT u.u_id, u.name, u.user_name, u.mob_no, u.company_name, u.email_id, u.date_register, u.route, u.u_status, u.sales_id,r.type as route_name FROM user AS u INNER JOIN route r ON u.route = r.route_no WHERE u.sales_id = '".$user_id."' ORDER BY u.u_id DESC ";
        //$qry = "SELECT * FROM client_details WHERE client_status = 'C' AND bde_user_id = '".$user_id."' ";
        $result = $con2->data_select($qry);

        $get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND reporting_id = '".$user_id."' AND flag = '1' ";
        $get_bde_result = $con->data_select($get_bde);
    }

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "5")
    {
        //Admin,RM
        //$qry = "SELECT * FROM client_details WHERE client_status = 'C' ";
        ///old
        //  $qry = "SELECT u.u_id, u.name, u.user_name, u.mob_no, u.company_name, u.email_id, u.date_register, u.route, u.u_status, u.sales_id,r.type as route_name FROM user AS u INNER JOIN route r ON u.route = r.route_no WHERE u.sales_id != 0 ORDER BY u.u_id DESC ";
        // $result = $con2->data_select($qry);
        $qry = "SELECT u.id, u.name, u.user_name, u.mobile, u.company_name, u.email, u.created,u.status,u.salesperson_id,r.type as route_name FROM user AS u INNER JOIN  user_product as up on up.user_id=u.id INNER JOIN route r ON up.route_id = r.id WHERE u.status != 0 ORDER BY u.id DESC ";
        $result = $con2->data_select($qry);
    
    }
       

?>
<script type="text/javascript">

$(function(){
    $("#viewSelfCustomers").click(function(){
        location.reload();            
    });

    $("#viewCustomers").click(function(){
        var user_id = $("#bde_name").val();
        if(user_id == ''){
            alert('Please select BDE');
            return false;
        }
        //alert(user_id);
        $.ajax({
            url:"ajax_service/mks_customer_ajax.php",                
            data:"user_id="+user_id+"&action=getMksUserData",
            async: false,
            //dataType:"json",
            success:function(data){                
                //console.log(data);
                $('#cust_tbody').empty();
                
                var oTable = $("#example-table").DataTable();
                oTable.fnDestroy();
                $('#cust_tbody').html(data);
                $("#example-table").DataTable();
            }
        
        });
    });


});
</script>


 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Client 
                                <small>Confirm Client Details</small>

                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Client</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                            if( $_SESSION['role'] == "2"){

                                echo "<div class='col-md-3'>
                                        <div>
                                            
                                               <b> Select BDE</b>
                                           
                                            <select  class='form-control' id='bde_name' name='bde_name' required>
                                                <option value='' selected disabled >--------Select BDE---------</option>";

                                                foreach ($get_bde_result as $key => $value) {
                                                    echo "<option value='".$get_bde_result[$key]['user_id']."'>".$get_bde_result[$key]['fname']." ".$get_bde_result[$key]['lname']."</option>";
                                                }

                                            echo"</select>
                                        </div>
                                    </div>";

                                 echo"<div class='col-md-1'>
                                            <div>
                                                &nbsp;&nbsp;&nbsp;
                                                <input type='hidden' id='assigned_user_id' name='assigned_user_id'>
                                                <button type='button' id='viewCustomers' class='btn btn-green'>Search</button>
                                            </div>
                                        </div>";
                               
                                echo"<div class='col-md-2'>
                                    <div>
                                        </br>
                                        <button type='button' id='viewSelfCustomers' class='btn btn-green'>Self Customer</button>
                                    </div>
                                </div>";
                                   
                                
                            }
                        ?>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Client</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>SrNo</th>
                                                <th>Company Name</th>
                                                <th>User Name</th>
                                                <th>Cont. Number</th>
                                                <th>Cont. Person</th>
                                                <th>Email ID</th>
                                                <th>Sales Person</th>                            
                                                <th>Route</th>
                                                <th>Registration Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="cust_tbody">
                                        <?php 
                                            /*echo "<pre>";
                                            print_r($result);
                                            exit;*/
                                            if($result !='no')
                                            {
                                                foreach ($result as $key => $value) {
                                                
                                                    $sr = $key +1;
                                                    echo "<tr>";
                                                    echo "<td>".$sr."</td>";
                                                    echo "<td>".$result[$key]['company_name']."</td>";
                                                    echo "<td>".$result[$key]['user_name']."</td>";
                                                    echo "<td>".$result[$key]['mob_no']."</td>";
                                                    echo "<td>".$result[$key]['name']."</td>";
                                                    echo "<td>".$result[$key]['email_id']."</td>";
                                                    
                                                    $get_user="SELECT fname,lname FROM user_details WHERE user_id = '".$result[$key]['sales_id']."' ";
                                                    //echo $get_user."<br>";
                                                    $get_user_result = $con->data_select($get_user);
                                                    echo "<td>".$get_user_result[0]['fname']." ".$get_user_result[0]['lname']."</td>";
                                                    
                                                    /*echo "<pre>";
                                                    print_r($get_user_result);*/

                                                    echo "<td>".$result[$key]['route_name']."</td>";
                                                    echo "<td>".$result[$key]['date_register']."</td>";
                                                    if($result[$key]['u_status'] == 1){
                                                        echo "<td>Active</td>";
                                                    }else{
                                                        echo "<td>Inactive</td>";
                                                    }

                                                    echo "<td><a href='proforma_invoice.php?u_id=".$result[$key]['u_id']."'><i class='fa fa-file-text-o'></i></a></td>";

                                                    echo "</tr>";
                                                }


                                            }
                                            
                                            
                                            
                                        ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
                </div>
                <div class="modal-body">
                   <form id="updateUserData" class="form-horizontal" role="form">
                        <input type="hidden" id="user_id" name="user_id">                   

                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Contact No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="role" name="role" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="status" name="status" required>
                                        <option value="">Select One:</option>
                                        <option id="status1" value="1">Active</option>
                                        <option id="status0" value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="update" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
<?php
 include('footer_crm.php');
?>