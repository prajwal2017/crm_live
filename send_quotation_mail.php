<?php
    ob_start();  /*Flush the output from the buffer*/

    ini_set('max_execution_time', 300); 
	include('class/functions.php');
	include('class/fpdf.php');
	/*echo json_encode($_REQUEST);
    exit;*/
	$con = new functions();
	session_start(); 
    $date = $con->get_datetime();
	$user_id = $_SESSION['user_id'];
	$sqlQry = "SELECT * FROM user_details WHERE user_id = '".$user_id."' ";
	$result = $con->data_select($sqlQry);

	$user_name = $result[0]['fname']." ".$result[0]['lname'];
	$user_emailId = $result[0]['email_id'];
	$phone_number = "+91 22 27564515";
	$mobile_number = $result[0]['contact_number'];
    $reply_to = $_SESSION['email_id'];

    $role = "";
    if($result[0]['role'] == 1){
        $role = "CEO";
    }else if($result[0]['role'] == 2){
        $role = "BDM";
    }else if($result[0]['role'] == 3){
        $role = "BDE";
    }else if($result[0]['role'] == 4){
        $role = "Telle Caller";
    }else if($result[0]['role'] == 5){
        $role = "RM";
    }

class PDF extends FPDF
{
// Current column
var $col = 0;
// Ordinate of column start
var $y0;
	
	function Header(){
    // Page header
    //global $title;

    //$this->SetFont('Arial','B',15);
    //$w = $this->GetStringWidth($title)+6;
   // $this->SetX((210-$w)/2);//we are align
   // $this->SetDrawColor(0,80,100);
   // $this->SetFillColor(230,230,0);
   // $this->SetTextColor(220,50,50);
   // $this->SetLineWidth(1);
   // $this->Cell($w,9,$title,1,1,'C',true);
   // $this->Ln(10);
    // Save ordinate
   // $this->y0 = $this->GetY();
}

function Footer()
{
    // Page footer
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->SetTextColor(128);
    //$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

function SetCol($col)
{
    // Set position at a given column
    $this->col = $col;
    $x = 6+$col*100;//columns
    $this->SetLeftMargin($x);
    $this->SetX($x);
}



function ChapterTitle1($label)
{
    // Title
    $this->SetFont('Arial','',12);
    $this->SetFillColor(200,220,255);
	$this->Cell(0,6,"$label",0,1,'L',true);
       $this->Ln(4);
    // Save ordinate
    $this->y0 = $this->GetY();
}



function AcceptPageBreak()
{
    // Method accepting or not automatic page break
    if($this->col<2)
    {
        // Go to next column
        $this->SetCol($this->col+1);
        // Set ordinate to top
        $this->SetY($this->y0);
        // Keep on page
        return false;
    }
    else
    {
        // Go back to first column
        $this->SetCol(0);
        // Page break
        return true;
    }
}

function ChapterTitle($label)
{
    // Title
    $this->SetFont('Arial','',12);
    $this->SetFillColor(200,220,255);
	//$this->Cell(90,6,"$label",0,1,'L',true);
       $this->Ln(4);
    // Save ordinate
    $this->y0 = $this->GetY();
}

function ChapterBody($file)
{
    // Read text file
    $txt = file_get_contents($file);
    // Font
    $this->SetFont('Times','',12);
    // Output text in a 6 cm width column
    $this->MultiCell(90,6,$txt);
    $this->Ln();
    // Mention
    $this->SetFont('','I');
    
    // Go back to first column
    $this->SetCol(0);
}

function PrintChapter($title, $file)
{
    // Add chapter
    $this->AddPage();
    $this->ChapterTitle($title);
    $this->ChapterBody($file);
}
}
	

// fpdf object
$pdf = new PDF();

$pdf->AddPage();
$pdf->Image('img/proposal-img.jpg',16,5,180);
$pdf->Image('img/img1.jpg',10,30,5);

$pdf->Image('img/img1.jpg',10,90,5);


$pdf->Image('img/img1.jpg',200,30,5);

$pdf->Image('img/img1.jpg',200,60,5);



$pdf->SetFont('Arial','B',15);
	
//$pdf->Image('img/img.jpg',18,5,180);//Greeting image


$pdf->SetFont('Arial','',11);

$pdf->SetTextColor(20,39,47);
$pdf->text(30,50,'To,');
$pdf->SetFont('Arial','',11);
$pdf->SetTextColor(220,39,47);



$pdf->text(30,56,$_POST['name']);
$pdf->text(30,62,$_POST['compname']);
$pdf->text(30,68,$_POST['contactNumber']);
$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(20,39,47);
$pdf->text(30,83,'Sub :- ');
$pdf->text(43,83,"Quotation");

$pdf->SetFont('Arial','',11);
$pdf->text(30,95,'Dear Sir,');
$pdf->text(30,101,'As  Per your Conversation here under given the rate for required services');

 
$pdf->SetFont('Arial','',11);
$pdf->SetFillColor(0, 255,0);
	


$pdf->SetFont( "Arial","B",10);
	

$pdf->setXY(28,112);
$pdf->SetFillColor(128,128,128);


$pdf->Cell (41,9,"Product",1,0,"C");
$pdf->Cell( 25,9,"Qty",1,0,"C");
$pdf->Cell( 25,9,"Rate",1,0,"C");
$pdf->Cell (35,9,"Amount",1,0,"C");
$pdf->Cell (31,9,"Validity",1,0,"C");

/*$pdf->setXY(28,121);
  $pdf->SetFillColor(128,128,128);

$pdf->Cell (41,5,"",1,0,"C");
$pdf->Cell( 35,5,"",1,0,"C");
$pdf->Cell (40,5,"",1,0,"C");
$pdf->Cell (31,5,"",1,0,"C");*/






$prod_name = $_POST['appendProductName'];
$prod_qty = $_POST['appendProductQty'];
$prod_rate = $_POST['appendProductRate'];
$prod_amt = $_POST['appendTotalAmount'];
$code_validity = $_POST['appendCodeValidity'];
if(isset($_POST['appendSmsValidity'])){
$sms_validity = $_POST['appendSmsValidity'];
}


$prod_len = strlen($prod_name);
//echo $prod_len;
$prod_substr = substr($prod_name, 0, $prod_len-2);
//echo $prod_substr;
$pname = explode("__", $prod_substr);
$product_name = serialize($pname);

$qty_len = strlen($prod_qty);
//echo $qty_len;
$qty_substr = substr($prod_qty, 0, $qty_len-2);
//echo $qty_substr;
$pqty = explode("__", $qty_substr);
$product_quantity = serialize($pqty);

$rate_len = strlen($prod_rate);
//echo $rate_len;
$rate_substr = substr($prod_rate, 0, $rate_len-2);
//echo $rate_substr;
$prate = explode("__", $rate_substr);
$product_rate = serialize($prate);

$amt_len = strlen($prod_amt);
//echo $amt_len;
$amt_substr = substr($prod_amt, 0, $amt_len-2);
//echo $amt_substr;
$pamt = explode("__", $amt_substr);

$code_len = strlen($code_validity);
//echo $amt_len;
$code_substr = substr($code_validity, 0, $code_len-2);
//echo $amt_substr;
$codeValidity = explode("__", $code_substr);
$product_validity = serialize($codeValidity);

/*$sms_len = strlen($sms_validity);
$sms_substr = substr($sms_validity, 0, $sms_len-2);
$smsValidity = explode("__", $sms_substr);
$product_sms_validity = serialize($smsValidity);*/

/*echo "<pre>";
print_r($_REQUEST);
print_r($codeValidity);
exit;*/
/*echo "<pre>";

print_r($pname);
print_r($psmstype);
print_r($pqty);
print_r($prate);
print_r($pamt);
exit;*/

$pdf->SetFont( "Arial","",10);
$n = 125;
$c = 121;
for ($i=0; $i < count($pname); $i++) { 

	$pdf->setXY(28,$c);
	$pdf->SetFillColor(128,128,128);

	$pdf->Cell (41,5,"",1,0,"C");
	$pdf->Cell( 25,5,"",1,0,"C");
    $pdf->Cell( 25,5,"",1,0,"C");
	$pdf->Cell (35,5,"",1,0,"C");
	$pdf->Cell (31,5,"",1,0,"C");

	$pdf->text(31,$n,$pname[$i]);
	$pdf->text(76, $n,$pqty[$i]);
    $pdf->text(103, $n,$prate[$i]);
	$pdf->text(132, $n,$pamt[$i]);
    $pdf->text(162, $n,$codeValidity[$i]);
	/*if(strcmp($pname[$i],'ShortCode')==0 || strcmp($pname[$i],'LongCode')==0){
		$pdf->text(150, $n,$codeValidity[$i]);

	}else{
		$pdf->text(150, $n,"Unlimited");
	}*/
	

	$n = $n+5;
	$c = $c+5;
	
}

$pdf->SetFont( "Arial","B",10);
$pdf->text(30, $n+5,'* NOTE');
$pdf->text(35, $n+10,'1) GST (18%)EXTRA');
$pdf->text(35, $n+15,'2) Quotation valid for 7 days only');


$pdf->SetFont( "Arial","B",10);

$pdf->Image('img/img2.jpg',20,182,69);//$n+25
$pdf->Image('img/img3.jpg',20,182,2);//$n+25
$pdf->Image('img/img3.jpg',85,182,2);//$n+25

$pdf->Image('img/img2.jpg',20,244,69);//$n+87


$pdf->Image('img/mobi.png',23,185,60);//$n+28
 //$pdf->Image('img/profile-pic.jpg',20,166,69);

/*$pdf->text(106, 190,'Payment      : Prepaid Mode');
$pdf->text(106, 195,'Bank Details : ICICI Bank');

$pdf->text(106, 200,'Account Name : Mobisoft Technology India Pvt Ltd');
$pdf->text(106, 205,'Account No   : 015105012883');
$pdf->text(106, 210,'IFSC         : ICIC0000151');
$pdf->text(106, 215,'Bank Branch  : VASHI');
*/

$pdf->text(106, 190,'Payment'); 
$pdf->text(132, 190,':  Prepaid Mode');

$pdf->text(106, 195,'Bank Details'); 
$pdf->text(132, 195,':  ICICI Bank');

$pdf->text(106, 200,'Account Name'); 
$pdf->text(132, 200,':  Mobisoft Technology India Pvt Ltd');

$pdf->text(106, 205,'Account No'); 
$pdf->text(132, 205,':  015105012883');

$pdf->text(106, 210,'IFSC'); 
$pdf->text(132, 210,':  ICIC0000151');

$pdf->text(106, 215,'Bank Branch'); 
$pdf->text(132, 215,':  VASHI');



$pdf->SetFont('Arial','',10);
$pdf->text(106, 230,'Terms & Conditions : -');
$pdf->text(106, 235,'1. Rate are subject to change as per TRAI');

$pdf->SetFont('Arial','U',11);
$pdf->SetTextColor(150,39,47);
$pdf->text(112, 240,'www.mobisofttech.co.in');

$pdf->Image('img/mobisoft.png',120,260,80);

$pdf->Image('img/img1.jpg',10,30,5);

$pdf->Image('img/img1.jpg',10,60,5);

$pdf->Image('img/img1.jpg',200,30,5);

$pdf->Image('img/img1.jpg',200,60,5);


 	$filename = "Mobisoft-Business-Proposal.pdf";

   // encode data (puts attachment in proper format)

    
    $pdfdoc = $pdf->Output('PDF_File/Mobisoft-Business-Proposal.pdf', "F");
    
    

    //$attachment = chunk_split(base64_encode($pdfdoc));
 
    $email_id = $_REQUEST['emailId'];

	$to = $email_id;
	$subject = "MobiSoft Technology";
	$attachment_name = array('PDF_File/Mobisoft-Business-Proposal.pdf','PDF_File/Presentation_Mobisoft Technology India Pvt Ltd.pdf',);
	//$attachment_name = array('PDF_File/Mobisoft Business Proposal.pdf');
	//$attachment_name = "doc/Mobisoft Business Proposal.pdf","doc/MobiSoftppt.ppt";
	$body = "

	<font color='#500050'><b>Dear Sir/Madam, </b><br><br>
	           
	Greetings from Mobisoft Technology India Pvt. Ltd.<br><br>
 
    Hope my email finds you well & doing great. <br><br>
     
    Mobisoft  is India's Top Growing <b>Bulk SMS Service Provider when it comes to Bulk SMS Solution</b>.<br><br>
     
    At current we cater over 2000+ Customers with 100+ Enterprise across Industries in India varying from major Banks to retails by end to end. Our messaging routes are being used by major Enterprises, varying from  Religare, India Bulls, Angle Brokings  and More.<br><br>
     
    If you are looking for sms service routes, we can cater your <b>Indian</b> termination requirements for both <b>Promotional & Transactional Traffic</b>.<br><br><br>
                                                                                                                                                                                       
     
    <font size='3'>1:- Promotional SMS :–</font><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Features:</b>
    <ul>
    <li> Promo on NON-DND Numbers</li>
    <li> DND Numbers list can be extracted and will not be charged.</li>
    <li> Delivery Time – Instant Delivery</li>
    <li> Delivery Report - Instant Delivery Report, 100% of Guaranteed Delivery</li>
    <li> To get activate it takes maximum 10 Minutes of time</li>
    </ul><br>
    <font size='3'>2:- Transactional SMS :-</font><br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Features:</b>
    <ul>
    <li> Six Character Sender ID will be provided (ONLY Alphabets).</li>
    <li> Web Interface/HTTP API will be given.</li>
    <li> Messages gets delivered to DND Numbers also.</li>
    <li> It works on template basis. (Provide your sample to support@mobisofttech.co.in) . The templates with 70% of Static Content and 30% Variables.</li>
    <li> We would require details on how your customers are subscribing or getting registered and one sample of subscription form is mandatory.</li>
    <li> To get activate it takes 30 – 45 Minutes of time.</li>
    </ul><br>
    To know our exciting pricing and other features, kindly reply to the mail so that our sales team call back to you instantly.<br><br>
     
    We are also dealing in Mobile Applications and Products as below:<br><br>
     
    <b>
    1. Short Code & Long Code<br>
    2. Virtual Number which is Ten digit Mobile number<br>
    3. IVR, Voice services (Click to Call & Missed Call)<br>
    4. Website Design, Application development ( customize)<br>
    5. Software development.
    </b><br><br><br>
     
     
    Looking forward to hear from you soon .<br><br>

    Regards, <br><br></font>

    <font size='3' color='#CC0000'><b>".$user_name." // MobiSoft (".$role.")</b></font><br>
    <hr>
    <font color='#365F91'><b>MobisoftTechnology India Pvt. Ltd.</b><br>
    Office No. H-004, P Level, Tower-3,<br><br/>
    
    Railway Station Complex, Sector 11,<br /><br/>
    
    CBD Belapur, Navi Mumbai 400 614.<br><br/>
    

    India.<br><br><br>


    Mobile : + 91 ".$mobile_number."<br><br>
     
    Phone : +91 22 27574515<br><br><br>
     

     
    Email: ".$user_emailId."<br><br>

    www.mobisofttech.co.in | www.makemysms.in<br></font>

    ------------------------------------------------------------------------------------------------------------------------------------------------------
    <br><br>
    This email, and any attachments, may be confidential and also privileged. If you are not the
    intended recipient, please notify the sender and delete all copies of this transmission along with
    any attachments immediately. You should not copy or use it for any purpose, nor disclose its
    contents to any other person.All rights reserved Mobisofttechnology India Pvt Ltd 2011
    <br><br>
    ------------------------------------------------------------------------------------------------------------------------------------------------------
    ";
	

	$result2 = $con->send_quotation_mail($subject,$body,$to,$attachment_name,$reply_to,$user_name);
    // $result2 = 1;
    //echo $result2;
	if($result2 == 1){

        if($_REQUEST['quotationType'] == "Direct"){
            $status = "Accept";
            $remark = "NA";
            $reject_in = "NA";
            
            $insert_customer="INSERT INTO customer_details (company_name,contact_number,status,reject_in,remark,user_id,c_date,flag) VALUES('".$_POST['compname']."','".$_POST['contactNumber']."','$status','$remark','$reject_in',".$user_id.",'$date',1)";
            $insert_customer_result=$con->data_insert_return_id($insert_customer);

            if($insert_customer_result > 0)
            {
                $qry="INSERT INTO client_details (cust_id,company_name,contact_number,contact_person,email_id,address,client_status,remark,lead_user_id,bde_user_id,payment_status,c_date,u_date,flag,mks_u_id,client_key) VALUES ($insert_customer_result,'".$_POST['compname']."','".$_POST['contactNumber']."','".$_POST['name']."','".$email_id."','".$_POST['address']."','Q','$remark',".$user_id.",".$user_id.",'NA','$date','$date',1,00,'00')";
                $result=$con->data_insert($qry); 

                if($result > 0){
                    $save_quotation = "INSERT INTO quotation (user_id,cust_id,company_name,contact_person,contact_number,email_id,product_name,product_quantity,product_rate,product_validity,c_date) VALUES($user_id,".$insert_customer_result.",'".$_POST['compname']."','".$_POST['name']."','".$_POST['contactNumber']."','".$email_id."','".$product_name."','".$product_quantity."','".$product_rate."','".$product_validity."','$date')";
                    $save_quotation_result = $con->data_insert($save_quotation);

                    if($save_quotation_result > 0){
                        echo 1;
                        unlink('PDF_File/Mobisoft-Business-Proposal.pdf');
                    }
                }        
            }
             
        }else if($_REQUEST['quotationType'] == "Customer"){

            $qry="INSERT INTO client_details (cust_id,company_name,contact_number,contact_person,email_id,address,client_status,remark,lead_user_id,bde_user_id,payment_status,c_date,u_date,flag,mks_u_id,client_key) VALUES (".$_POST['cust_id'].",'".$_POST['compname']."','".$_POST['contactNumber']."','".$_POST['name']."','".$email_id."','".$_POST['address']."','Q','$remark',".$user_id.",".$user_id.",'NA','$date','$date',1,00,'00')";
            $result=$con->data_insert($qry);

            if($result > 0){
                $update_customer = "UPDATE customer_details SET status = 'Accept' WHERE cust_id ='".$_POST['cust_id']."' ";
                $update_customer_result = $con->data_update($update_customer);

                if($update_customer_result > 0){                   
                    $save_quotation = "INSERT INTO quotation (user_id,cust_id,company_name,contact_person,contact_number,email_id,product_name,product_quantity,product_rate,product_validity,c_date) VALUES($user_id,'".$_POST['cust_id']."','".$_POST['compname']."','".$_POST['name']."','".$_POST['contactNumber']."','".$email_id."','".$product_name."','".$product_quantity."','".$product_rate."','".$product_validity."','$date')";
                    $save_quotation_result = $con->data_insert($save_quotation);

                    if($save_quotation_result > 0){
                         if (isset($_REQUEST["reminder_id"])) {
                        
                            $remove_reminder = "DELETE FROM reminder WHERE r_id = ".$_REQUEST['reminder_id'];                    
                            $remove_reminder_result = $con->data_delete($remove_reminder);
                        }
                        echo 1;
                        unlink('PDF_File/Mobisoft-Business-Proposal.pdf');
                    } 
                }
                
            }
            
        }else if($_REQUEST['quotationType'] == "Lead"){

            $update_client = "UPDATE client_details SET client_status = 'Q' WHERE cust_id ='".$_POST['cust_id']."' ";
            $update_client_result = $con->data_update($update_client);

            if($update_client_result > 0){
                $save_quotation = "INSERT INTO quotation (user_id,cust_id,company_name,contact_person,contact_number,email_id,product_name,product_quantity,product_rate,product_validity,c_date) VALUES($user_id,'".$_POST['cust_id']."','".$_POST['compname']."','".$_POST['name']."','".$_POST['contactNumber']."','".$email_id."','".$product_name."','".$product_quantity."','".$product_rate."','".$product_validity."','$date')";
                $save_quotation_result = $con->data_insert($save_quotation);

                if($save_quotation_result > 0){
                    echo 1;
                    unlink('PDF_File/Mobisoft-Business-Proposal.pdf');
                }
            }
            
        }
        unlink('PDF_File/Mobisoft-Business-Proposal.pdf');
       
		
	}
	

?>

<?php
    ob_end_flush(); // Flush the output from the buffer
?>