<?php
    include('header_sidebar_crm.php');

    //include('class/functions.php');
    $con = new functions();
    $curent_date = date("Y-m");

    if($_SESSION['role'] == "4"){
        $user_id = $_SESSION['user_id'];
        $qry = "select * from user_details ud inner join lead_target lt on ud.user_id = lt.user_id where lt.user_id='".$user_id."' and lt.s_date='".$curent_date."' order by lt.lt_id desc";
        $result = $con->data_select($qry);
    }

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){
         $qry = "select ud.fname,ud.lname,ud.role,lt.* from lead_target lt INNER JOIN  user_details ud on ud.user_id = lt.user_id where lt.s_date='".$curent_date."'  order by lt.lt_id desc";
         $result = $con->data_select($qry);
    }
    
    $qry1 = "select user_id,fname,lname from user_details where role= '4'";
    $result1 = $con -> data_select($qry1);
       
    $get_tc = "SELECT user_id,fname,lname,role FROM user_details WHERE role =4 AND flag = '1' ";
    $get_tc_result = $con->data_select($get_tc);

?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="js_functions/datepicker-calendar.js"></script>
<style>
.ui-datepicker-calendar {
    display: none;
}
</style>

<script type="text/javascript">

    $(document).ready(function(){


        $("#update").click(function(){
            //alert("inside submit");
            var temp =validateField();
            if(temp == false)
            {
                return false;

            }
            $(".ajax-loader").show();
            $(".close_model_div").hide();
            $("#update").hide();
            $.ajax({

                url:"ajax_service.php",
                data:$("#updateleadtarget").serialize()+"&action=updateleadtarget",
                success:function(data){
                    console.log(data);
                    if(data == "success")
                    {
                        $(".ajax-loader").hide();
                        $(".close_model_div").show();
                        alert("Data updated successfully");
                        location.reload();
                    }
                    else
                    {
                        $(".ajax-loader").hide();
                        $(".close_model_div").show();
                        $("#update").show();
                        alert("Data not updated");
                        
                    }
                }
            });
        });

    });
        function viewData(ltid,sr)
        {
            var tc_name = $("#telle_name"+sr).html();
            $("#telecaller").val(tc_name);            
            $.ajax({
                url:"ajax_service.php",
                data:"ltid="+ltid+"&action=viewleadtarget",
                dataType:"json",
                success:function(data){
                    //alert("inside ajax");
                    console.log(data);
                    
                    $("#telecallerid").val(data[0].user_id); 
                    $("#target").val(data[0].target_lead);
                    
                    $("#lt_id").val(data[0].lt_id);
                    $("#achieved").val(data[0].achieved);
                    $("#balance_lead").val(data[0].target_total_lead - data[0].target_lead);
                    $("#status").val(data[0].flag);
                    
                    /*if(data[0].flag == "1")
                    {
                        $("#status1").attr("selected",true);
                    }
                    else
                    {
                        $("#status0").attr("selected",true);
                    }*/


                }
            });
            
        }

        
        function deleteData(ltid,sr)
        {
            //alert("Inside delete");
                var confirm_delete = confirm("Are you sure you want to delete?");
                if(confirm_delete == true)
                    {
                        $.ajax({

                            url:"ajax_service.php",
                            data:"ltid="+ltid+"&action=deleteleadtarget",
                            success:function(data){
                                console.log(data);
                                if(data == "success")
                                {
                                    alert("Data deleted successfully");
                                    $("#row"+sr).fadeOut();
                                    location.reload();
                                }
                                else
                                {
                                    alert("Data not deleted");
                                    
                                }

                            }
                        });
                    }
        }


        function validateField()
        { 
            
            var target=$("#target").val(); 

            if(target.length == "" || target.length =='' || target.length == null)
            {
                alert("Please Enter Target");
                return false;
            }  
            
            /*if(salesperson == "" || salesperson =='' || salesperson == null)
            {
                alert("Please Select Lead Person");
                return false;
            }
            else if(target.length == "" || target.length =='' || target.length == null)
            {
                alert("Please Enter Target");
                return false;
            }

            else if(startDate == "" || startDate =='' || startDate == null)
            {
                alert("Please Enter Start Date");
                return false;
            }
           
            else if(endDate.length == "" || endDate.length =='' || endDate.length == null)
            {
                alert("Please Enter End Date ");
                return false;
            }*/
            else
            {

            }
        }

function showLeadTarget(){
    var input_date = $("#datepicker").val();
    var tc_name = $("#tc_name").val();
    //alert(tc_name);
    
    $.ajax({
        url:"ajax_service/view_leads_ajax.php",
        data:"target_month="+input_date+"&tc_name="+tc_name+"&action=showLeadTarget",
        success:function(data){
            //alert(data);
            $('#lead_target_data').empty();
            var oTable = $("#example-table").dataTable();
            oTable.fnDestroy();
            $('#lead_target_data').html(data);
            $("#example-table").dataTable();
        }
    });
}
    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Lead Target 
                                <small>Target Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Lead Target</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                <div class="row">

                    <div class="col-md-2" <?php if($_SESSION['role']=='4'){echo "style='display:none'"; }?>>
                        <div>                                        
                            <b> Select Caller</b>                                   
                                <select  class='form-control' id='tc_name' name='tc_name' required>
                                    <option value='' selected disabled >-----Select TC-----</option>";
                                    <?php
                                        if($_SESSION['role']=='4'){
                                            echo "<option value='".$_SESSION['user_id']."' selected></option>";
                                        }else{
                                            echo "<option value='All' selected>All</option>";
                                            foreach ($get_tc_result as $key => $value) {                                                
                                                echo "<option value='".$get_tc_result[$key]['user_id']."'>".$get_tc_result[$key]['fname']." ".$get_tc_result[$key]['lname']."</option>";        
                                            }
                                        }
                                        
                                    ?>
                                </select>
                        </div>
                    </div>

                     <div class="col-md-2">
                        <div>                                        
                            <b> Select Month</b>  
                                <input class='form-control' type="text" id="datepicker" name="startDate" placeholder="Select Month"/>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div>
                             <b>&nbsp;</b><br>   
                            <button class="btn btn-green btn-sm" onclick="showLeadTarget()">Submit</button>
                        </div>
                    </div>

                </div>
                <br>
                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">                       

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Lead Target</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>Sr.No</th>
                                                <?php
                                                if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){
                                                    echo "<th>Telle Caller</th>";
                                                }
                                                ?>
                                                
                                                <th>Target Month</th>
                                                <th>Target</th>
                                                <th>Total Target</th>
                                                <th>Achieved</th>
                                                <th>Balance</th>
                                                
                                                <?php
                                                if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){
                                                    echo "<th>Status</th>";
                                                    echo "<th>Action</th>";
                                                }
                                               ?> 
                                            </tr>
                                        </thead>
                                        <tbody id="lead_target_data">
                                           <?php

                                                foreach ($result as $key => $value) {

                                                    $res =explode("-", $result[$key]['s_date']);
                                                    $month = $res[1];
                                                    $year = $res[0];
                                                    $mon="";

                                                    if($month == 1)
                                                    {
                                                        $mon = "January";
                                                    }else if($month == 2){
                                                        $mon = "February";
                                                    }else if($month == 3){
                                                        $mon = "March";
                                                    }else if($month == 4){
                                                        $mon = "April";
                                                    }else if($month == 5){
                                                        $mon = "May";
                                                    }else if($month == 6){
                                                        $mon = "June";
                                                    }else if($month == 7){
                                                        $mon = "July";
                                                    }else if($month == 8){
                                                        $mon = "August";
                                                    }else if($month == 9){
                                                        $mon = "September";
                                                    }else if($month == 10){
                                                        $mon = "October";
                                                    }else if($month == 11){
                                                        $mon = "November";
                                                    }else if($month == 12){
                                                        $mon = "December";
                                                    }
                                                    
                                                    $start_date = $mon." ".$year;

                                                    $sr = $key + 1;
                                                    echo '<tr id="row'.$sr.'">';

                                                    echo "<td>".$sr."</td>";
                                                    if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){
                                                        echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
                                                    }
                                                    //echo "<td>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
                                                    echo "<td>".$start_date."</td>";
                                                    echo "<td>".$result[$key]['target_lead']."</td>";
                                                    echo "<td>".$result[$key]['target_total_lead']."</td>";
                                                    //echo "<td>".$result[$key]['e_date']."</td>";
                                                    echo "<td>".$result[$key]['achieved']."</td>";
                                                    echo "<td>".$result[$key]['balance']."</td>";
                                                   
                                                   // echo "<td>".$result[$key]['flag']."</td>";
                                                    if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "5"){
                                                        if($result[$key]['flag'] == 1){
                                                        echo "<td>Active</td>";
                                                        }
                                                        if($result[$key]['flag'] == 0){
                                                            echo "<td>Inactive</td>";
                                                        }
                                                        echo '<td align="center"><a href="#" title="Edit Details" data-toggle="modal" data-target="#flexModal" onclick="return viewData('.$result[$key]["lt_id"].');"><i class="fa fa-cut btn-green btn-sm"></i></a>  <a href="#" title="Delete" onclick="deleteData('.$result[$key]["lt_id"].','.$sr.')"><i class="fa fa-trash-o btn-red btn-sm"></i></a></td>';
                                                    }
                                                   
                                                    echo "</tr>";
                                                }
                                                
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Edit Lead Target</h4>
                </div>
                <form id="updateleadtarget" class="form-horizontal" role="form">
                <div class="modal-body">
                   
                        <input type="hidden" id="lt_id" name="lt_id">  
                        <input type="hidden" id="achieved" name="achieved">                 

                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Telle Caller</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="telecaller" name="telecallername" value="" placeholder="Placeholder Text" readonly>
                                                <input type="hidden" class="form-control" id="telecallerid" name="telecaller" value="" >
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Balance Lead</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="balance_lead" name="balance_lead" placeholder="Placeholder Text" readonly>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Taget</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="target" name="target" placeholder="Placeholder Text" required>                                               
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Start Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" id="startDate" id="startDate" name="startDate"  required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">End Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" id="endDate" name="endDate"  required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <select  class="form-control" id="status" name="status" required>
                                                <option value="" selected disabled>---Select Status---</option>
                                                <option id="status1" value="1">Active</option>
                                                <option id="status0" value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>

                                              
                </div>
                <div class="modal-footer">
                    <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                     <button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
                     <button type="button" id="update" class="btn btn-green">Save changes</button>
                </div>
                </form> 
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script type="text/javascript">
        $(function(){
            $('#example-table').DataTable();
        });
    </script>
    
<?php
 include('footer_crm.php');
?>