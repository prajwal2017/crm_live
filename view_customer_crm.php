<?php
error_reporting(E_ALL ^ E_NOTICE);
include('header_sidebar_crm.php');   
/*include('class/functions.php');*/
$con = new functions();
$usersId = "SELECT user_id from user_details where reporting_id =".$_SESSION['user_id'];
$usersIdData = $con->data_select($usersId);
if(!empty($usersIdData)){
    foreach ($usersIdData as $key => $value) {
        # code...
        $arr[$key] = (int)$usersIdData[$key]['user_id'];

    } $key++;
    $arr[$key] = (int) $_SESSION['user_id'];
    $ids = join("','",$arr); 
}else{
    $ids = [0];
}
if($_SESSION['role'] == "2" || $_SESSION['role'] == "3" || $_SESSION['role'] == "4" ){
	$user_id = $_SESSION['user_id'];
	/*$user_id = "1";*/
	/*temp variable are used intesd of session for testing purpose*/
	if($_SESSION['role'] == "2"){
		$qry = "SELECT ud.fname,ud.lname,cd.cust_id,cd.company_name,cd.contact_number,cd.customer_name,cd.customer_email,cd.c_date,cd.flag FROM customer_details as cd inner join user_details as ud on cd.user_id = ud.user_id WHERE cd.status = 'Pending' AND cd.status!='Reject' AND cd.flag = '1' and cd.user_id IN ('$ids') ORDER BY cd.cust_id DESC";
	$result = $con->data_select($qry);
	// echo $ids;
	}else{
		$qry = "SELECT cust_id,company_name,contact_number,customer_name,customer_email,c_date,flag FROM customer_details WHERE user_id ='".$user_id."' AND status = 'Pending' AND status!='Reject' AND flag = '1' ORDER BY cust_id DESC";
	$result = $con->data_select($qry);
	}
	
	 // print_r($result);
  /*echo "<pre>";
  print_r($result);
  exit;*/
}else{
	// $qry = "SELECT * FROM customer_details ORDER BY cust_id desc";
	$qry = "SELECT ud.fname,ud.lname,ud.role,cd.* FROM customer_details as cd inner join user_details as ud on cd.user_id = ud.user_id WHERE  cd.status!='Reject'  AND cd.status != 'Accept' AND cd.flag = '1'  ORDER BY cd.cust_id DESC ";
	/*echo $qry;*/
	$result = $con->data_select($qry);
	// echo $qry;
	/*echo "<pre>";
	print_r($result);
	exit;*/
}

$qry1 = "SELECT user_id,fname,lname,role FROM user_details WHERE role =2 OR role =3 OR role =8 AND flag = '1' ORDER BY role DESC";
$result1 = $con->data_select($qry1);

if($_SESSION['role'] == "1"){
	$get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND flag = '1' ";
	$get_bde_result = $con->data_select($get_bde);

	$get_bdm = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 2 AND flag = '1' ";
	$get_bdm_result = $con->data_select($get_bdm);
}

if($_SESSION['role'] == "2"){
	$get_bde = "SELECT user_id,fname,lname,role FROM user_details WHERE role = 3 AND reporting_id = '".$user_id."' AND flag = '1' ";
	$get_bde_result = $con->data_select($get_bde);
}

/*$get_tc = "SELECT user_id,fname,lname,role FROM user_details WHERE role =4 AND reporting_id = '".$user_id."' AND flag = '1' ";*/
$get_tc = "SELECT user_id,fname,lname,role FROM user_details WHERE role =4 AND flag = 1 ";
$get_tc_result = $con->data_select($get_tc);
/*echo "<pre>";
print_r($get_tc_result);
exit();*/
?>


<script type="text/javascript">
	$(document).ready(function(){
		/*$('#example-table').DataTable();*/
		$("#addClientDetails").click(function(){
			var temp = validateField();
			if(temp == false){
				return false;
			}
			$(".close_model_div").hide();
			$("#addClientDetails").hide();
			$(".ajax-loader").show();
			$.ajax({
				url:"ajax_service.php",
				data:$("#addClientData").serialize()+"&action=addClientDetails",
				success:function(data){
					console.log(data);
					if (data == "success") {
						alert("Client data inserted.");
						$(".close_model_div").show();                       
						$(".ajax-loader").hide();
						location.reload();
					}else{
						alert("Client data inserted.");
						$(".close_model_div").show();
						$("#addClientDetails").show();
						$(".ajax-loader").hide();
					}
				}
			});
		});



		$("#updateRejectDetails").click(function(){
			var temp =validateReject();
			if(temp == false){
				return false;
			}
			$(".close_model_div").hide();
			$("#updateRejectDetails").hide();
			$(".ajax-loader").show();

			$.ajax({
				url:"ajax_service.php",
				data:$("#updateRejectData").serialize()+"&action=updateRejectDetails&type=Call",
				success:function(data){
					if (data == "success") {
						alert("Customer data Updated.");
						$(".close_model_div").show();
						$(".ajax-loader").hide();
						location.reload();
					}else{
						alert("Client data Updated.");
						$(".close_model_div").show();
						$("#updateRejectDetails").show();
						$(".ajax-loader").hide();
					}
				}
			});
		});



		$("#updateDelayDetails").click(function(){
			var temp = validateDelay();
			if(temp == false){
				return false;
			}
			$(".close_model_div").hide();
			$("#updateDelayDetails").hide();
			$(".ajax-loader").show();
			$.ajax({
				url:"ajax_service.php",
				data:$("#updateDelayData").serialize()+"&action=updateDelayDetails&r_type=Call",
				success:function(data){
					if (data == 1) {
						alert("Customer data Updated.");
						$(".close_model_div").show();
						$(".ajax-loader").hide();
						location.reload();
					}else{
						alert("Client data Updated.");
						$(".close_model_div").show();
						$("#updateDelayDetails").show();
						$(".ajax-loader").hide();
					}
				}
			});
		});



		$("#viewCustomers").click(function(){
			var user_id = $("#assigned_user_id").val();
			/*alert(user_id);*/
			$.ajax({
				url:"ajax_service/customer_ajax.php",                
				data:"user_id="+user_id+"&action=viewCustomers",                
				async: false,
				// dataType:'JSON',
				success:function(data){
					
					$('#cust_data').empty();
					var oTable = $("#example-table").dataTable();
					oTable.fnDestroy();
					/*if(data.code==400){
						alert(data.message);
						return false;
					}*/
					
					$('#cust_data').html(data);
				},
				complete:function(){
					$('#example-table').DataTable({
						"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
							$('.customer_name').editable({            
								validate: function(value) {
									if($.trim(value) == '') return 'This field is required';
								}
							});
						}
					});
					$('.customer_name').editable({            
						validate: function(value) {
							if($.trim(value) == '') return 'This field is required';
						}
					});
				}
			});
		});



		$("#viewSelfCustomers").click(function(){
			location.reload();            
		});
	});



	function accept(sr,cid){
		var cname = $("#cnm"+sr).html();
		var cno = $("#cno"+sr).html();
		var custName = $("#custName"+sr+" a").html();
		var custEmail = $("#custEmail"+sr).html();

		$("#companyName").val(cname);
		$("#contactNumber").val(cno);
		$("#contactPerson").val(custName);
		$("#emailId").val(custEmail);
		$("#accept_cust_id").val(cid);
		$("body").append("<div class='overlay js-modal-close'></div>");
		$(".overlay").fadeTo(500, 0.9);
		$("#popup1").show();
	}



	function reject(sr,cid){
		$("#reject_cust_id").val(cid);
		$("body").append("<div class='overlay js-modal-close'></div>");
		$(".overlay").fadeTo(500, 0.9);
		$("#popup2").show();
	}



	function delay(sr,cid){
		$("#delay_cust_id").val(cid);
		$("body").append("<div class='overlay js-modal-close'></div>");
		$(".overlay").fadeTo(500, 0.9);
		$("#popup3").show();
	}



	function delete_customer(sr,cid,user_id){
		/*alert(cid);
		return false;*/
		var confirm_delete = confirm("Are you sure you want to delete?");
		if(confirm_delete == true){
			$.ajax({
				url:"ajax_service/customer_ajax.php",
				data:"user_id="+user_id+"&cust_id="+cid+"&action=delete",
				success:function(data){
					if(data == 1){
						alert("Data deleted successfully!!");
						$("#row"+sr).fadeOut(3000);
						location.reload();
					}else{
						alert("Data not deleted.");
					}
				}
			});
		}
	}



	function setValue123(idd){
		var id = idd.id;
		var v = $("#"+id).val();
		$("#assigned_user_id").val(v);
		if(id == "bde_name"){
			$("#tc_name").val('');
			$("#bdm_name").val('');
		}
		if(id == "bdm_name"){
			$("#tc_name").val('');
			$("#bde_name").val('');
		}
		if(id == "tc_name"){
			$("#bde_name").val('');
			$("#bdm_name").val('');
		}
	}



	function validateReject(){
		var  remark=$("#remark").val();
		if(remark.length == "" || remark.length == '' || remark.length == null){
			alert("Please Enter Remark ");
			return false;
		}else{
		}
	}



	function validateDelay(){
		var delay_remark=$("#delay_remark").val();

		var rdate=$("#rdate").val();
		var rtime=$("#rtime").val();
		if(delay_remark.length == "" || delay_remark.length == '' || delay_remark.length == null){
			alert("Please Enter Delay Remark ");
			return false;
		}else if(rdate.length == "" || rdate.length == '' || rdate.length == null){
			alert("Please Select  Date ");
			return false;
		}else if(rtime.length == "" || rtime.length == '' || rtime.length == null){
			alert("Please Select  rtime ");
			return false;
		}
	}



	function validateField(){ 
		var companyName=$("#companyName").val();
		var contactNumber=$("#contactNumber").val().replace(/ /g,'');
		var contactPerson=$("#contactPerson").val();
		var emailId=$("#emailId").val();
		var address=$("#address").val();
		var assignLead=$("#assignLead").val();
		if(companyName.length == "" || companyName.length =='' || companyName.length == null){
			alert("Please Enter Company Name");
			return false;
		}else if(contactNumber.length == "" || contactNumber.length =='' || contactNumber.length == null){
		}else if(contactNumber.length < 10 || contactNumber.length > 12){
			alert("Please Enter correct number");
			return false;
		}else if(contactPerson.length == "" ||contactPerson.length =='' || contactPerson.length == null){
			alert("Please Enter Contact Person");
			return false;
		}else if(emailId.length == "" ||emailId.length =='' || emailId.length == null){
			alert("Please Enter email Id");
			return false;
		}else if(address.length == "" ||address.length =='' || address.length == null){
			alert("Please Enter Address");
			return false;
}/* else if(assignLead =="" || assignLead == '' || assignLead == null)
    {
        alert("Please Select  Assign Lead");
        return false;
    }*/else{
    }
}
</script>



<div id="page-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h1>View Customer 
						<small>Customer Details</small>
					</h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
						</li>
						<li class="active">View Customer</li>
					</ol>
				</div>
			</div>
		</div>

		<div class="row">
			<?php
			if($_SESSION['role'] == "1" || $_SESSION['role'] == "2" || $_SESSION['role'] == "3"){
				if($_SESSION['role'] == "1"){
					echo "<div class='col-md-3'>
					<div>
					<b> Select BDM</b>
					<select  class='form-control' id='bdm_name' name='bdm_name' onchange='setValue123(this);' required>
					<option value='' selected disabled >--------Select BDM---------</option>";
					foreach ($get_bdm_result as $key => $value) {
						echo "<option value='".$get_bdm_result[$key]['user_id']."'>".$get_bdm_result[$key]['fname']." ".$get_bdm_result[$key]['lname']."</option>";
					}
					echo"</select>
					</div>
					</div>";
				}
				if($_SESSION['role'] == "1" || $_SESSION['role'] == "2"){
					echo "<div class='col-md-3'>
					<div>
					<b> Select BDE</b>
					<select  class='form-control' id='bde_name' name='bde_name' onchange='setValue123(this);' required>
					<option value='' selected disabled >--------Select BDE---------</option>";
					foreach ($get_bde_result as $key => $value) {
						echo "<option value='".$get_bde_result[$key]['user_id']."'>".$get_bde_result[$key]['fname']." ".$get_bde_result[$key]['lname']."</option>";
					}
					echo"</select>
					</div>
					</div>";
				}
				echo "<div class='col-md-3'>
				<div>
				<b> Select Caller</b>
				<select  class='form-control' id='tc_name' name='tc_name' onchange='setValue123(this);' required>
				<option value='' selected disabled >--------Select TC---------</option>";
				foreach ($get_tc_result as $key => $value) {
					echo "<option value='".$get_tc_result[$key]['user_id']."'>".$get_tc_result[$key]['fname']." ".$get_tc_result[$key]['lname']."</option>";
				}
				echo"</select>
				</div>
				</div>";
				echo"<div class='col-md-1'>
				<div>
				&nbsp;&nbsp;&nbsp;
				<input type='hidden' id='assigned_user_id' name='assigned_user_id'>
				<button type='button' id='viewCustomers' class='btn btn-green'>Search</button>
				</div>
				</div>";
				if($_SESSION['role'] != "1"){
					echo"<div class='col-md-2'>
					<div>
					</br>
					<button type='button' id='viewSelfCustomers' class='btn btn-green'>Self Customer</button>
					</div>
					</div>";
				}
				if($_SESSION['role'] == "1"){
					echo"<div class='col-md-2'>
					<div>
					</br>
					<button type='button' id='viewSelfCustomers' class='btn btn-green'>All Customer</button>
					</div>
					</div>";                              
				}
			}
			?>
		</div>
		<br>

		<div class="row">
			<div class="col-lg-12">
				<div class="portlet portlet-default">
					<div class="portlet-heading">
						<div class="portlet-title">
							<h4>View Customer</h4>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table id="example-table" class="table table-striped table-bordered table-hover table-green">
								<thead id="cust_thead">
									<tr>
										<th>Sr No.</th>
										<?php 
											if($_SESSION['role'] == "1"){
											echo "
											
											<th>Sales Person</th>
											";
										}
										?>
										<th>Company Name</th>
										<th>Contact Number</th>
										<th>Customer Name</th>
										<th>Email Id</th>
										<th>Created Date</th>
										<?php
										if($_SESSION['role'] == "1"){
											echo "
											<th>Client Status</th>
											<th>Remark</th>
											
											";
										}
										if($_SESSION['role'] != "1" && $_SESSION['role'] != "2" &&$_SESSION['role'] != "8"){
											echo "
											<th>Action_For_Call</th>
											<th>Quotation</th>
											";
										}elseif($_SESSION['role'] == "2" || $_SESSION['role'] == "8"){
											echo "
											<th>Sales Person</th>
											<th>Action_For_Call</th>
											<th>Quotation</th>
											";
										}
										?>
									</tr>
								</thead>
								<tbody id="cust_data">
									<?php
									if($result > 0){
										/*print_r($result);*/
										foreach ($result as $key => $value) {
											

											$sr = $key + 1;
											echo "<tr id='row".$sr."'>";
											echo "<td>".$sr."</td>";
											if($_SESSION['role'] == "1"){
												
												echo "<td id='cnm".$sr."'>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
											}
											echo "<td id='cnm".$sr."'>".$result[$key]['company_name']."</td>";
											echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
											echo '<td id="custName'.$sr.'"><a href="#" title="Click here to edit name" class="customer_name" data-type="text" data-pk="'.$result[$key]['cust_id'].'" data-placement="right" data-placeholder="Required" data-title="Enter customer name" class="editable editable-click editable-empty">'.$result[$key]['customer_name'].'</a></td>';
											echo "<td id='custEmail".$sr."'>".$result[$key]['customer_email']."</td>";
											echo "<td id='custC_date".$sr."'>".$result[$key]['c_date']."</td>";
											if($_SESSION['role'] == "1"){
												echo "<td id='cnm".$sr."'>".$result[$key]['status']."</td>";
												echo "<td id='cnm".$sr."'>".$result[$key]['remark']."</td>";
												
											}elseif($_SESSION['role'] == "2" || $_SESSION['role'] == "8"){
												echo "<td id='cnm".$sr."'>".$result[$key]['fname']." ".$result[$key]['lname']."</td>";
											}

											if($result[$key]['flag'] == 1 && $_SESSION['role'] != "1"){

												if($result[$key]['status'] != 'Accept'){
													echo "<td align='center'><div class='btn-group btn-group-sm'>
													<a href='#' data-toggle='modal' data-target='#flexModal' title='Make Lead' onclick='return accept(".$sr.",".$result[$key]['cust_id'].");'><i class='fa fa-thumbs-o-up btn-green btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#flexModal1' title='Reject Call' onclick='return reject(".$sr.",".$result[$key]['cust_id'].");'><i class='fa fa-thumbs-o-down btn-red btn-sm'></i><a>  <a href='#' data-toggle='modal' data-target='#flexModal2' title='Set Reminder' onclick='return delay(".$sr.",".$result[$key]['cust_id'].");'><i class='fa fa-clock-o btn-blue btn-sm'></i><a>  <a href='#' title='Delete Contact' onclick='delete_customer(".$sr.",".$result[$key]['cust_id'].",".$user_id.");'><i class='fa fa-trash-o btn-red btn-sm'></i></a>
													</div></td>";
													echo "<td align='center'><a title='Send Quotation' href='send_quotation_crm.php?contactNumber=".$result[$key]['contact_number']."&companyName=".$result[$key]['company_name']."&emailId=".$result[$key]['customer_email']."&cust_id=".$result[$key]['cust_id']."&name=".$result[$key]['customer_name']."&q_type=Customer'><i class='fa fa-exchange btn-green btn-sm'></i><a></td>";
												}else{
													echo "<td align='center'>".$result[$key]['status']."</td>";
													echo "<td align='center'>---</td>";
												}
											}
											echo "</tr>";
										}
									}                                           
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Assign Lead </h4>
			</div>
			<form id="addClientData" class="form-horizontal" role="form">
				<div class="modal-body">
					<input type="hidden" id="accept_cust_id" name="cust_id">              
					<div class="form-group">
						<label class="col-sm-2 control-label">Company Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="companyName" name="companyName" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Contact No</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Contact Person</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contactPerson" name="contactPerson" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Email ID</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="address" name="address" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<?php
					echo '<div class="form-group">';
					echo '<label class="col-sm-2 control-label">Assign To</label>';
					echo '<div class="col-sm-10">';
					echo '<select  class="form-control" id="assignLead" name="assignLead" required>';
					echo '<option value="" selected disabled >--------Select User---------</option>';
					foreach ($result1 as $key => $value) {
						$role_name = "";
						if($result1[$key]['role'] == 1){
							$role_name = "Admin";
						}
						if($result1[$key]['role'] == 2){
							$role_name = "BDM";
						}
						if($result1[$key]['role'] == 3){
							$role_name = "BDE";
						}
						if($result1[$key]['role'] == 4){
							$role_name = "TelleCaller";
						}
						if($result1[$key]['role'] == 5){
							$role_name = "RM";
						}
						if($result1[$key]['role'] == 8){
							$role_name = "BH";
						}
						echo "<option value='".$result1[$key]['user_id']."'>".$result1[$key]['fname']." ".$result1[$key]['lname']." - ".$role_name."</option>";
					}
					echo '</select>';
					echo '</div>';
					echo '</div>';
					?>
				</div>
				<div class="modal-footer">
					<div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
					<button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
					<button type="button" id="addClientDetails" class="btn btn-green">Save changes</button>
				</div>
			</form> 
		</div>
	</div>
</div>


<div class="modal modal-flex fade" id="flexModal1" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Reject Customer Form</h4>
			</div>
			<form id="updateRejectData" class="form-horizontal" role="form">
				<div class="modal-body">
					<input type="hidden" id="reject_cust_id" name="cust_id">              
					<div class="form-group">
						<label for="textArea" class="col-sm-2 control-label">Remark</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="remark" name="remark" cols="100" rows="10" placeholder="Placeholder Text" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
					<button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
					<button type="button" id="updateRejectDetails" class="btn btn-green">Save changes</button> 
				</div>
			</form> 
		</div>
	</div>
</div>


<div class="modal modal-flex fade" id="flexModal2" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="flexModalLabel">Delay Customer Form </h4>
			</div>
			<form id="updateDelayData" class="form-horizontal" role="form">
				<div class="modal-body">
					<input type="hidden" id="delay_cust_id" name="cust_id">           
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id;?>"> 
					<div class="form-group">
						<label for="textArea" class="col-sm-2 control-label">Remark</label>
						<div class="col-sm-10">
							<textarea class="form-control" id="delay_remark" name="remark" placeholder="Placeholder Text" required></textarea>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Date</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="rdate" name="rdate" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group has-success">
						<label class="col-sm-2 control-label">Time</label>
						<div class="col-sm-10">
							<input  class="form-control" type="time" id="rtime" name="rtime" placeholder="Placeholder Text" required>
							<span class="help-block"></span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
					<button type="button" class="btn btn-default close_model_div" data-dismiss="modal">Close</button> 
					<button type="button" id="updateDelayDetails" class="btn btn-green">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function(){
		$('#example-table').DataTable({
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('.customer_name').editable({            
					validate: function(value) {
						if($.trim(value) == '') return 'This field is required';
					}
				});
			}
		});

		$('.customer_name').editable({            
			validate: function(value) {
				if($.trim(value) == '') return 'This field is required';
			}
		});

		$('#cust_data').on('click','.editable-submit',function(){
			var customer_name = $('.editable-input input:text').val();
			var id = $('.editable-open').attr('data-pk');
			$.ajax({
				url:'ajax_service/customer_ajax.php',
				data:'id='+id+'&customer_name='+customer_name+'&action=updateCustomerName',
				async:false,
				success:function(data){ 
					if(data == 1){
						alert('Name updated successfully');
						location.reload();
						return false;
					}else{
						alert('Name not updated. Please try again.');
						location.reload();
						return false;
					}
				}
			});
		});
	});
</script>

<?php
include('footer_crm.php');
?>