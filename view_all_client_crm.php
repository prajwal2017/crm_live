<?php
    include('header_sidebar_crm.php');

    //include("class/functions.php");
    if($_SESSION['role'] == "3" || $_SESSION['role'] == "4")//TC
    {
        header("Location:index.php");
    }
    //include("class/functions.php");
    $con = new functions();

    if($_SESSION['role'] == "1" || $_SESSION['role'] == "2")
    {
        //Admin,BDM
        $qry = "SELECT * FROM client_details";
        $result = $con->data_select($qry);
    
    }
       

?>
<script type="text/javascript">

  
    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View All Clients
                                <small>Client Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View All Clients</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View All Clients</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                               <th>SrNo</th>
                                                <th>Company Name</th>
                                                <th>Cont. Number</th>
                                                <th>Cont. Person</th>
                                                <th>Email ID</th>
                                                <th>Address</th>
                                                <th>Client Status</th>
                                                <th>Lead Open</th>
                                                <th>Lead Assigned</th>
                                                <th>Payment Status</th>
                                                <th>Status</th>
                                                <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php 
                                          if($result > 0)
                                          {
                                            foreach ($result as $key => $value) {
                                                $sr = $key +1;
                                                echo "<tr>";
                                                echo "<td>".$sr."</td>";
                                                echo "<td>".$result[$key]['company_name']."</td>";
                                                echo "<td>".$result[$key]['contact_number']."</td>";
                                                echo "<td>".$result[$key]['contact_person']."</td>";
                                                echo "<td>".$result[$key]['email_id']."</td>";
                                                echo "<td>".$result[$key]['address']."</td>";
                                                if ($result[$key]['client_status'] == 'C') {
                                                    echo "<td>Client</td>";
                                                }
                                                if ($result[$key]['client_status'] == 'L') {
                                                    echo "<td>Lead</td>";
                                                }
                                                if ($result[$key]['client_status'] == 'Q') {
                                                    echo "<td>Quotation Sent</td>";
                                                }
                                                //echo "<td>".$result[$key]['client_status']."</td>";
                                                //echo "<td>".$result[$key]['lead_user_id']."</td>";
                                               // echo "<td>".$result[$key]['bde_user_id']."</td>";

                                                $get_user="SELECT fname,lname FROM user_details WHERE user_id = '".$result[$key]['lead_user_id']."' ";
                                                $get_user_result = $con->data_select($get_user);
                                                echo "<td>".$get_user_result[0]['fname']." ".$get_user_result[0]['lname']."</td>";
                                           
                                                $get_user1="SELECT fname,lname FROM user_details WHERE user_id = '".$result[$key]['bde_user_id']."' ";
                                                $get_user_result1 = $con->data_select($get_user1);
                                                echo "<td>".$get_user_result1[0]['fname']." ".$get_user_result1[0]['lname']."</td>";

                                                echo "<td>".$result[$key]['payment_status']."</td>";
                                                
                                               
                                                if ($result[$key]['flag'] == 1) {
                                                    echo "<td>Active</td>";
                                                }
                                                if ($result[$key]['flag'] == 0) {
                                                    echo "<td>Deactive</td>";
                                                }
                                                //echo "<td>".$result[$key]['flag']."</td>";
                                                /*echo "<td><center><a href='#' onclick='openActionPopup();' ><img src='images/Setting-icon.png'></a></center></td>";*/
                                                echo "</tr>";
                                            }
                                          }
                                          else
                                          {
                                            echo "<td colspan='11' align='center'>No Data Found</td>";
                                          }
                                            
                                        ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
                </div>
                <div class="modal-body">
                   <form id="updateUserData" class="form-horizontal" role="form">
                        <input type="hidden" id="user_id" name="user_id">                   

                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Contact No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Email ID</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Role</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="role" name="role" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="status" name="status" required>
                                        <option value="">Select One:</option>
                                        <option id="status1" value="1">Active</option>
                                        <option id="status0" value="0">Deactive</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="update" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php
 include('footer_crm.php');
?>