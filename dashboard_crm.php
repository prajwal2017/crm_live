
<?php 
$key=0;
if(isset($_GET['key'])){
   
    if($_GET['key'] == 1){
        $key = 1;
    }else if($_GET['key'] == 2){
        $key = 2;
    } else if($_GET['key'] == 3){
        $key = 3;
    }
     else if($_GET['key'] == 4){
        $key = 4;
    }
    else if($_GET['key'] == 5){
        $key = 5;
    }
    echo $key;
}



?>
 <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Dashboard
                                <small>Content Overview</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                                <li class="pull-right">
                                    
                                </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->
               <!--  <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <h1 class="error-title">503</h1>
                        <h4 class="error-msg"><i class="fa fa-warning text-red"></i> Service Unavailable</h4>
                        <p class="lead">The page you've requested is under construction.</p>                                   
                    </div>

                </div> -->
                <!-- /.row -->

                <!-- DASHBOARD -->
                 <!-- begin DASHBOARD CIRCLE TILES -->
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="main_report_dashboard_crm.php?key=1">
                                <div class="circle-tile-heading dark-blue">
                                    <i class="fa fa-users fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content dark-blue">
                                <div class="circle-tile-description text-faded">
                                    Users Created
                                </div>
                                <div class="circle-tile-number text-faded" id="USERCOUNT">
                                    
                                    <span id="sparklineA"></span>
                                </div>
                                <a href="main_report_dashboard_crm.php?key=1" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="main_report_dashboard_crm.php?key=2">
                                <div class="circle-tile-heading green">
                                    <i class="fa fa-money fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content green">
                                <div class="circle-tile-description text-faded">
                                    Payments
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span id="payments"></span>
                                </div>
                                <a href="main_report_dashboard_crm.php?key=2" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($_SESSION['role'] == 7 || $_SESSION['role'] == 1){
                    ?>
                      <div class="col-lg-2 col-sm-6">
                       <!--  <div class="circle-tile" onclick="location.href='view_reminder_crm.php';"> -->
                         <div class="circle-tile" onclick="location.href='main_report_dashboard_crm.php?key=3';">
                            <a href="main_report_dashboard_crm.php?key=3">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-bell fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    Credits
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span id="credits"></span>

                                </div>
                                <a href="main_report_dashboard_crm.php?key=3" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php 
                    }else{ 
                    ?>
                    <div class="col-lg-2 col-sm-6">
                       <!--  <div class="circle-tile" onclick="location.href='view_reminder_crm.php';"> -->
                        <!-- Credit_request_new old -->
                         <div class="circle-tile" onclick="location.href='main_report_dashboard_crm.php?key=3';">
                            <a href="main_report_dashboard_crm.php?key=3">
                                <div class="circle-tile-heading orange">
                                    <i class="fa fa-bell fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content orange">
                                <div class="circle-tile-description text-faded">
                                    Credits
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span id="credits"></span>

                                </div>
                                <a href="main_report_dashboard_crm.php?key=3" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <?php
                     }
                    ?>
                   
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="main_report_dashboard_crm.php?key=4">
                                <div class="circle-tile-heading blue">
                                    <i class="fa fa-tasks fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content blue">
                                <div class="circle-tile-description text-faded">
                                    Leads
                                </div>
                                <div class="circle-tile-number text-faded">
                                    <span id="leads"></span>
                                    <span id="sparklineB"></span>
                                </div>
                                <a href="view_leads_crm.php" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="main_report_dashboard_crm.php?key=5">
                                <div class="circle-tile-heading red">
                                    <i class="fa fa-shopping-cart fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content red">
                                <div class="circle-tile-description text-faded">
                                    Calls
                                </div>
                                <div class="circle-tile-number text-faded">
                                     <span id="calls"></span>
                                    <span id="sparklineC"></span>
                                </div>
                                <a href="view_calls.php" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-2 col-sm-6">
                        <div class="circle-tile">
                            <a href="#">
                                <div class="circle-tile-heading purple">
                                    <i class="fa fa-comments fa-fw fa-3x"></i>
                                </div>
                            </a>
                            <div class="circle-tile-content purple">
                                <div class="circle-tile-description text-faded">
                                    Mentions
                                </div>
                                <div class="circle-tile-number text-faded">
                                    96
                                    <span id="sparklineD"></span>
                                </div>
                                <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- end DASHBOARD CIRCLE TILES -->
                <?php 
                    if($key == 1){
                       include 'dashboard_user.php';  
                    }else if($key==2){
                        include 'dashboard_payment.php';
                    }else if($key ==3){
                        include 'dashboard_credit.php';
                    }else if($key ==4){
                        include 'dashboard_leads.php';
                    }else if($key ==5){
                        include 'dashboard_calls.php';
                    }
                ?>
               
            <!-- /.page-content -->
            </div>
        </div>
        <!-- /#page-wrapper -->
        <script src="js/dashboard_js.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#example').DataTable( {
                    responsive: true
                } );
            } );
        </script>
        <!-- end MAIN PAGE CONTENT -->