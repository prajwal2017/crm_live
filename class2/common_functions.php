<?php

/**
 * Description of common_functions
 *
 * @author sms
 */
include_once 'functions.php';
error_reporting(0);
class common_functions extends functions{
    public function group_dropdown($uid,$sel_id)
    {
            $sql="SELECT g_id,group_name FROM group1 WHERE g_status=1 AND u_id='$uid'";
            $show=$this->data_select($sql);
			echo $sel_id;
            if($show!='no'){
                foreach($show as $value)
                {
                    if($value['g_id']==$sel_id){
                        echo "<option value=".base64_encode($value['g_id'])." selected='selected'>".ucfirst($value['group_name'])."</option>";
                    }else{
                        echo "<option value=".base64_encode($value['g_id']).">".ucfirst($value['group_name'])."</option>";
                    }
                }
            }else{
                
                echo "<option value='0'>No Group Found</option>";
            }     
    }
    function get_file_extension($file_name) {
        $file=strtolower($file_name);
        return end(explode('.',$file));
    } // close function

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
}
