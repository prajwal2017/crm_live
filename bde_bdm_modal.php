
<!-- Flex Modal -->
<div class="modal modal-flex fade" id="closeLeadModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           <form id="saveClientData" class="form-horizontal" role="form">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Save Client Details</h4>
            </div>
            
              <div class="modal-body">
               
                <div id="product_div_main">
                    <div class="divRow1">
                     <label>1.Product Details </label>              

                         <div class="form-group">

                            <label class="col-sm-2 control-label">Product</label>
                            <div class="col-sm-10">
                                <select  class="form-control" id="product_name1" name="product_name" onchange="changeQty(this);"  required>
                                   <option value="" selected disabled >--------Select Product---------</option>
                                    <?php

                                    foreach ($result1 as $key => $value) {
                                        echo "<option value='".$result1[$key]['product_name']."'>".$result1[$key]['product_name']."</option>";
                                       
                                    }


                                    ?>
                                </select>
                            </div>
                        </div>

                         <div class="form-group" >
                            <label class="col-sm-2 control-label" id="label1">Quantity</label>
                            <div class="col-sm-10">
                                <input  class="form-control" id="product_quantity1" name="product_quantity" onchange="changeRate(this);" required>
                               <!--  <select  class="form-control" id="product_quantity1" name="product_quantity" onchange="changeRate(this);" required>
                                   <option value="" selected disabled >--------Select Quantity---------</option>
                                  
                                </select> -->
                            </div>
                        </div>

                          <div class="form-group" style="display:none;" id="div_code_validity1">
                                        <label class="col-sm-2 control-label">Validity</label>
                                        <div class="col-sm-10">
                                            <select  class="form-control" id="code_validity1" name="code_validity" required>
                                              <option value="" selected disabled >--------Select Month---------</option>
                                              <option value="1 Month" >1 Month</option>
                                              <option value="3 Months" >3 Months</option>
                                              <option value="6 Months" >6 Months</option>
                                              <option value="12 Months" >12 Months</option>
                                                  
                                            </select>
                                        </div>
                                    </div>

                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Rate per unit</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="product_rate1" name="product_rate" onkeyup="calculaterate(this);" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                       
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label"><span style="margin: 0 10px 0 0;">Tax</span> <input type="checkbox" style="border: 2px solid #16A085;width: 15px;height: 15px;" id="send_tax1" name="send_tax" onclick="disableRadio(this);" value="N"> </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="tax1" name="tax" value="18.00"  onkeyup="calculatetax(this);" disabled required>
                                <input type="hidden" id="tax_amount1" name="tax_amount"  disabled >
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Total Amount</label>
                            <div class="col-sm-10">
                                <input type="hidden" id="hidden_total_amount1" name="hidden_total_amount" placeholder="Placeholder Text" readonly>
                                <input type="text" class="form-control" id="total_amount1" name="total_amount" placeholder="0" readonly required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                         

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Payment Mode</label>
                            <div class="col-sm-10">
                                <select  class="form-control" id="paymode1" name="paymode" onchange="checkNumber(this);" required>
                                    <option value="" selected disabled >--------Select Payment Mode---------</option>
                                    <option value="Cash" >Cash</option>
                                    <option value="Cheque" >Cheque</option>
                                    <option value="Online" >Online</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-success" style="display:none;" id="check_number_div1">
                            <label class="col-sm-2 control-label">Cheque Number</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="check_number1" name="check_number" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group has-success" style="display:none;" id="bank_name_div1">
                            <label class="col-sm-2 control-label">Bank Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="bank_name1" name="bank_name" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Received By</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="amount_received1" value="<?php echo  $result[0]['fname']." ".$result[0]['lname'] ?>" name="amount_received" placeholder="Placeholder Text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        

                      </div>  
                    </div> 


                      <div class="col-sm-10" style="width: 145px;float: right;">
                            <div class="checkbox">
                                <label>
                                    <strong><input type="checkbox" id="send_invoice" name="send_invoice" value="send_invoice" style="border: 2px solid #16A085;width: 15px;height: 15px;">Send Invoice</strong>
                                </label>
                            </div>                          
                      </div>
                 
                  <input type="hidden" id="accept_q_id" name="q_id">
                  <input type="hidden" id="accept_cust_id" name="cust_id">
                  <input type="hidden" id="accept_client_id" name="client_id">  
                  <input type="hidden" id="accept_email_id" name="email_id">
                  <input type="hidden" id="accept_client_add" name="client_add">
                  <input type="hidden" id="accept_client_name" name="client_name">
                  <input type="hidden" id="accept_company_name" name="company_name">
                  <input type="hidden" id="accept_contact_number" name="contact_number">

                  <input type="hidden" id="appendProductName" name="appendProductName">
                  <!-- <input type="hidden" id="appendSmsType" name="appendSmsType"> -->
                  <input type="hidden" id="appendProductQty" name="appendProductQty">
                  <input type="hidden" id="appendProductRate" name="appendProductRate">
                  <input type="hidden" id="appendTotalAmount" name="appendTotalAmount">
                  <input type="hidden" id="appendPayMode" name="appendPayMode">
                  <input type="hidden" id="appendCheckNumber" name="appendCheckNumber">
                  <input type="hidden" id="appendAmountReceived" name="appendAmountReceived">
                  <input type="hidden" id="appendTaxAmount" name="appendTaxAmount">
                  <input type="hidden" id="appendSendTax" name="appendSendTax">
                  <input type="hidden" id="appendhidden_total_amount" name="appendhidden_total_amount">


            </div>
                <div class="modal-footer">
                                       
                  <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                  <div >
                    <br>
                    <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-default" id="add_more_div">Add Another Product</button> -->
                    <button type="button" class="btn btn-green" id="saveClientDetails" >Save changes</button>

                  </div>
                    
                </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Flex Modal -->
<div class="modal modal-flex fade" id="rejectLeadModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Reject Remark</h4>
            </div>
            <form id="updateRejectData" class="form-horizontal" role="form">
            <div class="modal-body">

                <input type="hidden" id="reject_q_id" name="q_id">
                <input type="hidden" id="reject_cust_id" name="cust_id">              
                <input type="hidden" id="reject_client_id" name="client_id"> 
                <div class="form-group">
                    <label for="textArea" class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="remark" name="remark" placeholder="Placeholder Text" required></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
                <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="updateRejectDetails">Submit</button>
            </div>
            </form> 
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<!-- Flex Modal -->
<div class="modal modal-flex fade" id="delayLeadModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Set Reminder</h4>
            </div>
            <form id="updateDelayData" class="form-horizontal" role="form">
            <div class="modal-body">
               
                <input type="hidden" id="delay_q_id" name="q_id">
                <input type="hidden" id="delay_cust_id" name="cust_id">
                <input type="hidden" id="delay_client_id" name="client_id">           

                <div class="form-group">
                    <label for="textArea" class="col-sm-2 control-label">Remark</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="5" id="delay_remark" name="remark" placeholder="Placeholder Text" required></textarea>
                    </div>
                </div>
                <div class="form-group has-success">
                    <label class="col-sm-2 control-label">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="rdate" name="rdate" placeholder="Placeholder Text" required>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group has-success">
                    <label class="col-sm-2 control-label">Time</label>
                    <div class="col-sm-10">
                        <input  class="form-control" type="time" id="rtime" name="rtime" placeholder="Placeholder Text" required>
                        <span class="help-block"></span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
               <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="updateDelayDetails">Submit</button>
            </div>
              </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Flex Modal -->
<div class="modal modal-flex fade" id="reassignLeadModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_client_model" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="flexModalLabel">Re-Assign Lead</h4>
            </div>
            <form id="updateReassignData" class="form-horizontal" role="form">
            <div class="modal-body">
               
                     <input type="hidden" id="reassign_q_id" name="q_id">
                     <input type="hidden" id="reassign_client_id" name="client_id">           

                     <div class="form-group" >
                        <label class="col-sm-2 control-label" id="label1">Re-Assign To</label>
                        <div class="col-sm-10">
                            <select  class="form-control" id="reassign_user_id" name="reassign_user_id" required>
                               <option value="" selected disabled >--------Select User---------</option>
                              <?php
                                foreach ($reassign_user_result as $key => $value) {
                                    $role_name = "";
                                    if($reassign_user_result[$key]['role'] == 1){
                                        $role_name = "Admin";
                                    }
                                    if($reassign_user_result[$key]['role'] == 2){
                                        $role_name = "BDM";
                                    }
                                    if($reassign_user_result[$key]['role'] == 3){
                                        $role_name = "BDE";
                                    }
                                    if($reassign_user_result[$key]['role'] == 4){
                                        $role_name = "TelleCaller";
                                    }
                                    if($reassign_user_result[$key]['role'] == 5){
                                        $role_name = "RM";
                                    }
                                            echo "<option value='".$reassign_user_result[$key]['user_id']."'>".$reassign_user_result[$key]['fname']." ".$reassign_user_result[$key]['lname']." - ".$role_name."</option>";
                                           
                                }
                              ?>
                            </select>
                        </div>
                    </div>


            </div>
            <div class="modal-footer">
                <div  align="center" class="ajax-loader" style="display:none" ><img src="img/ajax-loader.gif"></div>
               <button type="button" class="btn btn-default close_client_model close_model_div" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-green" id="updateReassignDetails">Submit</button>
            </div>
              </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->