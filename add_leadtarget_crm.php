<?php
 include('header_sidebar_crm.php');
 include_once("class/functions.php");
 $obj=new functions();
 $user_id = $_SESSION['user_id'];

    if($_SESSION['role'] == "1")
    {
        $qry = "select user_id,fname,lname,role from user_details order by role asc";
        $result = $obj -> data_select($qry);
    }else if($_SESSION['role'] == "2"){
        $qry = "select user_id,fname,lname from user_details where role= '4' and reporting_id='".$user_id."' ";
        $result = $obj -> data_select($qry);
    }

?>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="js_functions/datepicker-calendar.js"></script>
<style>
.ui-datepicker-calendar {
    display: none;
}
</style>
<!-- java script start -->

<script type="text/javascript">

$(document).ready(function(){

    $("#addLeadTargetSubmit").click(function(){
        $.ajax({

            url:"ajax_service.php",
            data:$("#addLeadTarget").serialize()+"&action=addleadtarget",
            success:function(data){

                if(data == "wrong_date"){
                    alert("Please Do Not Select Previous Month.");
                }else if(data == "exist")
                {
                    alert("Lead Target Already Assigned To This User.");
                }else if(data == "success")
                {
                    alert("Data inserted successfully");
                    location.reload();
                }else
                {
                    alert("Data not inserted");
                    
                }
            }
        });
    });

});

function get_previous_leads(){
    var user_id = $("#telecaller").val();
   // alert(user_id);
    $.ajax({
        url:"ajax_service/check_sales_target.php",
        data:"user_id="+user_id+"&action=get_previous_leads",
        dataType:"json",
        success:function(data){
            alert(data);
            //alert(data[0].balance);
            if(data != "no"){
                $("#balance_lead").val(data[0].balance);
            }else{
                $("#balance_lead").val('0');
            }
            
        }
    });
}


</script>
<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Add Lead Target
                                <small>Form </small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="main_report_dashboard_crm.php">Dashboard</a>
                                </li>
                                <li class="active">Add Lead Target</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <div class="row">

                    <!-- Validation States -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Add Lead Target</h4>
                                </div>
                                <div class="portlet-widgets">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#validationStates"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationStates" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="addLeadTarget" role="form">
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Telle Caller</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="telecaller" name="telecaller" onchange="get_previous_leads()">
                                                            
                                                            <?php
                                                            if($result != 'no')
                                                            {
                                                                echo '<option value="">---Select Telecaller---</option>';
                                                                foreach ($result as $key => $value) {
                                                                   echo "<option value=".$result[$key]['user_id'].">".$result[$key]['fname']." ".$result[$key]['lname']."</option>";
                                                                }
                                                            }else{
                                                                echo "<option value=''>--No Result Found--</option>";
                                                            }    
                                                            ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-group has-success">                     
                                            <label class="col-sm-2 control-label">Lead Target</label>
                                            <div class="col-sm-10">                                               
                                               <div class="input-group">
                                                    <span class="input-group-addon">Lead Balance</span>
                                                    <input type="text" class="form-control" id="balance_lead" name="balance_lead" placeholder="Previous Month Balance" style="width: 247px;" readonly>&nbsp;<i class='fa fa-plus btn-xs'></i>&nbsp;
                                                    <span class="input-group-addon">Lead Target</span>
                                                    <input type="text" class="form-control" id="target"  pattern="[0-9]" name="target" placeholder="Enter Lead Target" required style="width: 355px;float: inherit;">                                                    
                                                </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Target Month</label>
                                            <div class="col-sm-10">
                                                <!-- <input type="hidden" id="startDate" name="startDate"  required>  -->                                             
                                                <input class="form-control" type="text" id="startDate1" name="startDate" placeholder="Select Start Date" value="" readonly/>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                       <!--  <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">End Date</label>
                                            <div class="col-sm-10">
                                                <input type="date" class="form-control" name="endDate"  required>
                                                <span class="help-block"></span>
                                            </div>
                                        </div> -->                                      
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <h1 align="center"><button type="button"  id="addLeadTargetSubmit" class="btn btn-default">Submit</button></h1>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                  

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
<?php
 include('footer_crm.php');
?>