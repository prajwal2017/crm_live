<?php
include("header.php");
include("side_navigation.php");
include_once('class/functions.php');
$obj=new functions();
?>
<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

    <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Sample Entry
                                <small>Add Sample</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">Sample Entry </li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
                <div id="testDivMain">
                <div class="row">

                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">

                       <div class="col-lg-2">
                            <div class="form-group">
                                <select class="form-control" name="test" id="test0">
                                <option value="">Select Test</option>
                                <?php
                                $sql="SELECT `id`,`name`,`parameter` FROM `lab_test`";
                                $result=$obj->data_select($sql);
                                $val=sort($result);
                                if($result!='no')
                                {
                                    foreach($result as $value)
                                    {
                                        echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                    }
                                }
                                
                                ?>
                                </select>
                            </div>
                       </div>
                       
                       <div class="col-lg-3">
                            <div class="form-group">
                                <select class="form-control" name="parameter" id="parameter0">
                                <option value="">Select Parameter</option>
                                <?php
                                $sql="SELECT `id`,`name`,`parameter` FROM `lab_test`";
                                $result=$obj->data_select($sql);
                                $val=sort($result);
                                if($result!='no')
                                {
                                    foreach($result as $value)
                                    {
                                        echo '<option value="'.$value['parameter'].'">'.$value['parameter'].'</option>';
                                    }
                                }
                                
                                ?>
                                </select>
                                <input type="hidden" name="tempId" id="0">
                                
                            </div>
                       </div>
                       
                       <div class="col-lg-2">
                            <div class="form-group">
                                <input class="form-control" type="text" name="valu" id="valu0" placeholder="Value ">
                                <input class="form-control" accept="application/pdf" type="file" name="lot0" id="grphFile0" placeholder="LOT # "style="display:none;">
                            </div>
                       </div>
                      
                       <div class="col-lg-2">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" onchange="changeDiv('0')" id="graph0">Graph
                                    </label>
                                </div>
                            </div>
                       </div>
                       
                       

                    </div>
                    
                  
                </div>
                
                
                <div class="row">

                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">

                       <div class="col-lg-8">
                            <div class="form-group">
                                <input class="form-control" type="text" name="note" id="note0" placeholder="Note">
                            </div>
                       </div>
                       
                       <div class="col-lg-1" id="addMore0">
                            <div class="form-group">
                                <input class="form-control" type="button" value="+" onclick="addFieldButton();" id="addField0" title="Add More Test">
                            </div>
                       </div>
                       
                    </div>
                    
                  
                </div>
                </div>
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Test</th>
                                                <th>Parameter</th>
                                                <th>Value</th>
                                                <th>Unit</th>
                                                <th>Specification</th>
                                                <th>Repeatibility</th>
                                                <th>SOP</th>
                                              
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $sql="SELECT lt.id as tid, lt.name,lt.parameter,lt.unit,lt.specification,lt.repeatability,lt.sop,lt.status,hl.value,hl.graph FROM history_log hl JOIN  lab_test lt ON lt.id=hl.test_id GROUP BY lt.id ORDER BY lt.id DESC ";
                                        $result=$obj->data_select($sql);
                                        if($result!='no')
                                        {
                                            $no=1;
                                            foreach($result as $value)
                                            {
                                              
                                                if($value['sop']=='--')
                                                {
                                                    $pdf="N/A";
                                                }
                                                else
                                                {
                                                    $pdf="<a target=\"_blank\" href=\"uploaded/".$value['sop']."\">Pdf Report</a>";
                                                }
                                                $sqlhis="SELECT `value`,`graph` FROM `history_log` WHERE `test_id`='".$value['tid']."' ORDER BY `id` DESC LIMIT 1";
                                                $result1=$obj->data_select($sqlhis);
                                                foreach($result1 as $value1)
                                                {
                                                    if($value1['value']=='--')
                                                    {
                                                        $data="<td class=\"editvalue\" ondblclick=\"editItem(".$value['tid'].",'".$value['value']."');\"
                                                         id=\"editvalue".$value['tid']."\"><a target=\"_blank\" href=\"uploaded/".$value1['graph']."\">Excel Report</a></td>";
                                                                                                    
                                                    }
                                                    else
                                                    {
                                                        $data="<td class=\"editvalue\" ondblclick=\"editItem(".$value['tid'].",'".$value['value']."');\"
                                                         id=\"editvalue".$value['tid']."\">".$value1['value']."</td>";                                          
                                                    }
                                                }
                                                    
                                                
                                            ?>
                                            
                                                <tr class="odd gradeX" id="row<?=$value['tid'];?>">
                                                    <td><?=$no;?></td>
                                                    <input type="hidden" id="tempId" />
                                                    <input type="hidden" id="tempdata" />
                                                    <td><?=$value['name'];?></td>
                                                    <td><?=$value['parameter'];?></td>
                                                    <?php echo $data; ?>
                                                    <td><?=$value['unit'];?></td>
                                                    <td><?=$value['specification'];?></td>
                                                    <td><?=$value['repeatability'];?></td>
                                                    <td><?=$pdf;?></td>
                                                    
                                                    <td><div class="btn-group">
                                                            <a class="btn btn-green btn-xs" id="edit<?=$value['tid'];?>" onclick="editItem(<?=$value['tid'];?>,'<?=$value['value'];?>');" role="button"><button class="btn btn-green btn-xs">Edit</button></a>
                                                            <a class="btn btn-red btn-xs" onclick="deleteItem(<?=$value['tid'];?>);" role="button"><button class="btn btn-red btn-xs">Delete</button></a>
                                                            <a class="btn btn-blue btn-xs" onclick="historyLog(<?=$value['tid'];?>)" role="button"><button class="btn btn-blue btn-xs" data-toggle="modal" data-target="#standardModal">
                                    Log
                                </button></a>
                                <br>
                                <br>

                                <!-- Standard Modal -->
                                <div class="modal fade" id="standardModal" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="standardModalLabel">History Log</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p><div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Value</th>
                                                            <th>Graph</th>
                                                            <th>Added By</th>
                                                            <th>Modify By</th>
                                                            <th>Created Date</th>
                                                            <th>Last Update</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody id="hisLog">
                                                    
                                                    </tbody>
                                                </table>
                                                </div></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                                       </div>
                                                    </td>
                                                </tr>
                                         <?php 
                                            $no++;
                                            }
                                        }
                                        else
                                        {
                                            echo '<tr class="odd gradeX"><td  colspan="10">No Record Found</td></tr>';
                                        }
                                         ?>   
                                                
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

                <div align="right">
                    <input class="btn btn-default" type="submit" value="Save" id="save">
                    <input class="btn btn-default" type="button" value="Cancel">
                    <input class="btn btn-default"  type="button" value="Exit">
                </div>

                    <!-- begin LEFT COLUMN -->
                    

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->





<?php

include("footer.php");
?>
<script>
function changeDiv(sr)
{
        if(document.getElementById("graph"+sr).checked)
        {
            $("#valu"+sr).slideUp(500);
            $("#grphFile"+sr).slideDown(500);
        }
        else
        {
            $("#grphFile"+sr).slideUp(500);
            $("#valu"+sr).slideDown(500);
        }
    
}   
var sr=1;

function addFieldButton()
{
    
    $("#testDivMain").append('<div id="test'+sr+'"><div class="row">'+
    '<div class="col-lg-12"><div class="col-lg-2">'+
    '<div class="form-group">'+
    '<select class="form-control" name="test" id="test'+sr+'">'+
    '<option value=" ">Select Test</option><option value="Test 1">Test 1</option><option value="Test 2">Test 2</option><option value="Test 3">Test 3</option>'+
    '</select>'+
    '</div></div><div class="col-lg-3"><div class="form-group">'+
    '<select class="form-control" name="parameter" id="parameter'+sr+'">'+
    '<option value="">Select Parameter</option><option value="Parameter 1">Parameter 1</option><option value="Parameter 2">Parameter 2</option><option value="Parameter 3">Parameter 3</option>'+
    '</select><input type="hidden" name="tempId" id="'+sr+'">'+
    
    '</div></div><div class="col-lg-2">'+
    '<div class="form-group">'+
    '<input class="form-control" type="text" name="valu" id="valu'+sr+'" placeholder="Value ">'+
     '                           <input class="form-control" type="file" accept="application/pdf" name="lot'+sr+'" id="grphFile'+sr+'" placeholder="LOT # "style="display:none;">'+
      '                      </div>'+
       '                </div>'+
         '              <div class="col-lg-2">'+
          '                     <div class="form-group">'+
           '                     <div class="checkbox">'+
            '                        <label>'+
             '                          <input type="checkbox" onchange="changeDiv('+sr+')" id="graph'+sr+'">Graph'+
               '                     </label>'+
                '                </div>'+
                 '           </div>'+
                  '     </div>'+
                   '</div>'+
                    '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-12">'+
                    '   <div class="col-lg-8">'+
                     '          <div class="form-group">'+
                      '          <input class="form-control" type="text" name="note" id="note" placeholder="Note">'+
                       '     </div>'+
                       '</div>'+
                      ' <div class="col-lg-1" id="addMore'+sr+'">'+
                       '        <div class="form-group">'+
                        '        <input class="form-control" onclick="addFieldButton();" type="button" value="+" title="Add More Test">'+
                         '   </div>'+
                       '</div>'+
                       ' <div class="col-lg-1" id="removeMore'+sr+'">'+
                       '        <div class="form-group">'+
                                '<input class="form-control" type="button" value="-" onclick="removeFieldButton('+sr+');" title="Remove Test">'+
                         '   </div>'+
                       '</div>'+
                   ' </div>'+
                '</div><div>');
    sr++;
}


function removeFieldButton(id)
{
    $("#test"+id).remove();
    
}


$("#save").click(function(e){
    $("input[name=tempId]").each(function(){
        var id=this.id;
        $("#save").hide();
        var temptest=$("#test"+id).val();
        var parameter=$("#parameter"+id).val();
        var note=$("#note"+id).val();
        
        if(document.getElementById("graph"+id).checked)
        {
            var valu=$("#valu"+id).val();
            var file_data = $('#grphFile'+id).prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('userImage', file_data);
            $.ajax({
                url: 'webservice/upload.php', // point to server-side PHP script 
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(data)
                {
                    $.ajax({
                        url:"webservice/insert_sample.php",
                        type:"POST",
                        data:"test="+temptest+"&parameter="+parameter+"&note="+note+"&valu=--&graph="+data,
                        success: function(data){
                            //console.log(data);
                            location.reload();
                        }
                    });
                }
            });
        }
        else
        {
            
            var valu=$("#valu"+id).val();
            $.ajax({
                url:"webservice/insert_sample.php",
                type:"POST",
                data:"test="+temptest+"&parameter="+parameter+"&note="+note+"&valu="+valu+"&graph=--",
                success: function(data){
                    //console.log(data);
                    location.reload();
                }
            });
        }
    }); 
});

function changeStatus(id)
{
    var flag=confirm("Are you sure to publish it");
    if(flag==true)
    {
        $.ajax({
            url:"webservice/change_status.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest",
            success: function(data){
                $("#changeStatus"+id).html("Published");
            }
        });
    }
}

function deleteItem(id)
{
    var flag=confirm("Are you sure you want to delete");
    if(flag==true)
    {
        $.ajax({
            url:"webservice/delete.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest",
            success: function(data){
                $("#row"+id).fadeOut(2000);
            }
        });
    }
}

function editItem(id,data)
{
    $("#editvalue"+id).html('<input type="text" onkeypress="keyValue(event,'+id+')" style="width:50px;" id="data'+id+'" value="'+data+'">');
    $("#data"+id).focus();
    $("#edit"+id).html("<button class=\"btn btn-green btn-xs\">Save</button>");
    $("#edit"+id).attr('onclick','saveItem('+id+')');
    $("#edit"+id).attr('onclick','saveItem('+id+')');
    $("#tempId").val(id);
    $("#tempdata").val(data);
}

function saveItem(id)
{
    
    var data=$("#data"+id).val();
    $("#editvalue"+id).html('<img src="img/ajax-loader.gif">');
    $.ajax({
            url:"webservice/editItem.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest&data="+data,
            success: function(data1){
                $("#editvalue"+id).html(data);
                $("#edit"+id).html("<button class=\"btn btn-green btn-xs\">Edit</button>");
                $("#edit"+id).attr('onclick','editItem('+id+','+data+')');
                $("#editvalue"+id).attr('ondblclick','editItem('+id+','+data+')');
                $("#changeStatus"+id).html("<a href='javascript:void(0)' onclick='changeStatus("+id+")'>Unpublished</a>");
            }
        });
}



function keyValue(e,id)
{
    if(e.keyCode==13)
    {
        
        var data=$("#data"+id).val();
        $("#editvalue"+id).html('<img src="img/ajax-loader.gif">');
        $.ajax({
            url:"webservice/editItem.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest&data="+data,
            success: function(data1){
                console.log(data1);
                $("#editvalue"+id).html(data);
                $("#edit"+id).html("<button class=\"btn btn-green btn-xs\">Edit</button>");
                $("#edit"+id).attr('onclick','editItem('+id+','+data+')');
                $("#editvalue"+id).attr('ondblclick','editItem('+id+','+data+')');
                $("#changeStatus"+id).html("<a href='javascript:void(0)' onclick='changeStatus("+id+")'>Unpublished</a>");
            }
        });
    }
}



function historyLog(id)
{
    
        $.ajax({
            url:"webservice/select_history.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest",
            dataType:"JSON",
            success: function(data1){
                console.log(data1);
                var temp="";
                for(var i=0;i<data1.length;i++)
                {
                    if(data1[i].graph=='--')
                    {
                        var pdf="N/A";
                    }
                    else
                    {
                        var pdf="<a target=\"_blank\" href=\"uploaded/"+data1[i].graph+"\">Pdf Report</a>";
                    }
                    temp=temp+'<tr>'+
                        '<td>'+(i+1)+'</td>'+
                        '<td>'+data1[i].value+'</td>'+
                        '<td>'+pdf+'</td>'+
                        '<td>'+data1[i].addedby+'</td>'+
                        '<td>'+data1[i].modifyby+'</td>'+
                        '<td>'+data1[i].cr+'</td>'+
                        '<td>'+data1[i].up+'</td>'+
                    '</tr>' 
                }
                $("#hisLog").html(temp);
            }
        });
    
}

/*$('html').click(function() {
    var id=$("#tempId").val();
    var data=$("#data"+id).val();
        $("#editvalue"+id).html('<img src="img/ajax-loader.gif">');
        $.ajax({
            url:"webservice/editItem.php",
            type:"POST",
            data:"id="+id+"&parameter=labTest&data="+data,
            success: function(data1){
                console.log(data1);
                $("#editvalue"+id).html(data);
                $("#edit"+id).html("<button class=\"btn btn-green btn-xs\">Edit</button>");
                $("#edit"+id).attr('onclick','editItem('+id+','+data+')');
                $("#editvalue"+id).attr('ondblclick','editItem('+id+','+data+')');
                $("#changeStatus"+id).html("<a href='javascript:void(0)' onclick='changeStatus("+id+")'>Unpublished</a>");
            }
        });
});

$('.editvalue').click(function(event){
    event.stopPropagation();
});

*/
</script>