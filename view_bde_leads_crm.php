<?php
    include('header_sidebar_crm.php');
    //include('class/functions.php');
    $con = new functions();

    //$user_id = $_SESSION['user_id'];
    $user_id = "2";//temp variable are used intesd of session for testing purpose

    //$qry = "SELECT cust_id,company_name,contact_number FROM customer_details WHERE user_id ='".$user_id."' AND status = 'Pending' AND flag = '1' ORDER BY cust_id ASC";

    $qry = "SELECT cd.cust_id,cd.client_id,cd.company_name,cd.contact_number,cd.contact_person,cd.email_id,cd.address,cd.c_date  FROM client_details as cd WHERE cd.bde_user_id = ".$user_id." and cd.client_status = 'L' and cd.flag = 1 ";



    $result = $con->data_select($qry);

    $qry1 = "SELECT * FROM products WHERE flag = 1 ";

    $result1 = $con->data_select($qry1);
    /*echo "<pre>";
    echo  $qry;
    print_r($result); 
    exit;*/

?>
<script type="text/javascript">

   
$(document).ready(function(){


       


        $("#saveClientDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#saveClientData").serialize()+"&action=saveClientDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Client data inserted successfully.");
                        location.reload();
                    };
                }
            });
        });


        $("#updateRejectDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateRejectData").serialize()+"&action=updateRejectDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup2").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });

        $("#updateDelayDetails").click(function(){
            //alert();
            $.ajax({
                url:"ajax_service.php",
                data:$("#updateDelayData").serialize()+"&action=updateDelayDetails",
                success:function(data){
                    //alert(data);
                    console.log(data);
                    if (data == "success") {
                        alert("Customer data Updated.");
                        $("#popup3").hide();
                        $(".overlay").remove();
                        location.reload();
                    };
                }
            });
        });


});
function accept(sr,cid,clid)
{
    var cname = $("#cnm"+sr).html();
    var cno = $("#cno"+sr).html();
    var eid = $("#eid"+sr).html();

    //alert("name: "+cname+" number:"+cno+" uid:"+uid);
    $("#companyName").val(cname);
    $("#contactNumber").val(cno);
    $("#accept_cust_id").val(cid);
    $("#accept_client_id").val(clid);
    $("#accept_email_id").val(eid);
    

}

function reject(sr,cid)
{
    
    $("#reject_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup2").show();

}

function delay(sr,cid)
{
    
    $("#delay_cust_id").val(cid);
    $("body").append("<div class='overlay js-modal-close'></div>");
    $(".overlay").fadeTo(500, 0.9);
    $("#popup3").show();

}
    </script>
 <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Leads 
                                <small>View Leads</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index-2.html">Dashboard</a>
                                </li>
                                <li class="active">View Leads</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>View Leads</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                        <tr>
                                            <th>SrNo.</th>
                                            <th>Company Name</th>
                                            <th>Contact Number</th>
                                            <th>Contact Person</th>
                                            <th>Email Id</th>
                                            <th>Address</th>
                                            <th>Created date/time</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                          <?php
                                                foreach ($result as $key => $value) {
                                                    $sr = $key + 1;
                                                    echo "<tr>";

                                                    echo "<td>".$sr."</td>";
                                                    echo "<td id='cnm".$sr."'>".$result[$key]['company_name']."</td>";
                                                    echo "<td id='cno".$sr."'>".$result[$key]['contact_number']."</td>";
                                                    echo "<td>".$result[$key]['contact_person']."</td>";
                                                    echo "<td id='eid".$sr."'>".$result[$key]['email_id']."</td>";
                                                    echo "<td>".$result[$key]['address']."</td>";
                                                    echo "<td>".$result[$key]['c_date']."</td>";
                                                    echo "<td><a href='#' class='btn btn-green btn-xs' data-toggle='modal' data-target='#flexModal' onclick='return accept(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'>Accept<a><a href='#' data-toggle='modal' data-target='#flexModal1' class='btn btn-red btn-xs' onclick='return reject(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'>Reject<a><a href='#' class='btn btn-blue btn-xs' data-toggle='modal' data-target='#flexModal2' onclick='return delay(".$sr.",".$result[$key]['cust_id'].",".$result[$key]['client_id'].");'>Delay<a></td>";

                                                    echo "</tr>";
                                                }
                                            ?>
                                           
                                      </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin </h4>
                </div>
                <div class="modal-body">
                   <form id="saveClientData" class="form-horizontal" role="form">
                        <input type="hidden" id="accept_cust_id" name="cust_id">
                         <input type="hidden" id="accept_client_id" name="client_id">  
                         <input type="hidden" id="accept_email_id" name="email_id">             

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Product</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="product_id" name="product_id" required>
                                       <option value="" selected disabled >--------Select Product---------</option>
                                        <?php

                                        foreach ($result1 as $key => $value) {
                                            echo "<option value='".$result1[$key]['p_id']."'>".$result1[$key]['product_name']."</option>";
                                        }


                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Package</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="package" name="package" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Amount</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="amount" name="amount" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Payment Mode</label>
                                <div class="col-sm-10">
                                    <select  class="form-control" id="paymode" name="paymode" required>
                                        <option value="" selected disabled >--------Select Payment Mode---------</option>
                                        <option value="Cash" >Cash</option>
                                        <option value="Check" >Check</option>
                                        <option value="Online" >Online</option>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="saveClientDetails" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 


                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->





 <!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal1" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin Styled Modal</h4>
                </div>
                <div class="modal-body">
                   <form id="updateRejectData" class="form-horizontal" role="form">
                         <input type="hidden" id="reject_cust_id" name="cust_id">              

                            <div class="form-group">
                                <label for="textArea" class="col-sm-2 control-label">Remark</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="remark" name="remark" placeholder="Placeholder Text" required></textarea>
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="updateRejectDetails" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 


                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



<!-- Flex Modal -->
    <div class="modal modal-flex fade" id="flexModal2" tabindex="-1" role="dialog" aria-labelledby="flexModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="flexModalLabel">Flex Admin </h4>
                </div>
                <div class="modal-body">
                   <form id="updateDelayData" class="form-horizontal" role="form">
                         <input type="hidden" id="delay_cust_id" name="cust_id">           

                            <div class="form-group">
                                <label for="textArea" class="col-sm-2 control-label">Remark</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="delay_remark" name="remark" placeholder="Placeholder Text" required></textarea>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Date</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="rdate" name="rdate" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group has-success">
                                <label class="col-sm-2 control-label">Time</label>
                                <div class="col-sm-10">
                                    <input  class="form-control" type="time" id="rtime" name="rtime" placeholder="Placeholder Text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            
                           
                             <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <h1 align="center"><button type="button"  id="updateDelayDetails" class="btn btn-default">Submit</button></h1>
                                </div>
                            </div>
                    </form> 


                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                   <!--  <button type="button" class="btn btn-green">Save changes</button> -->
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->






<!-- 
<form id="updateDelayData" method="post">

     <input type="hidden" id="delay_cust_id" name="cust_id">

     <div class="form-group">
         <label>Remark</label>&nbsp;&nbsp;<span id="errcompanyName"></span>
         <textarea col="3" row="5" class="textarea-form-control" id="delay_remark" name="remark" placeholder="Enter Text"></textarea>
     </div>

     <div class="form-group">
        <label>Date</label>&nbsp;&nbsp;<span id="errrdate"></span>
        <input class="lightbox_form-control" type="date" id="rdate" name="rdate" placeholder="Enter Text">
    </div>

    <div class="form-group">
        <label>Time</label>&nbsp;&nbsp;<span id="errrtime"></span><br>
        <input class="lightbox_form-control" type="time" id="rtime" name="rtime" placeholder="Enter Text">
    </div>

     <div class="form-group" align="center">
         <input type="button" class="btn btn-primary" id="updateDelayDetails" value="Update" style="width: 50%;">
     </div>

</form>
 -->



    
<?php
 include('footer_crm.php');
?>