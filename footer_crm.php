   </div>
    <!-- /#wrapper -->

    <!-- GLOBAL SCRIPTS -->
   
    <script src="js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="js/plugins/popupoverlay/defaults.js"></script>
    <script src="vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <!-- Logout Notification Box -->
    <div id="logout">
        <div class="logout-message">
            <!-- <img class="img-circle img-logout" src="img/profile-pic.jpg" alt=""> -->
            <h3>
                <i class="fa fa-sign-out text-green"></i> Ready to go?
            </h3>
            <p>Select "Logout" below if you are ready<br> to end your current session.</p>
            <ul class="list-inline">
                <li>
                    <a href="logout_crm.php" class="btn btn-green">
                        <strong>Logout</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-green">Cancel</button>
                </li>
            </ul>
        </div>
    </div>
    <!-- /#logout -->
    <!-- Logout Notification jQuery -->
    
    <script src="js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <!-- HubSpot Messenger -->
    <script src="js/plugins/messenger/messenger.min.js"></script>
    <script src="js/plugins/messenger/messenger-theme-flat.js"></script>
    <!-- Date Range Picker -->
    <script src="js/plugins/daterangepicker/moment.js"></script>
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Morris Charts -->

    <!-- Sparkline Charts -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Moment.js -->
    <script src="js/plugins/moment/moment.min.js"></script>
    <!-- jQuery Vector Map -->
    <script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/demo/map-demo-data.js"></script>
    <!-- Easy Pie Chart -->
    <script src="js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
    <!-- DataTables -->
     <script src="vendor/datatable/js/jquery.dataTables.js"></script>
    <script src="vendor/datatable/js/datatables-bs3.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="js/plugins/summernote/summernote.min.js"></script>

    <!--<script src="js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/scrollspy.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/affix.js"></script>
    <script src="js/plugins/bootstrap-tokenfield/typeahead.min.js"></script>-->

    <script type="text/javascript" src="js/plugins/tokenfield/bootstrap-tokenfield.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/plugins/tokenfield/scrollspy.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/plugins/tokenfield/typeahead.bundle.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/plugins/tokenfield/docs.js" charset="UTF-8"></script>


    <!-- THEME SCRIPTS -->
    <script src="js/flex.js"></script>
    <script src="js/demo/advanced-form-demo.js"></script>
    <script src="js/common_validation.js"></script>

   

</body>
</html>