<?php
     include('header_customer_form.php');
    
?>

<!-- javascript end -->
<!-- begin MAIN PAGE CONTENT -->
        <div id="customer_form_page-wrapper">

           
            <div class="page-content">

               
                <!-- end PAGE TITLE ROW -->

                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <h1 class="error-title">404</h1>
                        <h4 class="error-msg"><i class="fa fa-warning text-red"></i> URL Expired</h4>
                        <p class="lead">The page you've requested could not be found on the server. Please contact your service provider.</p>                                   
                    </div>
                <!-- /.row -->

                </div>
            <!-- /.page-content -->

            </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

      
        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
<?php
 include('footer_crm.php');
?>